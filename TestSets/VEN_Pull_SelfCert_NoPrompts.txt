SetName = "VEN_Pull_SelfCert_NoPrompts"
#
# Target use cases include testing integration of certified modular
# OpenADR stacks into a product, migration of code between products 
# in a family, and minor code changes to already certified products.
# All tests run without prompts and can be run secured or unsecured. 
#
# DUT SHOULD BE IN AN UNREGISTERED STATE BEFORE STARTING THIS TEST SET
#
# Registration Service Tests 
N1_0020_TH_VTN_1, prompt # VEN Registers
#
# Event Service Tests
A_E1_0082_TH_VTN_1 # Event State Sequence Test
A_E1_0086_TH_VTN_1 # Event State Sequence Test
A_E1_0090_TH_VTN_1 # Event State Sequence Test
A_E1_0092_TH_VTN_1 # Event State Sequence Test
A_E1_0094_TH_VTN_1 # Event State Sequence Test
A_E1_0096_TH_VTN_1 # Event State Sequence Test
A_E1_0098_TH_VTN_1 # Event State Sequence Test
A_E1_0100_TH_VTN_1 # Event State Sequence Test
A_E1_0102_TH_VTN_1 # Event State Sequence Test
A_E1_0104_TH_VTN_1 # Event State Sequence Test
A_E1_0180_TH_VTN_1 # New Modified, Cancelled in on Event
E1_1020_TH_VTN_1   # Electricity Price Event
E1_1025_TH_VTN_1   # Load Dispatch Event
E1_1030_TH_VTN_1   # Updated Event
E1_1040_TH_VTN_1   # Mixed Simple and Complex in same payload
E1_1055_TH_VTN_1   # Mixed Simple and Complex in same event
#
# General Tests
G1_4015_TH_VTN_1 # Dequeuing oadrPoll
#
# Report Service Tests
R1_3010_TH_VTN_1 # One Shot Report
R1_3020_TH_VTN_1 # Periodic Report
PendingReportCleanUp_Pull_TH_VTN
R1_3027_TH_VTN_1 # Short Periodic Report Duration
PendingReportCleanUp_Pull_TH_VTN
R1_3030_TH_VTN_1 # Multiple Report Requests
PendingReportCleanUp_Pull_TH_VTN
R1_3040_TH_VTN_1 # Periodic Report Cancelled
R1_3150_TH_VTN_1 # One Shot Report Telemetry Usage
R1_3160_TH_VTN_1 # Periodic Report Telemetry Usage
PendingReportCleanUp_Pull_TH_VTN
R1_3170_TH_VTN_1 # Metadata Request
#Will leave periodic metadata report request active in VEN for 4 minutes after this run
#
#End of Tests




