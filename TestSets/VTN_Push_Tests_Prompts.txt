SetName = "VTN_Push_Tests_Prompts.txt"
#
# VTN PUSH TESTS
#
# DO NOT REMOVE  "prompt" after test case name
# These tests may not function correctly if prompts are overridden
#
# Program Guide Tests
#
CBP_CMP_B0_TH_VEN, prompt
CBP_SMP_B0_TH_VEN_1, prompt
CBP_TYP_B0_TH_VEN_1, prompt
CPP_CMP_B0_TH_VEN_1, prompt
CPP_SMP_B0_TH_VEN_1, prompt
CPP_TYP_B0_TH_VEN_1, prompt
DER_TYP_B0_TH_VEN_1, prompt
EVP_TYP_B0_TH_VEN_1, prompt
FDR_CMP_B0_TH_VEN, prompt
FDR_SMP_B0_TH_VEN_1, prompt
FDR_TYP_B0_TH_VEN, prompt
THR_CMP_B0_TH_VEN_1, prompt
THR_SMP_B0_TH_VEN_1, prompt
THR_TYP_B0_TH_VEN, prompt
#
# Event Service Tests
#
A_E2_0430_TH_VEN_1, prompt
A_E2_0432_TH_VEN_1, prompt
A_E2_0435_TH_VEN_1, prompt
A_E2_0440_TH_VEN_1, prompt
A_E2_0450_TH_VEN_1, prompt
A_E2_0452_TH_VEN_1, prompt
A_E2_0454_TH_VEN_1, prompt
A_E2_0468_TH_VEN_1, prompt
A_E2_0470_TH_VEN_1, prompt
A_E2_0474_TH_VEN_1, prompt
A_E2_0480_TH_VEN_1, prompt
A_E2_0484_TH_VEN_1, prompt
A_E2_0490_TH_VEN_1, prompt
A_E2_0492_TH_VEN_1, prompt
A_E2_0494_TH_VEN_1, prompt
A_E2_0496_TH_VEN_1, prompt
A_E2_0498_TH_VEN_1, prompt
A_E2_0500_TH_VEN_1, prompt
A_E2_0510_TH_VEN_1, prompt
A_E2_0525_TH_VEN_1, prompt
A_E2_0527_TH_VEN_1, prompt
A_E2_0657_TH_VEN_1, prompt
A_E2_0680_TH_VEN_1, prompt
A_E2_0685_TH_VEN_1, prompt
A_E2_0710_TH_VEN_1, prompt
A_E2_0720_TH_VEN_1, prompt
A_E2_0730_TH_VEN_1, prompt
A_E2_0750_TH_VEN_1, prompt
A_E2_0780_TH_VEN_1, prompt
E0_6010_TH_VEN_1, prompt
E0_6020_TH_VEN_1, prompt
E0_6025_TH_VEN_1, prompt
E0_6027_TH_VEN_1, prompt
E0_6030_TH_VEN_1, prompt
E0_6040_TH_VEN_1, prompt
E0_6050_TH_VEN, prompt
E0_6055_TH_VEN_1, prompt
E0_6060_TH_VEN_1, prompt
E0_6065_TH_VEN_1, prompt
E0_6070_TH_VEN_1, prompt
E0_6080_TH_VEN_1, prompt
E0_6090_TH_VEN_1, prompt
#
# General Tests 
#
G0_9005_TH_VEN_1, prompt
G0_9010_TH_VEN_1, prompt
G0_9020_TH_VEN_1, prompt
G0_9030_TH_VEN_1, prompt
#
# Opt Service Tests
#
P0_7010_TH_VEN, prompt
P0_7015_TH_VEN, prompt
P0_7020_TH_VEN, prompt
P0_7030_TH_VEN, prompt
P0_7040_TH_VEN, prompt
P0_7050_TH_VEN, prompt
#
# Registraion Service Tests
#
N0_5010_TH_VEN, prompt
N0_5015_TH_VEN, prompt
N0_5020_TH_VEN, prompt
N0_5025_TH_VEN, prompt
N0_5030_TH_VEN, prompt
N0_5040_TH_VEN_1, prompt
N0_5050_TH_VEN_1, prompt
N0_5060_TH_VEN, prompt
N0_5065_TH_VEN, prompt
N0_5070_TH_VEN, prompt
N0_5080_TH_VEN, prompt
#
# Report Service Tests
#
R0_8010_TH_VEN_1, prompt
R0_8020_TH_VEN_1, prompt
R0_8025_TH_VEN_1, prompt
R0_8027_TH_VEN_1, prompt
R0_8030_TH_VEN_1, prompt
R0_8040_TH_VEN_1, prompt
R0_8045_TH_VEN_1, prompt
R0_8050_TH_VEN_1, prompt
R0_8060_TH_VEN, prompt
R0_8070_TH_VEN, prompt
R0_8080_TH_VEN, prompt
R0_8090_TH_VEN, prompt
R0_8100_TH_VEN, prompt
R0_8120_TH_VEN_1, prompt
R0_8130_TH_VEN_1, prompt
R0_8150_TH_VEN_1, prompt
R0_8160_TH_VEN_1, prompt
R0_8170_TH_VEN_1, prompt
R0_8180_TH_VEN_1, prompt
R0_8190_TH_VEN_1, prompt
#
# End of Tests
