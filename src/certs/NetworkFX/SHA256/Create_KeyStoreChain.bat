

set RANDFILE=.rnd
set TYPE=%1
set CN=%2
set PRIVATE_KEY_RSA=TEST_RSA_%TYPE%_%CN%_privkey.pem
set PRIVATE_KEY_ECC=TEST_ECC_%TYPE%_%CN%_privkey.pem
set CERTIFICATE_RSA=TEST_RSA_%TYPE%_%CN%_cert
set CERTIFICATE_ECC=TEST_ECC_%TYPE%_%CN%_cert
set INTERMEDIATE_RSA=TEST_OpenADR_RSA_%TYPE%_Int_cert.pem
set INTERMEDIATE_ECC=TEST_OpenADR_ECC_%TYPE%_Int_cert.pem
set ROOT_CA_ECC=TEST_OpenADR_ECC_Root_CA3_cert.pem
set ROOT_CA_RSA=TEST_OPENADR_RSA_ROOT_R2.pem


set KEYSTORE_JKS=keystore_%TYPE%_%CN%.jks

echo Delete output keystore
del %KEYSTORE_JKS%

echo Convert private key and certificate in PEM encoding to PCKS#12 keystore, include intermedaite in chain
rem openssl pkcs12 -export -certfile %INTERMEDIATE_RSA% -name %CN%_rsa -inkey %PRIVATE_KEY_RSA% -in %CERTIFICATE_RSA%.pem -passout pass:password -out %CERTIFICATE_RSA%.pfx
rem openssl pkcs12 -export -certfile %INTERMEDIATE_ECC% -name %CN%_ecc -inkey %PRIVATE_KEY_ECC% -in %CERTIFICATE_ECC%.pem -passout pass:password -out %CERTIFICATE_ECC%.pfx

REM just device cert
openssl pkcs12 -export -name %CN%_rsa -inkey %PRIVATE_KEY_RSA% -in %CERTIFICATE_RSA%.pem -passout pass:password -out %CERTIFICATE_RSA%.pfx
openssl pkcs12 -export -name %CN%_ecc -inkey %PRIVATE_KEY_ECC% -in %CERTIFICATE_ECC%.pem -passout pass:password -out %CERTIFICATE_ECC%.pfx

del .rnd

echo Convert PCKS#12 keystore to JKS keystore
keytool -importkeystore -srcstoretype PKCS12 -srcstorepass password -deststorepass password -srckeystore %CERTIFICATE_RSA%.pfx -destkeystore %KEYSTORE_JKS%
keytool -importkeystore -srcstoretype PKCS12 -srcstorepass password -deststorepass password -srckeystore %CERTIFICATE_ECC%.pfx -destkeystore %KEYSTORE_JKS%

echo Delete temporary PCKS#12 files.
del %CERTIFICATE_RSA%.pfx
del %CERTIFICATE_ECC%.pfx

echo Display output keystore
keytool -list -v -storepass password -keystore %KEYSTORE_JKS%
