echo ****** START **********

set TRUSTSTORE=truststore.jks
set PASSWORD=password

if exist truststore.jks del %TRUSTSTORE%

rem Import root CA certificate to truststore
rem
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias ECC_Root_01 -file TEST_OpenADR_ECC_Root_CA3_cert.pem
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias RSA_Root_01 -file TEST_OPENADR_RSA_ROOT_R2.pem

rem Import Intermediate certificate to truststore
rem
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias ECC_VTN_Int_01 -file TEST_OpenADR_ECC_VTN_Int_cert.pem
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias ECC_VEN_Int_01 -file TEST_OpenADR_ECC_VEN_Int_cert.pem
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias RSA_VTN_Int_01 -file TEST_OpenADR_RSA_VTN_Int_cert.pem
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias RSA_VEN_Int_01 -file TEST_OpenADR_RSA_VEN_Int_cert.pem


rem List truststore
rem

keytool -list -v -storepass %PASSWORD% -Keystore %TRUSTSTORE%

echo ****** DONE **********