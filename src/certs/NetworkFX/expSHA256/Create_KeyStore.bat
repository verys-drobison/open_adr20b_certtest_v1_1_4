rem @echo off
if '%2'=='' goto help

set RANDFILE=.rnd
set TYPE=%1
set CN=%2
set PRIVATE_KEY_RSA_DER=RSA_%TYPE%_%CN%_privkey
set PRIVATE_KEY_ECC_DER=ECC_%TYPE%_%CN%_privkey
set CERTIFICATE_RSA=RSA_%TYPE%_%CN%_cert
set CERTIFICATE_ECC=ECC_%TYPE%_%CN%_cert
set KEYSTORE_JKS=keystore_%TYPE%_%CN%.jks

echo.
echo Adding %PRIVATE_KEY_RSA_DER%, %CERTIFICATE_RSA% and %PRIVATE_KEY_ECC_DER%, %CERTIFICATE_ECC% to %KEYSTORE_JKS%.
echo.

echo Delete output keystore
del %KEYSTORE_JKS%

echo Convert unencrypted PKCS#8 private key from DER encoding to PEM encoding
rem openssl pkcs8 -nocrypt -inform DER -outform PEM -in %PRIVATE_KEY_RSA_DER% -out %PRIVATE_KEY_RSA_DER%.pem
rem openssl pkcs8 -nocrypt -inform DER -outform PEM -in %PRIVATE_KEY_ECC_DER% -out %PRIVATE_KEY_ECC_DER%.pem

echo Convert certificate from DER encoding to PEM encoding
rem openssl x509 -inform DER -outform PEM -in %CERTIFICATE_RSA% -out %CERTIFICATE_RSA%.pem
rem openssl x509 -inform DER -outform PEM -in %CERTIFICATE_ECC% -out %CERTIFICATE_ECC%.pem

echo Convert private key and certificate in PEM encoding to PCKS#12 keystore
openssl pkcs12 -export -name %CN%_rsa -inkey %PRIVATE_KEY_RSA_DER%.pem -in %CERTIFICATE_RSA%.crt -passout pass:password -out %CERTIFICATE_RSA%.pfx
openssl pkcs12 -export -name %CN%_ecc -inkey %PRIVATE_KEY_ECC_DER%.pem -in %CERTIFICATE_ECC%.crt -passout pass:password -out %CERTIFICATE_ECC%.pfx

echo Delete temporary PEM encoded files
rem del %PRIVATE_KEY_RSA_DER%.pem
rem del %PRIVATE_KEY_ECC_DER%.pem
rem del %CERTIFICATE_RSA%.pem
rem del %CERTIFICATE_ECC%.pem
del .rnd

echo Convert PCKS#12 keystore to JKS keystore
keytool -importkeystore -srcstoretype PKCS12 -srcstorepass password -deststorepass password -srckeystore %CERTIFICATE_RSA%.pfx -destkeystore %KEYSTORE_JKS%
keytool -importkeystore -srcstoretype PKCS12 -srcstorepass password -deststorepass password -srckeystore %CERTIFICATE_ECC%.pfx -destkeystore %KEYSTORE_JKS%

echo Delete temporary PCKS#12 files.
del %CERTIFICATE_RSA%.pfx
del %CERTIFICATE_ECC%.pfx

echo Display output keystore
keytool -list -v -storepass password -keystore %KEYSTORE_JKS%
goto end

:help
echo Create_KeyStore [vtn,ven] CN

:end
pause
