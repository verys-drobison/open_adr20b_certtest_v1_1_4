@echo off
set TRUSTSTORE=truststore.jks
set PASSWORD=password

rem Import root CA certificate to truststore
rem
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias RSA_RCA0001 -file TEST_OpenADR_RSA_RCA0001_Cert.der
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias ECC_RCA0003 -file TEST_OpenADR_ECC_Root_CA3_cert.der

rem Import intermediate CA certificate for VTN server certificates to truststore
rem
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias RSA_SPCA0002 -file TEST_OpenADR_RSA_SPCA0002_Cert.pem
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias ECC_VTNCA0003 -file TEST_OpenADR_ECC_SHA256_VTN_Int_CA3_cert.der

rem Import intermediate CA certificate for VEN device certificates to truststore
rem
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias RSA_MCA0002 -file TEST_OpenADR_RSA_MCA0002_Cert.pem
keytool -import -keystore %TRUSTSTORE% -storepass %PASSWORD% -alias ECC_VENCA0004 -file TEST_OpenADR_ECC_SHA256_VEN_Int_CA4_cert.der

rem List truststore
rem
keytool -list -v -storepass %PASSWORD% -Keystore %TRUSTSTORE%

pause
