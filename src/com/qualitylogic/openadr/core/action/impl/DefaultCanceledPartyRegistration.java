package com.qualitylogic.openadr.core.action.impl;

import com.qualitylogic.openadr.core.base.BaseCanceledPartyRegistrationAction;
import com.qualitylogic.openadr.core.base.ErrorConst;
import com.qualitylogic.openadr.core.signal.EiResponseType;
import com.qualitylogic.openadr.core.signal.OadrCanceledPartyRegistrationType;
import com.qualitylogic.openadr.core.signal.helper.CanceledPartyRegistrationHelper;

public class DefaultCanceledPartyRegistration extends BaseCanceledPartyRegistrationAction{


	private boolean isEventCompleted;
	private String responseCode = "200";

	// Use this Constructor if you want to set specific response code other than
	// 200.
	public DefaultCanceledPartyRegistration(String responseCode) {
		this.responseCode = responseCode;
	}

	// Default Constructor sets response code to 200.
	public DefaultCanceledPartyRegistration(){
	}

	public boolean isPreConditionsMet() {
		return true;
	}

	public boolean isEventCompleted() {
		return isEventCompleted;
	}

	public void setEventCompleted(boolean isEventCompleted) {
		this.isEventCompleted = isEventCompleted;
	}

	public OadrCanceledPartyRegistrationType getOadrCanceledPartyRegistration(){

		OadrCanceledPartyRegistrationType oadrCanceledPartyRegistrationType = CanceledPartyRegistrationHelper.loadOadrCanceledPartyRegistrationType();
		EiResponseType eiResponse = oadrCanceledPartyRegistrationType.getEiResponse();
		eiResponse.setResponseCode(responseCode);
		if (!responseCode.equals(ErrorConst.OK_200)) {
			eiResponse.setResponseDescription("ERROR");
		}

		return oadrCanceledPartyRegistrationType;
	}
}