package com.qualitylogic.openadr.core.action.impl;

import java.util.List;

import com.qualitylogic.openadr.core.base.BaseDistributeEventAction;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.CurrencyType;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.ISO3AlphaCurrencyCodeContentType;
import com.qualitylogic.openadr.core.signal.IntervalType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.PayloadFloatType;
import com.qualitylogic.openadr.core.signal.SignalPayloadType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType.OadrEvent;
import com.qualitylogic.openadr.core.signal.OadrRequestEventType;
import com.qualitylogic.openadr.core.signal.SignalTypeEnumeratedType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;

public class DefaultDistributeEvent_CPP_CMP_Action extends
BaseDistributeEventAction {

	private static final long serialVersionUID = 1L;
	private OadrDistributeEventType oadrDistributeEvent = null;

	public boolean isPreConditionsMet(OadrRequestEventType oadrRequestEvent) {
		boolean isCommonPreConditionsMet = isCommonPreConditionsMet(oadrRequestEvent);
		return isCommonPreConditionsMet;
	}



	public OadrDistributeEventType getDistributeEvent() {
		PropertiesFileReader propertiesFileReader=new PropertiesFileReader();
		if (oadrDistributeEvent == null) {
			oadrDistributeEvent = new DistributeEventSignalHelper().loadOadrDistributeEvent("Event_CPP_CMP.xml");

			int firstIntervalStartTimeDelayMin = 1;
			DistributeEventSignalHelper.setNewReqIDEvntIDAndStartTime(oadrDistributeEvent, firstIntervalStartTimeDelayMin);
			OadrEvent firstEvent = oadrDistributeEvent.getOadrEvent().get(0);
			firstEvent.getEiEvent().getEventDescriptor().setPriority((long) 1);

			EiTargetType targetType = firstEvent.getEiEvent().getEiTarget();
			String propFileResourceID[]= propertiesFileReader.getTwoResourceID();
			
			targetType.getResourceID().clear();
			targetType.getResourceID().add(propFileResourceID[0]);
			targetType.getResourceID().add(propFileResourceID[1]);

			firstEvent.getEiEvent().getEiActivePeriod().getProperties().getDuration().setDuration("PT4M");

//			//1st interval
//			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(0).getDuration().setDuration("PT1M");
//			SignalPayloadType signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0)
//					.getIntervals().getInterval().get(0).getStreamPayloadBase().get(0).getValue();
//			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Simple_Medium")));
//
//			//2nd interval
//			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(1).getDuration().setDuration("PT2M");
//			signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0)
//					.getIntervals().getInterval().get(1).getStreamPayloadBase().get(0).getValue();
//			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Simple_High")));
//
//			//3rd interval
//			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(2).getDuration().setDuration("PT1M");
//			signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0)
//					.getIntervals().getInterval().get(2).getStreamPayloadBase().get(0).getValue();
//			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Simple_Special")));
//

			//Second signal
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).setSignalName("ELECTRICITY_PRICE");	
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).setSignalType(SignalTypeEnumeratedType.PRICE);	

			//1st interval
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(0).getDuration().setDuration("PT1M");
			SignalPayloadType signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0)
					.getIntervals().getInterval().get(0).getStreamPayloadBase().get(0).getValue();

			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Electricity_Price_Medium")));
			
			//2nd interval
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(1).getDuration().setDuration("PT2M");
			signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0)
					.getIntervals().getInterval().get(1).getStreamPayloadBase().get(0).getValue();

			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Electricity_Price_High")));
			
			//3rd interval
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(2).getDuration().setDuration("PT1M");
			signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0)
					.getIntervals().getInterval().get(2).getStreamPayloadBase().get(0).getValue();

			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Electricity_Price_Special")));
			
			
			CurrencyType curr = (CurrencyType) firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getItemBase().getValue();
			try{
				ISO3AlphaCurrencyCodeContentType testme = ISO3AlphaCurrencyCodeContentType.fromValue(propertiesFileReader.get("currencyPerKWh_itemUnits"));
			} catch(Exception e) {
				throw new FailedException(propertiesFileReader.get("currencyPerKWh_itemUnits") + 
						" from the property currencyPerKWh_itemUnits is not a valid ISO3AlphaCurrencyCodeContentType");
			}
			curr.setItemUnits(ISO3AlphaCurrencyCodeContentType.fromValue(propertiesFileReader.get("currencyPerKWh_itemUnits")));
			curr.setSiScaleCode(propertiesFileReader.get("currencyPerKWh_siScaleCode"));

		}

		return oadrDistributeEvent;
	}
}