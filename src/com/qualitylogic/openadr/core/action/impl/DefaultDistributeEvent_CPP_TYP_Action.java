package com.qualitylogic.openadr.core.action.impl;

import java.util.List;

import com.qualitylogic.openadr.core.base.BaseDistributeEventAction;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.CurrencyType;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.ISO3AlphaCurrencyCodeContentType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.PayloadFloatType;
import com.qualitylogic.openadr.core.signal.SignalPayloadType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType.OadrEvent;
import com.qualitylogic.openadr.core.signal.OadrRequestEventType;
import com.qualitylogic.openadr.core.signal.SignalTypeEnumeratedType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;

public class DefaultDistributeEvent_CPP_TYP_Action extends
BaseDistributeEventAction {

	private static final long serialVersionUID = 1L;
	private OadrDistributeEventType oadrDistributeEvent = null;

	public boolean isPreConditionsMet(OadrRequestEventType oadrRequestEvent) {
		boolean isCommonPreConditionsMet = isCommonPreConditionsMet(oadrRequestEvent);
		return isCommonPreConditionsMet;
	}



	public OadrDistributeEventType getDistributeEvent() {
		PropertiesFileReader propertiesFileReader=new PropertiesFileReader();
		if (oadrDistributeEvent == null) {
			oadrDistributeEvent = new DistributeEventSignalHelper().loadOadrDistributeEvent("Event_CPP_TYP.xml");

			int firstIntervalStartTimeDelayMin = 1;
			DistributeEventSignalHelper.setNewReqIDEvntIDAndStartTime(oadrDistributeEvent, firstIntervalStartTimeDelayMin);
			OadrEvent firstEvent = oadrDistributeEvent.getOadrEvent().get(0);
			firstEvent.getEiEvent().getEventDescriptor().setPriority((long) 1);

			EiTargetType targetType = firstEvent.getEiEvent().getEiTarget();
			targetType.getVenID().clear();
			targetType.getVenID().add(propertiesFileReader.getVenID());

			firstEvent.getEiEvent().getEiActivePeriod().getProperties().getDuration().setDuration("PT2M");
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(0).getDuration().setDuration("PT2M");
			SignalPayloadType signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent()
					.getEiEventSignals().getEiEventSignal().get(0)
					.getIntervals().getInterval().get(0).getStreamPayloadBase().get(0).getValue();

			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Simple_Special")));

			//firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).setSignalName("SIMPLE");	
			List<String> resourceList=firstEvent.getEiEvent().getEiTarget().getResourceID();
			resourceList.clear();
			//Second signal
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(1).setSignalName("ELECTRICITY_PRICE");	
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(1).setSignalType(SignalTypeEnumeratedType.PRICE);	
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(1).getIntervals().getInterval().get(0).getDuration().setDuration("PT2M");
			signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent()
					.getEiEventSignals().getEiEventSignal().get(1)
					.getIntervals().getInterval().get(0).getStreamPayloadBase().get(0).getValue();

			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Electricity_Price_Special")));
			
			CurrencyType curr = (CurrencyType) firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(1).getItemBase().getValue();
			try{
				ISO3AlphaCurrencyCodeContentType testme = ISO3AlphaCurrencyCodeContentType.fromValue(propertiesFileReader.get("currencyPerKWh_itemUnits"));
			} catch(Exception e) {
				throw new FailedException(propertiesFileReader.get("currencyPerKWh_itemUnits") + 
						" from the property currencyPerKWh_itemUnits is not a valid ISO3AlphaCurrencyCodeContentType");
			}
			curr.setItemUnits(ISO3AlphaCurrencyCodeContentType.fromValue(propertiesFileReader.get("currencyPerKWh_itemUnits")));
			curr.setSiScaleCode(propertiesFileReader.get("currencyPerKWh_siScaleCode"));

		}

		return oadrDistributeEvent;
	}
}