package com.qualitylogic.openadr.core.action.impl;

import java.math.BigDecimal;
import java.util.List;

import com.qualitylogic.openadr.core.base.BaseDistributeEventAction;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.PayloadFloatType;
import com.qualitylogic.openadr.core.signal.PowerRealType;
import com.qualitylogic.openadr.core.signal.SignalPayloadType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType.OadrEvent;
import com.qualitylogic.openadr.core.signal.OadrRequestEventType;
import com.qualitylogic.openadr.core.signal.SignalTypeEnumeratedType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;

public class DefaultDistributeEvent_FDR_CMP_Action extends
BaseDistributeEventAction {

	private static final long serialVersionUID = 1L;
	private OadrDistributeEventType oadrDistributeEvent = null;

	public boolean isPreConditionsMet(OadrRequestEventType oadrRequestEvent) {
		boolean isCommonPreConditionsMet = isCommonPreConditionsMet(oadrRequestEvent);
		return isCommonPreConditionsMet;
	}



	public OadrDistributeEventType getDistributeEvent() {
		PropertiesFileReader propertiesFileReader=new PropertiesFileReader();
		if (oadrDistributeEvent == null) {
			oadrDistributeEvent = new DistributeEventSignalHelper().loadOadrDistributeEvent("Event_FDR_CMP.xml");

			int firstIntervalStartTimeDelayMin = 2;
			DistributeEventSignalHelper.setNewReqIDEvntIDAndStartTime(oadrDistributeEvent, firstIntervalStartTimeDelayMin);
			OadrEvent firstEvent = oadrDistributeEvent.getOadrEvent().get(0);
			firstEvent.getEiEvent().getEventDescriptor().setPriority((long) 1);

			EiTargetType targetType = firstEvent.getEiEvent().getEiTarget();
			targetType.getVenID().clear();
			//targetType.getVenID().add(propertiesFileReader.getVenID());
			List<String> resourceList=firstEvent.getEiEvent().getEiTarget().getResourceID();
			resourceList.clear();
			resourceList.add(propertiesFileReader.getOneResourceID());

			firstEvent.getEiEvent().getEiActivePeriod().getProperties().getDuration().setDuration("PT4M");
			firstEvent.getEiEvent().getEiActivePeriod().getProperties().getXEiRampUp().setDuration("PT1M");
			firstEvent.getEiEvent().getEiActivePeriod().getProperties().getXEiRecovery().setDuration("PT1M");
//			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(0).getDuration().setDuration("PT2M");
//			SignalPayloadType signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent()
//					.getEiEventSignals().getEiEventSignal().get(0)
//					.getIntervals().getInterval().get(0).getStreamPayloadBase().get(0).getValue();
//
//			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Simple_Medium")));
//
//			//Second interval
//			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(1).getDuration().setDuration("PT2M");
//			signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent()
//					.getEiEventSignals().getEiEventSignal().get(0)
//					.getIntervals().getInterval().get(1).getStreamPayloadBase().get(0).getValue();
//
//			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Simple_Special")));
			
			//firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).setSignalName("SIMPLE");	
			//Second signal
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).setSignalName("LOAD_DISPATCH");	
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).setSignalType(SignalTypeEnumeratedType.SETPOINT);	
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(0).getDuration().setDuration("PT2M");
			SignalPayloadType signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent()
					.getEiEventSignals().getEiEventSignal().get(0)
					.getIntervals().getInterval().get(0).getStreamPayloadBase().get(0).getValue();

			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Load_Dispatch_Setpoint_Medium")));
			
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(1).getDuration().setDuration("PT2M");
			signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent()
					.getEiEventSignals().getEiEventSignal().get(0)
					.getIntervals().getInterval().get(1).getStreamPayloadBase().get(0).getValue();

			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Load_Dispatch_Setpoint_Special")));
			
			PowerRealType preal = (PowerRealType) firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getItemBase().getValue();
			preal.setItemUnits(propertiesFileReader.get("powerReal_itemUnits"));
			preal.setSiScaleCode(propertiesFileReader.get("powerReal_siScaleCode"));
			preal.getPowerAttributes().setAc(true);
			preal.getPowerAttributes().setHertz(BigDecimal.valueOf(Float.parseFloat((propertiesFileReader.get("powerReal_powerAttributes_hertz")))));
			preal.getPowerAttributes().setVoltage(BigDecimal.valueOf(Float.parseFloat((propertiesFileReader.get("powerReal_powerAttributes_voltage")))));


		}

		return oadrDistributeEvent;
	}
}