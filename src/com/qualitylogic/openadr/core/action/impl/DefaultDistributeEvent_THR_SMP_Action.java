package com.qualitylogic.openadr.core.action.impl;

import java.util.List;

import com.qualitylogic.openadr.core.base.BaseDistributeEventAction;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.PayloadFloatType;
import com.qualitylogic.openadr.core.signal.SignalPayloadType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType.OadrEvent;
import com.qualitylogic.openadr.core.signal.OadrRequestEventType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.signal.xcal.Properties.Tolerance;
import com.qualitylogic.openadr.core.signal.xcal.Properties.Tolerance.Tolerate;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;

public class DefaultDistributeEvent_THR_SMP_Action extends
BaseDistributeEventAction {

	private static final long serialVersionUID = 1L;
	private OadrDistributeEventType oadrDistributeEvent = null;

	public boolean isPreConditionsMet(OadrRequestEventType oadrRequestEvent) {
		boolean isCommonPreConditionsMet = isCommonPreConditionsMet(oadrRequestEvent);
		return isCommonPreConditionsMet;
	}



	public OadrDistributeEventType getDistributeEvent() {
		PropertiesFileReader propertiesFileReader=new PropertiesFileReader();
		if (oadrDistributeEvent == null) {
			oadrDistributeEvent = new DistributeEventSignalHelper().loadOadrDistributeEvent("Event_CBP.xml");

			int firstIntervalStartTimeDelayMin = 1;
			DistributeEventSignalHelper.setNewReqIDEvntIDAndStartTime(oadrDistributeEvent, firstIntervalStartTimeDelayMin);
			OadrEvent firstEvent = oadrDistributeEvent.getOadrEvent().get(0);
			firstEvent.getEiEvent().getEventDescriptor().setPriority((long) 1);

			String propFileResourceID[]= propertiesFileReader.getTwoResourceID();
			EiTargetType targetType = firstEvent.getEiEvent().getEiTarget();
			targetType.getVenID().clear();
			targetType.getResourceID().clear();
			targetType.getResourceID().add(propFileResourceID[0]);

			firstEvent.getEiEvent().getEiActivePeriod().getProperties().getDuration().setDuration("PT2M");

			Tolerance tolerance = new Tolerance();
			Tolerate tolerate = new Tolerate();
			tolerate.setStartafter("PT1M");
			tolerance.setTolerate(tolerate);
			firstEvent.getEiEvent().getEiActivePeriod().getProperties()
					.setTolerance(tolerance);

			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(0).getDuration().setDuration("PT2M");
			SignalPayloadType signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent()
			.getEiEventSignals().getEiEventSignal().get(0)
			.getIntervals().getInterval().get(0).getStreamPayloadBase().get(0).getValue();
			
			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Simple_Medium")));

			//firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).setSignalName("SIMPLE");	

		}

		return oadrDistributeEvent;
	}
}