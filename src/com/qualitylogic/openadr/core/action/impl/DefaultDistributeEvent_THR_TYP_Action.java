package com.qualitylogic.openadr.core.action.impl;

import com.qualitylogic.openadr.core.base.BaseDistributeEventAction;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.PayloadFloatType;
import com.qualitylogic.openadr.core.signal.SignalPayloadType;
import com.qualitylogic.openadr.core.signal.SignalTypeEnumeratedType;
import com.qualitylogic.openadr.core.signal.TemperatureUnitType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType.OadrEvent;
import com.qualitylogic.openadr.core.signal.OadrRequestEventType;
import com.qualitylogic.openadr.core.signal.TemperatureType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.signal.xcal.Properties.Tolerance;
import com.qualitylogic.openadr.core.signal.xcal.Properties.Tolerance.Tolerate;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;

public class DefaultDistributeEvent_THR_TYP_Action extends
BaseDistributeEventAction {

	private static final long serialVersionUID = 1L;
	private OadrDistributeEventType oadrDistributeEvent = null;

	public boolean isPreConditionsMet(OadrRequestEventType oadrRequestEvent) {
		boolean isCommonPreConditionsMet = isCommonPreConditionsMet(oadrRequestEvent);
		return isCommonPreConditionsMet;
	}



	public OadrDistributeEventType getDistributeEvent() {
		PropertiesFileReader propertiesFileReader=new PropertiesFileReader();
		if (oadrDistributeEvent == null) {
			oadrDistributeEvent = new DistributeEventSignalHelper().loadOadrDistributeEvent("Event_THR_TYP.xml");

			int firstIntervalStartTimeDelayMin = 1;
			DistributeEventSignalHelper.setNewReqIDEvntIDAndStartTime(oadrDistributeEvent, firstIntervalStartTimeDelayMin);
			OadrEvent firstEvent = oadrDistributeEvent.getOadrEvent().get(0);
			firstEvent.getEiEvent().getEventDescriptor().setPriority((long) 1);

			String propFileResourceID[]= propertiesFileReader.getTwoResourceID();
			EiTargetType targetType = firstEvent.getEiEvent().getEiTarget();
			targetType.getVenID().clear();
			targetType.getResourceID().clear();
			targetType.getResourceID().add(propFileResourceID[0]);
			targetType.getResourceID().add(propFileResourceID[1]);

			firstEvent.getEiEvent().getEiActivePeriod().getProperties().getDuration().setDuration("PT2M");

			Tolerance tolerance = new Tolerance();
			Tolerate tolerate = new Tolerate();
			tolerate.setStartafter("PT1M");
			tolerance.setTolerate(tolerate);
			firstEvent.getEiEvent().getEiActivePeriod().getProperties()
					.setTolerance(tolerance);

			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).setSignalName("SIMPLE");	
			//1st interval
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getIntervals().getInterval().get(0).getDuration().setDuration("PT2M");
			SignalPayloadType signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0)
					.getIntervals().getInterval().get(0).getStreamPayloadBase().get(0).getValue();
			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Simple_Medium")));


			//Second signal
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(1).setSignalName("LOAD_CONTROL");	
			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(1).setSignalType(SignalTypeEnumeratedType.X_LOAD_CONTROL_LEVEL_OFFSET);	

			firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(1).getIntervals().getInterval().get(0).getDuration().setDuration("PT2M");
			signalPayloadFloat = (SignalPayloadType)firstEvent.getEiEvent()
					.getEiEventSignals().getEiEventSignal().get(1)
					.getIntervals().getInterval().get(0).getStreamPayloadBase().get(0).getValue();

			((PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue()).setValue(Float.parseFloat(propertiesFileReader.get("Load_Control_TempOffset_Medium")));

			TemperatureType temp = (TemperatureType) firstEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(1).getItemBase().getValue();
			if(propertiesFileReader.get("temperature_itemUnits").contentEquals("fahrenheit")){
				temp.setItemUnits(TemperatureUnitType.FAHRENHEIT);
			} else {
				temp.setItemUnits(TemperatureUnitType.CELSIUS);
			}
			temp.setSiScaleCode("none");

		}

		return oadrDistributeEvent;
	}
}