package com.qualitylogic.openadr.core.action.impl;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBException;

import com.qualitylogic.openadr.core.base.BaseUpdateReportAction;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.OadrUpdateReportType;
import com.qualitylogic.openadr.core.signal.helper.UpdateReportEventHelper;

public class DefaultResponseUpdateReportTypeAckAction extends BaseUpdateReportAction{
	ServiceType serivceType;
	boolean isCreaterOfRegisterReport;
	OadrCreateReportType oadrCreateReportType;
	public DefaultResponseUpdateReportTypeAckAction(ServiceType serivceType,boolean isCreaterOfRegisterReport,OadrCreateReportType oadrCreateReportType){
		this.serivceType=serivceType;
		this.isCreaterOfRegisterReport=isCreaterOfRegisterReport;
		this.oadrCreateReportType=oadrCreateReportType;
	}


	public OadrUpdateReportType getOadrUpdateReportType() {

		try {
			if(oadrUpdateReportType!=null) return oadrUpdateReportType;
	/*		
			if(oadrCreateReportType==null){
				ArrayList<OadrCreateReportType>  oadrCreateReportTypeReceived=TestSession.getOadrCreateReportTypeReceivedList();
				oadrCreateReportType=oadrCreateReportTypeReceived.get(oadrCreateReportTypeReceived.size()-1);
			}*/
			oadrUpdateReportType = UpdateReportEventHelper.loadOadrUpdateReport(serivceType,isCreaterOfRegisterReport,oadrCreateReportType);
		} catch (FileNotFoundException | UnsupportedEncodingException
				| JAXBException e) {
			e.printStackTrace();
		}
			
		return oadrUpdateReportType;
	}

	
	public void resetToInitialState() {
		isEventCompleted = false;
		oadrUpdateReportType = null;
	}

}