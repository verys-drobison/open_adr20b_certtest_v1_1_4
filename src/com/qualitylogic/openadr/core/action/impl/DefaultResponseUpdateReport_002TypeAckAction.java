package com.qualitylogic.openadr.core.action.impl;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBException;

import com.qualitylogic.openadr.core.base.BaseUpdateReportAction;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.OadrUpdateReportType;
import com.qualitylogic.openadr.core.signal.helper.UpdateReportEventHelper;

public class DefaultResponseUpdateReport_002TypeAckAction extends BaseUpdateReportAction{
	ServiceType serivceType;
	boolean isCreaterOfRegisterReport;
	OadrCreateReportType oadrCreateReportType;
	public DefaultResponseUpdateReport_002TypeAckAction(ServiceType serivceType,boolean isCreaterOfRegisterReport,OadrCreateReportType oadrCreateReportType){
		this.serivceType=serivceType;
		this.isCreaterOfRegisterReport=isCreaterOfRegisterReport;
		this.oadrCreateReportType=oadrCreateReportType;
	}

	public OadrUpdateReportType getOadrUpdateReportType() {

		try {
			if(oadrUpdateReportType!=null) return oadrUpdateReportType;
			 oadrUpdateReportType = UpdateReportEventHelper.loadOadrUpdateReport_Update002();
		} catch (FileNotFoundException | UnsupportedEncodingException
				| JAXBException e) {
			e.printStackTrace();
		}
			
		return oadrUpdateReportType;
	}

	
	public void resetToInitialState() {
		isEventCompleted = false;
		oadrUpdateReportType = null;
	}

}