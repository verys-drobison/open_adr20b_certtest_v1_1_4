package com.qualitylogic.openadr.core.action.impl;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBException;

import com.qualitylogic.openadr.core.action.IResponseCreateReportTypeAckAction;
import com.qualitylogic.openadr.core.action.IResponseUpdatedReportTypeAckAction;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.common.IPrompt;
import com.qualitylogic.openadr.core.signal.OadrUpdatedReportType;
import com.qualitylogic.openadr.core.signal.helper.UpdatedReportEventHelper;

public class DefaultResponseUpdatedWithCancellationReportTypeAckAction implements IResponseUpdatedReportTypeAckAction{

	private boolean isEventCompleted;

	OadrUpdatedReportType oadrUpdatedReportType = null;
	boolean islastEvent;
	ServiceType serviceType;
	IResponseCreateReportTypeAckAction responseCreateReportTypeAckAction;
	public DefaultResponseUpdatedWithCancellationReportTypeAckAction(ServiceType serviceType,IResponseCreateReportTypeAckAction responseCreateReportTypeAckAction) {
		this.serviceType = serviceType;
		this.responseCreateReportTypeAckAction=responseCreateReportTypeAckAction;
	}


	public boolean isPreConditionsMet() {
		return true;
	}

	public boolean isEventCompleted() {
		return isEventCompleted;
	}

	public void setEventCompleted(boolean isEventCompleted) {
		this.isEventCompleted = isEventCompleted;
	}

	public OadrUpdatedReportType getOadrUpdatedReportResponse() {

		try {
			oadrUpdatedReportType = UpdatedReportEventHelper.loadOadrUpdatedReportWithCancelReport(serviceType,responseCreateReportTypeAckAction);
		} catch (FileNotFoundException | UnsupportedEncodingException
				| JAXBException e) {
			e.printStackTrace();
		}
			
		
		return oadrUpdatedReportType;
	}

	public boolean islastEvent(){
		return islastEvent;
	}
	public void setlastEvent(boolean islastEvent){
		this.islastEvent=islastEvent;
	}
	public void resetToInitialState() {
		isEventCompleted = false;
		oadrUpdatedReportType = null;
	}
	
	private IPrompt prompt;
	
	public void setPrompt(IPrompt prompt){
		this.prompt=prompt;
	}
	public IPrompt getPrompt(){
		return prompt;
	}
}