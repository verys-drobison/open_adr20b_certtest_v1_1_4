package com.qualitylogic.openadr.core.action.impl;

import com.qualitylogic.openadr.core.base.BaseReregistrationAction;
import com.qualitylogic.openadr.core.signal.OadrRequestReregistrationType;
import com.qualitylogic.openadr.core.signal.helper.RequestReregistrationSignalHelper;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;

public class Default_Pull_RequestReregistrationAction extends
	BaseReregistrationAction {



	public boolean isEventCompleted() {
		return isEventCompleted;
	}

	@Override
	public void setEventCompleted(boolean isEventCompleted) {
		this.isEventCompleted = isEventCompleted;
	}

	@Override
	public OadrRequestReregistrationType getOadrRequestReregistration() {

		if (oadrRequestReregistrationType == null) {
			oadrRequestReregistrationType = RequestReregistrationSignalHelper
					.loadOadrRequestReregistrationType();
		
			oadrRequestReregistrationType.setVenID(new PropertiesFileReader().getVenID());
		}

		return oadrRequestReregistrationType;
	}



}