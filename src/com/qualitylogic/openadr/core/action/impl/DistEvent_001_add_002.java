package com.qualitylogic.openadr.core.action.impl;


import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import com.qualitylogic.openadr.core.action.DistributeEventActionList;
import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.base.BaseDistributeEventAction;
import com.qualitylogic.openadr.core.bean.CalculatedEventStatusBean;
import com.qualitylogic.openadr.core.signal.EiEventType;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OadrRequestEventType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType.OadrEvent;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.signal.xcal.Dtstart;
import com.qualitylogic.openadr.core.util.Clone;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;

public class DistEvent_001_add_002 extends
		BaseDistributeEventAction {

	private static final long serialVersionUID = 1L;
	protected boolean isEventCompleted;
	private OadrDistributeEventType oadrDistributeEvent = null;

	public boolean isPreConditionsMet(OadrRequestEventType oadrRequestEvent) {
		if(oadrRequestEvent==null) return true;
		boolean isCommonPreConditionsMet = isCommonPreConditionsMet(oadrRequestEvent);
		IDistributeEventAction  distributeEventAction = DistributeEventActionList.getDistributeEventActionList().get(0);
		ICreatedEventResult  createdEventResult  = distributeEventAction.getCreatedEventTimeoutAction();

		if(isCommonPreConditionsMet && createdEventResult.isExpectedCreatedEventReceived()){
			return true;
		}else{
			return false;
		}
		
	}

	public boolean isEventCompleted() {
		return isEventCompleted;
	}

	public void setEventCompleted(boolean isEventCompleted) {
		this.isEventCompleted = isEventCompleted;
	}

	public OadrDistributeEventType getDistributeEvent() {

		PropertiesFileReader propertiesFileReader=new PropertiesFileReader();
		if (oadrDistributeEvent == null) {
			
			IDistributeEventAction  distributeEventAction = DistributeEventActionList.getDistributeEventActionList().get(0);
			oadrDistributeEvent = Clone.clone(distributeEventAction.getDistributeEvent());
			OadrDistributeEventType oadrDistributeEventType002 = new DefaultDistributeEvent_002Action().getDistributeEvent();
			oadrDistributeEvent.getOadrEvent().addAll(oadrDistributeEventType002.getOadrEvent());

			OadrEvent firstEvent = oadrDistributeEvent.getOadrEvent().get(0);
			OadrEvent secondEvent = oadrDistributeEvent.getOadrEvent().get(1);
				
			EiTargetType targetType = firstEvent.getEiEvent().getEiTarget();
			
			
			//Get the start time that is 5 minutes past the first
			Dtstart dtstartfirst = new Dtstart();
			dtstartfirst = firstEvent.getEiEvent().getEiActivePeriod().getProperties().getDtstart();
			Dtstart dtstartlast = new Dtstart();
			XMLGregorianCalendar prevEventCreateTime = null;
			try {
				Duration duration=DatatypeFactory.newInstance().newDuration("PT15M");
				String tm = dtstartfirst.getDateTime().toString();
				prevEventCreateTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(tm);
				prevEventCreateTime.add(duration);
				dtstartlast.setDateTime(prevEventCreateTime);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
//			firstEvent.getEiEvent().getEventDescriptor().setModificationDateTime(OadrUtil.getCurrentTime());
//			firstEvent.getEiEvent().getEventDescriptor().setModificationNumber(0);
			
			List<String> resourceList=firstEvent.getEiEvent().getEiTarget().getResourceID();
			resourceList.clear();
			
			String propFileResourceID[]= propertiesFileReader.getFourResourceID();
			
			targetType.getResourceID().clear();
			targetType.getResourceID().add(propFileResourceID[0]);
			targetType.getResourceID().add(propFileResourceID[1]);
			
			EiTargetType targetTypeSecondEvent = secondEvent.getEiEvent().getEiTarget();
			
			secondEvent.getEiEvent().getEiTarget().getGroupID().add(propertiesFileReader.getGroupID());
			secondEvent.getEiEvent().getEiTarget().getPartyID().add(propertiesFileReader.getParty_ID());
			
			secondEvent.getEiEvent().getEventDescriptor().setModificationDateTime(OadrUtil.getCurrentTime());
			secondEvent.getEiEvent().getEventDescriptor().setModificationNumber(0);

			secondEvent.getEiEvent().getEiActivePeriod().getProperties().setDtstart(dtstartlast);
			
			List<String> resourceListSecondEvent=secondEvent.getEiEvent().getEiTarget().getResourceID();
			resourceListSecondEvent.clear();
			
			targetTypeSecondEvent.getResourceID().clear();
			targetTypeSecondEvent.getResourceID().add(propFileResourceID[2]);
			targetTypeSecondEvent.getResourceID().add(propFileResourceID[3]);
			
			XMLGregorianCalendar createdTime = OadrUtil.getCurrentTime();
			oadrDistributeEvent.getOadrEvent().get(0).getEiEvent().getEventDescriptor().setCreatedDateTime(createdTime);
			oadrDistributeEvent.getOadrEvent().get(1).getEiEvent().getEventDescriptor().setCreatedDateTime(createdTime);
			
		}
		return oadrDistributeEvent;

	}
}