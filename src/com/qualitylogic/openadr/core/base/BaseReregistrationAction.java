package com.qualitylogic.openadr.core.base;

import java.util.List;

import com.qualitylogic.openadr.core.action.IRequestReregistrationAction;
import com.qualitylogic.openadr.core.common.IPrompt;
import com.qualitylogic.openadr.core.signal.OadrCreateOptType;
import com.qualitylogic.openadr.core.signal.OadrRequestReregistrationType;

public abstract class BaseReregistrationAction implements
		IRequestReregistrationAction{
	//private static final long serialVersionUID = 1L;
	protected boolean isEventCompleted;
	boolean isLastOadrRequestReregistration;

	protected OadrRequestReregistrationType oadrRequestReregistrationType = null;
	//private ICreatedEventResult createdEventTimeoutAction;

	private boolean isEventPickedUpFromOadrPoll;
	List<OadrCreateOptType> resourceResultList = null;
	
	public boolean isPreConditionsMet() {

		if (oadrRequestReregistrationType == null) {
			oadrRequestReregistrationType = getOadrRequestReregistration();
		}
			//resetToInitialState();
		
	 return true;
	}


	public void resetToInitialState() {
		isEventCompleted = false;
		oadrRequestReregistrationType = null;

	}
	
	public boolean islastOadrRequestReregistration(){
		return isLastOadrRequestReregistration;
	}
	
	public void setlastOadrRequestReregistration(boolean isLastOadrRequestReregistration){
		this.isLastOadrRequestReregistration=isLastOadrRequestReregistration;
	}
	
	public void setEventPickedUpFromOadrPoll(boolean isEventPickedUpFromOadrPoll) {
		this.isEventPickedUpFromOadrPoll = isEventPickedUpFromOadrPoll;
	}

	public boolean isEventPickedUpFromOadrPoll(){
		return isEventPickedUpFromOadrPoll;
	}
	
	private IPrompt prompt;
	
	public void setPrompt(IPrompt prompt){
		this.prompt=prompt;
	}
	public IPrompt getPrompt(){
		return prompt;
	}
}