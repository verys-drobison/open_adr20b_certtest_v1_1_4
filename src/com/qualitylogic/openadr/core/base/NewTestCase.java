package com.qualitylogic.openadr.core.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.DateTime;
import org.w3c.dom.Node;

import com.qualitylogic.openadr.core.action.IResponseCanceledPartyRegistrationAckAction;
import com.qualitylogic.openadr.core.action.IResponseCreatedReportTypeAckAction;
import com.qualitylogic.openadr.core.action.IResponseEventAction;
import com.qualitylogic.openadr.core.action.IResponseRegisteredReportTypeAckAction;
import com.qualitylogic.openadr.core.action.IResponseUpdatedReportTypeAckAction;
import com.qualitylogic.openadr.core.action.ResponseCanceledPartyRegistrationAckActionList;
import com.qualitylogic.openadr.core.action.ResponseCreatedReportTypeAckActionList;
import com.qualitylogic.openadr.core.action.ResponseEventActionList;
import com.qualitylogic.openadr.core.action.ResponseRegisteredReportTypeAckActionList;
import com.qualitylogic.openadr.core.action.ResponseUpdatedReportTypeAckActionList;
import com.qualitylogic.openadr.core.action.impl.DefaultCanceledPartyRegistration;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCreatedReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseRegisteredReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseUpdatedReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.Default_ResponseEventAction;
import com.qualitylogic.openadr.core.channel.util.StringUtil;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.common.UIUserPrompt;
import com.qualitylogic.openadr.core.common.UIUserPromptDualAction;
import com.qualitylogic.openadr.core.exception.CancelledException;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.exception.ValidationException;
import com.qualitylogic.openadr.core.internal.BatchTestRunner;
import com.qualitylogic.openadr.core.signal.EiResponseType;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.OadrCancelPartyRegistrationType;
import com.qualitylogic.openadr.core.signal.OadrCancelReportType;
import com.qualitylogic.openadr.core.signal.OadrCanceledPartyRegistrationType;
import com.qualitylogic.openadr.core.signal.OadrCanceledReportType;
import com.qualitylogic.openadr.core.signal.OadrCreatePartyRegistrationType;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.OadrCreatedReportType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OadrPayloadResourceStatusType;
import com.qualitylogic.openadr.core.signal.OadrRegisterReportType;
import com.qualitylogic.openadr.core.signal.OadrRegisteredReportType;
import com.qualitylogic.openadr.core.signal.OadrReportDescriptionType;
import com.qualitylogic.openadr.core.signal.OadrReportPayloadType;
import com.qualitylogic.openadr.core.signal.OadrReportRequestType;
import com.qualitylogic.openadr.core.signal.OadrReportType;
import com.qualitylogic.openadr.core.signal.OadrRequestReregistrationType;
import com.qualitylogic.openadr.core.signal.OadrResponseType;
import com.qualitylogic.openadr.core.signal.OadrUpdateReportType;
import com.qualitylogic.openadr.core.signal.OadrUpdatedReportType;
import com.qualitylogic.openadr.core.signal.PowerRealType;
import com.qualitylogic.openadr.core.signal.StreamPayloadBaseType;
import com.qualitylogic.openadr.core.signal.VoltageType;
import com.qualitylogic.openadr.core.signal.helper.CancelReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.CanceledPartyRegistrationHelper;
import com.qualitylogic.openadr.core.signal.helper.CanceledReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.CreatedReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.RegisteredReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.SchemaHelper;
import com.qualitylogic.openadr.core.signal.helper.UpdatedReportEventHelper;
import com.qualitylogic.openadr.core.signal.xcal.AvailableType;
import com.qualitylogic.openadr.core.signal.xcal.Dtstart;
import com.qualitylogic.openadr.core.signal.xcal.DurationPropType;
import com.qualitylogic.openadr.core.signal.xcal.Properties;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;
import com.qualitylogic.openadr.core.util.ResourceFileReader;
import com.qualitylogic.openadr.core.util.XMLDBUtil;
import com.qualitylogic.openadr.core.util.XMLDBUtil.DataPoint;
import com.qualitylogic.openadr.core.ven.VENServerResource;
import com.qualitylogic.openadr.core.signal.PayloadFloatType;

public abstract class NewTestCase extends BaseTestCase {

	protected static ResourceFileReader resources = new ResourceFileReader();
	protected static PropertiesFileReader properties = new PropertiesFileReader();

	private static boolean disable_DR_Guide_Overall_Duration_Check;

	static String  p_disable_DR_Guide_Overall_Duration_Check =   properties.get("disable_DR_Guide_Overall_Duration_Check");

	static{
		if(p_disable_DR_Guide_Overall_Duration_Check!=null && p_disable_DR_Guide_Overall_Duration_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_Overall_Duration_Check=true;
		}
	}

	private static boolean disable_DR_Guide_Target_Check;

	static String  p_disable_DR_Guide_Target_Check =   properties.get("disable_DR_Guide_Target_Check");

	static{
		if(p_disable_DR_Guide_Target_Check!=null && p_disable_DR_Guide_Target_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_Target_Check=true;
		}
	}

	protected void newInit(){
		// System.setErr(new PrintStream(new ByteArrayOutputStream()));
		newTest = true;
		OadrUtil.setServiceType(this.getClass().getName());
	}

	public void execute() {
		try {
			newInit();
			setEnableLogging(true);
			executeTestCase();		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void execute(BaseTestCase testCase) {
		try {
			testCase.newInit();
			testCase.setEnableLogging(true);
			testCase.executeTestCase();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected static void executeDut(BaseTestCase testCase) {
		try {
			testCase.newInit();
			testCase.setEnableLogging(false);
			testCase.executeTestCase();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void prompt(String message) {
		if ((!properties.isTestPrompt() || !TestSession.getBdoPrompt()) && !TestSession.getBalwaysPrompt()) {
			TestSession.setUserClickedContinuePlay(true);
			return;
		}

		new UIUserPrompt().Prompt(message);
		if (!TestSession.isUserClickedContinuePlay()) {
			throw new CancelledException();
		}

		checkForValidationErrors();
	}

	protected void checkForValidationErrors() {
		if (TestSession.isAtleastOneValidationErrorPresent()) {
			throw new ValidationException();
		}
	}

	protected boolean promptYes(String message) {
		if ((!properties.isTestPrompt() || !TestSession.getBdoPrompt()) && !TestSession.getBalwaysPrompt()) {
			if (message.equals(resources.prompt_036())) {
				return false;
			} else if (message.equals(resources.prompt_050())) {
				String className = this.getClass().getName();
				boolean isVen = (className.contains("_VEN"));
				boolean result = (isVen == BatchTestRunner.PROMPT_050_VEN_YES);
				System.out.println("prompt_050=" + result);
				return result;
			} else {
				return true;
			}
		}

		new UIUserPromptDualAction().Prompt(message);

		if(TestSession.isUserClickedToCancelUIDualAction()){
			throw new CancelledException();
		}

		return TestSession.isUiDualActionYesOptionClicked();
	}

	protected void alert(String message) {
		if ((!properties.isTestPrompt() || !TestSession.getBdoPrompt()) && !TestSession.getBalwaysPrompt()) {
			return;
		}

		new UIUserPrompt().Prompt(message);
	}

	protected void waitForCompletion() {
		long timeout = Long.valueOf(properties.get("asyncResponseTimeout"));
		waitForCompletion(timeout);
	}

	protected void waitForCompletion(long timeout) {
		pauseForTestCaseTimeout(timeout);

		checkForValidationErrors();
	}

	protected IResponseRegisteredReportTypeAckAction addRegisteredReportResponse() {
		IResponseRegisteredReportTypeAckAction registeredReport = new DefaultResponseRegisteredReportTypeAckAction();
		ResponseRegisteredReportTypeAckActionList.addResponseRegisteredReportAckAction(registeredReport);
		return registeredReport;
	}

	protected void addRegisteredReportResponse(IResponseRegisteredReportTypeAckAction registeredReport) {
		ResponseRegisteredReportTypeAckActionList.addResponseRegisteredReportAckAction(registeredReport);
	}

	protected void checkRegisterReportRequest(int size) {
		List<OadrRegisterReportType> registerReports = TestSession.getOadrRegisterReportTypeReceivedList();
		if (registerReports.size() != size) {
			throw new FailedException("Expected " + size + " OadrRegisterReport(s), received " + registerReports.size());
		}
	}

	protected void checkRegisterReportRequest_CBP(int size) {
		List<OadrRegisterReportType> registerReports = TestSession.getOadrRegisterReportTypeReceivedList();
		if (registerReports.size() != size) {
			throw new FailedException("Expected " + size + " OadrRegisterReport(s), received " + registerReports.size());
		}
		String propFileResourceID[]= properties.getTwoResourceID();
		OadrRegisterReportType oadrRegisterReportType = registerReports.get(0);
		List<OadrReportType>  OadrReportList = oadrRegisterReportType.getOadrReport();
		boolean checkTelemetry = false;
		String reportNames = "";
		String comma = "";

		for(OadrReportType eachOadrReportType:OadrReportList){
			List<OadrReportDescriptionType>  oadrReportDescriptionList = eachOadrReportType.getOadrReportDescription();
			String rname = eachOadrReportType.getReportName();
			reportNames += comma + rname;
			comma = ", ";

			for(OadrReportDescriptionType oadrReportDescriptionType:oadrReportDescriptionList){
				//check resources
				checkoadrReportDescriptionType(oadrReportDescriptionType, rname, properties.get("Nominal_Expected_Report_Granularity"),
						("none"));
				if(rname.contains("METADATA_TELEMETRY_USAGE")){
					checkTelemetry = true;
					baseItemCheck(oadrReportDescriptionType, rname, "Direct Read", "usage",
							properties.get("TelemetryUsage_PowerReal_WellKnown_rID"),
							properties.get("powerReal_itemUnits"),
							properties.get("powerReal_siScaleCode"),
							properties.get("powerReal_powerAttributes_hertz"), 
							properties.get("powerReal_powerAttributes_voltage"), 
							properties.get("powerReal_powerAttributes_ac"));

				}

			}
		}
		if (!checkTelemetry) {
			throw new FailedException("Expected to find METADATA_TELEMETRY_USAGE in OadrRegisterReport, received " + reportNames + ".");
		}


	}

	protected void checkRegisterReportRequest_THR(int size) {
		List<OadrRegisterReportType> registerReports = TestSession.getOadrRegisterReportTypeReceivedList();
		if (registerReports.size() != size) {
			throw new FailedException("Expected " + size + " OadrRegisterReport(s), received " + registerReports.size());
		}
		//		String propFileResourceID[]= properties.getTwoResourceID();
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();
		OadrRegisterReportType oadrRegisterReportType = registerReports.get(0);
		List<OadrReportType>  OadrReportList = oadrRegisterReportType.getOadrReport();
		boolean[] fnd = {false,false,false,false,false,false,false,false,false,false};
		String reportNames = "";
		String comma = "";

		for(OadrReportType eachOadrReportType:OadrReportList){
			List<OadrReportDescriptionType>  oadrReportDescriptionList = eachOadrReportType.getOadrReportDescription();
			String rname = eachOadrReportType.getReportName();
			reportNames += comma + rname;
			comma = ", ";
			String testName = "METADATA_TELEMETRY_STATUS";

			for(OadrReportDescriptionType oadrReportDescriptionType:oadrReportDescriptionList){
				//check resources
				checkoadrReportDescriptionType(oadrReportDescriptionType, rname, properties.get("Nominal_Expected_Report_Granularity"),
						("none"));
				if(rname.contains(testName) 
						&& wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("ThermostatState_Status_WellKnown_rID"))){
					fnd[0] = true;
				}
				if(rname.contains(testName) 
						&& wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("ThermostatState_CurrentTemp_WellKnown_rID"))){
					fnd[1] = true;
				}
				if(rname.contains(testName) 
						&& wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("ThermostatState_HeatTempSetting_WellKnown_rID"))){
					fnd[2] = true;
				}
				if(rname.contains(testName) 
						&& wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("ThermostatState_CoolTempSetting_WellKnown_rID"))){
					fnd[3] = true;
				}
				if(rname.contains(testName) 
						&& wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("ThermostatState_HVACModeSetting_WellKnown_rID"))){
					fnd[4] = true;
				}
				if(rname.contains(testName) 
						&& wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("ThermostatState_CurrentHVACMode_WellKnown_rID"))){
					fnd[5] = true;
				}
				if(rname.contains(testName) 
						&& wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("ThermostatState_FanModeSetting_WellKnown_rID"))){
					fnd[6] = true;
				}
				if(rname.contains(testName) 
						&& wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("ThermostatState_CurrentHoldMode_WellKnown_rID"))){
					fnd[7] = true;
				}
				if(rname.contains(testName) 
						&& wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("ThermostatState_CurrentAwayMode_WellKnown_rID"))){
					fnd[8] = true;
				}
				if(rname.contains(testName) 
						&& wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("ThermostatState_CurrentHumidity_WellKnown_rID"))){
					fnd[9] = true;
				}

			}
		}
		if(!fnd[0] && !fnd[1] && !fnd[2] && !fnd[3] && !fnd[4] && !fnd[5] && !fnd[6] && !fnd[7] && !fnd[8] && !fnd[9]){
			String ptext ="Failed to find an appropriate report data point for METADATA_TELEMETRY_STATUS with the following characteristics:\n";
			ptext +="Report Granularity of " + properties.get("Nominal_Expected_Report_Granularity") + " should fall on or between oadrMinPeriod and oadrMaxPeriod \n";
			ptext +="Reading Type: x-notApplicable\n\n";
			if(!fnd[0]){
				ptext +="<Status>\n";
				ptext +="Report Type: x-resourceStatus (oadrOnline, oadrLevelOffset:current)\n";
				ptext +="Units: x-notApplicable\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_Status_WellKnown_rID") + "\n\n";
			}
			if(!fnd[1]){
				ptext +="<Current Temp>\n";
				ptext +="Report Type: x-thermostatStatus\n";
				ptext +="Units: temperature\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CurrentTemp_WellKnown_rID") + "\n\n";
			}			
			if(!fnd[2]){
				ptext +="<Heat Temp Setting>\n";
				ptext +="Report Type:  setPoint\n";
				ptext +="Units: temperature\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_HeatTempSetting_WellKnown_rID") + "\n\n";
			}
			if(!fnd[3]){
				ptext +="<Cool Temp Setting>\n";
				ptext +="Report Type: setPoint\n";
				ptext +="Units: temperature\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CoolTempSetting_WellKnown_rID") + "\n\n";
			}			
			if(!fnd[4]){
				ptext +="<HVAC Mode Setting>\n";
				ptext +="Report Type: operatingState\n";
				ptext +="Units: customUnit\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_HVACModeSetting_WellKnown_rID") + "\n\n";
			}
			if(!fnd[5]){
				ptext +="<Current HVAC Mode>\n";
				ptext +="Report Type: operatingState\n";
				ptext +="Units: customUnit\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CurrentHVACMode_WellKnown_rID") + "\n\n";
			}			
			if(!fnd[6]){
				ptext +="<Fan Mode Setting>\n";
				ptext +="Report Type: operatingState\n";
				ptext +="Units: customUnit\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_FanModeSetting_WellKnown_rID") + "\n\n";
			}
			if(!fnd[7]){
				ptext +="<Current Hold Mode>\n";
				ptext +="Report Type: operatingState\n";
				ptext +="Units: customUnit\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CurrentHoldMode_WellKnown_rID") + "\n\n";
			}			
			if(!fnd[8]){
				ptext +="<Current Away Mode>\n";
				ptext +="Report Type: operatingState\n";
				ptext +="Units: customUnit\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CurrentAwayMode_WellKnown_rID") + "\n\n";
			}
			if(!fnd[9]){
				ptext +="<Current Humidity>\n";
				ptext +="Report Type: customUnit\n";
				ptext +="Units: customUnit (percent)\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CurrentHumidity_WellKnown_rID") + "\n\n";
			}
			throw new FailedException(ptext);
		}


	}

	protected void checkRegisterReportRequest_FDR_TYP(int size) {
		List<OadrRegisterReportType> registerReports = TestSession.getOadrRegisterReportTypeReceivedList();
		if (registerReports.size() != size) {
			throw new FailedException("Expected " + size + " OadrRegisterReport(s), received " + registerReports.size());
		}
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();
		OadrRegisterReportType oadrRegisterReportType = registerReports.get(0);
		List<OadrReportType>  OadrReportList = oadrRegisterReportType.getOadrReport();
		boolean checkTelemetry = false;
		String reportNames = "";
		String comma = "";

		for(OadrReportType eachOadrReportType:OadrReportList){
			List<OadrReportDescriptionType>  oadrReportDescriptionList = eachOadrReportType.getOadrReportDescription();
			String rname = eachOadrReportType.getReportName();
			reportNames += comma + rname;
			comma = ", ";

			for(OadrReportDescriptionType oadrReportDescriptionType:oadrReportDescriptionList){
				//check resources
				checkoadrReportDescriptionType(oadrReportDescriptionType, rname, properties.get("Nominal_Expected_Report_Granularity"),
						("none"));
				if(rname.contains("METADATA_TELEMETRY_USAGE")){
					checkTelemetry = true;
					baseItemCheck(oadrReportDescriptionType, rname, "Direct Read", "usage",
							properties.get("TelemetryUsage_PowerReal_WellKnown_rID"),
							properties.get("powerReal_itemUnits"),
							properties.get("powerReal_siScaleCode"),
							properties.get("powerReal_powerAttributes_hertz"), 
							properties.get("powerReal_powerAttributes_voltage"), 
							properties.get("powerReal_powerAttributes_ac"));

				}

			}
		}
		if (!checkTelemetry) {
			throw new FailedException("Expected to find METADATA_TELEMETRY_USAGE with powerReal in OadrRegisterReport, received " + reportNames + ".");
		}


	}

	protected void checkRegisterReportRequest_FDR_CMP(int size) {
		List<OadrRegisterReportType> registerReports = TestSession.getOadrRegisterReportTypeReceivedList();
		if (registerReports.size() != size) {
			throw new FailedException("Expected " + size + " OadrRegisterReport(s), received " + registerReports.size());
		}
		OadrRegisterReportType oadrRegisterReportType = registerReports.get(0);
		List<OadrReportType>  OadrReportList = oadrRegisterReportType.getOadrReport();
		boolean checkTelemetry = false;
		boolean checkTelemetry2 = false;
		String reportNames = "";
		String comma = "";

		for(OadrReportType eachOadrReportType:OadrReportList){
			List<OadrReportDescriptionType>  oadrReportDescriptionList = eachOadrReportType.getOadrReportDescription();
			String rname = eachOadrReportType.getReportName();
			reportNames += comma + rname;
			comma = ", ";

			for(OadrReportDescriptionType oadrReportDescriptionType:oadrReportDescriptionList){
				//check resources
				checkoadrReportDescriptionType(oadrReportDescriptionType, rname, "PT5S",
						("none"));
				if(rname.contains("METADATA_TELEMETRY_USAGE") && 
						wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("TelemetryUsage_PowerReal_WellKnown_rID"))){
					checkTelemetry = true;
					baseItemCheck(oadrReportDescriptionType, rname, "Direct Read", "usage",
							properties.get("TelemetryUsage_PowerReal_WellKnown_rID"),
							properties.get("powerReal_itemUnits"),
							properties.get("powerReal_siScaleCode"),
							properties.get("powerReal_powerAttributes_hertz"), 
							properties.get("powerReal_powerAttributes_voltage"), 
							properties.get("powerReal_powerAttributes_ac"));

				}

				if(rname.contains("METADATA_TELEMETRY_USAGE") && 
						wellKnownCheck(oadrReportDescriptionType.getRID(),properties.get("TelemetryUsage_Voltage_WellKnown_rID"))){
					checkTelemetry2 = true;
					baseItemCheckVoltage(oadrReportDescriptionType, rname, "Direct Read", "usage",
							properties.get("TelemetryUsage_Voltage_WellKnown_rID"),
							"V",
							"none");

				}

			}
		}
		if (!checkTelemetry || !checkTelemetry2) {
			throw new FailedException("Expected to find METADATA_TELEMETRY_USAGE with powerReal and voltage in OadrRegisterReport, received " + reportNames + ".");
		}


	}

	protected void checkoadrReportDescriptionType(OadrReportDescriptionType oadrReportDescriptionType, String rname, String granDur, String... targets){
		EiTargetType targetType =  oadrReportDescriptionType.getReportDataSource();
		
		if (!disable_DR_Guide_Target_Check && !targets[0].toLowerCase().contentEquals("none")){
			//Execution of this blocked of code has been disabled by passing in "none" as the target to this routine
			for (int j=0; j< targets.length; j++){
				String[] oneTarget = targets[j].split(":");
				if(oneTarget[0].toLowerCase().contentEquals("venid")){
					boolean found = false;
					int ix = targetType.getVenID().size();
					for (int i=0; i<ix; i++){
						if (oneTarget[1].contentEquals(targetType.getVenID().get(i))){
							found = true;
						}
					}
					if (!found){
						throw new FailedException("Did not find a VenID of " + oneTarget[1] 
								+ "  for OadrReportDescription in " + rname );
					}
				//Code below should not assume "resource" is going to be part of string
				} else if(oneTarget[0].toLowerCase().contentEquals("resource")){
					boolean found = false;
					int ix = targetType.getResourceID().size();
					for (int i=0; i<ix; i++){
						if (oneTarget[1].contentEquals(targetType.getResourceID().get(i))){
							found = true;
						}
					}
					if (!found){
						throw new FailedException("Did not find a resource of " + oneTarget[1] 
								+ "  for OadrReportDescription in " + rname );
					}
				} else {
					throw new FailedException("Did not understand a target prefix of " + oneTarget[0] 
							+ "  for OadrReportDescription in " + rname );

				}
			}

		}
		if (!disable_DR_Guide_Overall_Duration_Check && !granDur.toLowerCase().contentEquals("n/a")){
			if(!(OadrUtil.createDuration(oadrReportDescriptionType.getOadrSamplingRate().getOadrMinPeriod()).compare(OadrUtil.createDuration(granDur)) <= 0 &&
					OadrUtil.createDuration(oadrReportDescriptionType.getOadrSamplingRate().getOadrMaxPeriod()).compare(OadrUtil.createDuration(granDur)) >= 0)){
				throw new FailedException("Expected a granularity of " + granDur + " to fall on or between the OadrMinPeriod of " + 
						oadrReportDescriptionType.getOadrSamplingRate().getOadrMinPeriod() + " and the OadrMaxPeriod of " + 
						oadrReportDescriptionType.getOadrSamplingRate().getOadrMaxPeriod() +
						" for " + rname);

			}
		}

	}

	private void baseItemCheck(OadrReportDescriptionType oadrReportDescriptionType, String rname, String readType, String reportType, String rId,
			String units, String scale, String hertz, String volt, String ac){
		/*
		 * Check for powerReal
		 * Items are descriptive in the call
		 */

		if(!oadrReportDescriptionType.getItemBase().getValue().getClass().equals(PowerRealType.class)){
			throw new FailedException("powerReal does not appear to exist for OadrReportDescription in "
					+ rname +".");
		}

		if(!oadrReportDescriptionType.getReadingType().toLowerCase().contentEquals(readType.toLowerCase())){
			throw new FailedException("Expected a readingType of " + readType + " but found " 
					+ oadrReportDescriptionType.getReadingType() + " for OadrReportDescription in "	+ rname +".");
		}

		if(!oadrReportDescriptionType.getReportType().toLowerCase().contentEquals(reportType.toLowerCase())){
			throw new FailedException("Expected a reportType of " + reportType + " but found " 
					+ oadrReportDescriptionType.getReportType() + " for OadrReportDescription in "	+ rname +".");
		}

		if(!oadrReportDescriptionType.getRID().toLowerCase().contains(rId.toLowerCase())){
			throw new FailedException("Expected a rId containing " + rId + " but found " 
					+ oadrReportDescriptionType.getRID() + " for OadrReportDescription in "	+ rname +".");
		}

		PowerRealType preal = (PowerRealType) oadrReportDescriptionType.getItemBase().getValue();
		if(!preal.getItemUnits().contentEquals(units)){
			throw new FailedException("Expect powerReal itemUnits of " + units + " but found " + preal.getItemUnits() + " for OadrReportDescription in "
					+ rname +".");
		}

		if(!preal.getSiScaleCode().contentEquals(scale)){
			throw new FailedException("Expect powerReal SiScaleCode of " + scale + " but found " + preal.getSiScaleCode() + " for OadrReportDescription in "
					+ rname +".");
		}

		if(preal.getPowerAttributes().isAc() != Boolean.parseBoolean(ac)){
			throw new FailedException("Expect powerReal attribute AC of " + ac + " but found " + preal.getPowerAttributes().isAc() + " for OadrReportDescription in "
					+ rname +".");
		}

		if(preal.getPowerAttributes().getHertz().floatValue() != Float.parseFloat(hertz)){
			throw new FailedException("Expect powerReal attribute Hertz of " + hertz + " but found " + preal.getPowerAttributes().getHertz()+ " for OadrReportDescription in "
					+ rname +".");
		}

		if(preal.getPowerAttributes().getVoltage().floatValue() != Float.parseFloat(volt)){
			throw new FailedException("Expect powerReal attribute volts of " + volt + " but found " + preal.getPowerAttributes().getVoltage() + " for OadrReportDescription in "
					+ rname +".");
		}

	}

	private void baseItemCheckVoltage(OadrReportDescriptionType oadrReportDescriptionType, String rname, String readType, String reportType, String rId,
			String units, String scale){
		/*
		 * Check for voltage
		 * Items are descriptive in the call
		 */

		if(!oadrReportDescriptionType.getItemBase().getValue().getClass().equals(VoltageType.class)){
			throw new FailedException("voltage does not appear to exist for OadrReportDescription in "
					+ rname +".");
		}

		if(!oadrReportDescriptionType.getReadingType().toLowerCase().contentEquals(readType.toLowerCase())){
			throw new FailedException("Expected a readingType of " + readType + " but found " 
					+ oadrReportDescriptionType.getReadingType() + " for OadrReportDescription in "	+ rname +".");
		}

		if(!oadrReportDescriptionType.getReportType().toLowerCase().contentEquals(reportType.toLowerCase())){
			throw new FailedException("Expected a reportType of " + reportType + " but found " 
					+ oadrReportDescriptionType.getReportType() + " for OadrReportDescription in "	+ rname +".");
		}

		if(!oadrReportDescriptionType.getRID().toLowerCase().contains(rId.toLowerCase())){
			throw new FailedException("Expected a rId containing " + rId + " but found " 
					+ oadrReportDescriptionType.getRID() + " for OadrReportDescription in "	+ rname +".");
		}

		VoltageType volt = (VoltageType) oadrReportDescriptionType.getItemBase().getValue();
		if(!volt.getItemUnits().contentEquals(units)){
			throw new FailedException("Expect voltage itemUnits of " + units + " but found " + volt.getItemUnits() + " for OadrReportDescription in "
					+ rname +".");
		}

		if(!volt.getSiScaleCode().contentEquals(scale)){
			throw new FailedException("Expect voltage SiScaleCode of " + scale + " but found " + volt.getSiScaleCode() + " for OadrReportDescription in "
					+ rname +".");
		}

	}


	protected void checkRegisterReportRequestLess(int size) {
		List<OadrRegisterReportType> registerReports = TestSession.getOadrRegisterReportTypeReceivedList();
		if (registerReports.size() < size) {
			throw new FailedException("Expected " + size + " OadrRegisterReport(s), received " + registerReports.size());
		}
	}

	protected AvailableType getAvailable(int startDays, int durationHours) {
		XMLGregorianCalendar currentTime = OadrUtil.getCurrentTime();

		Duration durationDays = OadrUtil.createDuration(startDays * 24, 0, 0);
		currentTime.add(durationDays);
		Dtstart durationStartTime = new Dtstart();
		durationStartTime.setDateTime(currentTime);

		Duration duration = OadrUtil.createDuration(durationHours, 0, 0);
		DurationPropType durationProp = new DurationPropType();
		durationProp.setDuration(duration.toString());

		AvailableType available = new AvailableType();
		available.setProperties(new Properties());
		Properties properties = available.getProperties();
		properties.setDtstart(durationStartTime);
		properties.setDuration(durationProp);

		return available;
	}

	protected AvailableType getAvailable(String duration) {
		Dtstart durationStartTime = new Dtstart();
		durationStartTime.setDateTime(OadrUtil.getCurrentTime());

		DurationPropType durationProp = new DurationPropType();
		durationProp.setDuration(duration);

		AvailableType available = new AvailableType();
		available.setProperties(new Properties());
		Properties properties = available.getProperties();
		properties.setDtstart(durationStartTime);
		properties.setDuration(durationProp);

		return available;
	}

	protected IResponseCanceledPartyRegistrationAckAction addCanceledPartyRegistrationResponse() {
		return addCanceledPartyRegistrationResponse(ErrorConst.OK_200);
	}

	protected IResponseCanceledPartyRegistrationAckAction addCanceledPartyRegistrationResponse(String responseCode) {
		IResponseCanceledPartyRegistrationAckAction canceledPartyRegistration = new DefaultCanceledPartyRegistration(responseCode);
		ResponseCanceledPartyRegistrationAckActionList.addResponseCanceledPartyRegistrationAckAction(canceledPartyRegistration);
		return canceledPartyRegistration;
	}

	protected void waitForCancelPartyRegistration(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			if (TestSession.getCancelPartyRegistrationTypeListReceived().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}
		pause(2);

		int listSize = TestSession.getCancelPartyRegistrationTypeListReceived().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " CancelPartyRegistration(s), received " + listSize);
		}

		checkForValidationErrors();
	}

	protected void waitForRequestReregistration(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			if (TestSession.getOadrRequestReregistrationReceivedList().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}
		pause(2);

		int listSize = TestSession.getOadrRequestReregistrationReceivedList().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " RequestReregistration(s), received " + listSize);
		}

		checkForValidationErrors();
	}

	protected void waitForCancelOpt(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);
			if (TestSession.getCancelOptTypeReceivedList().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}
		pause(2);

		int listSize = TestSession.getCancelOptTypeReceivedList().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " CancelOpt(s), received " + listSize);
		}

		checkForValidationErrors();
	}

	protected void waitForCreatePartyRegistration(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);
			if (TestSession.getCreatePartyRegistrationTypeReceivedList().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}
		pause(2);

		checkCreatePartyRegistrationRequest(size);

		checkForValidationErrors();
	}

	protected OadrCanceledPartyRegistrationType sendCancelPartyRegistration(OadrCancelPartyRegistrationType cancelPartyRegistration) throws Exception {
		return sendCancelPartyRegistration(cancelPartyRegistration, ErrorConst.OK_200);
	}

	protected OadrCanceledPartyRegistrationType sendCancelPartyRegistration(OadrCancelPartyRegistrationType cancelPartyRegistration, String responseCode) throws Exception {
		String text = SchemaHelper.getCancelPartyRegistrationAsString(cancelPartyRegistration);
		String response = post(text, "EiRegistration");
		if (!OadrUtil.isExpected(response, OadrCanceledPartyRegistrationType.class)){
			throw new FailedException("Expected OadrCanceledPartyRegistration has not been received");
		}

		OadrCanceledPartyRegistrationType canceledPartyRegistration = CanceledPartyRegistrationHelper.createOadrCanceledPartyRegistrationTypeFromString(response);
		String eiResponseCode = canceledPartyRegistration.getEiResponse().getResponseCode();
		if (!eiResponseCode.equals(responseCode)) {
			throw new FailedException("Expected OadrCanceledPartyRegistration responseCode of " + responseCode + " has not been received. Got " + eiResponseCode + ".");
		}
		return canceledPartyRegistration;
	}

	protected void sendRequestReregistration(OadrRequestReregistrationType requestReregistration) throws Exception {
		String text = SchemaHelper.getOadrRequestReregistrationTypeAsString(requestReregistration);
		String response = post(text, "EiRegistration");
		if (!OadrUtil.isExpected(response, OadrResponseType.class)){
			throw new FailedException("Expected OadrRequestReregistration has not been received");
		}
	}

	protected IResponseEventAction addResponse() {
		return addResponse(ErrorConst.OK_200);
	}

	protected IResponseEventAction addResponse(String responseCode) {
		IResponseEventAction response = new Default_ResponseEventAction(responseCode);
		ResponseEventActionList.addResponseEventAction(response);
		return response;
	}

	protected OadrDistributeEventType waitForDistributeEvent(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			if (VENServerResource.getOadrDistributeEventReceivedsList().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}
		pause(2);

		int listSize = VENServerResource.getOadrDistributeEventReceivedsList().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " DistributeEvent(s), received " + listSize);
		}

		checkForValidationErrors();

		return VENServerResource.getOadrDistributeEventReceivedsList().get(size - 1);
	}

	protected OadrDistributeEventType waitForDistributeEventSpecificNumberOfEvents(int expectedNumberOfEvents) {

		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			ArrayList<OadrDistributeEventType> distributeEventList = VENServerResource.getOadrDistributeEventReceivedsList();

			for(OadrDistributeEventType eachOadrDistributeEventType:distributeEventList){
				if(eachOadrDistributeEventType.getOadrEvent().size()==expectedNumberOfEvents){
					return eachOadrDistributeEventType;
				}
			}


		}
		pause(2);

		throw new FailedException("Did not receive the expected DistributeEvent");

	}

	protected void waitForResponse(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			if (TestSession.getOadrResponse().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}

		int listSize = TestSession.getOadrResponse().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " Response(s), received " + listSize);
		}

		checkForValidationErrors();
	}

	protected void waitForCreatedOpt(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			if (TestSession.getCreatedOptReceivedList().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}
		pause(2);

		int listSize = TestSession.getCreatedOptReceivedList().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " CreatedOpt(s), received " + listSize);
		}

		checkForValidationErrors();
	}

	protected void pauseForTestCaseTimeout(long timeout) {
		long testCaseBufferTimeout = Long.valueOf(properties.get("testCaseBufferTimeout"));
		long testCaseTimeout = System.currentTimeMillis() + timeout;

		while (System.currentTimeMillis() < testCaseTimeout) {
			if (TestSession.isAtleastOneValidationErrorPresent()) {
				System.out.println("Validation error(s) is present " + new Date());
				break;
			}

			if (TestSession.isCreatedEventNotReceivedTillTimeout()) {
				System.out.println("Created Event not received breaking out of loop " + new Date());
				break;
			}

			if (TestSession.isTestCaseDone()) {
				long testCaseBufferTime = System.currentTimeMillis() + testCaseBufferTimeout;

				while (System.currentTimeMillis() < testCaseBufferTime) {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
					}
				}
				break;
			}

			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
			}
		}
	}

	protected void pauseInMillis(int millis) {
		long pauseTimeout = System.currentTimeMillis() + (millis);
		while (System.currentTimeMillis() < pauseTimeout) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// ignore
			}
		}
	}

	protected void checkCreatePartyRegistrationRequest(int size) {
		List<OadrCreatePartyRegistrationType> createPartyRegistrations = TestSession.getCreatePartyRegistrationTypeReceivedList();
		if (createPartyRegistrations.size() != size) {
			throw new FailedException("Expected " + size + " OadrCreatePartyRegistration(s), received " + createPartyRegistrations.size());
		}
	}

	protected IResponseUpdatedReportTypeAckAction addUpdatedReportResponse() {
		IResponseUpdatedReportTypeAckAction updatedReport = new DefaultResponseUpdatedReportTypeAckAction();
		ResponseUpdatedReportTypeAckActionList.addResponseUpdatedReportAckAction(updatedReport);
		return updatedReport;
	}

	protected IResponseUpdatedReportTypeAckAction addUpdatedReportResponse(IResponseUpdatedReportTypeAckAction updatedReport) {
		ResponseUpdatedReportTypeAckActionList.addResponseUpdatedReportAckAction(updatedReport);
		return updatedReport;
	}

	protected IResponseUpdatedReportTypeAckAction addUpdatedReportResponse(String responseCode) {
		IResponseUpdatedReportTypeAckAction updatedReport = new DefaultResponseUpdatedReportTypeAckAction(responseCode);
		ResponseUpdatedReportTypeAckActionList.addResponseUpdatedReportAckAction(updatedReport);
		return updatedReport;
	}

	protected void checkNoActiveRegistration() {
		String registrationID = new XMLDBUtil().getRegistrationID();
		if (!StringUtil.isBlank(registrationID)){
			throw new FailedException("Registration is active.");
		}
	}

	protected static boolean isCalendarWithinMinutes(XMLGregorianCalendar calendar, int minutesOffset, int minutes) {
		Date time = calendar.toGregorianCalendar().getTime();

		DateTime dateTime = new DateTime().minusMinutes(minutesOffset);
		Date start = dateTime.minusMinutes(minutes).toDate();
		Date end = dateTime.plusMinutes(minutes).toDate();

		return (time.after(start) && time.before(end));
	}

	protected DataPoint getDataPoint(Node reportNode, String type) {
		DataPoint result = null;

		XMLDBUtil xmlDb = new XMLDBUtil();
		List<DataPoint> dataPoints = xmlDb.getDataPoints(reportNode);
		for (DataPoint dataPoint : dataPoints) {
			if (dataPoint.getType().equals(type)) {
				result = dataPoint;
			}
		}
		return result;
	}

	protected void pause(int seconds, String message) {
		System.out.println(message);
		pause(seconds);
	}

	protected IResponseCreatedReportTypeAckAction addCreatedReportResponse() {
		return addCreatedReportResponse(ErrorConst.OK_200);
	}

	protected IResponseCreatedReportTypeAckAction addCreatedReportResponse(String responseCode) {
		IResponseCreatedReportTypeAckAction createdReport = new DefaultResponseCreatedReportTypeAckAction(responseCode);
		ResponseCreatedReportTypeAckActionList.addResponseCreatedReportAckAction(createdReport);
		return createdReport;
	}

	protected OadrCreateReportType waitForCreateReport(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			if (TestSession.getOadrCreateReportTypeReceivedList().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}

		int listSize = TestSession.getOadrCreateReportTypeReceivedList().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " CreateReport(s), received " + listSize);
		}

		checkForValidationErrors();

		return TestSession.getOadrCreateReportTypeReceivedList().get(size - 1);
	}

	protected OadrUpdatedReportType sendUpdateReport(OadrUpdateReportType updateReport) throws Exception {
		return sendUpdateReport(updateReport, ErrorConst.OK_200);
	}

	protected OadrUpdatedReportType sendUpdateReport(OadrUpdateReportType updateReport, String responseCode) throws Exception {
		String text = SchemaHelper.getUpdateReportTypeAsString(updateReport);
		String response = post(text, "EiReport");

		if (!OadrUtil.isExpected(response, OadrUpdatedReportType.class)) {
			throw new FailedException("Expected OadrUpdatedReport has not been received");
		}

		OadrUpdatedReportType updatedReport = UpdatedReportEventHelper.loadReportFromString(response);
		String eiResponseCode = updatedReport.getEiResponse().getResponseCode();
		if (!eiResponseCode.equals(responseCode)) {
			throw new FailedException("Expected OadrUpdateReport responseCode of " + responseCode + " has not been received. Got " + eiResponseCode + ".");
		}

		return updatedReport;
	}

	protected void sendUpdateReportWithSignedId(OadrUpdateReportType updateReport) throws Exception {
		String text = SchemaHelper.getUpdateReportTypeWithSignedIdAsString(updateReport);
		String response = post(text, "EiReport");

		if (!OadrUtil.isExpected(response, OadrUpdatedReportType.class)) {
			throw new FailedException("Expected OadrUpdatedReport has not been received");
		}
	}

	protected OadrCreatedReportType sendCreateReport(OadrCreateReportType createReport) throws Exception {
		return sendCreateReport(createReport, ErrorConst.OK_200);
	}

	protected OadrCreatedReportType sendCreateReport(OadrCreateReportType createReport, String responseCode) throws Exception {
		// TODO move to VtnToVenClient
		TestSession.getOadrCreateReportTypeReceivedList().add(createReport);

		String text = SchemaHelper.getCreateReportTypeAsString(createReport);
		String responseText = post(text, "EiReport");

		if (!OadrUtil.isExpected(responseText, OadrCreatedReportType.class)) {
			throw new FailedException("Expected OadrCreatedReport has not been received");
		}

		OadrCreatedReportType createdReport = CreatedReportEventHelper.createOadrCreatedReportFromString(responseText);
		String eiResponseCode = createdReport.getEiResponse().getResponseCode();
		if (!eiResponseCode.equals(responseCode)) {
			throw new FailedException("Expected OadrCreatedReport responseCode of " + responseCode + " has not been received. Got " + eiResponseCode + ".");
		}

		return createdReport;
	}

	protected OadrUpdateReportType checkUpdateReport(int size) {
		List<OadrUpdateReportType> updateReports = TestSession.getOadrUpdateReportTypeReceivedList();
		if (updateReports.size() != size){
			throw new FailedException("Expected " + size + " OadrUpdateReport(s), received " + updateReports.size());
		}

		return TestSession.getOadrUpdateReportTypeReceivedList().get(size - 1);
	}

	protected OadrUpdateReportType checkUpdateReport_CBP(int size) {
		List<OadrUpdateReportType> updateReports = TestSession.getOadrUpdateReportTypeReceivedList();
		if (updateReports.size() != size){
			throw new FailedException("Expected " + size + " OadrUpdateReport(s), received " + updateReports.size());
		}
		boolean fnd = false;
		String minMax[] = properties.get("TelemetryUsage_PowerReal_Range").split(",");
		Float min = Float.parseFloat(minMax[0]);
		Float max = Float.parseFloat(minMax[1]);
		for (OadrUpdateReportType reports:updateReports){

			List<JAXBElement<? extends StreamPayloadBaseType>> rptPayload = reports.getOadrReport().get(0).getIntervals()
					.getInterval().get(0).getStreamPayloadBase();
			OadrReportPayloadType reportPayload = (OadrReportPayloadType) rptPayload.get(0).getValue();
			String rID = reportPayload.getRID();
			if(wellKnownCheck(rID, properties.get("TelemetryUsage_PowerReal_WellKnown_rID"))){
				Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
				if(value < min || value > max){
					throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the TelemetryUsage_PowerReal_Range of "
							+ min.toString() + " to " + max.toString());
				}
				fnd = true;
			}
		}
		if(!fnd){
			String ptext ="Failed to find an appropriately configured report rID for TelemetryUsage_PowerReal_WellKnown_rID of ";
			ptext += properties.get("TelemetryUsage_PowerReal_WellKnown_rID"); 
			throw new FailedException(ptext);
		}

		return TestSession.getOadrUpdateReportTypeReceivedList().get(size - 1);
	}

	protected OadrUpdateReportType checkUpdateReport_FDR_TYP(int size) {
		List<OadrUpdateReportType> updateReports = TestSession.getOadrUpdateReportTypeReceivedList();
		if (updateReports.size() != size){
			throw new FailedException("Expected " + size + " OadrUpdateReport(s), received " + updateReports.size());
		}
		boolean fnd = false;
		String minMax[] = properties.get("TelemetryUsage_PowerReal_Range").split(",");
		Float min = Float.parseFloat(minMax[0]);
		Float max = Float.parseFloat(minMax[1]);
		for (OadrUpdateReportType reports:updateReports){
			List<JAXBElement<? extends StreamPayloadBaseType>> rptPayload = reports.getOadrReport().get(0).getIntervals()
					.getInterval().get(0).getStreamPayloadBase();
			OadrReportPayloadType reportPayload = (OadrReportPayloadType) rptPayload.get(0).getValue();
			String rID = reportPayload.getRID();
			if(wellKnownCheck(rID, properties.get("TelemetryUsage_PowerReal_WellKnown_rID"))){
				Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
				if(value < min || value > max){
					throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the TelemetryUsage_PowerReal_Range of "
							+ min.toString() + " to " + max.toString());
				}
				fnd = true;
			}
		}
		if(!fnd){
			String ptext ="Failed to find an appropriately configured report rID for TelemetryUsage_PowerReal_WellKnown_rID of ";
			ptext += properties.get("TelemetryUsage_PowerReal_WellKnown_rID"); 
			throw new FailedException(ptext);
		}

		return TestSession.getOadrUpdateReportTypeReceivedList().get(size - 1);
	}

	protected OadrUpdateReportType checkUpdateReport_FDR_CMP(int size) {
		List<OadrUpdateReportType> updateReports = TestSession.getOadrUpdateReportTypeReceivedList();
		if (updateReports.size() != size){
			throw new FailedException("Expected " + size + " OadrUpdateReport(s), received " + updateReports.size());
		}
		boolean fnd = false;
		boolean fnd2 = false;
		for (OadrUpdateReportType reports:updateReports){
			List<JAXBElement<? extends StreamPayloadBaseType>> rptPayload = reports.getOadrReport().get(0).getIntervals()
					.getInterval().get(0).getStreamPayloadBase();
			for (int j=0; j<rptPayload.size();j++){
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) rptPayload.get(j).getValue();
				String rID = reportPayload.getRID();
				if(wellKnownCheck(rID, properties.get("TelemetryUsage_PowerReal_WellKnown_rID"))){
					String minMax[] = properties.get("TelemetryUsage_PowerReal_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
					Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
					if(value < min || value > max){
						throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the TelemetryUsage_PowerReal_Range of "
								+ min.toString() + " to " + max.toString());
					}
					fnd = true;
				}
				if(wellKnownCheck(rID, properties.get("TelemetryUsage_Voltage_WellKnown_rID"))){
					String minMax[] = properties.get("TelemetryUsage_Voltage_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
					Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
					if(value < min || value > max){
						throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the TelemetryUsage_Voltage_Range of "
								+ min.toString() + " to " + max.toString());
					}
					fnd2 = true;
				}
			}
		}
		if(!fnd || !fnd2){
			String ptext ="Failed to find an appropriately configured report rIDs for TelemetryUsage_PowerReal_WellKnown_rID of ";
			ptext += properties.get("TelemetryUsage_PowerReal_WellKnown_rID"); 
			ptext +=" and for TelemetryUsage_Voltage_WellKnown_rID of ";
			ptext += properties.get("TelemetryUsage_Voltage_WellKnown_rID"); 
			throw new FailedException(ptext);

		}

		return TestSession.getOadrUpdateReportTypeReceivedList().get(size - 1);
	}

	protected OadrUpdateReportType checkUpdateReport_THR_TYP(int size) {
		List<OadrUpdateReportType> updateReports = TestSession.getOadrUpdateReportTypeReceivedList();
		if (updateReports.size() != size){
			throw new FailedException("Expected " + size + " OadrUpdateReport(s), received " + updateReports.size());
		}
		for (OadrUpdateReportType reports:updateReports){
			List<JAXBElement<? extends StreamPayloadBaseType>> rptPayload = reports.getOadrReport().get(0).getIntervals()
					.getInterval().get(0).getStreamPayloadBase();
			for (int j=0; j<rptPayload.size();j++){
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) rptPayload.get(j).getValue();
				String rID = reportPayload.getRID();
				if(wellKnownCheck(rID, properties.get("ThermostatState_Status_WellKnown_rID"))){
					String minMax[] = properties.get("ThermostatState_Status_LevelOffset_Current_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
			
					if (((OadrPayloadResourceStatusType)reportPayload.getPayloadBase().getValue()).getOadrLoadControlState().getOadrLevelOffset() != null){
						Float value = ((OadrPayloadResourceStatusType)reportPayload.getPayloadBase().getValue()).getOadrLoadControlState().getOadrLevelOffset().getOadrCurrent();
						if(value < min || value > max){
							throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the ThermostatState_Status_LevelOffset_Current_Range of "
									+ min.toString() + " to " + max.toString());
						}
					}else {
						throw new FailedException("oadrLevelOffset is null. Only one Telemetry_Status report should be present and it should contain oadrLevelOffset");
					}
						
				}
				if(wellKnownCheck(rID, properties.get("ThermostatState_CurrentTemp_WellKnown_rID"))){
					String minMax[] = properties.get("ThermostatState_CurrentTemp_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
					Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
					if(value < min || value > max){
						throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the ThermostatState_CurrentTemp_Range of "
								+ min.toString() + " to " + max.toString());
					}
				}
				if(wellKnownCheck(rID, properties.get("ThermostatState_HeatTempSetting_WellKnown_rID"))){
					String minMax[] = properties.get("ThermostatState_HeatTempSetting_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
					Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
					if(value < min || value > max){
						throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the ThermostatState_HeatTempSetting_Range of "
								+ min.toString() + " to " + max.toString());
					}
				}
				if(wellKnownCheck(rID, properties.get("ThermostatState_CoolTempSetting_WellKnown_rID"))){
					String minMax[] = properties.get("ThermostatState_CoolTempSetting_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
					Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
					if(value < min || value > max){
						throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the ThermostatState_CoolTempSetting_Range of "
								+ min.toString() + " to " + max.toString());
					}
				}
				if(wellKnownCheck(rID, properties.get("ThermostatState_HVACModeSetting_WellKnown_rID"))){
					String minMax[] = properties.get("ThermostatState_HVACModeSetting_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
					Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
					if(value < min || value > max){
						throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the ThermostatState_HVACModeSetting_Range of "
								+ min.toString() + " to " + max.toString());
					}
				}
				if(wellKnownCheck(rID, properties.get("ThermostatState_CurrentHVACMode_WellKnown_rID"))){
					String minMax[] = properties.get("ThermostatState_HVACMode_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
					Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
					if(value < min || value > max){
						throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the ThermostatState_HVACMode_Range of "
								+ min.toString() + " to " + max.toString());
					}
				}
				if(wellKnownCheck(rID, properties.get("ThermostatState_FanModeSetting_WellKnown_rID"))){
					String minMax[] = properties.get("ThermostatState_FanModeSetting_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
					Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
					if(value < min || value > max){
						throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the ThermostatState_FanModeSetting_Range of "
								+ min.toString() + " to " + max.toString());
					}
				}
				if(wellKnownCheck(rID, properties.get("ThermostatState_CurrentHoldMode_WellKnown_rID"))){
					String minMax[] = properties.get("ThermostatState_CurrentHoldMode_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
					Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
					if(value < min || value > max){
						throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the ThermostatState_CurrentHoldMode_Range of "
								+ min.toString() + " to " + max.toString());
					}
				}
				if(wellKnownCheck(rID, properties.get("ThermostatState_CurrentAwayMode_WellKnown_rID"))){
					String minMax[] = properties.get("ThermostatState_CurrentAwayMode_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
					Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
					if(value < min || value > max){
						throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the ThermostatState_CurrentAwayMode_Range of "
								+ min.toString() + " to " + max.toString());
					}
				}
				if(wellKnownCheck(rID, properties.get("ThermostatState_CurrentHumidity_WellKnown_rID"))){
					String minMax[] = properties.get("ThermostatState_CurrentHumidity_Range").split(",");
					Float min = Float.parseFloat(minMax[0]);
					Float max = Float.parseFloat(minMax[1]);
					Float value = ((PayloadFloatType)reportPayload.getPayloadBase().getValue()).getValue();
					if(value < min || value > max){
						throw new FailedException("The report value of " + value.toString() + " does not appear to fall in the ThermostatState_CurrentHumidity_Range of "
								+ min.toString() + " to " + max.toString());
					}
				}
			}
		}

		return TestSession.getOadrUpdateReportTypeReceivedList().get(size - 1);
	}

	protected void checkUpdateReport(int minSize, int maxSize) {
		List<OadrUpdateReportType> updateReports = TestSession.getOadrUpdateReportTypeReceivedList();
		if (updateReports.size() < minSize || updateReports.size() > maxSize) {
			throw new FailedException("Expected " + minSize + " to " + maxSize + " OadrUpdateReport(s), received " + updateReports.size());
		}
	}

	protected OadrCanceledReportType checkCanceledReport(int size) {
		List<OadrCanceledReportType> canceledReports = TestSession.getOadrCanceledReportTypeReceivedList();
		if (canceledReports.size() != size){
			throw new FailedException("Expected " + size + " OadrCanceledReport(s), received " + canceledReports.size());
		}

		return TestSession.getOadrCanceledReportTypeReceivedList().get(size - 1);
	}

	protected OadrRegisterReportType waitForRegisterReport(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			if (TestSession.getOadrRegisterReportTypeReceivedList().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}
		pause(2);

		int listSize = TestSession.getOadrRegisterReportTypeReceivedList().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " RegisterReport(s), received " + listSize);
		}

		checkForValidationErrors();

		return TestSession.getOadrRegisterReportTypeReceivedList().get(size - 1);
	}

	protected void checkCancelPartyRegistrationRequest(int size) {
		List<OadrCancelPartyRegistrationType> cancelPartyRegistrations = TestSession.getCancelPartyRegistrationTypeListReceived();
		if (cancelPartyRegistrations.size() != size) {
			throw new FailedException("Expected " + size + " OadrCancelPartyRegistration(s), received " + cancelPartyRegistrations.size());
		}
	}

	protected void waitForReportBackDuration(OadrCreateReportType createReport) {
		String reportBackDuration = null;
		try {
			reportBackDuration = createReport.getOadrReportRequest().get(0).getReportSpecifier().getReportBackDuration().getDuration();
		} catch (Exception e) {
			throw new FailedException("Unable to get reportBackDuration");
		}

		Duration duration = OadrUtil.createDuration(reportBackDuration);
		XMLGregorianCalendar calendar = OadrUtil.getCurrentTime();
		calendar.add(duration);

		System.out.println("Pausing for " + reportBackDuration + "...");
		long expiration = OadrUtil.getTimeMilliseconds(calendar);
		while (System.currentTimeMillis() < expiration) {
			pause(1);
		}
	}

	protected OadrCanceledReportType sendCancelReport(OadrCreateReportType createReport) throws Exception {
		return sendCancelReport(createReport, ErrorConst.OK_200);
	}

	protected OadrCanceledReportType sendCancelReport(OadrCreateReportType createReport, String responseCode) throws Exception {
		OadrCancelReportType cancelReport = CancelReportEventHelper.loadOadrCancelReportType(createReport);
		return sendCancelReport(cancelReport, responseCode);
	}

	protected OadrCanceledReportType sendCancelReport(OadrCancelReportType cancelReport) throws Exception {
		return sendCancelReport(cancelReport, ErrorConst.OK_200);
	}

	protected OadrCanceledReportType sendCancelReport(OadrCancelReportType cancelReport, String responseCode) throws Exception {
		String text = SchemaHelper.getCancelReportTypeAsString(cancelReport);
		String responseText = post(text, "EiReport");
		if (!OadrUtil.isExpected(responseText, OadrCanceledReportType.class)) {
			throw new FailedException("Expected OadrCanceledReport has not been received");
		}

		OadrCanceledReportType response = CanceledReportEventHelper.loadCanceledReportTypeFromString(responseText);
		String eiResponseCode = response.getEiResponse().getResponseCode();
		if (!eiResponseCode.equals(responseCode)) {
			//This is a redundant check. Response code is validated in DisableCanceledReport_ResponseCodeSuccess_Check
			//throw new FailedException("Expected OadrResponse " + responseCode + ", received " + eiResponseCode);
		}

		return response;
	}

	protected void sendCreatedReport() throws Exception {
		sendCreatedReport(ErrorConst.OK_200);
	}

	protected void sendCreatedReport(String responseCode) throws Exception {
		OadrCreatedReportType createdReport = CreatedReportEventHelper.loadOadrCreatedReport();

		EiResponseType eiResponse = createdReport.getEiResponse();
		eiResponse.setResponseCode(responseCode);
		eiResponse.setResponseDescription(responseCode.equals(ErrorConst.OK_200) ? "OK" : "ERROR");

		String text = SchemaHelper.getCreatedReportTypeAsString(createdReport);
		String response = post(text, "EiReport");
		if (!OadrUtil.isExpected(response, OadrResponseType.class)) {
			throw new FailedException("Expected OadrResponse has not been received");
		}
	}

	protected void sendCreatedReport(OadrCreatedReportType createdReport) throws Exception {
		String text = SchemaHelper.getCreatedReportTypeAsString(createdReport);
		String response = post(text, "EiReport");
		if (!OadrUtil.isExpected(response, OadrResponseType.class)) {
			throw new FailedException("Expected OadrResponse has not been received");
		}
	}

	/*
	private void sendCreatedReport(OadrCreateReportType createReport) throws Exception {
		OadrCreatedReportType createdReport = CreatedReportEventHelper.loadOadrCreatedReport(createReport);

		EiResponseType eiResponse = createdReport.getEiResponse();
		eiResponse.setResponseCode(ErrorConst.OK_200);
		eiResponse.setResponseDescription("OK");

		String text = SchemaHelper.getCreatedReportTypeAsString(createdReport);
		String response = post(text, "EiReport");
		if (!OadrUtil.isExpected(response, OadrResponseType.class)) {
			throw new FailedException("Expected OadrResponse has not been received");
		}
	}
	 */

	protected void checkCreatedReportPendingReports(OadrCreatedReportType createdReport, int size) {
		int listSize = createdReport.getOadrPendingReports().getReportRequestID().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " OadrPendingReport(s). Got " + listSize);
		}
	}

	protected void checkCreatedReportPendingReports(OadrCreatedReportType createdReport, int minSize, int maxSize) {
		int listSize = createdReport.getOadrPendingReports().getReportRequestID().size();
		if (listSize < minSize || listSize > maxSize) {
			throw new FailedException("Expected " + minSize + " to " + maxSize + " OadrPendingReport(s). Got " + listSize);
		}
	}

	protected OadrCancelReportType waitForCancelReport(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			if (TestSession.getOadrCancelReportTypeReceivedList().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}
		pause(2);

		int listSize = TestSession.getOadrCancelReportTypeReceivedList().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " CancelReport(s), received " + listSize);
		}

		checkForValidationErrors();

		return TestSession.getOadrCancelReportTypeReceivedList().get(size - 1);
	}

	protected void checkReportRequest(OadrCreateReportType createReport, int size) {
		List<OadrReportRequestType> reportRequest = createReport.getOadrReportRequest();
		if (reportRequest.size() != size) {
			throw new FailedException("Expected " + size + " OadrRegisterReport OadrReportRequest(s), received " + reportRequest.size());
		}
	}

	protected void checkReportRequest(OadrRegisteredReportType registeredReport) {
		List<OadrReportRequestType> reportRequests = registeredReport.getOadrReportRequest();
		if (reportRequests == null || reportRequests.size() < 1) {
			throw new FailedException("Did not receive the expected OadrReportRequest with the OadrRegisteredReport received");
		}
	}

	protected void checkSecurity() {
		if (!properties.isSecurity_Enabled()) {
			throw new FailedException("Security is required for this test case.");
		}
	}
	protected boolean wellKnownCheck(String rID, String sCheck){

		if(properties.get("disable_DR_Guide_WellKnown_rID_Check").toLowerCase().contentEquals("true"))
			return true;
		
		if(rID.contentEquals(sCheck))
			return true;
		
		if(rID.indexOf(":") < 0)
			return false;
		
		String sToColon = rID.substring(0, rID.indexOf(":"));
		if(sToColon.contentEquals(sCheck))
			return true;
		
		return false;
	}

	protected OadrRegisteredReportType sendRegisterReport(OadrRegisterReportType registerReport) throws Exception {
		String text = SchemaHelper.getRegisterReportTypeAsString(registerReport);
		String response = post(text, "EiReport");
		if (!OadrUtil.isExpected(response, OadrRegisteredReportType.class)) {
			throw new FailedException("Expected OadrRegisteredReport has not been received");
		}

		return RegisteredReportEventHelper.createOadrRegisteredReportFromString(response);
	}

	abstract protected String post(String message, String type) throws Exception;
}
