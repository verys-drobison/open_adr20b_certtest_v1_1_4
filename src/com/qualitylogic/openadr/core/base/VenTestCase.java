package com.qualitylogic.openadr.core.base;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.IResponseCanceledReportTypeAckAction;
import com.qualitylogic.openadr.core.action.ResponseCanceledReportTypeAckActionList;
import com.qualitylogic.openadr.core.action.impl.DefaultOadrCreateOptEvent;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCanceledReportTypeAckAction;
import com.qualitylogic.openadr.core.bean.VTNServiceType;
import com.qualitylogic.openadr.core.channel.util.StringUtil;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.CurrencyType;
import com.qualitylogic.openadr.core.signal.EiEventSignalType;
import com.qualitylogic.openadr.core.signal.EiEventSignalsType;
import com.qualitylogic.openadr.core.signal.EventStatusEnumeratedType;
import com.qualitylogic.openadr.core.signal.ISO3AlphaCurrencyCodeContentType;
import com.qualitylogic.openadr.core.signal.OadrCancelOptType;
import com.qualitylogic.openadr.core.signal.OadrCanceledOptType;
import com.qualitylogic.openadr.core.signal.OadrCreateOptType;
import com.qualitylogic.openadr.core.signal.OadrCreatePartyRegistrationType;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.OadrCreatedEventType;
import com.qualitylogic.openadr.core.signal.OadrCreatedOptType;
import com.qualitylogic.openadr.core.signal.OadrCreatedPartyRegistrationType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.PayloadFloatType;
import com.qualitylogic.openadr.core.signal.PowerRealType;
import com.qualitylogic.openadr.core.signal.ResponseRequiredType;
import com.qualitylogic.openadr.core.signal.SignalPayloadType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType.OadrEvent;
import com.qualitylogic.openadr.core.signal.OadrQueryRegistrationType;
import com.qualitylogic.openadr.core.signal.OadrResponseType;
import com.qualitylogic.openadr.core.signal.OptTypeType;
import com.qualitylogic.openadr.core.signal.ReportSpecifierType;
import com.qualitylogic.openadr.core.signal.SpecifierPayloadType;
import com.qualitylogic.openadr.core.signal.helper.CancelOptEventHelper;
import com.qualitylogic.openadr.core.signal.helper.CanceledOptEventHelper;
import com.qualitylogic.openadr.core.signal.helper.CreatedEventHelper;
import com.qualitylogic.openadr.core.signal.helper.CreatedPartyRegistrationSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.QueryRegistrationSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.ResponseHelper;
import com.qualitylogic.openadr.core.signal.helper.SchemaHelper;
import com.qualitylogic.openadr.core.signal.xcal.AvailableType;
import com.qualitylogic.openadr.core.signal.xcal.Dtstart;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;
import com.qualitylogic.openadr.core.util.XMLDBUtil;
import com.qualitylogic.openadr.core.util.XMLDBUtil.DataPoint;
import com.qualitylogic.openadr.core.ven.VENService;
import com.qualitylogic.openadr.core.ven.VenToVtnClient;

public abstract class VenTestCase extends NewTestCase {

	private static boolean disable_DR_Guide_Overall_Duration_Check;

	static String  p_disable_DR_Guide_Overall_Duration_Check =   properties.get("disable_DR_Guide_Overall_Duration_Check");

	static{
		if(p_disable_DR_Guide_Overall_Duration_Check!=null && p_disable_DR_Guide_Overall_Duration_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_Overall_Duration_Check=true;
		}
	}

	private static boolean disable_DR_Guide_Random_Check;

	static String  p_disable_DR_Guide_Random_Check =   properties.get("disable_DR_Guide_Random_Check");

	static{
		if(p_disable_DR_Guide_Random_Check!=null && p_disable_DR_Guide_Random_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_Random_Check=true;
		}
	}

	private static boolean disable_DR_Guide_RampUp_Check;

	static String  p_disable_DR_Guide_RampUp_Check =   properties.get("disable_DR_Guide_RampUp_Check");

	static{
		if(p_disable_DR_Guide_RampUp_Check!=null && p_disable_DR_Guide_RampUp_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_RampUp_Check=true;
		}
	}

	private static boolean disable_DR_Guide_Recovery_Check;

	static String  p_disable_DR_Guide_Recovery_Check =   properties.get("disable_DR_Guide_Recovery_Check");

	static{
		if(p_disable_DR_Guide_Recovery_Check!=null && p_disable_DR_Guide_Recovery_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_Recovery_Check=true;
		}
	}

	private static boolean disable_DR_Guide_NumSignals_Check;

	static String  p_disable_DR_Guide_NumSignals_Check =   properties.get("disable_DR_Guide_NumSignals_Check");

	static{
		if(p_disable_DR_Guide_NumSignals_Check!=null && p_disable_DR_Guide_NumSignals_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_NumSignals_Check=true;
		}
	}

	private static boolean disable_DR_Guide_Target_Check;

	static String  p_disable_DR_Guide_Target_Check =   properties.get("disable_DR_Guide_Target_Check");

	static{
		if(p_disable_DR_Guide_Target_Check!=null && p_disable_DR_Guide_Target_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_Target_Check=true;
		}
	}

	private static boolean disable_DR_Guide_Priority_Check;

	static String  p_disable_DR_Guide_Priority_Check =   properties.get("disable_DR_Guide_Priority_Check");

	static{
		if(p_disable_DR_Guide_Priority_Check!=null && p_disable_DR_Guide_Priority_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_Priority_Check=true;
		}
	}

	private static boolean disable_DR_Guide_ResponseRequired_Check;

	static String  p_disable_DR_Guide_ResponseRequired_Check =   properties.get("disable_DR_Guide_ResponseRequired_Check");

	static{
		if(p_disable_DR_Guide_ResponseRequired_Check!=null && p_disable_DR_Guide_ResponseRequired_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_ResponseRequired_Check=true;
		}
	}

	private static boolean disable_DR_Guide_SignalName_Check;

	static String  p_disable_DR_Guide_SignalName_Check =   properties.get("disable_DR_Guide_SignalName_Check");

	static{
		if(p_disable_DR_Guide_SignalName_Check!=null && p_disable_DR_Guide_SignalName_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_SignalName_Check=true;
		}
	}

	private static boolean disable_DR_Guide_SignalType_Check;

	static String  p_disable_DR_Guide_SignalType_Check =   properties.get("disable_DR_Guide_SignalType_Check");

	static{
		if(p_disable_DR_Guide_SignalType_Check!=null && p_disable_DR_Guide_SignalType_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_SignalType_Check=true;
		}
	}

	private static boolean disable_DR_Guide_NumIntervals_Check;

	static String  p_disable_DR_Guide_NumIntervals_Check =   properties.get("disable_DR_Guide_NumIntervals_Check");

	static{
		if(p_disable_DR_Guide_NumIntervals_Check!=null && p_disable_DR_Guide_NumIntervals_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_NumIntervals_Check=true;
		}
	}

	private static boolean disable_DR_Guide_IntervalValue_Check;

	static String  p_disable_DR_Guide_IntervalValue_Check =   properties.get("disable_DR_Guide_IntervalValue_Check");

	static{
		if(p_disable_DR_Guide_IntervalValue_Check!=null && p_disable_DR_Guide_IntervalValue_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_IntervalValue_Check=true;
		}
	}

	private static boolean disable_DR_Guide_IntervalDuration_Check;

	static String  p_disable_DR_Guide_IntervalDuration_Check =   properties.get("disable_DR_Guide_IntervalDuration_Check");

	static{
		if(p_disable_DR_Guide_IntervalDuration_Check!=null && p_disable_DR_Guide_IntervalDuration_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_IntervalDuration_Check=true;
		}
	}

	private static boolean disable_DR_Guide_EventStatus_Check;

	static String  p_disable_DR_Guide_EventStatus_Check =   properties.get("disable_DR_Guide_EventStatus_Check");

	static{
		if(p_disable_DR_Guide_EventStatus_Check!=null && p_disable_DR_Guide_EventStatus_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_EventStatus_Check=true;
		}
	}

	protected void listenForRequests() throws Exception {
		VENService.startVENService();
	}

	@Override
	public void cleanUp() throws Exception {
		VENService.stopVENService();
	}

	protected String post(String message, String type) throws Exception {
		VTNServiceType vtnService = null;
		switch (type) {
		case "EiRegistration" : vtnService = VTNServiceType.EiRegistration; break;
		case "EiReport" : vtnService = VTNServiceType.EiReport; break;
		default : throw new IllegalArgumentException("Unknown type" + type);
		}

		return VenToVtnClient.post(message, vtnService);
	}

	protected OadrCreatedPartyRegistrationType sendCreatePartyRegistration(OadrCreatePartyRegistrationType createPartyRegistration) throws Exception {
		String text = SchemaHelper.getCreatePartyRegistrationAsString(createPartyRegistration);
		String response = VenToVtnClient.post(text, VTNServiceType.EiRegistration);
		if (!OadrUtil.isExpected(response, OadrCreatedPartyRegistrationType.class)) {
			throw new FailedException("Expected OadrCreatedPartyRegistration has not been received");
		}

		return CreatedPartyRegistrationSignalHelper.createCreatedPartyRegistrationTypeFromString(response);
	}

	protected OadrCreatedPartyRegistrationType sendQueryRegistration() throws Exception {
		OadrQueryRegistrationType queryRegistration = QueryRegistrationSignalHelper.loadOadrQueryRegistrationRequest();
		String text = SchemaHelper.getOadrQueryRegistrationTypeAsString(queryRegistration);
		String response = VenToVtnClient.post(text, VTNServiceType.EiRegistration);
		if (!OadrUtil.isExpected(response, OadrCreatedPartyRegistrationType.class)) {
			throw new FailedException("Expected OadrCreatedPartyRegistration has not been received");
		}

		OadrCreatedPartyRegistrationType createdPartyRegistration = CreatedPartyRegistrationSignalHelper.createCreatedPartyRegistrationTypeFromString(response);
		return createdPartyRegistration;
	}

	protected void sendCreateOpt(OadrCreateOptType createOpt) throws Exception {
		String text = SchemaHelper.getOadrCreateOptAsString(createOpt);
		String response = VenToVtnClient.post(text, VTNServiceType.EiOpt);
		if (!OadrUtil.isExpected(response, OadrCreatedOptType.class)) {
			throw new FailedException("Expected OadrCreatedOpt has not been received");
		}
	}

	protected void sendCancelOpt(String optID) throws Exception {
		sendCancelOpt(optID, "200");
	}

	protected void sendCancelOpt(String optID, String responseCode) throws Exception {
		OadrCancelOptType cancelOpt = CancelOptEventHelper.createOadrCancelOptEvent(optID);
		String text = SchemaHelper.getOadrCancelOptTypeAsString(cancelOpt);
		String response = VenToVtnClient.post(text, VTNServiceType.EiOpt);
		if (!OadrUtil.isExpected(response, OadrCanceledOptType.class)) {
			throw new FailedException("Expected OadrCanceledOpt has not been received");
		}

		OadrCanceledOptType canceledOpt = CanceledOptEventHelper.createCanceledOptTypeEventFromString(response);
		String receivedResponseCode = canceledOpt.getEiResponse().getResponseCode();
		if (!receivedResponseCode.equals(responseCode)) {
			throw new FailedException("Expected OadrCanceledOpt responseCode of " + responseCode + " has not been received. Got " + receivedResponseCode + ".");
		}
	}

	protected OadrCreateOptType getCreateOptSchedule_001() {
		List<AvailableType> availables = new ArrayList<AvailableType>();
		availables.add(getAvailable(1, 4));
		availables.add(getAvailable(3, 8));

		return getCreateOpt(OptTypeType.OPT_IN, availables);
	}

	protected OadrCreateOptType getCreateOptSchedule_002() {
		List<AvailableType> availables = new ArrayList<AvailableType>();
		availables.add(getAvailable(2, 4));
		availables.add(getAvailable(4, 8));

		return getCreateOpt(OptTypeType.OPT_OUT, availables);
	}

	protected OadrCreateOptType getCreateOptSchedule_003() {
		List<AvailableType> availables = new ArrayList<AvailableType>();
		availables.add(getAvailable(2, 6));
		availables.add(getAvailable(4, 8));

		return getCreateOpt(OptTypeType.OPT_IN, availables);
	}

	protected OadrCreateOptType getCreateOptSchedule_004() {
		List<AvailableType> availables = new ArrayList<AvailableType>();
		availables.add(getAvailable(1, 0));

		return getCreateOpt(OptTypeType.OPT_IN, availables);
	}

	private static OadrCreateOptType getCreateOpt(OptTypeType optInOut, List<AvailableType> availables) {
		String reason = "participating";
		String marketContext = properties.get("DR_MarketContext_1_Name");
		DefaultOadrCreateOptEvent creatOptEvent = new DefaultOadrCreateOptEvent();
		OadrCreateOptType createOpt = creatOptEvent.getOadrCreateOptEvent(
				optInOut, reason, marketContext, Collections.<String>emptyList(), Collections.<String>emptyList(), availables);
		return createOpt;
	}

	protected OadrCreateOptType getCreateOptOptOut(OadrEvent oadrEvent) {
		OptTypeType optInOut = OptTypeType.OPT_OUT;
		String reason = "participating";
		String marketContext = properties.get("DR_MarketContext_1_Name");
		String eventID = oadrEvent.getEiEvent().getEventDescriptor().getEventID();
		long modificationNumber = oadrEvent.getEiEvent().getEventDescriptor().getModificationNumber();

		List<AvailableType> availables = new ArrayList<AvailableType>();
		availables.add(getAvailable("PT5M"));

		List<String> resourceIDs = new ArrayList<>();
		resourceIDs.add(oadrEvent.getEiEvent().getEiTarget().getResourceID().get(0));

		DefaultOadrCreateOptEvent createOptEvent = new DefaultOadrCreateOptEvent();
		OadrCreateOptType createOpt = createOptEvent.getOadrCreateOptEvent(
				optInOut, reason, marketContext, eventID, modificationNumber, resourceIDs , Collections.<String>emptyList(), availables);
		return createOpt;
	}

	protected OadrCreateOptType getCreateOptOptOut_ResId(OadrEvent oadrEvent, Integer iRes) {
		OptTypeType optInOut = OptTypeType.OPT_OUT;
		String reason = "participating";
		String marketContext = properties.get("DR_MarketContext_1_Name");
		String eventID = oadrEvent.getEiEvent().getEventDescriptor().getEventID();

		long modificationNumber = oadrEvent.getEiEvent().getEventDescriptor().getModificationNumber();

		List<AvailableType> availables = new ArrayList<AvailableType>();
		availables.add(getAvailable("PT5M"));

		List<String> resourceIDs = new ArrayList<>();
		resourceIDs.add(oadrEvent.getEiEvent().getEiTarget().getResourceID().get(iRes));

		DefaultOadrCreateOptEvent createOptEvent = new DefaultOadrCreateOptEvent();
		OadrCreateOptType createOpt = createOptEvent.getOadrCreateOptEvent(
				optInOut, reason, marketContext, eventID, modificationNumber, resourceIDs , Collections.<String>emptyList(), availables);
		return createOpt;
	}

	protected OadrCreateOptType getCreateOptOptIn(OadrEvent oadrEvent, String resourceID) {
		OptTypeType optInOut = OptTypeType.OPT_IN;
		String reason = "participating";
		String marketContext = properties.get("DR_MarketContext_1_Name");
		String eventID = oadrEvent.getEiEvent().getEventDescriptor().getEventID();
		long modificationNumber = oadrEvent.getEiEvent().getEventDescriptor().getModificationNumber();

		List<AvailableType> availables = new ArrayList<AvailableType>();
		availables.add(getAvailable("PT5M"));

		DefaultOadrCreateOptEvent createOptEvent = new DefaultOadrCreateOptEvent();
		List<String> resourceIDs = new ArrayList<>(); 
		resourceIDs.add(oadrEvent.getEiEvent().getEiTarget().getResourceID().get(1));

		OadrCreateOptType createOpt = createOptEvent.getOadrCreateOptEvent(
				optInOut, reason, marketContext, eventID, modificationNumber, resourceIDs , Collections.<String>emptyList(), availables);
		return createOpt;
	}

	protected OadrCreateOptType getCreateOptOptIn_ResId(OadrEvent oadrEvent, Integer iRes) {
		OptTypeType optInOut = OptTypeType.OPT_IN;
		String reason = "participating";
		String marketContext = properties.get("DR_MarketContext_1_Name");
		String eventID = oadrEvent.getEiEvent().getEventDescriptor().getEventID();
		long modificationNumber = oadrEvent.getEiEvent().getEventDescriptor().getModificationNumber();

		List<AvailableType> availables = new ArrayList<AvailableType>();
		availables.add(getAvailable("PT5M"));

		DefaultOadrCreateOptEvent createOptEvent = new DefaultOadrCreateOptEvent();
		List<String> resourceIDs = new ArrayList<>(); 
		resourceIDs.add(oadrEvent.getEiEvent().getEiTarget().getResourceID().get(iRes));

		OadrCreateOptType createOpt = createOptEvent.getOadrCreateOptEvent(
				optInOut, reason, marketContext, eventID, modificationNumber, resourceIDs , Collections.<String>emptyList(), availables);
		return createOpt;
	}

	protected void sendCreatedEvent(ICreatedEventAction createdEvent) throws Exception {
		String response = VenToVtnClient.post(createdEvent.getCreateEvent());
		if (!OadrUtil.isExpected(response, OadrResponseType.class)) {
			throw new FailedException("Expected OadrResponse has not been received");
		}
	}

	protected void checkRegistrationIdAndVenId(OadrCreatedPartyRegistrationType createdPartyRegistration) {
		if (StringUtil.isBlank(createdPartyRegistration.getRegistrationID())) {
			throw new FailedException("venID not in payload. VEN should be in registered state.");
		}
		if (StringUtil.isBlank(createdPartyRegistration.getVenID())) {
			throw new FailedException("registrationID not in payload. VEN should be in registered state.");
		}
	}

	protected void checkRegistrationIdAndVenId(OadrCreatePartyRegistrationType createPartyRegistration, OadrCreatedPartyRegistrationType createdPartyRegistration) {
		if (!createPartyRegistration.getRegistrationID().equals(createdPartyRegistration.getRegistrationID())) {
			throw new FailedException("registrationID mismatch");
		}
		if (!createPartyRegistration.getVenID().equals(createdPartyRegistration.getVenID())) {
			throw new FailedException("venID mismatch");
		}
	}

	protected void checkDuration(OadrCreateReportType createReport) {
		String durationText = createReport.getOadrReportRequest().get(0).getReportSpecifier().getReportInterval().getProperties().getDuration().getDuration();
		Duration duration = OadrUtil.createDuration(durationText);
		long timeInMillis = duration.getTimeInMillis(new Date());
		// System.out.println("millis=" + timeInMillis);
		if (!(timeInMillis == 0 || timeInMillis >= _60_MINUTES)) {
			throw new FailedException("Duration of CreateReport should either be zero or a value greater than or equal to 60 minutes.");
		}
	}

	private static final int _60_MINUTES = 60 * 60 * 1000;


	protected void checkCreateReport_x130(OadrCreateReportType createReport) {
		Node reportNode = new XMLDBUtil().getReportFromVenByName("HISTORY_USAGE");
		ReportSpecifierType reportSpecifier = createReport.getOadrReportRequest().get(0).getReportSpecifier();

		checkReportSpecifier(reportNode, reportSpecifier);

		DataPoint energyRealDataPoint = getDataPoint(reportNode, "EnergyReal");
		DataPoint powerRealDataPoint = getDataPoint(reportNode, "PowerReal");
		DataPoint pulseCountDataPoint = getDataPoint(reportNode, "PulseCount");

		List<SpecifierPayloadType> specifierPayloads = reportSpecifier.getSpecifierPayload();

		boolean isExpectedDatapointsFound = false;
		for (SpecifierPayloadType specifierPayload : specifierPayloads) {
			String rID = specifierPayload.getRID();
			if ((energyRealDataPoint!=null&&rID.equals(energyRealDataPoint.getRID())) || (powerRealDataPoint!=null&&rID.equals(powerRealDataPoint.getRID()))|| (pulseCountDataPoint!=null&&rID.equals(pulseCountDataPoint.getRID()))) {
				isExpectedDatapointsFound = true;
				break;
			}
		}

		if(!isExpectedDatapointsFound){
			throw new FailedException("The rIDs should be for at least one of EnergyReal or PowerReal or PulseCount. There can be other datapoints");
		}

		XMLGregorianCalendar dtstart = reportSpecifier.getReportInterval().getProperties().getDtstart().getDateTime();
		if (!isCalendarWithinMinutes(dtstart, 30, 4)) {
			throw new FailedException("CreateReport dtstart should be approximatly 30 minutes in the past.");
		}

		checkDuration(createReport);
	}

	protected void checkCreateReport_x140(OadrCreateReportType createReport) {
		Node reportNode = new XMLDBUtil().getReportFromVenByName("HISTORY_USAGE");
		ReportSpecifierType reportSpecifier = createReport.getOadrReportRequest().get(0).getReportSpecifier();

		checkReportSpecifier(reportNode, reportSpecifier);

		DataPoint energyRealDataPoint = getDataPoint(reportNode, "EnergyReal");
		if (energyRealDataPoint == null) {
			throw new FailedException("Expected EnergyReal rID in CreateReport.");
		}

		List<SpecifierPayloadType> specifierPayloads = reportSpecifier.getSpecifierPayload();
		if (specifierPayloads.size() != 1) {
			throw new FailedException("Expected 1 SpecifierPayload in CreateReport.");
		}

		if (!energyRealDataPoint.getRID().contains(specifierPayloads.get(0).getRID())) {
			throw new FailedException("CreateReport rID is not found in XML database.");
		}

		XMLGregorianCalendar dtstart = reportSpecifier.getReportInterval().getProperties().getDtstart().getDateTime();
		if (!isCalendarWithinMinutes(dtstart, 30, 2)) {
			throw new FailedException("CreateReport dtstart is not within plus or minus 2 minutes of current time minus 30.");
		}
	}

	protected void checkCreateReport_x150(OadrCreateReportType createReport) {
		Node reportNode = new XMLDBUtil().getReportFromVenByName("TELEMETRY_USAGE");
		ReportSpecifierType reportSpecifier = createReport.getOadrReportRequest().get(0).getReportSpecifier();

		checkReportSpecifier(reportNode, reportSpecifier);

		DataPoint energyRealDataPoint = getDataPoint(reportNode, "EnergyReal");
		DataPoint powerRealDataPoint = getDataPoint(reportNode, "PowerReal");
		//DataPoint pulseCountDataPoint = getDataPoint(reportNode, "PulseCount");

		if (energyRealDataPoint == null) {
			throw new FailedException("Expected EnergyReal rID in CreateReport.");
		}
		if (powerRealDataPoint == null) {
			throw new FailedException("Expected PowerReal rID in CreateReport.");
		}

		List<SpecifierPayloadType> specifierPayloads = reportSpecifier.getSpecifierPayload();
		if (specifierPayloads.size() != 2) {
			throw new FailedException("Expected 2 SpecifierPayloads in CreateReport.");
		}

		for (SpecifierPayloadType specifierPayload : specifierPayloads) {
			String rID = specifierPayload.getRID();
			if (!rID.equals(energyRealDataPoint.getRID()) && !rID.equals(powerRealDataPoint.getRID())) {
				throw new FailedException("CreateReport rIDs are not found in XML database.");
			}
		}

		String durationText = reportSpecifier.getReportBackDuration().getDuration();
		if (!durationEquals(durationText, "PT0M")) {
			throw new FailedException("Expected zero reportBackDuration, got " + durationText);
		}
	}

	protected void checkCreateReport_x160(OadrCreateReportType createReport) {
		Node reportNode = new XMLDBUtil().getReportFromVenByName("TELEMETRY_USAGE");
		ReportSpecifierType reportSpecifier = createReport.getOadrReportRequest().get(0).getReportSpecifier();

		checkReportSpecifier(reportNode, reportSpecifier);

		DataPoint powerRealDataPoint = getDataPoint(reportNode, "PowerReal");
		if (powerRealDataPoint == null) {
			throw new FailedException("Expected PowerReal rID in CreateReport.");
		}

		List<SpecifierPayloadType> specifierPayloads = reportSpecifier.getSpecifierPayload();
		if (specifierPayloads.size() != 1) {
			throw new FailedException("Expected 1 SpecifierPayloads in CreateReport.");
		}

		String rID = specifierPayloads.get(0).getRID();
		if (!rID.equals(powerRealDataPoint.getRID())) {
			throw new FailedException("CreateReport rIDs are not found in XML database.");
		}

		String reportBackDuration = reportSpecifier.getReportBackDuration().getDuration();
		if (!durationEquals(reportBackDuration, "PT2M")) { 
			throw new FailedException("Expected 2 minutes reportBackDuration.");
		}

		String granularityText = reportSpecifier.getGranularity().getDuration();
		if (!durationEquals(granularityText, "PT1M")) {
			throw new FailedException("Expected 1 minute granularity.");
		}
	}

	protected void checkReportSpecifier(Node reportNode, ReportSpecifierType reportSpecifier) {
		String reportSpecifierID = reportSpecifier.getReportSpecifierID();
		String xmlReportSpecifierID = reportNode.getAttributes().getNamedItem("reportSpecifierID").getNodeValue();
		if (!reportSpecifierID.equals(xmlReportSpecifierID)) {
			throw new FailedException("ReportSpecifierID mismatch, " + reportSpecifierID + " != " + reportSpecifierID);
		}
	}

	protected IResponseCanceledReportTypeAckAction addCanceledReportResponse() {
		IResponseCanceledReportTypeAckAction canceledReportAction = new DefaultResponseCanceledReportTypeAckAction();
		ResponseCanceledReportTypeAckActionList.addResponseCanceledReportAckAction(canceledReportAction);

		return canceledReportAction;
	}

	protected IResponseCanceledReportTypeAckAction addCanceledReportResponse(String responseCode, String responseDescription) {
		IResponseCanceledReportTypeAckAction canceledReportAction = new DefaultResponseCanceledReportTypeAckAction(responseCode, responseDescription);
		ResponseCanceledReportTypeAckActionList.addResponseCanceledReportAckAction(canceledReportAction);

		return canceledReportAction;
	}

	protected void checkCreateReport_x170(OadrCreateReportType createReport) {
		ReportSpecifierType reportSpecifier = createReport.getOadrReportRequest().get(0).getReportSpecifier();
		if (!reportSpecifier.getReportSpecifierID().equals("METADATA")) {
			throw new FailedException("Expected METADATA reportSpecifier. Got " + reportSpecifier.getReportSpecifierID());
		}

		String reportBackDurationText = reportSpecifier.getReportBackDuration().getDuration();
		if (!durationEquals(reportBackDurationText, "PT1M")) {
			throw new FailedException("Expected PT1M reportBackDuration. Got " + reportBackDurationText);
		}

		String granularityText = reportSpecifier.getGranularity().getDuration();
		if (!durationEquals(granularityText, "PT0M")) {
			throw new FailedException("Expected PT0M reportSpecifier granularity. Got " + granularityText);
		}
	}

	protected void checkCreateReport_CBP(OadrCreateReportType createReport) {
		ReportSpecifierType reportSpecifier = createReport.getOadrReportRequest().get(0).getReportSpecifier();

		String reportBackDurationText = reportSpecifier.getReportBackDuration().getDuration();
		if (!durationEquals(reportBackDurationText, "PT1M")) {
			throw new FailedException("Expected PT1M reportBackDuration. Got " + reportBackDurationText);
		}

		String granularityText = reportSpecifier.getGranularity().getDuration();
		if (!durationEquals(granularityText, "PT1M")) {
			throw new FailedException("Expected PT1M reportSpecifier granularity. Got " + granularityText);
		}
		
		XMLGregorianCalendar dtstart = reportSpecifier.getReportInterval().getProperties().getDtstart().getDateTime();
		if (!isCalendarWithinMinutes(dtstart, 0, 2)) {
			throw new FailedException("CreateReport dtstart is not within plus or minus 2 minutes of current time.");
		}
	}

	protected void checkCreateReport_FDR_CMP(OadrCreateReportType createReport) {
		ReportSpecifierType reportSpecifier = createReport.getOadrReportRequest().get(0).getReportSpecifier();

		String reportBackDurationText = reportSpecifier.getReportBackDuration().getDuration();
		if (!durationEquals(reportBackDurationText, "PT5S")) {
			throw new FailedException("Expected PT1M reportBackDuration. Got " + reportBackDurationText);
		}

		String granularityText = reportSpecifier.getGranularity().getDuration();
		if (!durationEquals(granularityText, "PT5S")) {
			throw new FailedException("Expected PT1M reportSpecifier granularity. Got " + granularityText);
		}
		
		XMLGregorianCalendar dtstart = reportSpecifier.getReportInterval().getProperties().getDtstart().getDateTime();
		if (!isCalendarWithinMinutes(dtstart, 0, 2)) {
			throw new FailedException("CreateReport dtstart is not within plus or minus 2 minutes of current time.");
		}
	}

	protected void waitForDtstart(OadrCreateReportType createReport) {
		Dtstart dtstart = null;
		try {
			dtstart = createReport.getOadrReportRequest().get(0).getReportSpecifier().getReportInterval().getProperties().getDtstart();
		} catch (Exception e) {
			throw new FailedException("Unable to get dtstart");
		}

		System.out.println("Pausing until " + dtstart.getDateTime() + "...");
		long expiration = dtstart.getDateTime().toGregorianCalendar().getTimeInMillis();
		while (System.currentTimeMillis() < expiration) {
			pause(1);
		}
	}

	protected void checkResponse(int size, String responseCode) {
		OadrResponseType response = checkResponse(size);
		String receivedResponseCode = response.getEiResponse().getResponseCode();
		if (!receivedResponseCode.equals(responseCode)) {
			throw new FailedException("Expected OadrResponse responseCode of " + responseCode + " has not been received. Got " + receivedResponseCode + ".");
		}
	}

	protected void checkResponse(OadrResponseType response, String responseCode) {
		String receivedResponseCode = response.getEiResponse().getResponseCode();
		if (!receivedResponseCode.equals(responseCode)) {
			throw new FailedException("Expected OadrResponse responseCode of " + responseCode + " has not been received.");
		}
	}

	protected OadrResponseType checkResponse(int size) {
		List<OadrResponseType> responses = TestSession.getOadrResponse();
		if (responses.size() != size) {
			throw new FailedException("Expected " + size + " OadrResponse(s), received " + responses.size());
		}

		return responses.get(size - 1);
	}

	protected void checkIDs_Nx015(OadrCreatedPartyRegistrationType createdPartyRegistration) {
		String receivedRegistrationID = createdPartyRegistration.getRegistrationID();
		String receivedVenID = createdPartyRegistration.getVenID();

		XMLDBUtil xmldbUtil = new XMLDBUtil();

		if (StringUtils.isNotBlank(receivedRegistrationID)) {
			String registrationID = xmldbUtil.getRegistrationID();
			if (!receivedRegistrationID.equals(registrationID)) {
				throw new FailedException("RegistrationID mismatch. " + registrationID + "!=" + receivedRegistrationID);
			}
		}

		if (StringUtils.isNotBlank(receivedVenID)) {
			String venID = properties.getVenID();
			if (!receivedVenID.equals(venID)) {
				throw new FailedException("VenID mismatch. " + venID + "!=" + receivedVenID);
			}
		}
	}

	protected void checkSignalNames(OadrDistributeEventType distributeEvent, String... signalNames) {
		int signalSize = distributeEvent.getOadrEvent().size();
		if (signalNames.length != signalSize) {
			throw new FailedException("Expected " + signalNames.length + " EiEventSignal(s). Got " + signalSize);
		}

		for (int i = 0; i < signalNames.length; i++) {
			String signalName = signalNames[i];
			String receivedSignalName = getSignalName(distributeEvent, i);

			if(signalName.equalsIgnoreCase("SIMPLE")){
				if (!signalName.equalsIgnoreCase(receivedSignalName)) {
					throw new FailedException("Did not receive expected signal name " + signalName + ". Got " + receivedSignalName);
				}
			}else if (!signalName.equals(receivedSignalName)) {
				throw new FailedException("Did not receive expected signal name " + signalName + ". Got " + receivedSignalName);
			}
		}
	}

	protected void checkSignalTypes(OadrDistributeEventType distributeEvent, String... signalTypes) {
		int signalSize = distributeEvent.getOadrEvent().size();
		if (signalTypes.length != signalSize) {
			throw new FailedException("Expected " + signalTypes.length + " EiEventSignal(s). Got " + signalSize);
		}

		for (int i = 0; i < signalTypes.length; i++) {
			String signalType = signalTypes[i];
			String receivedSignalType = getSignalType(distributeEvent, i);

			if(signalType.equalsIgnoreCase("level")){
				if (!signalType.equalsIgnoreCase(receivedSignalType)) {
					throw new FailedException("Did not receive expected signal name " + signalType + ". Got " + receivedSignalType);
				}
			}else if (!signalType.equals(receivedSignalType)) {
				throw new FailedException("Did not receive expected signal name " + signalType + ". Got " + receivedSignalType);
			}
		}
	}

	private String getSignalName(OadrDistributeEventType distributeEvent, int index) {
		String receivedSignalName = null;
		try {
			OadrEvent event = distributeEvent.getOadrEvent().get(index);
			receivedSignalName = event.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getSignalName();
		} catch (Exception e) {
			e.printStackTrace();
			// ignore
		}
		return receivedSignalName;
	}

	private String getSignalType(OadrDistributeEventType distributeEvent, int index) {
		String receivedSignalType = null;
		try {
			OadrEvent event = distributeEvent.getOadrEvent().get(index);
			receivedSignalType = event.getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getSignalType().toString();
		} catch (Exception e) {
			e.printStackTrace();
			// ignore
		}
		return receivedSignalType;
	}

	private String getSignalNameI(OadrDistributeEventType distributeEvent, int index, int isng) {
		String receivedSignalName = null;
		try {
			OadrEvent event = distributeEvent.getOadrEvent().get(index);
			receivedSignalName = event.getEiEvent().getEiEventSignals().getEiEventSignal().get(isng).getSignalName();
		} catch (Exception e) {
			e.printStackTrace();
			// ignore
		}
		return receivedSignalName;
	}

	private String getSignalTypeI(OadrDistributeEventType distributeEvent, int index, int isng) {
		String receivedSignalType = null;
		try {
			OadrEvent event = distributeEvent.getOadrEvent().get(index);
			receivedSignalType = event.getEiEvent().getEiEventSignals().getEiEventSignal().get(isng).getSignalType().value();
		} catch (Exception e) {
			e.printStackTrace();
			// ignore
		}
		return receivedSignalType;
	}

	protected OadrResponseType sendCreatedEvent(String distributeEvent, OptTypeType optType) throws Exception {
		List<String> distributeEvents = new ArrayList<String>();
		distributeEvents.add(distributeEvent);

		String text = CreatedEventHelper.createCreatedEvent(distributeEvents, false, (optType == OptTypeType.OPT_IN), null);
		String responseText = VenToVtnClient.post(text);
		if (!OadrUtil.isExpected(responseText, OadrResponseType.class)) {
			throw new FailedException("Expected OadrResponse has not been received");
		}

		return ResponseHelper.createOadrResponseFromString(responseText);
	}

	protected OadrResponseType sendCreatedEventWithResponseCode(String distributeEvent, String eiResponseCode, String eventResponseCode) throws Exception {
		List<String> distributeEvents = new ArrayList<String>();
		distributeEvents.add(distributeEvent);

		String text = CreatedEventHelper.createCreatedEventError(distributeEvents, eiResponseCode, eventResponseCode, Collections.<String>emptyList(), false, true);
		String response = VenToVtnClient.post(text);
		if (!OadrUtil.isExpected(response, OadrResponseType.class)) {
			throw new FailedException("Expected OadrResponse has not been received");
		}

		return ResponseHelper.createOadrResponseFromString(response);
	}

	protected void checkDistributeEvent_Ex040(OadrDistributeEventType distributeEvent) {
		checkSignalNames(distributeEvent, "SIMPLE", "ELECTRICITY_PRICE");

		List<OadrEvent> events = distributeEvent.getOadrEvent();
		if (events == null || events.size() != 2) {
			int eventSize = (events == null) ? 0 : events.size();
			throw new FailedException("Expected 2 events in the OadrDistributeEvent payload. Received " + eventSize + " Event(s)");
		}
	}

	protected void checkDistributeEvent_Ex040_60(OadrDistributeEventType distributeEvent) {
		checkSignalNames(distributeEvent, "SIMPLE", "ELECTRICITY_PRICE");

		List<OadrEvent> events = distributeEvent.getOadrEvent();
		if (events == null || events.size() != 2) {
			int eventSize = (events == null) ? 0 : events.size();
			throw new FailedException("Expected 2 events in the OadrDistributeEvent payload. Received " + eventSize + " Event(s)");
		}
		XMLGregorianCalendar tm1 = events.get(0).getEiEvent().getEiActivePeriod().getProperties().getDtstart().getDateTime();
		String tm1org = tm1.toString();
		XMLGregorianCalendar tm2 = events.get(1).getEiEvent().getEiActivePeriod().getProperties().getDtstart().getDateTime();
		try {
			Duration duration=DatatypeFactory.newInstance().newDuration("PT5M");
			tm1.add(duration);
			if(!tm1.toString().contentEquals(tm2.toString())){
				throw new FailedException("Expected 2 events 5 minutes appart in the OadrDistributeEvent payload. Start time (dtstart) of first event is " 
						+ tm1org + "  and the second time is " + tm2.toString());
			}
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
	}

	protected void sendCreatedEvent_Ex040(String distributeEvent, int eventResponseIndexToRemove) throws Exception {
		List<String> distributeEvents = new ArrayList<String>();
		distributeEvents.add(distributeEvent);

		String text = CreatedEventHelper.createCreatedEvent(distributeEvents, false, true, null);

		OadrCreatedEventType createdEvent = CreatedEventHelper.createCreatedEventFromString(text);
		createdEvent.getEiCreatedEvent().getEventResponses().getEventResponse().remove(eventResponseIndexToRemove);

		text = SchemaHelper.getCreatedEventAsString(createdEvent);

		String response = VenToVtnClient.post(text);
		if (!OadrUtil.isExpected(response, OadrResponseType.class)) {
			throw new FailedException("Expected OadrResponse has not been received");
		}
	}

	protected void checkDistributeEvent_Ex055(OadrDistributeEventType distributeEvent) {
		List<OadrEvent> oadrEventList = distributeEvent.getOadrEvent();

		for(OadrEvent eachOadrEvent:oadrEventList){
			EiEventSignalsType eiEventSignalsType = eachOadrEvent.getEiEvent().getEiEventSignals();
			List<EiEventSignalType> eiEventSignalTypeList = eiEventSignalsType.getEiEventSignal();


			if(eiEventSignalTypeList.size()<2){
				throw new FailedException("Expected atleast two EventSignals.");
			}

			boolean isSimpleSignalFound = false;
			boolean isElectricitySignalFound = false;

			for(EiEventSignalType eachEiEventSignalType:eiEventSignalTypeList){
				String signalName = eachEiEventSignalType.getSignalName();

				if(signalName.equalsIgnoreCase("SIMPLE")){
					isSimpleSignalFound = true;	
				}else if(signalName.equalsIgnoreCase("ELECTRICITY_PRICE")){
					isElectricitySignalFound = true;
				}
			}

			if(!isSimpleSignalFound || !isElectricitySignalFound){

				if(!isSimpleSignalFound){
					throw new FailedException("Expected SIMPLE EventSignal was not present in the Event.");					
				}

				if(!isElectricitySignalFound){
					throw new FailedException("Expected ELECTRICITY_PRICE EventSignal was not present in the Event.");
				}
			}
		}
	}

	protected void checkDistributeEvent_CBP(OadrDistributeEventType distributeEvent, String Simple_type) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();

		topLevelChecks(distributeEvent, 0, "PT2M", "none", "none", "none", 1, "ALWAYS", 1, ("venid:" + vid));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, Simple_type, 1, "SIMPLE", "level", "PT2M");

	}

	protected void checkDistributeEvent_THR(OadrDistributeEventType distributeEvent, String Simple_type) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		String propFileResourceID[]= properties.getTwoResourceID();

		topLevelChecks(distributeEvent, 0, "PT2M", "PT1M", "none", "none", 1, "ALWAYS", 1, ("resource:" + propFileResourceID[0]));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, Simple_type, 1, "SIMPLE", "level", "PT2M");

	}

	protected void checkDistributeEvent_THR_CMP(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		String propFileResourceID[]= properties.getTwoResourceID();

		topLevelChecks(distributeEvent, 0, "PT4M", "PT1M", "none", "none", 1, "ALWAYS", 1,
				("resource:" + propFileResourceID[0]), ("resource:" + propFileResourceID[1]));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
//		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Simple_High", 2, "SIMPLE", "level", "PT1M");
//		signalLevelChecks(distributeEvent, 0, 0, 1, 0, "Simple_Medium", 2, "SIMPLE", "level", "PT3M");
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Load_Control_Capacity_High", 2, "LOAD_CONTROL", "x-loadControlCapacity", "PT1M");
		signalLevelChecks(distributeEvent, 0, 0, 1, 0, "Load_Control_Capacity_Medium", 2, "LOAD_CONTROL", "x-loadControlCapacity", "PT3M");

	}

	protected void checkDistributeEvent_THR_TYP(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		String propFileResourceID[]= properties.getTwoResourceID();

		topLevelChecks(distributeEvent, 0, "PT2M", "PT1M", "none", "none", 1, "ALWAYS", 2,
				("resource:" + propFileResourceID[0]), ("resource:" + propFileResourceID[1]));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Simple_Medium", 1, "SIMPLE", "level", "PT2M");
		signalLevelChecks(distributeEvent, 0, 1, 0, 0, "Load_Control_TempOffset_Medium", 1, "LOAD_CONTROL", "x-loadControlLevelOffset", "PT2M");

	}

	protected void checkDistributeEvent_FDR(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();

		topLevelChecks(distributeEvent, 0, "PT0M", "none", "none", "none", 1, "ALWAYS", 1, ("venid:" + vid));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Simple_Special", 1, "SIMPLE", "level", "PT0M");

	}

	protected void checkDistributeEvent_FDR_Cancel(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();

		topLevelChecksCancel(distributeEvent, 0, "PT0M", "none", "none", "none", 1, "ALWAYS", 1, ("venid:" + vid));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Simple_Special", 1, "SIMPLE", "level", "PT0M");

	}

	protected void checkDistributeEvent_CBP_TYP(OadrDistributeEventType distributeEvent, String Simple_type, String Bid_Load_Setpoint) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();

		topLevelChecks(distributeEvent, 0, "PT2M", "none", "none", "none", 1, "ALWAYS", 2, ("venid:" + vid));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, Simple_type, 1, "SIMPLE", "level", "PT2M");
		signalLevelChecks(distributeEvent, 0, 1, 0, 0, Bid_Load_Setpoint, 1, "BID_LOAD", "setpoint", "PT2M");
		baseItemCheck(distributeEvent, 0, 1,
				properties.get("powerReal_itemUnits"),
				properties.get("powerReal_siScaleCode"),
				properties.get("powerReal_powerAttributes_hertz"), 
				properties.get("powerReal_powerAttributes_voltage"), 
				properties.get("powerReal_powerAttributes_ac"));

	}

	protected void checkDistributeEvent_CPP_TYP(OadrDistributeEventType distributeEvent, String Simple_type, String Electricity_Price_Special) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();

		topLevelChecks(distributeEvent, 0, "PT2M", "none", "none", "none", 1, "ALWAYS", 2, ("venid:" + vid));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, Simple_type, 1, "SIMPLE", "level", "PT2M");
		signalLevelChecks(distributeEvent, 0, 1, 0, 0, Electricity_Price_Special, 1, "Electricity_Price", "price", "PT2M");
		baseItemCheck_Elec(distributeEvent, 0, 1,
				properties.get("currencyPerKWh_itemUnits"),
				properties.get("currencyPerKWh_siScaleCode"));

	}


	protected void checkDistributeEvent_EVP_TYP(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();

		topLevelChecks(distributeEvent, 0, "PT2M", "none", "none", "none", 1, "ALWAYS", 1, ("venid:" + vid));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Electricity_Price_Medium", 1, "Electricity_Price", "price", "PT2M");
		baseItemCheck_Elec(distributeEvent, 0, 0,
				properties.get("currencyPerKWh_itemUnits"),
				properties.get("currencyPerKWh_siScaleCode"));

	}

	protected void checkDistributeEvent_DER_TYP(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();

		topLevelChecks(distributeEvent, 0, "PT48M", "none", "none", "none", 1, "NEVER", 1, ("venid:" + vid));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		for (int i=0; i<24;i++){
			if ( (i & 1) == 0 ) {
				signalLevelChecks(distributeEvent, 0, 0, i, 0, "Electricity_Price_Medium", 24, "Electricity_Price", "price", "PT2M");
				
			} else {
				signalLevelChecks(distributeEvent, 0, 0, i, 0, "Electricity_Price_High", 24, "Electricity_Price", "price", "PT2M");
				
			}

			
		}
		baseItemCheck_Elec(distributeEvent, 0, 0,
				properties.get("currencyPerKWh_itemUnits"),
				properties.get("currencyPerKWh_siScaleCode"));

	}

	protected void checkDistributeEvent_DER_TYP_Cancel(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();

		topLevelChecksCancel(distributeEvent, 0, "PT48M", "none", "none", "none", 1, "NEVER", 1, ("venid:" + vid));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		for (int i=0; i<24;i++){
			if ( (i & 1) == 0 ) {
				signalLevelChecks(distributeEvent, 0, 0, i, 0, "Electricity_Price_Medium", 24, "Electricity_Price", "price", "PT2M");
				
			} else {
				signalLevelChecks(distributeEvent, 0, 0, i, 0, "Electricity_Price_High", 24, "Electricity_Price", "price", "PT2M");
				
			}

			
		}
		baseItemCheck_Elec(distributeEvent, 0, 0,
				properties.get("currencyPerKWh_itemUnits"),
				properties.get("currencyPerKWh_siScaleCode"));

	}

	protected void checkDistributeEvent_CPP_CMP(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		String propFileResourceID[]= properties.getTwoResourceID();

		topLevelChecks(distributeEvent, 0, "PT4M", "none", "none", "none", 1, "ALWAYS", 1,
				("resource:" + propFileResourceID[0]), ("resource:" + propFileResourceID[1]));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		//signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Simple_Medium", 3, "SIMPLE", "level", "PT1M");
		//signalLevelChecks(distributeEvent, 0, 0, 1, 0, "Simple_High", 3, "SIMPLE", "level", "PT2M");
		//signalLevelChecks(distributeEvent, 0, 0, 2, 0, "Simple_Special", 3, "SIMPLE", "level", "PT1M");
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Electricity_Price_Medium", 3, "Electricity_Price", "price", "PT1M");
		signalLevelChecks(distributeEvent, 0, 0, 1, 0, "Electricity_Price_High", 3, "Electricity_Price", "price", "PT2M");
		signalLevelChecks(distributeEvent, 0, 0, 2, 0, "Electricity_Price_Special", 3, "Electricity_Price", "price", "PT1M");
		baseItemCheck_Elec(distributeEvent, 0, 0,
				properties.get("currencyPerKWh_itemUnits"),
				properties.get("currencyPerKWh_siScaleCode"));

	}

	protected void checkDistributeEvent_CBP_CMP(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		String propFileResourceID[]= properties.getTwoResourceID();

		topLevelChecks(distributeEvent, 0, "PT4M", "none", "none", "none", 1, "ALWAYS", 2,
				("resource:" + propFileResourceID[0]), ("resource:" + propFileResourceID[1]));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		//signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Simple_High", 2, "SIMPLE", "level", "PT2M");
		//signalLevelChecks(distributeEvent, 0, 0, 1, 0, "Simple_Medium", 2, "SIMPLE", "level", "PT2M");
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Bid_Load_Setpoint_High", 2, "BID_LOAD", "setpoint", "PT2M");
		signalLevelChecks(distributeEvent, 0, 0, 1, 0, "Bid_Load_Setpoint_Medium", 2, "BID_LOAD", "setpoint", "PT2M");
		baseItemCheck(distributeEvent, 0, 0,
				properties.get("powerReal_itemUnits"),
				properties.get("powerReal_siScaleCode"),
				properties.get("powerReal_powerAttributes_hertz"), 
				properties.get("powerReal_powerAttributes_voltage"), 
				properties.get("powerReal_powerAttributes_ac"));
		signalLevelChecks(distributeEvent, 0, 1, 0, 0, "Bid_Price_Value", 1, "BID_PRICE", "price", "PT4M");
		baseItemCheck_Elec(distributeEvent, 0, 1,
				properties.get("currencyPerKWh_itemUnits"),
				properties.get("currencyPerKWh_siScaleCode"));

	}

	protected void checkDistributeEvent_FDR_TYP(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();

		topLevelChecks(distributeEvent, 0, "PT2M", "none", "PT1M", "PT1M", 1, "ALWAYS", 2,
				("venid:" + vid));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Simple_High", 1, "SIMPLE", "level", "PT2M");
		signalLevelChecks(distributeEvent, 0, 1, 0, 0, "Load_Dispatch_Delta_High", 1, "LOAD_DISPATCH", "delta", "PT2M");
		baseItemCheck(distributeEvent, 0, 1,
				properties.get("powerReal_itemUnits"),
				properties.get("powerReal_siScaleCode"),
				properties.get("powerReal_powerAttributes_hertz"), 
				properties.get("powerReal_powerAttributes_voltage"), 
				properties.get("powerReal_powerAttributes_ac"));

	}

	protected void checkDistributeEvent_FDR_CMP(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */

		topLevelChecks(distributeEvent, 0, "PT4M", "none", "PT1M", "PT1M", 1, "ALWAYS", 1,
				("resource:" + properties.getOneResourceID()));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		//signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Simple_Medium", 2, "SIMPLE", "level", "PT2M");
		//signalLevelChecks(distributeEvent, 0, 0, 1, 0, "Simple_Special", 2, "SIMPLE", "level", "PT2M");
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Load_Dispatch_Setpoint_Medium", 2, "LOAD_DISPATCH", "setpoint", "PT2M");
		signalLevelChecks(distributeEvent, 0, 0, 1, 0, "Load_Dispatch_Setpoint_Special", 2, "LOAD_DISPATCH", "setpoint", "PT2M");
		baseItemCheck(distributeEvent, 0, 0,
				properties.get("powerReal_itemUnits"),
				properties.get("powerReal_siScaleCode"),
				properties.get("powerReal_powerAttributes_hertz"), 
				properties.get("powerReal_powerAttributes_voltage"), 
				properties.get("powerReal_powerAttributes_ac"));

	}

	protected void checkDistributeEvent_EVR_TYP(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();

		topLevelChecks(distributeEvent, 0, "PT4M", "none", "none", "none", 1, "ALWAYS", 2, ("venid:" + vid));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Simple_Medium", 3, "SIMPLE", "level", "PT1M");
		signalLevelChecks(distributeEvent, 0, 0, 1, 0, "Simple_High", 3, "SIMPLE", "level", "PT2M");
		signalLevelChecks(distributeEvent, 0, 0, 2, 0, "Simple_Medium", 3, "SIMPLE", "level", "PT1M");
		signalLevelChecks(distributeEvent, 0, 1, 0, 0, "Electricity_Price_Medium", 3, "Electricity_Price", "price", "PT1M");
		signalLevelChecks(distributeEvent, 0, 1, 1, 0, "Electricity_Price_High", 3, "Electricity_Price", "price", "PT2M");
		signalLevelChecks(distributeEvent, 0, 1, 2, 0, "Electricity_Price_Medium", 3, "Electricity_Price", "price", "PT1M");
		baseItemCheck_Elec(distributeEvent, 0, 1,
				properties.get("currencyPerKWh_itemUnits"),
				properties.get("currencyPerKWh_siScaleCode"));

	}

	protected void checkDistributeEvent_EVR_SMP(OadrDistributeEventType distributeEvent) {

		/*
		 * topLevelChecks
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Resource2"
		 * 
		 */
		XMLDBUtil vtnDbXml = new XMLDBUtil();
		String vid = vtnDbXml.getVENID();

		topLevelChecks(distributeEvent, 0, "PT4M", "none", "none", "none", 1, "ALWAYS", 1, ("venid:" + vid));
		
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		signalLevelChecks(distributeEvent, 0, 0, 0, 0, "Simple_Medium", 3, "SIMPLE", "level", "PT1M");
		signalLevelChecks(distributeEvent, 0, 0, 1, 0, "Simple_High", 3, "SIMPLE", "level", "PT2M");
		signalLevelChecks(distributeEvent, 0, 0, 2, 0, "Simple_Medium", 3, "SIMPLE", "level", "PT1M");

	}

	private void baseItemCheck_Elec(OadrDistributeEventType distributeEvent, int eventNumber, int signalNumber,
			String units, String scale){
		/*
		 * Check for Electricity_Price
		 * Items are descriptive in the call
		 */
		if (eventNumber == -1) 
			return;

		List<OadrEvent> oadrEventList = distributeEvent.getOadrEvent();
		if(oadrEventList.size() < eventNumber + 1){
			throw new FailedException("Event " + eventNumber + " does not appear to exist as there are " + oadrEventList.size() + " event(s).");
		}

		OadrEvent eachOadrEvent = oadrEventList.get(eventNumber);
		
		EiEventSignalsType eiEventSignalsType = eachOadrEvent.getEiEvent().getEiEventSignals();
		if(eiEventSignalsType.getEiEventSignal().size() < signalNumber + 1){
			throw new FailedException("EventSignal " + signalNumber + " does not appear to exist as there are "
					+ eiEventSignalsType.getEiEventSignal().size() + " EventSignals for event " + eventNumber + ".");
		}

		if(!eiEventSignalsType.getEiEventSignal().get(signalNumber).getItemBase().getValue().getClass().equals(CurrencyType.class)){
			throw new FailedException("Electricity_Price does not appear to exist for signal "
					+ signalNumber + " for event " + eventNumber + ".");
		}
		
		try{
			ISO3AlphaCurrencyCodeContentType testme = ISO3AlphaCurrencyCodeContentType.fromValue(units);
		} catch(Exception e) {
			throw new FailedException(units + " from the property currencyPerKWh_itemUnits is not a valid ISO3AlphaCurrencyCodeContentType");
		}
		CurrencyType curr = (CurrencyType) eiEventSignalsType.getEiEventSignal().get(signalNumber).getItemBase().getValue();
		if(!curr.getItemUnits().equals(ISO3AlphaCurrencyCodeContentType.fromValue(units))){
			throw new FailedException("Expect Electricity_Price_Special itemUnits of " + units + " but found " + curr.getItemUnits() + " for signal "
					+ signalNumber + " for event " + eventNumber + ".");
		}
		
		if(!curr.getSiScaleCode().contentEquals(scale)){
			throw new FailedException("Expect Electricity_Price_Special SiScaleCode of " + scale + " but found " + curr.getSiScaleCode() + " for signal "
					+ signalNumber + " for event " + eventNumber + ".");
		}
		
	}
	
	private void baseItemCheck(OadrDistributeEventType distributeEvent, int eventNumber, int signalNumber,
			String units, String scale, String hertz, String volt, String ac){
		/*
		 * Check for powerReal
		 * Items are descriptive in the call
		 */
		if (eventNumber == -1) 
			return;

		List<OadrEvent> oadrEventList = distributeEvent.getOadrEvent();
		if(oadrEventList.size() < eventNumber + 1){
			throw new FailedException("Event " + eventNumber + " does not appear to exist as there are " + oadrEventList.size() + " event(s).");
		}

		OadrEvent eachOadrEvent = oadrEventList.get(eventNumber);
		
		EiEventSignalsType eiEventSignalsType = eachOadrEvent.getEiEvent().getEiEventSignals();
		if(eiEventSignalsType.getEiEventSignal().size() < signalNumber + 1){
			throw new FailedException("EventSignal " + signalNumber + " does not appear to exist as there are "
					+ eiEventSignalsType.getEiEventSignal().size() + " EventSignals for event " + eventNumber + ".");
		}

		if(!eiEventSignalsType.getEiEventSignal().get(signalNumber).getItemBase().getValue().getClass().equals(PowerRealType.class)){
			throw new FailedException("powerReal does not appear to exist for signal "
					+ signalNumber + " for event " + eventNumber + ".");
		}
		
		PowerRealType preal = (PowerRealType) eiEventSignalsType.getEiEventSignal().get(signalNumber).getItemBase().getValue();
		if(!preal.getItemUnits().contentEquals(units)){
			throw new FailedException("Expect powerReal itemUnits of " + units + " but found " + preal.getItemUnits() + " for signal "
					+ signalNumber + " for event " + eventNumber + ".");
		}
		
		if(!preal.getSiScaleCode().contentEquals(scale)){
			throw new FailedException("Expect powerReal SiScaleCode of " + scale + " but found " + preal.getSiScaleCode() + " for signal "
					+ signalNumber + " for event " + eventNumber + ".");
		}
		
		if(preal.getPowerAttributes().isAc() != Boolean.parseBoolean(ac)){
			throw new FailedException("Expect powerReal attribute AC of " + ac + " but found " + preal.getPowerAttributes().isAc() + " for signal "
					+ signalNumber + " for event " + eventNumber + ".");
		}
		
		if(preal.getPowerAttributes().getHertz().floatValue() != Float.parseFloat(hertz)){
			throw new FailedException("Expect powerReal attribute Hertz of " + hertz + " but found " + preal.getPowerAttributes().getHertz()+ " for signal "
					+ signalNumber + " for event " + eventNumber + ".");
		}
		
		if(preal.getPowerAttributes().getVoltage().floatValue() != Float.parseFloat(volt)){
			throw new FailedException("Expect powerReal attribute volts of " + volt + " but found " + preal.getPowerAttributes().getVoltage() + " for signal "
					+ signalNumber + " for event " + eventNumber + ".");
		}
		
	}
	
	private void signalLevelChecks(OadrDistributeEventType distributeEvent, int eventNumber, int signalNumber,
			int intervalNumber, int payloadNumber, String Simple_type, 
			int numberOfIntervals, String signalName, String signalType, String intervalDuration){
		/*
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * eventNumber - the event to be checked, generally 0 (int)
		 * signalNumber - the signal to be checked
		 * intervalNumber - the interval to be checked
		 * payloadNumber - the payload to be checked
		 * Simple_type - passthrough of the medium/high/special
		 * numberOfIntervals - the number of expected intervals
		 * signalName - signalName for the interval
		 * signalType - signalType for the interval
		 * intervalDuration - the duration for the interval
		 * 
		 */
		//Programmer control. This would be to skip the test while retaining the call in a routine above.
		if (eventNumber == -1) 
			return;

		List<OadrEvent> oadrEventList = distributeEvent.getOadrEvent();
		if(oadrEventList.size() < eventNumber + 1){
			throw new FailedException("Event " + eventNumber + " does not appear to exist as there are " + oadrEventList.size() + " event(s).");
		}

		OadrEvent eachOadrEvent = oadrEventList.get(eventNumber);
		
		EiEventSignalsType eiEventSignalsType = eachOadrEvent.getEiEvent().getEiEventSignals();
		if(eiEventSignalsType.getEiEventSignal().size() < signalNumber + 1){
			throw new FailedException("EventSignal " + signalNumber + " does not appear to exist as there are "
					+ eiEventSignalsType.getEiEventSignal().size() + " EventSignals for event " + eventNumber + ".");
		}
		
		if(eiEventSignalsType.getEiEventSignal().get(signalNumber).getIntervals().getInterval().size() < intervalNumber + 1){
			throw new FailedException("Interval " + intervalNumber + " does not appear to exist as there are "
					+ eiEventSignalsType.getEiEventSignal().get(signalNumber).getIntervals().getInterval().size() 
					+ " Intervals for EventSignal " + signalNumber + " for event " + eventNumber + ".");
		}
		
		if(!disable_DR_Guide_SignalName_Check && !signalName.toLowerCase().contentEquals("n/a")){
			String receivedSignalName = getSignalNameI(distributeEvent, eventNumber, signalNumber);
			if (!signalName.equalsIgnoreCase(receivedSignalName)) {
				throw new FailedException("Did not receive expected signal name " + signalName + ". Got " + receivedSignalName
						+ " for EventSignal " + signalNumber + " for event " + eventNumber + ".");
			}
		}

		if(!disable_DR_Guide_SignalType_Check && !signalType.toLowerCase().contentEquals("n/a")){
			String receivedSignalType = getSignalTypeI(distributeEvent, eventNumber, signalNumber);
			if (!signalType.equals(receivedSignalType)) {
				throw new FailedException("Did not receive expected signal Type " + signalType + ". Got " + receivedSignalType 
						+ " for EventSignal " + signalNumber + " for event " + eventNumber + ".");
			}
		}

		if (!disable_DR_Guide_NumIntervals_Check && numberOfIntervals > -1){
			if(eiEventSignalsType.getEiEventSignal().get(signalNumber).getIntervals().getInterval().size() != numberOfIntervals){
				throw new FailedException("Expected " + numberOfIntervals + " Intervals(s) but found " 
						+ eiEventSignalsType.getEiEventSignal().get(signalNumber).getIntervals().getInterval().size() 
						+ " for EventSignal " + signalNumber + " for event " + eventNumber + ".");
			}
		}

		if(!disable_DR_Guide_IntervalValue_Check && !Simple_type.toLowerCase().contentEquals("n/a")){

			if(eiEventSignalsType.getEiEventSignal().get(signalNumber)
				.getIntervals().getInterval().get(intervalNumber).getStreamPayloadBase().size() < payloadNumber +1){

				throw new FailedException("Payload " + payloadNumber + " does not appear to exist as there are " + 
						eiEventSignalsType.getEiEventSignal().get(signalNumber)
						.getIntervals().getInterval().get(intervalNumber).getStreamPayloadBase().size() 
						+ " payloads for interval " + intervalNumber + " for EventSignal " 
						+ signalNumber + " for event " + eventNumber + ".");
			}

			SignalPayloadType signalPayloadFloat = (SignalPayloadType)eiEventSignalsType.getEiEventSignal().get(signalNumber)
					.getIntervals().getInterval().get(intervalNumber).getStreamPayloadBase().get(payloadNumber).getValue();
			PayloadFloatType payloadFloat = (PayloadFloatType)signalPayloadFloat.getPayloadBase().getValue();
			Float signalPayloadFloatv = payloadFloat.getValue();
			if (signalPayloadFloatv != Float.parseFloat(properties.get(Simple_type))){
				throw new FailedException("Expected value for " + Simple_type + " of " + 
						properties.get(Simple_type) + " does not match the value " +
						signalPayloadFloatv.toString() + " found in the interval payload"
						+ " for payload " + payloadNumber + " for interval " + intervalNumber + " for EventSignal " 
						+ signalNumber + " for event " + eventNumber + ".");
			}
		}

		if(!disable_DR_Guide_IntervalDuration_Check && !intervalDuration.toLowerCase().contentEquals("n/a")){
			String sDur = eiEventSignalsType.getEiEventSignal().get(signalNumber).getIntervals().getInterval().get(intervalNumber).getDuration().getDuration();
			if(!(OadrUtil.createDuration(sDur).equals(OadrUtil.createDuration(intervalDuration)))){
				throw new FailedException("Expected an interval duration of " + intervalDuration + " but found " + sDur 
						+ " for interval " + intervalNumber + " for EventSignal " 
						+ signalNumber + " for event " + eventNumber + ".");
			}
		}
		
		return;
	}

	private void topLevelChecks(OadrDistributeEventType distributeEvent, int eventNumber, String overallDuration, String randomization, 
			String rampUp, String recovery, int priority, String responseRequired, int numberOfSignals, String... targets){
		/*
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Rsource2"
		 * 
		 */

		//Programmer control. This would be to skip the test while retaining the call in a routine above.
		if (eventNumber == -1) 
			return;

		List<OadrEvent> oadrEventList = distributeEvent.getOadrEvent();
		if(oadrEventList.size() < eventNumber + 1){
			throw new FailedException("Event " + eventNumber + " does not appear to exist as there are " + oadrEventList.size() + " event(s).");
		}
		OadrEvent eachOadrEvent = oadrEventList.get(eventNumber);

		if (!disable_DR_Guide_NumSignals_Check && numberOfSignals > -1){
			if(eachOadrEvent.getEiEvent().getEiEventSignals().getEiEventSignal().size() != numberOfSignals){
				throw new FailedException("Expected " + numberOfSignals + " EiEventSignal(s) but found " 
						+ eachOadrEvent.getEiEvent().getEiEventSignals().getEiEventSignal().size()  + " for event " + eventNumber);
			}
		}

		if (!disable_DR_Guide_EventStatus_Check){
			EventStatusEnumeratedType enumStat = eachOadrEvent.getEiEvent().getEventDescriptor().getEventStatus();
			if(!(enumStat == EventStatusEnumeratedType.FAR || enumStat == EventStatusEnumeratedType.NEAR || enumStat == EventStatusEnumeratedType.ACTIVE )){
				throw new FailedException("Expected an eventStatus of Far, Near, or Active but found " + enumStat.name() + " for event " + eventNumber);
			}
		}

		if (!disable_DR_Guide_Overall_Duration_Check && !overallDuration.toLowerCase().contentEquals("n/a")){
			if(overallDuration.toLowerCase().contentEquals("none")){
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getDuration()!=null && !OadrUtil.createDuration(eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getDuration().getDuration()).equals(OadrUtil.createDuration("PT0M")) ){
					throw new FailedException("Over all duration should be none. Found duration set to " + eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getDuration().getDuration() + " for event " + eventNumber );
				}
			} else {
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getDuration()!=null){
					String sDur = eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getDuration().getDuration();
					if(!(OadrUtil.createDuration(sDur).equals(OadrUtil.createDuration(overallDuration)))){
						throw new FailedException("Expected an over all duration of " + overallDuration + " but found " + sDur + " for event " + eventNumber );
					}
				} else {
					throw new FailedException("Expected an over all duration of " + overallDuration 
							+ " but could not find a duration for event " + eventNumber );
				}
			}

		}

		if (!disable_DR_Guide_Random_Check && !randomization.toLowerCase().contentEquals("n/a")){
			// Check that randomization is none
			if(randomization.toLowerCase().contentEquals("none")){
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate().getStartafter()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate().getStartafter().length()>0   && !OadrUtil.createDuration(eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance().getTolerate().getStartafter()).equals(OadrUtil.createDuration("PT0M"))
){
					throw new FailedException("Randomization should be none. Found StartAfter set to " + eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getTolerance()
							.getTolerate().getStartafter() );
				}
			} else {
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate().getStartafter()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate().getStartafter().length()>0){
					String sDur = eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getTolerance()
							.getTolerate().getStartafter();
					if(!(OadrUtil.createDuration(sDur).equals(OadrUtil.createDuration(randomization)))){
						throw new FailedException("Expected a randomization duration of " + randomization + " but found " + sDur + " for event " + eventNumber );
					}
				} else {
					throw new FailedException("Expected an randomization duration of " + randomization 
							+ " but could not find a StartAfter duration for event " + eventNumber );
				}
			}
		}

		if (!disable_DR_Guide_RampUp_Check && !rampUp.toLowerCase().contentEquals("n/a")){
			if(rampUp.toLowerCase().contentEquals("none")){
				// Check that ramp up is none
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getXEiRampUp()!=null && !OadrUtil.createDuration(eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getXEiRampUp().getDuration()).equals(OadrUtil.createDuration("PT0M")) ){
					throw new FailedException("Ramp up should be none. Found EiRampUP set to " + eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getXEiRampUp().getDuration() + " for event " + eventNumber );
				}
			} else {
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getXEiRampUp()!=null && !OadrUtil.createDuration(eachOadrEvent.getEiEvent()
								.getEiActivePeriod().getProperties().getXEiRampUp().getDuration()).equals(OadrUtil.createDuration("PT0M")) ){
					String sDur = eachOadrEvent.getEiEvent().getEiActivePeriod().getProperties().getXEiRampUp().getDuration();
					if(!(OadrUtil.createDuration(sDur).equals(OadrUtil.createDuration(rampUp)))){
						throw new FailedException("Expected a rampUp duration of " + rampUp + " but found " + sDur + " for event " + eventNumber );
					}
				} else {
					throw new FailedException("Expected an rampUp duration of " + rampUp 
							+ " but could not find a EiRampUp duration for event " + eventNumber );
				}
			}

		}

		if (!disable_DR_Guide_Recovery_Check && !recovery.toLowerCase().contentEquals("n/a")){
			// Check that recovery is none
			if(recovery.toLowerCase().contentEquals("none")){
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getXEiRecovery()!=null && !OadrUtil.createDuration(eachOadrEvent.getEiEvent()
								.getEiActivePeriod().getProperties().getXEiRecovery().getDuration()).equals(OadrUtil.createDuration("PT0M")) ){
					throw new FailedException("Recovery should be none. Found EiRecovery set to " + eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getXEiRecovery().getDuration() + " for event " + eventNumber );
				}
			} else {
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getXEiRecovery()!=null){
					String sDur = eachOadrEvent.getEiEvent().getEiActivePeriod().getProperties().getXEiRecovery().getDuration();
					if(!(OadrUtil.createDuration(sDur).equals(OadrUtil.createDuration(recovery)))){
						throw new FailedException("Expected a recovery duration of " + recovery 
								+ " but found " + sDur  + " for event " + eventNumber);
					}
				} else {
					throw new FailedException("Expected an recovery duration of " + recovery 
							+ " but could not find a EiRecovery duration for event " + eventNumber );
				}

			}

		}


		if (!disable_DR_Guide_Target_Check && !targets[0].toLowerCase().contentEquals("none")){
			for (int j=0; j< targets.length; j++){
				String[] oneTarget = targets[j].split(":");
				if(oneTarget[0].toLowerCase().contentEquals("venid")){
					boolean found = false;
					int ix = eachOadrEvent.getEiEvent().getEiTarget().getVenID().size();
					for (int i=0; i<ix; i++){
						if (oneTarget[1].contentEquals(eachOadrEvent.getEiEvent().getEiTarget().getVenID().get(i))){
							found = true;
						}
					}
					if (!found){
						throw new FailedException("Did not find a VenID of " + oneTarget[1] 
								+ "  for event " + eventNumber );
					}
				} else if(oneTarget[0].toLowerCase().contentEquals("resource")){
					boolean found = false;
					int ix = eachOadrEvent.getEiEvent().getEiTarget().getResourceID().size();
					for (int i=0; i<ix; i++){
						if (oneTarget[1].contentEquals(eachOadrEvent.getEiEvent().getEiTarget().getResourceID().get(i))){
							found = true;
						}
					}
					if (!found){
						throw new FailedException("Did not find a resource of " + oneTarget[1] 
								+ "  for event " + eventNumber );
					}
				} else {
					throw new FailedException("Did not understand a target prefix of " + oneTarget[0] 
							+ "  for event " + eventNumber );
					
				}
			}

		}

		if (!disable_DR_Guide_Priority_Check && priority > -1){
			Long ipriority = eachOadrEvent.getEiEvent().getEventDescriptor().getPriority();
			if(ipriority != priority) {
				throw new FailedException("Expected Priority to be " + priority + " but found " + ipriority.toString() );
			}
		}

		if (!disable_DR_Guide_ResponseRequired_Check){
			if (responseRequired.toUpperCase().contentEquals("ALWAYS")){
				if (!(eachOadrEvent.getOadrResponseRequired().equals(ResponseRequiredType.ALWAYS))) {
					throw new FailedException("Expected Response Required ALWAYS but found " 
							+ eachOadrEvent.getOadrResponseRequired().value() + " for event " + eventNumber);
				}
			} else {
				if (!(eachOadrEvent.getOadrResponseRequired().equals(ResponseRequiredType.NEVER))) {
					throw new FailedException("Expected Response Required NEVER but found " 
							+ eachOadrEvent.getOadrResponseRequired().value() + " for event " + eventNumber);
				}
			}
		}
		return;
	}

	private void topLevelChecksCancel(OadrDistributeEventType distributeEvent, int eventNumber, String overallDuration, String randomization, 
			String rampUp, String recovery, int priority, String responseRequired, int numberOfSignals, String... targets){
		/*
		 * eventNumber - the event to be checked, generally 0 (int)
		 * 
		 * Entering a n/a for string, -1 for int, for the following will skip that check.
		 * Entering -NONE will check to see that the entry does not exist
		 * 
		 * overallDuration - the duration for the event
		 * randomization - checks Startafter
		 * rampUp - checks XEiRampUp
		 * recovery - checks the duration for the recovery or check for "none"
		 * priority - checks the priority, (int)
		 * responseRequired - will look for "ALWAYS" or "NEVER", ie "ALWAYS" will check ResponseRequiredType.ALWAYS
		 * numberOfSignals - will check for the number of eiEventSignal (int)
		 * targets - the venid or resource(s) to check for. The entries can be a string array or separated by commas.
		 * 	Preface each entry with either venid: or resource: ie "resource:Resource1" , "resource:Rsource2"
		 * 
		 */

		//Programmer control. This would be to skip the test while retaining the call in a routine above.
		if (eventNumber == -1) 
			return;

		List<OadrEvent> oadrEventList = distributeEvent.getOadrEvent();
		if(oadrEventList.size() < eventNumber + 1){
			throw new FailedException("Event " + eventNumber + " does not appear to exist as there are " + oadrEventList.size() + " event(s).");
		}
		OadrEvent eachOadrEvent = oadrEventList.get(eventNumber);

		if (!disable_DR_Guide_NumSignals_Check && numberOfSignals > -1){
			if(eachOadrEvent.getEiEvent().getEiEventSignals().getEiEventSignal().size() != numberOfSignals){
				throw new FailedException("Expected " + numberOfSignals + " EiEventSignal(s) but found " 
						+ eachOadrEvent.getEiEvent().getEiEventSignals().getEiEventSignal().size()  + " for event " + eventNumber);
			}
		}

		if (!disable_DR_Guide_EventStatus_Check){
			EventStatusEnumeratedType enumStat = eachOadrEvent.getEiEvent().getEventDescriptor().getEventStatus();
			if(!(enumStat == EventStatusEnumeratedType.CANCELLED)){
				throw new FailedException("Expected an eventStatus of Canceled but found " + enumStat.name() + " for event " + eventNumber);
			}
		}

		if (!disable_DR_Guide_Overall_Duration_Check && !overallDuration.toLowerCase().contentEquals("n/a")){
			if(overallDuration.toLowerCase().contentEquals("none")){
				// Check that ramp up is none
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getDuration()!=null){
					throw new FailedException("Over all duration should be none. Found duration set to " + eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getDuration().getDuration() + " for event " + eventNumber );
				}
			} else {
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getDuration()!=null){
					String sDur = eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getDuration().getDuration();
					if(!(OadrUtil.createDuration(sDur).equals(OadrUtil.createDuration(overallDuration)))){
						throw new FailedException("Expected an over all duration of " + overallDuration + " but found " + sDur + " for event " + eventNumber );
					}
				} else {
					throw new FailedException("Expected an over all duration of " + overallDuration 
							+ " but could not find a duration for event " + eventNumber );
				}
			}

		}

		if (!disable_DR_Guide_Random_Check && !randomization.toLowerCase().contentEquals("n/a")){
			// Check that randomization is none
			if(randomization.toLowerCase().contentEquals("none")){
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate().getStartafter()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate().getStartafter().length()>0){
					throw new FailedException("Randomization should be none. Found StartAfter set to " + eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getTolerance()
							.getTolerate().getStartafter() );
				}
			} else {
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate().getStartafter()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getTolerance()
						.getTolerate().getStartafter().length()>0){
					String sDur = eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getTolerance()
							.getTolerate().getStartafter();
					if(!(OadrUtil.createDuration(sDur).equals(OadrUtil.createDuration(rampUp)))){
						throw new FailedException("Expected a randomization duration of " + randomization + " but found " + sDur + " for event " + eventNumber );
					}
				} else {
					throw new FailedException("Expected an randomization duration of " + randomization 
							+ " but could not find a StartAfter duration for event " + eventNumber );
				}
			}
		}

		if (!disable_DR_Guide_RampUp_Check && !rampUp.toLowerCase().contentEquals("n/a")){
			if(rampUp.toLowerCase().contentEquals("none")){
				// Check that ramp up is none
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getXEiRampUp()!=null){
					throw new FailedException("Ramp up should be none. Found EiRampUP set to " + eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getXEiRampUp().getDuration() + " for event " + eventNumber );
				}
			} else {
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getXEiRampUp()!=null){
					String sDur = eachOadrEvent.getEiEvent().getEiActivePeriod().getProperties().getXEiRampUp().getDuration();
					if(!(OadrUtil.createDuration(sDur).equals(OadrUtil.createDuration(rampUp)))){
						throw new FailedException("Expected a rampUp duration of " + rampUp + " but found " + sDur + " for event " + eventNumber );
					}
				} else {
					throw new FailedException("Expected an rampUp duration of " + rampUp 
							+ " but could not find a EiRampUp duration for event " + eventNumber );
				}
			}

		}

		if (!disable_DR_Guide_Recovery_Check && !recovery.toLowerCase().contentEquals("n/a")){
			// Check that recovery is none
			if(recovery.toLowerCase().contentEquals("none")){
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getXEiRecovery()!=null){
					throw new FailedException("Recovery should be none. Found EiRecovery set to " + eachOadrEvent.getEiEvent()
							.getEiActivePeriod().getProperties().getXEiRecovery().getDuration() + " for event " + eventNumber );
				}
			} else {
				if(eachOadrEvent.getEiEvent()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties()!=null && eachOadrEvent.getEiEvent()
						.getEiActivePeriod().getProperties().getXEiRecovery()!=null){
					String sDur = eachOadrEvent.getEiEvent().getEiActivePeriod().getProperties().getXEiRecovery().getDuration();
					if(!(OadrUtil.createDuration(sDur).equals(OadrUtil.createDuration(recovery)))){
						throw new FailedException("Expected a recovery duration of " + recovery 
								+ " but found " + sDur  + " for event " + eventNumber);
					}
				} else {
					throw new FailedException("Expected an recovery duration of " + recovery 
							+ " but could not find a EiRecovery duration for event " + eventNumber );
				}

			}

		}


		if (!disable_DR_Guide_Target_Check && !targets[0].toLowerCase().contentEquals("none")){
			for (int j=0; j< targets.length; j++){
				String[] oneTarget = targets[j].split(":");
				if(oneTarget[0].toLowerCase().contentEquals("venid")){
					boolean found = false;
					int ix = eachOadrEvent.getEiEvent().getEiTarget().getVenID().size();
					for (int i=0; i<ix; i++){
						if (oneTarget[1].contentEquals(eachOadrEvent.getEiEvent().getEiTarget().getVenID().get(i))){
							found = true;
						}
					}
					if (!found){
						throw new FailedException("Did not find a VenID of " + oneTarget[1] 
								+ "  for event " + eventNumber );
					}
				} else if(oneTarget[0].toLowerCase().contentEquals("resource")){
					boolean found = false;
					int ix = eachOadrEvent.getEiEvent().getEiTarget().getResourceID().size();
					for (int i=0; i<ix; i++){
						if (oneTarget[1].contentEquals(eachOadrEvent.getEiEvent().getEiTarget().getResourceID().get(i))){
							found = true;
						}
					}
					if (!found){
						throw new FailedException("Did not find a resource of " + oneTarget[1] 
								+ "  for event " + eventNumber );
					}
				} else {
					throw new FailedException("Did not understand a target prefix of " + oneTarget[0] 
							+ "  for event " + eventNumber );
					
				}
			}

		}

		if (!disable_DR_Guide_Priority_Check && priority > -1){
			Long ipriority = eachOadrEvent.getEiEvent().getEventDescriptor().getPriority();
			if(ipriority != priority) {
				throw new FailedException("Expected Priority to be " + priority + " but found " + ipriority.toString() );
			}
		}

		if (!disable_DR_Guide_ResponseRequired_Check){
			if (responseRequired.toUpperCase().contentEquals("ALWAYS")){
				if (!(eachOadrEvent.getOadrResponseRequired().equals(ResponseRequiredType.ALWAYS))) {
					throw new FailedException("Expected Response Required ALWAYS but found " 
							+ eachOadrEvent.getOadrResponseRequired().value() + " for event " + eventNumber);
				}
			} else {
				if (!(eachOadrEvent.getOadrResponseRequired().equals(ResponseRequiredType.NEVER))) {
					throw new FailedException("Expected Response Required NEVER but found " 
							+ eachOadrEvent.getOadrResponseRequired().value() + " for event " + eventNumber);
				}
			}
		}
		return;
	}

	private boolean durationEquals(String durationText1, String durationText2) {
		// System.out.println("durationText " + durationText1 + "==" + durationText2);
		Duration duration1 = OadrUtil.createDuration(durationText1);
		Duration duration2 = OadrUtil.createDuration(durationText2);
		return (duration1.compare(duration2) == 0);
	}
}
