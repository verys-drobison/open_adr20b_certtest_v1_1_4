package com.qualitylogic.openadr.core.base;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IRequestReregistrationAction;
import com.qualitylogic.openadr.core.action.IResponseCanceledOptAckAction;
import com.qualitylogic.openadr.core.action.IResponseCanceledReportTypeAckAction;
import com.qualitylogic.openadr.core.action.IResponseCreatedOptAckAction;
import com.qualitylogic.openadr.core.action.IResponseCreatedPartyRegistrationAckAction;
import com.qualitylogic.openadr.core.action.ResponseCanceledOptAckActionList;
import com.qualitylogic.openadr.core.action.ResponseCanceledReportTypeAckActionList;
import com.qualitylogic.openadr.core.action.ResponseCreatedOptAckActionList;
import com.qualitylogic.openadr.core.action.ResponseCreatedPartyRegistrationAckActionList;
import com.qualitylogic.openadr.core.action.ResponseCreatedPartyRegistrationToQueryAckActionList;
import com.qualitylogic.openadr.core.action.impl.DefaultCreatedPartyRegistration;
import com.qualitylogic.openadr.core.action.impl.DefaultOadrCanceledOptEvent;
import com.qualitylogic.openadr.core.action.impl.DefaultOadrCreatedOptEvent;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCanceledReportTypeAckAction;
import com.qualitylogic.openadr.core.bean.VENServiceType;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.EventResponses.EventResponse;
import com.qualitylogic.openadr.core.signal.IntervalType;
import com.qualitylogic.openadr.core.signal.OadrCancelOptType;
import com.qualitylogic.openadr.core.signal.OadrCanceledReportType;
import com.qualitylogic.openadr.core.signal.OadrCreateOptType;
import com.qualitylogic.openadr.core.signal.OadrCreatePartyRegistrationType;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.OadrCreatedEventType;
import com.qualitylogic.openadr.core.signal.OadrCreatedReportType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OadrPendingReportsType;
import com.qualitylogic.openadr.core.signal.OadrQueryRegistrationType;
import com.qualitylogic.openadr.core.signal.OadrReportPayloadType;
import com.qualitylogic.openadr.core.signal.OadrReportType;
import com.qualitylogic.openadr.core.signal.OadrUpdateReportType;
import com.qualitylogic.openadr.core.signal.OptTypeType;
import com.qualitylogic.openadr.core.signal.StreamPayloadBaseType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.UpdateReportEventHelper;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.util.XMLDBUtil;
import com.qualitylogic.openadr.core.vtn.VTNService;
import com.qualitylogic.openadr.core.vtn.VtnToVenClient;

public abstract class VtnTestCase extends NewTestCase {

	private static final String RID_POWER_REAL = "PowerReal";
	private static final String RID_ENERGY_REAL = "EnergyReal";
	private static final String REPORT_TELEMETRY_STATUS = "TELEMETRY_STATUS";
	private static final String REPORT_TELEMETRY_USAGE = "TELEMETRY_USAGE";
	private static final String REPORT_HISTORY_USAGE = "HISTORY_USAGE";
	private static final String REPORT_THERMOSTAT_STATE = "x-THERMOSTAT_STATE";

	protected void listenForRequests() throws Exception {
		VTNService.startVTNService();
	}

	@Override
	public void cleanUp() throws Exception {
		VTNService.stopVTNService();
	}

	protected String post(String message, String type) throws Exception {
		VENServiceType venService = null;
		switch (type) {
		case "EiRegistration" : venService = VENServiceType.EiRegistration; break;
		case "EiReport" : venService = VENServiceType.EiReport; break;
		default : throw new IllegalArgumentException("Unknown type" + type);
		}

		return VtnToVenClient.post(message, venService);
	}

	protected IResponseCreatedPartyRegistrationAckAction addCreatedPartyRegistrationResponse() {
		IResponseCreatedPartyRegistrationAckAction createdPartyRegistration = new DefaultCreatedPartyRegistration(OadrCreatePartyRegistrationType.class);
		ResponseCreatedPartyRegistrationAckActionList.addResponsePartyRegistrationAckAction(createdPartyRegistration);
		return createdPartyRegistration;
	}

	protected IResponseCreatedPartyRegistrationAckAction addCreatedPartyRegistrationResponseToQuery() {
		IResponseCreatedPartyRegistrationAckAction createdPartyRegistration = new DefaultCreatedPartyRegistration(OadrQueryRegistrationType.class);
		ResponseCreatedPartyRegistrationToQueryAckActionList.addResponsePartyRegistrationAckAction(createdPartyRegistration);
		return createdPartyRegistration;
	}

	protected IResponseCreatedOptAckAction addCreatedOptResponse() {
		IResponseCreatedOptAckAction createdOpt = new DefaultOadrCreatedOptEvent();
		ResponseCreatedOptAckActionList.addResponseCreatedOptAckAction(createdOpt);
		return createdOpt;
	}

	protected IResponseCanceledOptAckAction addCanceledOptResponse() {
		IResponseCanceledOptAckAction canceledOpt = new DefaultOadrCanceledOptEvent();
		ResponseCanceledOptAckActionList.addResponseCanceledOptAckAction(canceledOpt);
		return canceledOpt;
	}

	protected void checkQueryRegistrationRequest(int size) {
		List<OadrQueryRegistrationType> queryRegistrations = TestSession.getOadrQueryRegistrationTypeReceivedList();
		if (queryRegistrations.size() != size) {
			throw new FailedException("Expected " + size + " OadrQueryRegistrationType(s), received " + queryRegistrations.size());
		}
	}

	protected void checkCreateOptRequest(int size) {
		List<OadrCreateOptType> createOpts = TestSession.getCreateOptEventReceivedList();
		if (createOpts.size() != size) {
			throw new FailedException("Expected " + size + " OadrCreateOpt(s), received " + createOpts.size());
		}
	}

	protected void checkCancelOptRequest(int size) {
		List<OadrCancelOptType> cancelOpts = TestSession.getCancelOptTypeReceivedList();
		if (cancelOpts.size() != size) {
			throw new FailedException("Expected " + size + " OadrCancelOpt(s), received " + cancelOpts.size());
		}
	}

	protected void checkCreatedEventReceived(ICreatedEventResult createdEvent) {
		if (!createdEvent.isExpectedCreatedEventReceived()) {
			throw new FailedException("OadrCreatedEvent has not been received.");
		}
	}

	protected void checkRequestRegistration(IRequestReregistrationAction requestReregistration) {
		if (!requestReregistration.isEventCompleted()) {
			throw new FailedException("OadRrequestReregistration has not been received.");
		}
	}
	protected void checkUpdateReport(OadrUpdateReportType updateReport, int minutesOffset, int minIntervalCount) {
		XMLGregorianCalendar dtstart = updateReport.getOadrReport().get(0).getDtstart().getDateTime();
		if (!isCalendarWithinMinutes(dtstart, minutesOffset, 3)) {
			throw new FailedException("UpdateReport dtstart is not within plus or minus 3 minutes of current time minus " + minutesOffset);
		}

		int intervalSize = updateReport.getOadrReport().get(0).getIntervals().getInterval().size();
		if (intervalSize < minIntervalCount) {
			throw new FailedException("At least " + minIntervalCount + " intervals of data are returned. Got " + intervalSize);
		}
	}

	protected void checkUpdateReport_lastInterval(OadrUpdateReportType updateReport, int minutesOffset, int minIntervalCount) {
		int intervalSize = updateReport.getOadrReport().get(0).getIntervals().getInterval().size();
		if (intervalSize < minIntervalCount) {
			throw new FailedException("At least " + minIntervalCount + " intervals of data are returned. Got " + intervalSize);
		}
		//Check for last interval to match current time.
		XMLGregorianCalendar dtstart = updateReport.getOadrReport().get(0).getIntervals().getInterval().get(intervalSize-1).getDtstart().getDateTime();
		if (!isCalendarWithinMinutes(dtstart, minutesOffset, 3)) {
			throw new FailedException("UpdateReport last interval dtstart is not within plus or minus 3 minutes of current time minus " + minutesOffset);
		}

	}

	protected void telemetry_Usage_CBP_DBCheck(){
		XMLDBUtil xmlDBUtil = new XMLDBUtil();
		Node reportNode = xmlDBUtil.getReportFromVenByName(REPORT_TELEMETRY_USAGE);
		if(reportNode == null){
			String ptext ="Failed to find an appropriately registered report for TELEMETRY_USAGE\n";
			throw new FailedException(ptext);
		}
		NodeList nodes = reportNode.getChildNodes();
		boolean fnd = false;
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element eachDataPoint = (Element) node;
				String stype = xmlDBUtil.getAttributeValue(eachDataPoint, "type");
				String oadrMaxPeriod = xmlDBUtil.getAttributeValue(eachDataPoint, "oadrMaxPeriod");
				String oadrMinPeriod = xmlDBUtil.getAttributeValue(eachDataPoint, "oadrMinPeriod");
				String granDur = properties.get("Nominal_Expected_Report_Granularity");
				String reportType= xmlDBUtil.getAttributeValue(eachDataPoint, "reportType");
				String readingType= xmlDBUtil.getAttributeValue(eachDataPoint, "readingType");
				String rID= xmlDBUtil.getAttributeValue(eachDataPoint, "rID");

				if((stype.equals("PowerReal")&& wellKnownCheck(rID,properties.get("TelemetryUsage_PowerReal_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("usage")){
							if(readingType.contentEquals("Direct Read")){
								fnd = true;
							}
						}
					}
				}
			}
		}
		if(!fnd){
			String ptext ="Failed to find an appropriately registered report for TELEMETRY_USAGE with the following characteristics:\n";
			ptext +="Report Granularity of " + properties.get("Nominal_Expected_Report_Granularity") + " should fall on or between oadrMinPeriod and oadrMaxPeriod \n";
			ptext +="Report Type: usage\n";
			ptext +="Reading Type: Direct Read\n";
			ptext +="Units: powerReal\n";
			ptext +="Refer to the db/vtndb.xml file for reports currently registered with the test harness.\n";
			throw new FailedException(ptext);
		}

	}

	//Verify expected reports are available in oadrRegisterRegisterReport as required by rule  510 and 331
		protected void ven_Bootstrap_Report_DBCheck(){
			XMLDBUtil xmlDBUtil = new XMLDBUtil();
			
			//Confirm Telemetry_Usage present
			Node reportNode = xmlDBUtil.getReportFromVenByName(REPORT_TELEMETRY_USAGE);
			boolean fnd = false;

			if (reportNode != null){
				NodeList nodes = reportNode.getChildNodes();			
				for (int i = 0; i < nodes.getLength(); i++) {
					Node node = nodes.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element eachDataPoint = (Element) node;
						String stype = xmlDBUtil.getAttributeValue(eachDataPoint, "type");
						String reportType= xmlDBUtil.getAttributeValue(eachDataPoint, "reportType");
						String readingType= xmlDBUtil.getAttributeValue(eachDataPoint, "readingType");
		
						if((stype.equals("PowerReal"))|| (stype.equals("EnergyReal")) || (stype.equals("PulseCount"))){
							if(reportType.contentEquals("usage")){
								if(readingType.contentEquals("Direct Read")){
									fnd = true;
								}
							}				
						}
						
					}
				}
			}

			//Confirm Telemetry_Status present
			Node reportNode1 = xmlDBUtil.getReportFromVenByName(REPORT_TELEMETRY_STATUS);
			boolean fnd1 = false;
			if (reportNode1 != null){
				NodeList nodes1 = reportNode1.getChildNodes();
				for (int i = 0; i < nodes1.getLength(); i++) {
					Node node1 = nodes1.item(i);
					if (node1.getNodeType() == Node.ELEMENT_NODE) {
						Element eachDataPoint1 = (Element) node1;
						String reportType1= xmlDBUtil.getAttributeValue(eachDataPoint1, "reportType");
						String readingType1= xmlDBUtil.getAttributeValue(eachDataPoint1, "readingType");
						if(reportType1.contentEquals("x-resourceStatus")){
							if(readingType1.contentEquals("x-notApplicable")){
								fnd1 = true;
							}		
						}				
					}
				}
			}
			
			String ptext = "";
			if(!fnd){
				ptext ="\nFailed to find an appropriately registered report for TELEMETRY_USAGE with the following characteristics:\n";
				ptext +="Report Type: usage\n";
				ptext +="Reading Type: Direct Read\n";
				ptext +="Units: powerReal or EnergyReal or PulseCount\n\n";
			}
			if(!fnd1){
				ptext = ptext + "\nFailed to find an appropriately registered report for TELEMETRY_STATUS with the following characteristics:\n";
				ptext +="Report Type: x-resourceStatus\n";
				ptext +="Reading Type: x-notApplicable\n\n";
			}

			if (!fnd || !fnd1){
				ptext +="Refer to the db/vtndb.xml file for reports currently registered with the test harness.\n";
				throw new FailedException(ptext);
			}
			
		}

	
	protected void telemetry_Status_THR_DBCheck(){
		XMLDBUtil xmlDBUtil = new XMLDBUtil();
		Node reportNode = xmlDBUtil.getReportFromVenByName(REPORT_TELEMETRY_STATUS);
		if(reportNode == null){
			String ptext ="Failed to find an appropriately registered report for TELEMETRY_STATUS\n";
			throw new FailedException(ptext);
		}
		NodeList nodes = reportNode.getChildNodes();
		boolean[] fnd = {false,false,false,false,false,false,false,false,false,false};
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element eachDataPoint = (Element) node;
				String stype = xmlDBUtil.getAttributeValue(eachDataPoint, "type");
				String oadrMaxPeriod = xmlDBUtil.getAttributeValue(eachDataPoint, "oadrMaxPeriod");
				String oadrMinPeriod = xmlDBUtil.getAttributeValue(eachDataPoint, "oadrMinPeriod");
				String granDur = properties.get("Nominal_Expected_Report_Granularity");
				String reportType= xmlDBUtil.getAttributeValue(eachDataPoint, "reportType");
				String readingType= xmlDBUtil.getAttributeValue(eachDataPoint, "readingType");
				String rID= xmlDBUtil.getAttributeValue(eachDataPoint, "rID");

				if((stype.equals("") && !fnd[0] && wellKnownCheck(rID,properties.get("ThermostatState_Status_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("x-resourceStatus")){
							if(readingType.contentEquals("x-notApplicable")){
								fnd[0] = true;
							}
						}
					}
				}
				if((stype.equals("temperature") && !fnd[1] && wellKnownCheck(rID,properties.get("ThermostatState_CurrentTemp_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("setPoint")){
							if(readingType.contentEquals("x-notApplicable")){
								fnd[1] = true;
							}
						}
					}
				}
				if((stype.equals("temperature") && !fnd[2] && wellKnownCheck(rID,properties.get("ThermostatState_HeatTempSetting_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("setPoint")){
							if(readingType.contentEquals("x-notApplicable")){
								fnd[2] = true;
							}
						}
					}
				}
				if((stype.equals("temperature") && !fnd[3] && wellKnownCheck(rID,properties.get("ThermostatState_CoolTempSetting_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("setPoint")){
							if(readingType.contentEquals("x-notApplicable")){
								fnd[3] = true;
							}
						}
					}
				}
				if((stype.equals("BaseUnit") && !fnd[4] && wellKnownCheck(rID,properties.get("ThermostatState_HVACModeSetting_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("operatingState")){
							if(readingType.contentEquals("x-notApplicable")){
								fnd[4] = true;
							}
						}
					}
				}
				if((stype.equals("BaseUnit") && !fnd[5] && wellKnownCheck(rID,properties.get("ThermostatState_CurrentHVACMode_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("operatingState")){
							if(readingType.contentEquals("x-notApplicable")){
								fnd[5] = true;
							}
						}
					}
				}
				if((stype.equals("BaseUnit") && !fnd[6] && wellKnownCheck(rID,properties.get("ThermostatState_FanModeSetting_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("operatingState")){
							if(readingType.contentEquals("x-notApplicable")){
								fnd[6] = true;
							}
						}
					}
				}
				if((stype.equals("BaseUnit") && !fnd[7] && wellKnownCheck(rID,properties.get("ThermostatState_CurrentHoldMode_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("operatingState")){
							if(readingType.contentEquals("x-notApplicable")){
								fnd[7] = true;
							}
						}
					}
				}
				if((stype.equals("BaseUnit") && !fnd[8] && wellKnownCheck(rID,properties.get("ThermostatState_CurrentAwayMode_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("operatingState")){
							if(readingType.contentEquals("x-notApplicable")){
								fnd[8] = true;
							}
						}
					}
				}
				if((stype.equals("BaseUnit") && !fnd[9] && wellKnownCheck(rID,properties.get("ThermostatState_CurrentHumidity_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("operatingState")){
							if(readingType.contentEquals("x-notApplicable")){
								fnd[9] = true;
							}
						}
					}
				}
			}
		}
		if(!fnd[0] && !fnd[1] && !fnd[2] && !fnd[3] && !fnd[4] && !fnd[5] && !fnd[6] && !fnd[7] && !fnd[8] && !fnd[9]){
			String ptext ="Failed to find an appropriately registered report data point for TELEMETRY_STATUS with the following characteristics:\n";
			ptext +="Report Granularity of 1 minute should fall on or between oadrMinPeriod and oadrMaxPeriod \n";
			ptext +="Reading Type: x-notApplicable\n\n";
			if(!fnd[0]){
				ptext +="<Status>\n";
				ptext +="Report Type: x-resourceStatus (oadrOnline, oadrLevelOffset:current)\n";
				ptext +="Units: x-notApplicable\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_Status_WellKnown_rID") + "\n\n";
			}
			if(!fnd[1]){
				ptext +="<Current Temp>\n";
				ptext +="Report Type: setPoint\n";
				ptext +="Units: temperature\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CurrentTemp_WellKnown_rID") + "\n\n";
			}			
			if(!fnd[2]){
				ptext +="<Heat Temp Setting>\n";
				ptext +="Report Type:  setPoint\n";
				ptext +="Units: temperature\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_HeatTempSetting_WellKnown_rID") + "\n\n";
			}
			if(!fnd[3]){
				ptext +="<Cool Temp Setting>\n";
				ptext +="Report Type: setPoint\n";
				ptext +="Units: temperature\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CoolTempSetting_WellKnown_rID") + "\n\n";
			}			
			if(!fnd[4]){
				ptext +="<HVAC Mode Setting>\n";
				ptext +="Report Type: operatingState\n";
				ptext +="Units: customUnit\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_HVACModeSetting_WellKnown_rID") + "\n\n";
			}
			if(!fnd[5]){
				ptext +="<Current HVAC Mode>\n";
				ptext +="Report Type: operatingState\n";
				ptext +="Units: customUnit\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CurrentHVACMode_WellKnown_rID") + "\n\n";
			}			
			if(!fnd[6]){
				ptext +="<Fan Mode Setting>\n";
				ptext +="Report Type: operatingState\n";
				ptext +="Units: customUnit\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_FanModeSetting_WellKnown_rID") + "\n\n";
			}
			if(!fnd[7]){
				ptext +="<Current Hold Mode>\n";
				ptext +="Report Type: operatingState\n";
				ptext +="Units: customUnit\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CurrentHoldMode_WellKnown_rID") + "\n\n";
			}			
			if(!fnd[8]){
				ptext +="<Current Away Mode>\n";
				ptext +="Report Type: operatingState\n";
				ptext +="Units: customUnit\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CurrentAwayMode_WellKnown_rID") + "\n\n";
			}
			if(!fnd[9]){
				ptext +="<Current Humidity>\n";
				ptext +="Report Type: operatingState\n";
				ptext +="Units: customUnit (percent)\n";
				ptext +="Well Known rID Property: " + properties.get("ThermostatState_CurrentHumidity_WellKnown_rID") + "\n\n";
			}
			ptext +="Refer to the db/vtndb.xml file for reports currently registered with the test harness.\n";
			throw new FailedException(ptext);
		}
	}

	protected void telemetry_Usage_FDR_CMP_DBCheck(){
		XMLDBUtil xmlDBUtil = new XMLDBUtil();
		Node reportNode = xmlDBUtil.getReportFromVenByName(REPORT_TELEMETRY_USAGE);
		if(reportNode == null){
			String ptext ="Failed to find an appropriately registered report for TELEMETRY_USAGE\n";
			throw new FailedException(ptext);
		}
		NodeList nodes = reportNode.getChildNodes();
		boolean fnd = false;
		boolean fnd2 = false;
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element eachDataPoint = (Element) node;
				String stype = xmlDBUtil.getAttributeValue(eachDataPoint, "type");
				String oadrMaxPeriod = xmlDBUtil.getAttributeValue(eachDataPoint, "oadrMaxPeriod");
				String oadrMinPeriod = xmlDBUtil.getAttributeValue(eachDataPoint, "oadrMinPeriod");
				String granDur = "PT5S";
				String reportType= xmlDBUtil.getAttributeValue(eachDataPoint, "reportType");
				String readingType= xmlDBUtil.getAttributeValue(eachDataPoint, "readingType");
				String rID= xmlDBUtil.getAttributeValue(eachDataPoint, "rID");

				if((stype.equals("PowerReal") && wellKnownCheck(rID,properties.get("TelemetryUsage_PowerReal_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("usage")){
							if(readingType.contentEquals("Direct Read")){
								fnd = true;
							}
						}
					}
				}
				if((stype.equals("Voltage") && wellKnownCheck(rID,properties.get("TelemetryUsage_Voltage_WellKnown_rID")))){
					if(OadrUtil.createDuration(oadrMinPeriod).compare(OadrUtil.createDuration(granDur)) <= 0 &&
							OadrUtil.createDuration(oadrMaxPeriod).compare(OadrUtil.createDuration(granDur)) >= 0){
						if(reportType.contentEquals("usage")){
							if(readingType.contentEquals("Direct Read")){
								fnd2 = true;
							}
						}
					}
				}
			}
		}
		if(!fnd){
			String ptext ="Failed to find an appropriately registered report for TELEMETRY_USAGE with the following characteristics:\n";
			ptext +="Report Granularity of 5 seconds should fall on or between oadrMinPeriod and oadrMaxPeriod \n";
			ptext +="Report Type: usage\n";
			ptext +="Reading Type: Direct Read\n";
			ptext +="Units: powerReal\n";
			ptext +="Refer to the db/vtndb.xml file for reports currently registered with the test harness.\n";
			throw new FailedException(ptext);
		}
		if(!fnd2){
			String ptext ="Failed to find an appropriately registered report for TELEMETRY_USAGE with the following characteristics:\n";
			ptext +="Report Granularity of 5 seconds should fall on or between oadrMinPeriod and oadrMaxPeriod \n";
			ptext +="Report Type: usage\n";
			ptext +="Reading Type: Direct Read\n";
			ptext +="Units: voltage\n";
			ptext +="Refer to the db/vtndb.xml file for reports currently registered with the test harness.\n";
			throw new FailedException(ptext);
		}

	}

	protected void checkUpdateReportRID(int type) {
		List<OadrUpdateReportType> updateReports = TestSession.getOadrUpdateReportTypeReceivedList();
		ArrayList<String> getRIDsent = new ArrayList<String>();
		//This is duplicating the setups for CreateReportEventHelper so that we can compare what is requested to what was received.
		switch (type){
		case 7:
			//
			XMLDBUtil xmlDBUtil = new XMLDBUtil();
			Node reportNode = xmlDBUtil.getReportFromVenByName(REPORT_HISTORY_USAGE);
			NodeList nodes = reportNode.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element eachDataPoint = (Element) node;
					String rID = xmlDBUtil.getAttributeValue(eachDataPoint, "rID");
					getRIDsent.add(rID);
				}
			}
			break;
		case 8:
			xmlDBUtil = new XMLDBUtil();
			reportNode = xmlDBUtil.getReportFromVenByName(REPORT_HISTORY_USAGE);
			nodes = reportNode.getChildNodes();
			boolean fnd = false;
			String notFound = null;
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element eachDataPoint = (Element) node;
					String rID = xmlDBUtil.getAttributeValue(eachDataPoint, "rID");
					String stype = xmlDBUtil.getAttributeValue(eachDataPoint, "type");

					if (RID_ENERGY_REAL.equals(stype)) {
						getRIDsent.add(rID);
						fnd = true;
					}else if(notFound==null){
						notFound = rID;
					}
				}
			}
			if(!fnd){
				getRIDsent.add(notFound);
			}
			break;
		case 9:
			xmlDBUtil = new XMLDBUtil();
			reportNode = xmlDBUtil.getReportFromVenByName(REPORT_TELEMETRY_USAGE);
			nodes = reportNode.getChildNodes();
			fnd = false;
			notFound = null;
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element eachDataPoint = (Element) node;
					String rID = xmlDBUtil.getAttributeValue(eachDataPoint, "rID");
					String stype = xmlDBUtil.getAttributeValue(eachDataPoint, "type");

					if((stype.equals("EnergyReal") || stype.equals("PowerReal"))){
						getRIDsent.add(rID);
						fnd = true;
					}else if(notFound==null){
						notFound = rID;
					}
				}
			}
			if(!fnd){
				getRIDsent.add(notFound);
			}
			break;
		default:
			throw new FailedException("UpdateReport check for RID failed with unknown selection for RIDs sent. Requested type = " + type);
		}
		//a check for size 1 precedes this test
		ArrayList<String> getRIDrec = new ArrayList<String>();
		List <IntervalType> rptIntervals = updateReports.get(0).getOadrReport().get(0).getIntervals().getInterval();
		for (int i = 0; i < rptIntervals.size(); i++){
			getRIDrec.clear();
			List<JAXBElement<? extends StreamPayloadBaseType>> rptPayload = rptIntervals.get(i).getStreamPayloadBase();
			for (int j = 0; j < rptPayload.size(); j++){
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) rptPayload.get(j).getValue();
				String rID = reportPayload.getRID();
				getRIDrec.add(rID);
			}
			for (int j = 0; j < getRIDsent.size(); j++){
				if( getRIDrec.indexOf(getRIDsent.get(j)) == -1){
					throw new FailedException("UpdateReport check for RID failed to find RID " + getRIDsent.get(j) + " in interval " + j);
				}
			}
		}
		return;
	}


	protected void checkUpdateReport_x150(OadrUpdateReportType updateReport) {
		checkUpdateReport(updateReport, 0, 1);

		/*Node reportNode = new XMLDBUtil().getReportFromVenByName("TELEMETRY_USAGE");

		DataPoint energyRealDataPoint = getDataPoint(reportNode, "EnergyReal");
		DataPoint powerRealDataPoint = getDataPoint(reportNode, "PowerReal");
		if (energyRealDataPoint == null) {
			throw new FailedException("Expected EnergyReal rID in CreateReport.");
		}
		if (powerRealDataPoint == null) {
			throw new FailedException("Expected PowerReal rID in CreateReport.");
		}

		List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBases = updateReport.getOadrReport().get(0).getIntervals().getInterval().get(0).getStreamPayloadBase();
		for (JAXBElement<? extends StreamPayloadBaseType> streamPayloadBase : streamPayloadBases) {
			OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.getValue();

			String rID = reportPayload.getRID();
			if (!rID.equals(energyRealDataPoint.getRID()) && !rID.equals(powerRealDataPoint.getRID())) {
				throw new FailedException("CreateReport rIDs are not found in XML database.");
			}
		}*/
	}

	protected void checkUpdateReport_x160(OadrUpdateReportType updateReport) {
		int intervalSize = updateReport.getOadrReport().get(0).getIntervals().getInterval().size();
		if (intervalSize != 2) {
			throw new FailedException("Expected 2 intervals of data are returned. Got " + intervalSize);
		}

		/*
		Node reportNode = new XMLDBUtil().getReportFromVenByName("TELEMETRY_USAGE");

		DataPoint powerRealDataPoint = getDataPoint(reportNode, "PowerReal");
		if (powerRealDataPoint == null) {
			throw new FailedException("Expected PowerReal rID in CreateReport.");
		}

		List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBases = updateReport.getOadrReport().get(0).getIntervals().getInterval().get(0).getStreamPayloadBase();
		for (JAXBElement<? extends StreamPayloadBaseType> streamPayloadBase : streamPayloadBases) {
			OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.getValue();

			String rID = reportPayload.getRID();
			if (!rID.equals(powerRealDataPoint.getRID())) {
				throw new FailedException("CreateReport rIDs are not found in XML database.");
			}
		}
		 */
	}

	protected void checkUpdateReports_x160(OadrUpdateReportType updateReport1, OadrUpdateReportType updateReport2) {
		checkUpdateReport_x160(updateReport1);
		checkUpdateReport_x160(updateReport2);

		GregorianCalendar cal1 = updateReport1.getOadrReport().get(0).getDtstart().getDateTime().toGregorianCalendar();
		GregorianCalendar cal2 = updateReport2.getOadrReport().get(0).getDtstart().getDateTime().toGregorianCalendar();
		long diff = (cal2.getTimeInMillis() - cal1.getTimeInMillis()) / 1000;
		if (diff < 75 || diff > 165) { // 2 minutes apart +/-45 seconds
			throw new FailedException("Expected 2 OadrUpdateReports approximately 2 minutes apart. Diff=" + diff);
		}
	}

	protected void checkSameReportRequestID(OadrCreateReportType createReport, OadrCreatedReportType createdReport) {
		OadrPendingReportsType pendingReports = createdReport.getOadrPendingReports();
		List<String> reportRequestIDs = pendingReports.getReportRequestID();
		if (pendingReports == null || reportRequestIDs.isEmpty()) {
			throw new FailedException("Expected oadrPendingReports/reportRequestID in CreatedReport payload");
		}

		boolean found = false;
		String createReportRequestID = createReport.getOadrReportRequest().get(0).getReportRequestID();
		for (String createdReportRequestID : reportRequestIDs) {
			if (createReportRequestID.equals(createdReportRequestID)) {
				found = true;
				break;
			}
		}

		if (!found) {
			throw new FailedException("ReportRequestID CreateReport/CreatedReport mismatch, expected " + createReportRequestID);
		}
	}

	// unused
	protected void checkDifferentReportRequestID(OadrCreateReportType createReport, OadrCreatedReportType createdReport) {
		String createReportRequestID = createReport.getOadrReportRequest().get(0).getReportRequestID();
		String createdReportRequestID = createdReport.getOadrPendingReports().getReportRequestID().get(0);
		if (createReportRequestID.equals(createdReportRequestID)) {
			throw new FailedException("ReportRequestID match, " + createReportRequestID + " == " + createdReportRequestID);
		}
	}

	protected void checkSameReportRequestID(OadrCreateReportType createReport, OadrUpdateReportType updateReport) {
		String createReportRequestID = createReport.getOadrReportRequest().get(0).getReportRequestID();
		String updateReportRequestID = updateReport.getOadrReport().get(0).getReportRequestID();
		if (!createReportRequestID.equals(updateReportRequestID)) {
			throw new FailedException("ReportRequestID CreateReport/UpdateReport mismatch, " + createReportRequestID + " != " + updateReportRequestID);
		}
	}

	protected void checkCanceledReportPendingReports(OadrCanceledReportType canceledReport, int size) {
		int listSize = canceledReport.getOadrPendingReports().getReportRequestID().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " OadrCanceledReport OadrPendingReport(s). Got " + listSize);
		}
	}

	/*
	private void checkUpdateReport_x170(OadrCreateReportType createReport, OadrUpdateReportType updateReport) {
		checkSameReportRequestID(createReport, updateReport);

		List<String> reportNames = new ArrayList<>();
		reportNames.add("METADATA_TELEMETRY_STATUS");
		reportNames.add("METADATA_TELEMETRY_USAGE");
		reportNames.add("METADATA_HISTORY_USAGE");

		for (OadrReportType report : updateReport.getOadrReport()) {
			// if (!report.getReportSpecifierID().equals("METADATA")) {
			//	throw new FailedException("Expected METADATA reportSpecifier. Got " + report.getReportSpecifierID());
			// }

			String reportName = report.getReportName();
			int index = reportNames.indexOf(reportName);
			if (index == -1) {
				throw new FailedException("Expected ReportName " + reportName + " not found.");
			} else {
				reportNames.remove(index);
			}
		}

		if (reportNames.size() > 0) {
			throw new FailedException("Expected ReportName " + reportNames.get(0) + " not found.");
		}
	}
	 */

	protected OadrUpdateReportType waitForUpdateReport(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			if (TestSession.getOadrUpdateReportTypeReceivedList().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}
		pause(2);

		int listSize = TestSession.getOadrUpdateReportTypeReceivedList().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " UpdateReport(s), received " + listSize);
		}

		checkForValidationErrors();

		return TestSession.getOadrUpdateReportTypeReceivedList().get(size - 1);
	}

	protected void checkCreateReportReceived(int size) {
		List<OadrCreateReportType> createReports = TestSession.getOadrCreateReportTypeReceivedList();
		if (createReports.size() != size) {
			throw new FailedException("Expected " + size + " OadrCreateReport(s), received " + createReports.size());
		}
	}

	protected IResponseCanceledReportTypeAckAction addCanceledReportResponse() {
		IResponseCanceledReportTypeAckAction canceledReportAction = new DefaultResponseCanceledReportTypeAckAction();
		ResponseCanceledReportTypeAckActionList.addResponseCanceledReportAckAction(canceledReportAction);
		return canceledReportAction;
	}

	protected OadrCreatedEventType waitForCreatedEvent(int size) {
		long testCaseTimeout = OadrUtil.getTestCaseTimeout();
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			if (TestSession.getCreatedEventReceivedList().size() >= size) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}
		pause(2);

		int listSize = TestSession.getCreatedEventReceivedList().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " CreatedEvent(s), received " + listSize);
		}

		checkForValidationErrors();

		return TestSession.getCreatedEventReceivedList().get(size - 1);
	}

	protected OadrCreatedEventType checkCreatedEvent(int size) {
		int listSize = TestSession.getCreatedEventReceivedList().size();
		if (listSize != size) {
			throw new FailedException("Expected " + size + " CreatedEvent(s), received " + listSize);
		}

		return TestSession.getCreatedEventReceivedList().get(size - 1);
	}

	protected void checkCreatedEvent(int size, String responseCode) {
		OadrCreatedEventType createdEvent = checkCreatedEvent(size);
		String receivedResponseCode = createdEvent.getEiCreatedEvent().getEiResponse().getResponseCode();
		if (!receivedResponseCode.equals(responseCode)) {
			throw new FailedException("Expected OadrCreatedEvent " + responseCode + ", received " + receivedResponseCode);
		}
	}

	protected void checkUpdateReport_Rx180() {
		List<OadrUpdateReportType> updateReports = TestSession.getOadrUpdateReportTypeReceivedList();

		if (updateReports.size() == 1) {
			OadrUpdateReportType updateReport = updateReports.get(0); 
			List<OadrReportType> reports = updateReport.getOadrReport();
			if (reports.size() != 2) {
				throw new FailedException("Expected 2 OadrUpdateReport OadrReport(s), received " + reports.size());
			}
		} else if (updateReports.size() == 2) {
			OadrUpdateReportType updateReport1 = updateReports.get(0); 
			OadrUpdateReportType updateReport2 = updateReports.get(1); 

			List<OadrReportType> reports1 = updateReport1.getOadrReport();
			List<OadrReportType> reports2 = updateReport2.getOadrReport();
			int totalSize = reports1.size() + reports2.size(); 
			// if (reports1.size() != 2 || reports2.size() != 2 || totalSize != 4) {
			if (reports1.size() != 1 || reports2.size() != 1 || totalSize != 2) {
				throw new FailedException("Expected total of 2 OadrUpdateReport OadrReport(s), 1 OadrReport from each OadrUpdateReport");
			}
		} else {
			throw new FailedException("Expected 1 or 2 OadrUpdateReport(s), received " + updateReports.size());
		}
	}

	protected void checkCreatedEvent(OadrCreatedEventType createdEvent, String eventResponseCode) {
		String eiResponseCode = createdEvent.getEiCreatedEvent().getEiResponse().getResponseCode();
		if (!eiResponseCode.equals(ErrorConst.OK_200)) {
			throw new FailedException("Expected a responseCode of 200 in EiResponse and responseCode of " + eventResponseCode + " in EventResponse, got " + eiResponseCode + " EiResponse");
		}

		String receivedEventResponseCode = createdEvent.getEiCreatedEvent().getEventResponses().getEventResponse().get(0).getResponseCode();
		if (!receivedEventResponseCode.equals(eventResponseCode)) {
			throw new FailedException("Expected a responseCode of 200 in EiResponse and responseCode of " + eventResponseCode + " in EventResponse, got " + receivedEventResponseCode + " EventResponse");
		}
	}
	
	protected void checkCreatedEvent_two(OadrCreatedEventType createdEvent, String eventResponseCode, String eventResponseCode1) {
		String eiResponseCode = createdEvent.getEiCreatedEvent().getEiResponse().getResponseCode();
		if (!eiResponseCode.equals(ErrorConst.OK_200)) {
			throw new FailedException("Expected a responseCode of 200 in EiResponse and responseCode of " + eventResponseCode + " in EventResponse, got " + eiResponseCode + " EiResponse");
		}

		String receivedEventResponseCode = createdEvent.getEiCreatedEvent().getEventResponses().getEventResponse().get(0).getResponseCode();
		if (!receivedEventResponseCode.equals(eventResponseCode) && !receivedEventResponseCode.equals(eventResponseCode1)) {
			throw new FailedException("Expected a responseCode of 200 in EiResponse and responseCode of " + eventResponseCode + " or " +  eventResponseCode1 + " in EventResponse, got " + receivedEventResponseCode + " EventResponse");
		}
	}

	protected void checkCreatedEvent_Ex040() {
		int listSize = TestSession.getCreatedEventReceivedList().size();

		if (listSize == 1) {
			OadrCreatedEventType createdEvent = TestSession.getCreatedEventReceivedList().get(0);

			List<EventResponse> eventResponses = createdEvent.getEiCreatedEvent().getEventResponses().getEventResponse();
			if (eventResponses.size() != 2) {
				throw new FailedException("Expected 2 EventResponses, received " + eventResponses.size());
			}

			OptTypeType optType1 = eventResponses.get(0).getOptType();
			OptTypeType optType2 = eventResponses.get(1).getOptType();
			if ((optType1 != OptTypeType.OPT_IN) || (optType2 != OptTypeType.OPT_IN)) {
				throw new FailedException("Expected 2 EventResponses with OPT_IN");
			}

			System.out.println("1 Created Event");			
		} else if (listSize == 2) {
			OadrCreatedEventType createdEvent1 = TestSession.getCreatedEventReceivedList().get(0);	
			OadrCreatedEventType createdEvent2 = TestSession.getCreatedEventReceivedList().get(1);	

			OptTypeType optType1 = createdEvent1.getEiCreatedEvent().getEventResponses().getEventResponse().get(0).getOptType();
			OptTypeType optType2 = createdEvent2.getEiCreatedEvent().getEventResponses().getEventResponse().get(0).getOptType();
			if ((optType1 != OptTypeType.OPT_IN) || (optType2 != OptTypeType.OPT_IN)) {
				throw new FailedException("Expected 2 EventResponses with OPT_IN");
			}

			System.out.println("2 Created Events");			
		} else {
			throw new FailedException("Expected 1 or 2 CreatedEvent(s), received " + listSize);
		}
	}

	protected void waitForOptionalCreatedEvent() {
		long testCaseTimeout = System.currentTimeMillis() + (30 * 1000);
		while (System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			if (TestSession.getCreatedEventReceivedList().size() >= 2) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}
		pause(2);

		checkForValidationErrors();
	}

	protected void checkCreatedReport(int size) {
		List<OadrCreatedReportType> createdReports = TestSession.getOadrCreatedReportTypeReceivedList();
		if (createdReports.size() != size) {
			throw new FailedException("Expected " + size + " OadrCreatedReport(s), received " + createdReports.size());
		}
	}

	protected void checkCreatedEventReceived(OadrDistributeEventType distributeEvent, ICreatedEventResult createdEvent) {
		if (!createdEvent.isExpectedCreatedEventReceived()) {
			ArrayList<OadrCreateOptType> createOpts = TestSession.getCreateOptEventReceivedList();

			int expectedOptOutCount = createdEvent.getExpectedCreateOpt_OptOutCount();
			int receivedOptOutCount = DistributeEventSignalHelper.numberOfOptOutCreateOptReceived(distributeEvent, createOpts);
			if (expectedOptOutCount != receivedOptOutCount) {
				throw new FailedException("Expected " + expectedOptOutCount + " CreatOpt of type optOut, got " + receivedOptOutCount);
			} else {
				throw new FailedException("OadrCreateOpt has not been received.");
			}
		}
	}

	protected void waitForUpdateReports_Rx030(OadrCreateReportType createReport1, OadrCreateReportType createReport2) {
		String reportRequestID1 = createReport1.getOadrReportRequest().get(0).getReportRequestID();
		String reportRequestID2 = createReport2.getOadrReportRequest().get(0).getReportRequestID();

		int updateReportCount1 = 0;
		int updateReportCount2 = 0;

		long testCaseTimeout = System.currentTimeMillis() + (5 * 60 * 1000); // 5 minutes
		while(System.currentTimeMillis() < testCaseTimeout) {
			pause(1);

			updateReportCount1 = UpdateReportEventHelper.numberOfReportsReceivedForReportRequestID(reportRequestID1);
			updateReportCount2 = UpdateReportEventHelper.numberOfReportsReceivedForReportRequestID(reportRequestID2);

			if (updateReportCount1 >= 2 && updateReportCount2 >= 2) {
				break;
			} else if (TestSession.isAtleastOneValidationErrorPresent()) {
				break;
			}
		}

		if (updateReportCount1 < 2 || updateReportCount2 < 2) {
			throw new FailedException("Expected 2 or more UpdateReports for " + reportRequestID1 + " and 2 or more Update Reports for " + reportRequestID2 + ".");
		}
	}
	protected boolean wellKnownCheck(String rID, String sCheck){

		if(properties.get("disable_DR_Guide_WellKnown_rID_Check").toLowerCase().contentEquals("true"))
			return true;
		
		if(rID.contentEquals(sCheck))
			return true;
		
		if(rID.indexOf(":") < 0)
			return false;
		
		String sToColon = rID.substring(0, rID.indexOf(":"));
		if(sToColon.contentEquals(sCheck))
			return true;
		
		return false;
	}
}
