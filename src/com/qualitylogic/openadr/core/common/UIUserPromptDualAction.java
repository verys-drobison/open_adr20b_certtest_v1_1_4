package com.qualitylogic.openadr.core.common;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

import com.qualitylogic.openadr.core.util.PropertiesFileReader;
import com.qualitylogic.openadr.core.util.ResourceFileReader;
import com.qualitylogic.openadr.core.util.Trace;

public class UIUserPromptDualAction implements IPrompt {
	public int SMALL = 0;
	public int MEDIUM = 1;
	public int LARGE = 2;
	public int VERYLARGE = 3;

	JFrame myJframe = new JFrame("");
	String promptMessage = "";

	public String getPromptMessage() {
		return promptMessage;
	}

	public UIUserPromptDualAction() {

	}

	public UIUserPromptDualAction(String promptMessage) {
		this.promptMessage = promptMessage;
	}

	private static int autoDetermineSize(String promptMessage) {
		int newlineCount = promptMessage.length() - promptMessage.replaceAll("\n", "").length();
		
		int size = UIUserPrompt.SMALL;
		if (promptMessage.length() > (870 - (newlineCount * 60))) {
			size = UIUserPrompt.VERYLARGE;
		} else if(promptMessage.length() > (570 - (newlineCount * 60))) {
			size = UIUserPrompt.LARGE;
		} else if (promptMessage.length() > (165 - newlineCount * 45)) {
			size = UIUserPrompt.MEDIUM;
		}
		return size;
	}
	
	// Default prompt.
	public void Prompt(String question) {
		Prompt(question, autoDetermineSize(question));
	}

	public void Prompt(String question, int size) {
		PropertiesFileReader properties = new PropertiesFileReader(); 
		if ((!properties.isTestPrompt() || !TestSession.getBdoPrompt()) && !TestSession.getBalwaysPrompt()) {
			ResourceFileReader resources = new ResourceFileReader();
			if (question.equals(resources.TestCase_0280_UIUserPromptDualActionQuestion())) {
				try {
					Thread.sleep(30 * 1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
			
			TestSession.setUiDualActionYesOptionClicked(true);
			TestSession.setUserClickedToCompleteUIDualAction(true);
			TestSession.setUserClickedContinuePlay(true);
			return;
		}
		//Check for variables surrounded by ![ !], drop the ![!] and add an = and the value after
		//Check for variables surrounded by &[ &], replace with value
		String stmp;
		Integer ib;
		Integer ie;
		while(true){
			ib = question.indexOf("![");
			if (ib == -1){
				break;
			} else {
				ie = question.indexOf("!]", ib);
				if (ie == -1){
					break;
				} else {
					stmp  = properties.get(question.substring(ib+2, ie));
					question = question.substring(0,ib) + question.substring(ib+2, ie) + " = " + stmp + question.substring(ie + 2);
				}
				
			}
			
		}
		while(true){
			ib = question.indexOf("&[");
			if (ib == -1){
				break;
			} else {
				ie = question.indexOf("&]", ib);
				if (ie == -1){
					break;
				} else {
					stmp  = properties.get(question.substring(ib+2, ie));
					question = question.substring(0,ib) + stmp + question.substring(ie + 2);
				}
				
			}
			
		}
		
		TestSession.setUserClickedToCompleteUIDualAction(false);
		TestSession.setUserClickedContinuePlay(false);

		myJframe.setAlwaysOnTop(true);
		myJframe.setVisible(true);
		myJframe.setSize(400, 300);
		myJframe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		myJframe.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				myJframe.setAlwaysOnTop(false);
				int confirmed = JOptionPane
						.showConfirmDialog(
								null,
								"Are you sure you want to exit and cancel the TestCase execution?",
								"User Confirmation", JOptionPane.YES_NO_OPTION);

				if (confirmed == JOptionPane.YES_OPTION) {
					TestSession.setUserClickedToCancelUIDualAction(true);
					exit(myJframe);
				} else {
					myJframe.setAlwaysOnTop(true);
				}
			}
		});

		JPanel panel = new JPanel();

		panel.setLayout(null);
		JButton yesButton = new JButton("Yes");

		if (size == SMALL) {
			yesButton.setBounds(120, 105, 80, 25);
		} else if (size == MEDIUM) {
			yesButton.setBounds(170, 205, 80, 25);
		} else if (size == LARGE) {
			yesButton.setBounds(220, 305, 80, 25);
		} else {
			yesButton.setBounds(260, 605, 80, 25);
		}

		yesButton
				.addActionListener(new UIUserPromptDualAction().new ButtonListener(
						myJframe));

		JButton noButton = new JButton("No");

		if (size == SMALL) {
			noButton.setBounds(205, 105, 80, 25);
		} else if (size == MEDIUM) {
			noButton.setBounds(255, 205, 80, 25);
		} else if (size == LARGE) {
			noButton.setBounds(305, 305, 80, 25);
		} else {
			noButton.setBounds(355, 605, 80, 25);
		}

		noButton.addActionListener(new UIUserPromptDualAction().new ButtonListener(
				myJframe));
		ResourceFileReader resourceFileReader = new ResourceFileReader();
		JTextArea _resultArea = new JTextArea();
		_resultArea.setEditable(false);
		_resultArea.setText(question  + resourceFileReader.TestCase_Name());
		if (size == SMALL) {
			_resultArea.setBounds(0, 0, 385, 100);
		} else if (size == MEDIUM) {
			_resultArea.setBounds(0, 0, 485, 200);
		} else if (size == LARGE)  {
			_resultArea.setBounds(0, 0, 585, 300);
		} else {
			_resultArea.setBounds(0, 0, 750, 600);
		}

		_resultArea.setLineWrap(true);
		_resultArea.setWrapStyleWord(true);
		_resultArea.setMargin(new Insets(6, 6, 6, 6));
		Font font = new Font("Verdana", Font.PLAIN, 12);
		_resultArea.setFont(font);

        JScrollPane scr = new JScrollPane(_resultArea,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);// Add your text area to scroll pane 
        scr.setBounds(0, 0, 750, 600);
		if(size == VERYLARGE){
			panel.add(scr);
		} else {
			panel.add(_resultArea);
		}
		
		//panel.add(_resultArea);
		panel.add(yesButton);
		panel.add(noButton);
		myJframe.add(panel);

		myJframe.setTitle("OpenADR TestHarness");
		if (size == SMALL) {
			myJframe.setSize(400, 175);
		} else if (size == MEDIUM) {
			myJframe.setSize(500, 275);
		} else if (size == LARGE)  {
			myJframe.setSize(600, 375);
		} else {
			myJframe.setSize(765, 675);
		}

		myJframe.setLocationRelativeTo(null);
		Trace trace = TestSession.getTraceObj();
		String userActionWaitingText = "\nWaiting for user action : "
				+ new Date() + "\n";
		if (trace != null) {
			trace.getLogFileContentTrace().append(userActionWaitingText);
		}
		System.out.print(userActionWaitingText);

		pauseForUserActionOnDualActionDialog();

	}

	class ButtonListener implements ActionListener {
		JFrame jframe;

		ButtonListener(JFrame myJframe) {
			this.jframe = myJframe;
		}

		public void actionPerformed(ActionEvent e) {
			String actionReceived = "User action received. Playing Test Case again. :"
					+ new Date() + "\n";
			Trace trace = TestSession.getTraceObj();
			if (trace != null) {
				trace.getLogFileContentTrace().append(actionReceived);
			}
			System.out.print(actionReceived);
			JButton o = (JButton) e.getSource();
			String label = o.getText();

			if (label.equals("Yes")) {
				TestSession.setUiDualActionYesOptionClicked(true);
				System.out.print("\nYes Clicked\n");

			} else if (label.equals("No")) {
				TestSession.setUiDualActionYesOptionClicked(false);
				System.out.print("\nNo Clicked\n");
			}
			TestSession.setUserClickedToCompleteUIDualAction(true);
			TestSession.setUserClickedContinuePlay(true);
			jframe.dispose();
		}
	}

	static void exit(JFrame j) {
		String actionReceivedTime = "Received user action :" + new Date()
				+ "\n";
		String actionReceived = "\nUser action received. Cancelling Test Case execution.\n";

		Trace trace = TestSession.getTraceObj();
		if (trace != null) {
			trace.getLogFileContentTrace().append(actionReceivedTime);
			trace.getLogFileContentTrace().append(actionReceived);
		}
		System.out.print(actionReceivedTime);
		System.out.print(actionReceived);

		TestSession.setUserClickedToCompleteUIDualAction(true);
		TestSession.setUserCancelledCompleteUIDualAction(true);
		j.dispose();
	}

	public static void pauseForUserActionOnDualActionDialog() {
		while (!TestSession.isUserClickedToCompleteUIDualAction()) {
			System.out.print("");

			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
			}

		}
	}
}
