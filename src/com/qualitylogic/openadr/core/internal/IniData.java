package com.qualitylogic.openadr.core.internal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class IniData {

	static Log logger = LogFactory.getLog(Class.class);
	public Double psclWid = 0.33d;
	public Double logSplit = 0.66d;
	public int pscrLocX = 50;
	public int pscrLocY = 50;
	public int pscrSzHgt = 600;
	public int pscrSzWid = 800;
	public boolean bdoScroll = true;
	public boolean bdoPause = false;
	public boolean bnoPrompt = false;
	public Integer saveSelected = 0;
	public ArrayList<Integer> runListNum = new ArrayList<Integer>();

	public IniData() {
	}
	public void readIni(){
		try {
			String workingDir = System.getProperty("user.dir");
			logger.debug("Current working directory : " + workingDir);
			File foadrIni=new File("oadr.ini");
			if(foadrIni.exists()){
				logger.info("Found " + foadrIni.getAbsolutePath().toString() + " to override settings.");
				FileReader fro = new FileReader("oadr.ini" );
				BufferedReader bro = new BufferedReader( fro );
				String stringFromFile = bro.readLine( );
				String overRide = "";
				//read in the file and drop the comments
				if( stringFromFile != null && !stringFromFile.trim().startsWith("#")) overRide += stringFromFile;
				while( stringFromFile != null ) // end of the file
				{
					stringFromFile = bro.readLine( );  // read next line
					if(stringFromFile != null && !stringFromFile.trim().startsWith("#")) overRide += stringFromFile;
				}
				String tmpString;
				bro.close( );
				fro.close();
				int ib;
				int ie;
				ib=overRide.indexOf("psclWid");
				if (ib != -1){
					ib=overRide.indexOf("\"",ib);
					ie=overRide.indexOf("\"",ib+1);
					tmpString=overRide.substring(ib+1, ie);
					try{
						psclWid = Double.parseDouble(tmpString);
					}catch (Exception e){
						logger.info("Failed to load psclWid with " + tmpString + "\nDue to: " + e.getMessage());
					}
				}
				ib=overRide.indexOf("logSplit");
				if (ib != -1){
					ib=overRide.indexOf("\"",ib);
					ie=overRide.indexOf("\"",ib+1);
					tmpString=overRide.substring(ib+1, ie);
					try{
						logSplit = Double.parseDouble(tmpString);
					}catch (Exception e){
						logger.info("Failed to load logSplit with " + tmpString + "\nDue to: " + e.getMessage());
					}
				}
				ib=overRide.indexOf("pscrLocX");
				if (ib != -1){
					ib=overRide.indexOf("\"",ib);
					ie=overRide.indexOf("\"",ib+1);
					tmpString=overRide.substring(ib+1, ie);
					try{
						pscrLocX = Integer.parseInt(tmpString.trim());
					}catch (Exception e){
						logger.info("Failed to load pscrLocX with " + tmpString + "\nDue to: " + e.getMessage());
					}
				}
				ib=overRide.indexOf("pscrLocY");
				if (ib != -1){
					ib=overRide.indexOf("\"",ib);
					ie=overRide.indexOf("\"",ib+1);
					tmpString=overRide.substring(ib+1, ie);
					try{
						pscrLocY = Integer.parseInt(tmpString.trim());
					}catch (Exception e){
						logger.info("Failed to load pscrLocY with " + tmpString + "\nDue to: " + e.getMessage());
					}
				}
				ib=overRide.indexOf("pscrSzHgt");
				if (ib != -1){
					ib=overRide.indexOf("\"",ib);
					ie=overRide.indexOf("\"",ib+1);
					tmpString=overRide.substring(ib+1, ie);
					try{
						pscrSzHgt = Integer.parseInt(tmpString.trim());
					}catch (Exception e){
						logger.info("Failed to load pscrSzHgt with " + tmpString + "\nDue to: " + e.getMessage());
					}
				}
				ib=overRide.indexOf("pscrSzWid");
				if (ib != -1){
					ib=overRide.indexOf("\"",ib);
					ie=overRide.indexOf("\"",ib+1);
					tmpString=overRide.substring(ib+1, ie);
					try{
						pscrSzWid = Integer.parseInt(tmpString.trim());
					}catch (Exception e){
						logger.info("Failed to load pscrSzWid with " + tmpString + "\nDue to: " + e.getMessage());
					}
				}
				ib=overRide.indexOf("bdoScroll");
				if (ib != -1){
					ib=overRide.indexOf("\"",ib);
					ie=overRide.indexOf("\"",ib+1);
					tmpString=overRide.substring(ib+1, ie);
					try{
						bdoScroll = Boolean.parseBoolean(tmpString);
					}catch (Exception e){
						logger.info("Failed to load bdoScroll with " + tmpString + "\nDue to: " + e.getMessage());
					}
				}
				ib=overRide.indexOf("bdoPause");
				if (ib != -1){
					ib=overRide.indexOf("\"",ib);
					ie=overRide.indexOf("\"",ib+1);
					tmpString=overRide.substring(ib+1, ie);
					try{
						bdoPause = Boolean.parseBoolean(tmpString);
					}catch (Exception e){
						logger.info("Failed to load bdoPause with " + tmpString + "\nDue to: " + e.getMessage());
					}
				}
				ib=overRide.indexOf("bnoPrompt");
				if (ib != -1){
					ib=overRide.indexOf("\"",ib);
					ie=overRide.indexOf("\"",ib+1);
					tmpString=overRide.substring(ib+1, ie);
					try{
						bnoPrompt = Boolean.parseBoolean(tmpString);
					}catch (Exception e){
						logger.info("Failed to load bnoPrompt with " + tmpString + "\nDue to: " + e.getMessage());
					}
				}
				ib=overRide.indexOf("saveSelected");
				if (ib != -1){
					ib=overRide.indexOf("\"",ib);
					ie=overRide.indexOf("\"",ib+1);
					tmpString=overRide.substring(ib+1, ie);
					try{
						saveSelected = Integer.parseInt(tmpString.trim());
					}catch (Exception e){
						logger.info("Failed to load saveSelected with " + tmpString + "\nDue to: " + e.getMessage());
					}
				}
				ib=overRide.indexOf("runListNum");
				if (ib != -1){
					ib=overRide.indexOf("\"",ib);
					ie=overRide.indexOf("\"",ib+1);
					tmpString=overRide.substring(ib+1, ie);
					if(tmpString == null || tmpString.equals("")){
						runListNum.clear();
					}else{
						try{
							runListNum.clear();
							String tmp[] = tmpString.split(",");
							for(int j=0;j<tmp.length;j++){
								runListNum.add(Integer.parseInt(tmp[j].trim()));
							}
						}catch (Exception e){
							logger.info("Failed to parse runListNum with " + tmpString + "\nDue to: " + e.getMessage());
						}
					}
				}
			} else {
				logger.info("oadr.ini not found to override settings.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void writeIni(){
		try {
			File foadrIni=new File("oadr.ini");
			String el = System.getProperty("line.separator");
			String overRide = "";
			if(foadrIni.exists()){
				logger.info("Found " + foadrIni.getAbsolutePath().toString() + " to save override settings.");
				//read in the file to get the comments first.
				FileReader fro = new FileReader("oadr.ini" );
				BufferedReader bro = new BufferedReader( fro );
				String stringFromFile = bro.readLine( );
				if( stringFromFile != null && stringFromFile.trim().startsWith("#")) overRide += stringFromFile + el;
				while( stringFromFile != null ) // end of the file
				{
					stringFromFile = bro.readLine( );  // read next line
					if(stringFromFile != null && stringFromFile.trim().startsWith("#")) overRide += stringFromFile + el;
				}
				bro.close();
				fro.close();
			}
			if(overRide.contentEquals(""))overRide = "# oadr initialization" + el;

			FileWriter fw = new FileWriter("oadr.ini");
			fw.write(overRide);
			overRide = "psclWid = \"" + Double.toString(psclWid) + "\"" + el;
			fw.write(overRide);
			overRide = "logSplit = \"" + Double.toString(logSplit) + "\"" + el;
			fw.write(overRide);
			overRide = "pscrLocX = \"" + Integer.toString(pscrLocX) + "\"" + el;
			fw.write(overRide);
			overRide = "pscrLocY = \"" + Integer.toString(pscrLocY) + "\"" + el;
			fw.write(overRide);
			overRide = "pscrSzHgt = \"" + Integer.toString(pscrSzHgt) + "\"" + el;
			fw.write(overRide);
			overRide = "pscrSzWid = \"" + Integer.toString(pscrSzWid) + "\"" + el;
			fw.write(overRide);
			overRide = "saveSelected = \"" + Integer.toString(saveSelected) + "\"" + el;
			fw.write(overRide);
			String sTrue = "false";
			if (bdoScroll) sTrue="true";
			overRide = "bdoScroll = \"" + sTrue + "\"" + el;
			fw.write(overRide);
			sTrue = "false";
			if (bdoPause) sTrue="true";
			overRide = "bdoPause = \"" + sTrue + "\"" + el;
			fw.write(overRide);
			sTrue = "false";
			if (bnoPrompt) sTrue="true";
			overRide = "bnoPrompt = \"" + sTrue + "\"" + el;
			fw.write(overRide);
			String sNums="";
			String comma = "";
			for(Integer ns:runListNum){
				sNums += comma + ns.toString();
				comma=",";
			}
			overRide = "runListNum = \"" + sNums + "\"" + el;
			fw.write(overRide);
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
