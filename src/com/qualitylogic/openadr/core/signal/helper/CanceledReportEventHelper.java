package com.qualitylogic.openadr.core.signal.helper;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.qualitylogic.openadr.core.base.ErrorConst;
import com.qualitylogic.openadr.core.signal.EiResponseType;
import com.qualitylogic.openadr.core.signal.OadrCancelReportType;
import com.qualitylogic.openadr.core.signal.OadrCanceledReportType;
import com.qualitylogic.openadr.core.signal.OadrPayload;
import com.qualitylogic.openadr.core.signal.OadrSignedObject;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;

public class CanceledReportEventHelper {

	public static OadrCanceledReportType loadCanceledReportTypeFromString(String data) {
		OadrCanceledReportType oadrCanceledReportType = null;
		if (data == null || data.length() < 1)
			return null;

		try {
			JAXBContext testcontext = JAXBContext.newInstance("com.qualitylogic.openadr.core.signal");
			InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));
			Unmarshaller unmarshall = testcontext.createUnmarshaller();

			//oadrCanceledReportType = (OadrCanceledReportType)((JAXBElement<Object>)unmarshall.unmarshal(is)).getValue();
			OadrSignedObject oadrSignedObject = ((OadrPayload)unmarshall.unmarshal(is)).getOadrSignedObject();
			oadrCanceledReportType = oadrSignedObject.getOadrCanceledReport();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return oadrCanceledReportType;
	}

	public static OadrCanceledReportType loadOadrCanceledReportType(String fileName)
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		OadrCanceledReportType oadrCanceledReportType = ((OadrCanceledReportType) new SchemaHelper()
				.loadTestDataXMLFile(fileName).getOadrCanceledReport());
		oadrCanceledReportType.setVenID(new PropertiesFileReader().getVenID());
		oadrCanceledReportType.getOadrPendingReports().getReportRequestID().clear();
		return oadrCanceledReportType;
	}

	public static OadrCanceledReportType loadOadrCanceledReportType()
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		String fileName = "oadrCanceledReport.xml";
		
		OadrCanceledReportType oadrCanceledReportType = ((OadrCanceledReportType) new SchemaHelper()
				.loadTestDataXMLFile(fileName).getOadrCanceledReport());
		oadrCanceledReportType.setVenID(new PropertiesFileReader().getVenID());

		return oadrCanceledReportType;
	}
	
	public static OadrCanceledReportType loadOadrCanceledReportType(OadrCancelReportType oadrCancelReportType)
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		String reqID = oadrCancelReportType.getRequestID();
		
		OadrCanceledReportType  oadrCanceledReportType  = loadOadrCanceledReportType();
		EiResponseType eiResponse = oadrCanceledReportType.getEiResponse();
		eiResponse.setResponseCode(ErrorConst.OK_200);
		eiResponse.setResponseDescription("OK");
		eiResponse.setRequestID(reqID);
		return oadrCanceledReportType;
	}	
}