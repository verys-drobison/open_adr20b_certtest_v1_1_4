package com.qualitylogic.openadr.core.signal.helper;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.OadrPayload;
import com.qualitylogic.openadr.core.signal.OadrRegisterReportType;
import com.qualitylogic.openadr.core.signal.OadrReportDescriptionType;
import com.qualitylogic.openadr.core.signal.OadrReportType;
import com.qualitylogic.openadr.core.signal.OadrSignedObject;
import com.qualitylogic.openadr.core.signal.PowerRealType;
import com.qualitylogic.openadr.core.signal.TemperatureType;
import com.qualitylogic.openadr.core.signal.TemperatureUnitType;
import com.qualitylogic.openadr.core.signal.VoltageType;
import com.qualitylogic.openadr.core.signal.xcal.DurationPropType;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;

public class RegisterReportEventHelper {

	public static OadrRegisterReportType createOadrRegisterReportFromString(String data) {
		OadrRegisterReportType oadrRegisterReportType = null;
		if (data == null || data.length() < 1)
			return null;

		try {
			JAXBContext testcontext = JAXBContext.newInstance("com.qualitylogic.openadr.core.signal");
			InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));
			Unmarshaller unmarshall = testcontext.createUnmarshaller();

			//oadrRegisterReportType = (OadrRegisterReportType)((JAXBElement<Object>)unmarshall.unmarshal(is)).getValue();
			OadrSignedObject oadrSignedObject = ((OadrPayload)unmarshall.unmarshal(is)).getOadrSignedObject();
			oadrRegisterReportType = oadrSignedObject.getOadrRegisterReport();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return oadrRegisterReportType;
	}

	public static OadrRegisterReportType loadOadrRegisterReportType(String fileName)
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		OadrRegisterReportType oadrRegisterReportType = ((OadrRegisterReportType) new SchemaHelper()
		.loadTestDataXMLFile(fileName).getOadrRegisterReport());

		return oadrRegisterReportType;
	}

	public static OadrRegisterReportType loadOadrRegisterTelemetryStatusReportType()
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		/*OadrRegisterReportType oadrRegisterReportType = ((OadrRegisterReportType) new SchemaHelper()
				.loadTestDataXMLFile("oadrRegisiterReport_History_Usage.xml").getOadrRegisterReport());
		 */
		OadrRegisterReportType oadrRegisterReportType = ((OadrRegisterReportType) new SchemaHelper()
		.loadTestDataXMLFile("oadrRegisiterReport_Telemetry_Status.xml").getOadrRegisterReport());

		PropertiesFileReader propertiesFileReader = new PropertiesFileReader();

		oadrRegisterReportType.setRequestID(OadrUtil.createUniqueRegisterReportReqID());
		oadrRegisterReportType.setVenID(propertiesFileReader.getVenID());
		oadrRegisterReportType.getOadrReport().get(0).setReportSpecifierID(OadrUtil.createUniqueReportSpecifierID());
		oadrRegisterReportType.getOadrReport().get(0).setReportName("METADATA_TELEMETRY_STATUS");
		oadrRegisterReportType.getOadrReport().get(0).getOadrReportDescription().get(0).getOadrSamplingRate().setOadrMinPeriod("PT5M");
		oadrRegisterReportType.getOadrReport().get(0).getOadrReportDescription().get(0).getOadrSamplingRate().setOadrMaxPeriod("PT5M");

		DurationPropType duration = new DurationPropType();
		duration.setDuration("PT30M");

		oadrRegisterReportType.getOadrReport().get(0).setDuration(duration);
		oadrRegisterReportType.getOadrReport().get(0).getOadrReportDescription().get(0).setRID(OadrUtil.createUniqueRID());

		oadrRegisterReportType.getOadrReport().get(0).setCreatedDateTime(OadrUtil.getCurrentTime());
		oadrRegisterReportType.getOadrReport().get(0).getOadrReportDescription().get(0).setMarketContext(propertiesFileReader.get("DR_MarketContext_1_Name"));
		return oadrRegisterReportType;
	}


	public static OadrRegisterReportType loadMetadata_001()
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		OadrRegisterReportType oadrRegisterReportType = ((OadrRegisterReportType) new SchemaHelper()
		.loadTestDataXMLFile("Metadata_001.xml").getOadrRegisterReport());

		String resourceInProperties = new PropertiesFileReader().getOneResourceID();


		PropertiesFileReader propertiesFileReader = new PropertiesFileReader();

		oadrRegisterReportType.setRequestID(OadrUtil.createUniqueRegisterReportReqID());
		oadrRegisterReportType.setVenID(propertiesFileReader.getVenID());

		List<OadrReportType>  OadrReportList = oadrRegisterReportType.getOadrReport();

		int i=0;
		for(OadrReportType eachOadrReportType:OadrReportList){


			List<OadrReportDescriptionType>  oadrReportDescriptionList = eachOadrReportType.getOadrReportDescription();

			eachOadrReportType.setReportSpecifierID(OadrUtil.createUniqueReportSpecifierID()+"_"+i++);

			eachOadrReportType.setCreatedDateTime(OadrUtil.getCurrentTime());
			int j=0;
			for(OadrReportDescriptionType oadrReportDescriptionType:oadrReportDescriptionList){
				EiTargetType targetType =  oadrReportDescriptionType.getReportDataSource();
				targetType.getResourceID().clear();
				targetType.getResourceID().add(resourceInProperties);
				String uniqueRID = OadrUtil.createUniqueRID()+"_"+j++;			 		
				oadrReportDescriptionType.setRID(uniqueRID);					
				oadrReportDescriptionType.setMarketContext(propertiesFileReader.get("DR_MarketContext_1_Name"));			 		
			}

		}

		return oadrRegisterReportType;
	}

	public static OadrRegisterReportType loadMetadata_001_CBP()
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		OadrRegisterReportType oadrRegisterReportType = ((OadrRegisterReportType) new SchemaHelper()
		.loadTestDataXMLFile("Metadata_001_CBP.xml").getOadrRegisterReport());

		String resourceInProperties[] = new PropertiesFileReader().getTwoResourceID();

		PropertiesFileReader propertiesFileReader = new PropertiesFileReader();

		oadrRegisterReportType.setRequestID(OadrUtil.createUniqueRegisterReportReqID());
		oadrRegisterReportType.setVenID(propertiesFileReader.getVenID());

		List<OadrReportType>  OadrReportList = oadrRegisterReportType.getOadrReport();

		int i=0;
		for(OadrReportType eachOadrReportType:OadrReportList){


			List<OadrReportDescriptionType>  oadrReportDescriptionList = eachOadrReportType.getOadrReportDescription();
			String rname = eachOadrReportType.getReportName();

			eachOadrReportType.setReportSpecifierID(OadrUtil.createUniqueReportSpecifierID()+"_"+i++);

			eachOadrReportType.setCreatedDateTime(OadrUtil.getCurrentTime());
			int j=0;
			for(OadrReportDescriptionType oadrReportDescriptionType:oadrReportDescriptionList){
				EiTargetType targetType =  oadrReportDescriptionType.getReportDataSource();
				targetType.getResourceID().clear();
				targetType.getResourceID().add(resourceInProperties[0]);
				targetType.getResourceID().add(resourceInProperties[1]);
				oadrReportDescriptionType.setMarketContext(propertiesFileReader.get("DR_MarketContext_1_Name"));	
				oadrReportDescriptionType.getOadrSamplingRate().setOadrMinPeriod("PT1M");
				oadrReportDescriptionType.getOadrSamplingRate().setOadrMaxPeriod("PT1M");

				if(rname.contains("METADATA_TELEMETRY_USAGE")){
					PowerRealType preal = (PowerRealType) oadrReportDescriptionType.getItemBase().getValue();
					preal.setItemUnits(propertiesFileReader.get("powerReal_itemUnits"));
					preal.setSiScaleCode(propertiesFileReader.get("powerReal_siScaleCode"));
					preal.getPowerAttributes().setAc(true);
					preal.getPowerAttributes().setHertz(BigDecimal.valueOf(Float.parseFloat((propertiesFileReader.get("powerReal_powerAttributes_hertz")))));
					preal.getPowerAttributes().setVoltage(BigDecimal.valueOf(Float.parseFloat((propertiesFileReader.get("powerReal_powerAttributes_voltage")))));

					String uniqueRID = new PropertiesFileReader().get("TelemetryUsage_PowerReal_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
					oadrReportDescriptionType.setRID(uniqueRID);					
				} else {
					String uniqueRID = OadrUtil.createUniqueRID()+"_"+j++;			 		
					oadrReportDescriptionType.setRID(uniqueRID);					

				}
			}

		}

		return oadrRegisterReportType;
	}

	public static OadrRegisterReportType loadMetadata_001_FDR_TYP()
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		OadrRegisterReportType oadrRegisterReportType = ((OadrRegisterReportType) new SchemaHelper()
		.loadTestDataXMLFile("Metadata_001_CBP.xml").getOadrRegisterReport());

		PropertiesFileReader propertiesFileReader = new PropertiesFileReader();

		oadrRegisterReportType.setRequestID(OadrUtil.createUniqueRegisterReportReqID());
		oadrRegisterReportType.setVenID(propertiesFileReader.getVenID());

		List<OadrReportType>  OadrReportList = oadrRegisterReportType.getOadrReport();

		int i=0;
		for(OadrReportType eachOadrReportType:OadrReportList){


			List<OadrReportDescriptionType>  oadrReportDescriptionList = eachOadrReportType.getOadrReportDescription();
			String rname = eachOadrReportType.getReportName();

			eachOadrReportType.setReportSpecifierID(OadrUtil.createUniqueReportSpecifierID()+"_"+i++);

			eachOadrReportType.setCreatedDateTime(OadrUtil.getCurrentTime());
			int j=0;
			for(OadrReportDescriptionType oadrReportDescriptionType:oadrReportDescriptionList){
				EiTargetType targetType =  oadrReportDescriptionType.getReportDataSource();
				targetType.getVenID().clear();
				targetType.getResourceID().clear();
				targetType.getVenID().add(propertiesFileReader.getVenID());
				oadrReportDescriptionType.setMarketContext(propertiesFileReader.get("DR_MarketContext_1_Name"));	
				oadrReportDescriptionType.getOadrSamplingRate().setOadrMinPeriod("PT1M");
				oadrReportDescriptionType.getOadrSamplingRate().setOadrMaxPeriod("PT1M");

				if(rname.contains("METADATA_TELEMETRY_USAGE")){
					PowerRealType preal = (PowerRealType) oadrReportDescriptionType.getItemBase().getValue();
					preal.setItemUnits(propertiesFileReader.get("powerReal_itemUnits"));
					preal.setSiScaleCode(propertiesFileReader.get("powerReal_siScaleCode"));
					preal.getPowerAttributes().setAc(true);
					preal.getPowerAttributes().setHertz(BigDecimal.valueOf(Float.parseFloat((propertiesFileReader.get("powerReal_powerAttributes_hertz")))));
					preal.getPowerAttributes().setVoltage(BigDecimal.valueOf(Float.parseFloat((propertiesFileReader.get("powerReal_powerAttributes_voltage")))));

					String uniqueRID = new PropertiesFileReader().get("TelemetryUsage_PowerReal_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
					oadrReportDescriptionType.setRID(uniqueRID);					
				} else {
					String uniqueRID = OadrUtil.createUniqueRID()+"_"+j++;			 		
					oadrReportDescriptionType.setRID(uniqueRID);					

				}
			}

		}

		return oadrRegisterReportType;
	}

	public static OadrRegisterReportType loadMetadata_001_FDR_CMP()
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		OadrRegisterReportType oadrRegisterReportType = ((OadrRegisterReportType) new SchemaHelper()
		.loadTestDataXMLFile("Metadata_001_FDR_CMP.xml").getOadrRegisterReport());

		PropertiesFileReader propertiesFileReader = new PropertiesFileReader();

		oadrRegisterReportType.setRequestID(OadrUtil.createUniqueRegisterReportReqID());
		oadrRegisterReportType.setVenID(propertiesFileReader.getVenID());

		List<OadrReportType>  OadrReportList = oadrRegisterReportType.getOadrReport();

		int i=0;
		for(OadrReportType eachOadrReportType:OadrReportList){


			List<OadrReportDescriptionType>  oadrReportDescriptionList = eachOadrReportType.getOadrReportDescription();
			String rname = eachOadrReportType.getReportName();

			eachOadrReportType.setReportSpecifierID(OadrUtil.createUniqueReportSpecifierID()+"_"+i++);

			eachOadrReportType.setCreatedDateTime(OadrUtil.getCurrentTime());
			int j=0;
			for(OadrReportDescriptionType oadrReportDescriptionType:oadrReportDescriptionList){
				EiTargetType targetType =  oadrReportDescriptionType.getReportDataSource();
				targetType.getVenID().clear();
				targetType.getResourceID().clear();
				targetType.getResourceID().add(new PropertiesFileReader().getOneResourceID());
				//targetType.getVenID().add(propertiesFileReader.getVenID());
				oadrReportDescriptionType.setMarketContext(propertiesFileReader.get("DR_MarketContext_1_Name"));	
				oadrReportDescriptionType.getOadrSamplingRate().setOadrMinPeriod("PT5S");
				oadrReportDescriptionType.getOadrSamplingRate().setOadrMaxPeriod("PT5S");

				if(rname.contains("METADATA_TELEMETRY_USAGE")){
					if(oadrReportDescriptionType.getRID().contains("power")){
						PowerRealType preal = (PowerRealType) oadrReportDescriptionType.getItemBase().getValue();
						preal.setItemUnits(propertiesFileReader.get("powerReal_itemUnits"));
						preal.setSiScaleCode(propertiesFileReader.get("powerReal_siScaleCode"));
						preal.getPowerAttributes().setAc(true);
						preal.getPowerAttributes().setHertz(BigDecimal.valueOf(Float.parseFloat((propertiesFileReader.get("powerReal_powerAttributes_hertz")))));
						preal.getPowerAttributes().setVoltage(BigDecimal.valueOf(Float.parseFloat((propertiesFileReader.get("powerReal_powerAttributes_voltage")))));

						String uniqueRID = new PropertiesFileReader().get("TelemetryUsage_PowerReal_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			
					} else if(oadrReportDescriptionType.getRID().contains("voltage")){
						VoltageType volt = (VoltageType) oadrReportDescriptionType.getItemBase().getValue();
						volt.setItemUnits("V");
						volt.setSiScaleCode("none");

						String uniqueRID = new PropertiesFileReader().get("TelemetryUsage_Voltage_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			

					}
				} else {
					String uniqueRID = OadrUtil.createUniqueRID()+"_"+j++;			 		
					oadrReportDescriptionType.setRID(uniqueRID);					

				}
			}

		}

		return oadrRegisterReportType;
	}

	public static OadrRegisterReportType loadMetadata_001_THR_TYP()
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		OadrRegisterReportType oadrRegisterReportType = ((OadrRegisterReportType) new SchemaHelper()
		.loadTestDataXMLFile("Metadata_001_THR_TYP.xml").getOadrRegisterReport());

		PropertiesFileReader propertiesFileReader = new PropertiesFileReader();
		String propFileResourceID[]= propertiesFileReader.getTwoResourceID();
		oadrRegisterReportType.setRequestID(OadrUtil.createUniqueRegisterReportReqID());
		oadrRegisterReportType.setVenID(propertiesFileReader.getVenID());

		List<OadrReportType>  OadrReportList = oadrRegisterReportType.getOadrReport();

		int i=0;
		for(OadrReportType eachOadrReportType:OadrReportList){


			List<OadrReportDescriptionType>  oadrReportDescriptionList = eachOadrReportType.getOadrReportDescription();
			String rname = eachOadrReportType.getReportName();

			eachOadrReportType.setReportSpecifierID(OadrUtil.createUniqueReportSpecifierID()+"_"+i++);

			eachOadrReportType.setCreatedDateTime(OadrUtil.getCurrentTime());
			boolean ftmp = false;
			if(propertiesFileReader.get("temperature_itemUnits").toLowerCase().contentEquals("fahrenheit"))
				ftmp=true;

			int j=0;
			for(OadrReportDescriptionType oadrReportDescriptionType:oadrReportDescriptionList){
				EiTargetType targetType =  oadrReportDescriptionType.getReportDataSource();
				targetType.getVenID().clear();
				targetType.getResourceID().clear();
				//targetType.getResourceID().add(propFileResourceID[0]);
				//targetType.getResourceID().add(propFileResourceID[1]);
				targetType.getVenID().add(propertiesFileReader.getVenID());
				oadrReportDescriptionType.setMarketContext(propertiesFileReader.get("DR_MarketContext_1_Name"));	
				oadrReportDescriptionType.getOadrSamplingRate().setOadrMinPeriod("PT1M");
				oadrReportDescriptionType.getOadrSamplingRate().setOadrMaxPeriod("PT1M");

				if(rname.contains("METADATA_TELEMETRY_STATUS")){
					if(oadrReportDescriptionType.getRID().contains("Status")){
						String uniqueRID = new PropertiesFileReader().get("ThermostatState_Status_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			

					} else if(oadrReportDescriptionType.getRID().contains("Current Temp")){
						TemperatureType temp = (TemperatureType) oadrReportDescriptionType.getItemBase().getValue();
						if(ftmp){
							temp.setItemUnits(TemperatureUnitType.FAHRENHEIT);
						} else {
							temp.setItemUnits(TemperatureUnitType.CELSIUS);
						}
						temp.setSiScaleCode("none");

						String uniqueRID = new PropertiesFileReader().get("ThermostatState_CurrentTemp_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			

					} else if(oadrReportDescriptionType.getRID().contains("Heat Temp Setting")){
						TemperatureType temp = (TemperatureType) oadrReportDescriptionType.getItemBase().getValue();
						if(ftmp){
							temp.setItemUnits(TemperatureUnitType.FAHRENHEIT);
						} else {
							temp.setItemUnits(TemperatureUnitType.CELSIUS);
						}
						temp.setSiScaleCode("none");

						String uniqueRID = new PropertiesFileReader().get("ThermostatState_HeatTempSetting_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			

					} else if(oadrReportDescriptionType.getRID().contains("Cool Temp Setting")){
						TemperatureType temp = (TemperatureType) oadrReportDescriptionType.getItemBase().getValue();
						if(ftmp){
							temp.setItemUnits(TemperatureUnitType.FAHRENHEIT);
						} else {
							temp.setItemUnits(TemperatureUnitType.CELSIUS);
						}
						temp.setSiScaleCode("none");

						String uniqueRID = new PropertiesFileReader().get("ThermostatState_CoolTempSetting_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			

					} else if(oadrReportDescriptionType.getRID().contains("HVAC Mode Setting")){

						String uniqueRID = new PropertiesFileReader().get("ThermostatState_HVACModeSetting_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			

					} else if(oadrReportDescriptionType.getRID().contains("Current HVAC Mode")){

						String uniqueRID = new PropertiesFileReader().get("ThermostatState_CurrentHVACMode_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			

					} else if(oadrReportDescriptionType.getRID().contains("Fan Mode Setting")){

						String uniqueRID = new PropertiesFileReader().get("ThermostatState_FanModeSetting_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			

					} else if(oadrReportDescriptionType.getRID().contains("Current Hold Mode")){

						String uniqueRID = new PropertiesFileReader().get("ThermostatState_CurrentHoldMode_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			

					} else if(oadrReportDescriptionType.getRID().contains("Current Away Mode")){

						String uniqueRID = new PropertiesFileReader().get("ThermostatState_CurrentAwayMode_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			

					} else if(oadrReportDescriptionType.getRID().contains("Current Humidity")){

						String uniqueRID = new PropertiesFileReader().get("ThermostatState_CurrentHumidity_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);			

					}

				} else 	if(rname.contains("METADATA_TELEMETRY_USAGE")){
					if(oadrReportDescriptionType.getRID().contains("Power")){
						String uniqueRID1 = new PropertiesFileReader().get("TelemetryUsage_PowerReal_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID1);
						PowerRealType preal = (PowerRealType) oadrReportDescriptionType.getItemBase().getValue();
						preal.setItemUnits(propertiesFileReader.get("powerReal_itemUnits"));
						preal.setSiScaleCode(propertiesFileReader.get("powerReal_siScaleCode"));
						preal.getPowerAttributes().setAc(true);
						preal.getPowerAttributes().setHertz(BigDecimal.valueOf(Float.parseFloat((propertiesFileReader.get("powerReal_powerAttributes_hertz")))));
						preal.getPowerAttributes().setVoltage(BigDecimal.valueOf(Float.parseFloat((propertiesFileReader.get("powerReal_powerAttributes_voltage")))));

						String uniqueRID = new PropertiesFileReader().get("TelemetryUsage_PowerReal_WellKnown_rID") + ":" + OadrUtil.createUniqueRID()+"_"+j++;			 		
						oadrReportDescriptionType.setRID(uniqueRID);	
					}
				}else {
					String uniqueRID = OadrUtil.createUniqueRID()+"_"+j++;			 		
					oadrReportDescriptionType.setRID(uniqueRID);					

				}
			}

		}

		return oadrRegisterReportType;
	}

	public static OadrRegisterReportType loadMetadata_003()
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		OadrRegisterReportType oadrRegisterReportType = ((OadrRegisterReportType) new SchemaHelper()
		.loadTestDataXMLFile("Metadata_003.xml").getOadrRegisterReport());



		PropertiesFileReader propertiesFileReader = new PropertiesFileReader();

		oadrRegisterReportType.setRequestID(OadrUtil.createUniqueRegisterReportReqID());
		oadrRegisterReportType.setVenID(propertiesFileReader.getVenID());

		return oadrRegisterReportType;
	}

	public static OadrRegisterReportType loadOadrRegisterHistoryReportType()
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		OadrRegisterReportType oadrRegisterReportType = ((OadrRegisterReportType) new SchemaHelper()
		.loadTestDataXMLFile("oadrRegisiterReport_History_Usage.xml").getOadrRegisterReport());


		PropertiesFileReader propertiesFileReader = new PropertiesFileReader();

		oadrRegisterReportType.setRequestID(OadrUtil.createUniqueRegisterReportReqID());
		oadrRegisterReportType.setVenID(propertiesFileReader.getVenID());

		List<OadrReportType>  OadrReportList = oadrRegisterReportType.getOadrReport();

		int i=0;
		for(OadrReportType eachOadrReportType:OadrReportList){

			eachOadrReportType.setReportSpecifierID(OadrUtil.createUniqueReportSpecifierID()+"_"+i++);
			eachOadrReportType.setReportName("METADATA_HISTORY_USAGE");
			eachOadrReportType.setCreatedDateTime(OadrUtil.getCurrentTime());

			DurationPropType duration = new DurationPropType();
			duration.setDuration("PT30M");

			eachOadrReportType.setDuration(duration);

			List<OadrReportDescriptionType>  oadrReportDescriptionList = eachOadrReportType.getOadrReportDescription();
			int j=0;
			for(OadrReportDescriptionType oadrReportDescriptionType:oadrReportDescriptionList){
				oadrReportDescriptionType.getOadrSamplingRate().setOadrMinPeriod("PT5M");
				oadrReportDescriptionType.getOadrSamplingRate().setOadrMaxPeriod("PT5M");
				String uniqueRID = OadrUtil.createUniqueRID()+"_"+j++;			 		
				oadrReportDescriptionType.setRID(uniqueRID);					
				oadrReportDescriptionType.setMarketContext(propertiesFileReader.get("DR_MarketContext_1_Name"));			 		
			}

		}

		return oadrRegisterReportType;
	}

	public static void main(String []args) throws FileNotFoundException, UnsupportedEncodingException, JAXBException{
		// OadrRegisterReportType oadrRegisterReportType = loadOadrRegisterHistoryReportType();
		System.out.println("Done");
	}

}