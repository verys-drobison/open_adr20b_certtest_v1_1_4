package com.qualitylogic.openadr.core.signal.helper;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.qualitylogic.openadr.core.signal.OadrPayload;
import com.qualitylogic.openadr.core.signal.OadrRequestReregistrationType;
import com.qualitylogic.openadr.core.signal.OadrSignedObject;

public class RequestReregistrationSignalHelper {

	public static OadrRequestReregistrationType loadOadrRequestReregistrationType() {

		return loadOadrRequestReregistrationType("oadrRequestReregistration.xml");
	}

	public static OadrRequestReregistrationType loadOadrRequestReregistrationType(String file) {

		OadrRequestReregistrationType oadrRequestReregistrationType = null;
		try {

			oadrRequestReregistrationType = (OadrRequestReregistrationType) new SchemaHelper()
			.loadTestDataXMLFile(file).getOadrRequestReregistration();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return oadrRequestReregistrationType;
	}

	public static OadrRequestReregistrationType createOadrRequestReregistrationTypeFromString(
			String data) {
		OadrRequestReregistrationType oadrRequestReregistrationType = null;
		if (data == null || data.length() < 1)
			return null;

		try {
			JAXBContext testcontext = JAXBContext.newInstance("com.qualitylogic.openadr.core.signal");
			InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));
			Unmarshaller unmarshall = testcontext.createUnmarshaller();
			OadrSignedObject oadrSignedObject = ((OadrPayload)unmarshall.unmarshal(is)).getOadrSignedObject();
			oadrRequestReregistrationType = oadrSignedObject.getOadrRequestReregistration();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return oadrRequestReregistrationType;
	}

}