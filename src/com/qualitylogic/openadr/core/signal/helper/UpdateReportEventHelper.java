package com.qualitylogic.openadr.core.signal.helper;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;

import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.IntervalType;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.OadrPayload;
import com.qualitylogic.openadr.core.signal.OadrPayloadResourceStatusType;
import com.qualitylogic.openadr.core.signal.OadrReportDescriptionType;
import com.qualitylogic.openadr.core.signal.OadrReportPayloadType;
import com.qualitylogic.openadr.core.signal.OadrReportRequestType;
import com.qualitylogic.openadr.core.signal.OadrReportType;
import com.qualitylogic.openadr.core.signal.OadrSignedObject;
import com.qualitylogic.openadr.core.signal.OadrUpdateReportType;
import com.qualitylogic.openadr.core.signal.PayloadFloatType;
import com.qualitylogic.openadr.core.signal.SpecifierPayloadType;
import com.qualitylogic.openadr.core.signal.StreamPayloadBaseType;
import com.qualitylogic.openadr.core.signal.xcal.Dtstart;
import com.qualitylogic.openadr.core.util.Clone;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;
import com.qualitylogic.openadr.core.util.XMLDBUtil;

public class UpdateReportEventHelper {

	public static OadrUpdateReportType createReportFromString(String data) {
		OadrUpdateReportType oadrUpdateReportType = null;
		if (data == null || data.length() < 1)
			return null;

		try {

			JAXBContext testcontext = JAXBContext
					.newInstance("com.qualitylogic.openadr.core.signal");
			InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));
			Unmarshaller unmarshall = testcontext.createUnmarshaller();

			OadrSignedObject oadrSignedObject = ((OadrPayload)unmarshall.unmarshal(is)).getOadrSignedObject();
			oadrUpdateReportType = oadrSignedObject.getOadrUpdateReport();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return oadrUpdateReportType;
	}


	public static int numberOfReportsReceivedForReportRequestID(String reportRequestID) {
		int i=0;
		ArrayList<OadrUpdateReportType>  updateReportList = TestSession.getOadrUpdateReportTypeReceivedList();

		for(OadrUpdateReportType oadrUpdateReportType:updateReportList){
			List<OadrReportType> oadrReportList = oadrUpdateReportType.getOadrReport();
			for(OadrReportType eachOadrReportType:oadrReportList){
				if(eachOadrReportType.getReportRequestID().equals(reportRequestID)){
					i++;
				}
			}

		}

		return i;
	}

	public static OadrUpdateReportType loadOadrUpdateReport(String fileName)
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		OadrUpdateReportType oadrUpdateReportType = ((OadrUpdateReportType) new SchemaHelper()
		.loadTestDataXMLFile(fileName).getOadrUpdateReport());

		return oadrUpdateReportType;
	}

	// looks unused
	public static OadrUpdateReportType loadOadrUpdateReport(ServiceType serivceType,boolean isCreaterOfRegisterReport,OadrCreateReportType oadrCreateReportType)
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		PropertiesFileReader properties = new PropertiesFileReader();

		OadrUpdateReportType oadrUpdateReportType = null;
		oadrUpdateReportType = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile("oadrUpdateReport_History_Usage.xml").getOadrUpdateReport());

		// Last Created event received if oadrCreateReportType is empty.
		if(oadrCreateReportType==null){

			ArrayList<OadrCreateReportType>  createReportTypeReceivedList= TestSession.getOadrCreateReportTypeReceivedList();
			oadrCreateReportType=createReportTypeReceivedList.get(createReportTypeReceivedList.size()-1);
		}

		oadrUpdateReportType.setRequestID(OadrUtil.createUniqueUpdReportRequestID());

		oadrUpdateReportType.setVenID(properties.getVenID());

		OadrReportType reportTemplate=Clone.clone(oadrUpdateReportType.getOadrReport().get(0));		
		oadrUpdateReportType.getOadrReport().clear();

		//Instead of DB it should come from CreateReport
		List<OadrReportRequestType> oadrReportRequestTypeList =oadrCreateReportType.getOadrReportRequest();
		XMLDBUtil xmlDBUtil=new XMLDBUtil();
		for(OadrReportRequestType eachReportRequest:oadrReportRequestTypeList){
			String reportReqID = eachReportRequest.getReportRequestID();
			String reportSpecifierID = eachReportRequest.getReportSpecifier().getReportSpecifierID();
			String durationReceived = eachReportRequest.getReportSpecifier().getReportInterval().getProperties().getDuration().getDuration();
			Dtstart dtStartReceived = eachReportRequest.getReportSpecifier().getReportInterval().getProperties().getDtstart();

			String reportName = "";

			if(serivceType.equals(ServiceType.VTN) && !isCreaterOfRegisterReport ){
				reportName = xmlDBUtil.getReportNameFromReportsReceivedFromVEN(reportSpecifierID);
			}else if(serivceType.equals(ServiceType.VEN) && !isCreaterOfRegisterReport ){
				reportName =  xmlDBUtil.getReportNameFromReportsReceivedFromVTN(reportSpecifierID);	
			}else if(serivceType.equals(ServiceType.VTN) && isCreaterOfRegisterReport ){
				reportName = xmlDBUtil.getReportNameFromReportsReceivedFromVTN(reportSpecifierID);
			}else if(serivceType.equals(ServiceType.VEN) && isCreaterOfRegisterReport ){
				reportName =  xmlDBUtil.getReportNameFromReportsReceivedFromVEN(reportSpecifierID);	
			}
			if (StringUtils.isBlank(reportName)) {
				throw new FailedException("Unrecognized ReportSpecifierID=" + reportSpecifierID);
			}

			reportTemplate.setReportRequestID(reportReqID);
			reportTemplate.setReportSpecifierID(reportSpecifierID);

			reportTemplate.setReportName(reportName);
			reportTemplate.setCreatedDateTime(OadrUtil.getCurrentTime());
			reportTemplate.getDuration().setDuration(durationReceived);
			reportTemplate.setDtstart(dtStartReceived);

			IntervalType interval = Clone.clone(reportTemplate.getIntervals().getInterval().get(0));
			reportTemplate.getIntervals().getInterval().clear();
			interval.setDtstart(dtStartReceived);
			interval.getDuration().setDuration(durationReceived);

			if(interval.getStreamPayloadBase().size()==2){
				interval.getStreamPayloadBase().remove(1);	
			}

			reportTemplate.getIntervals().getInterval().add(interval);



			List<SpecifierPayloadType>  reportSpecifierList = eachReportRequest.getReportSpecifier().getSpecifierPayload();
			for(SpecifierPayloadType eachSpecifierPayloadType:reportSpecifierList){
				OadrReportType  oadrReportType = Clone.clone(reportTemplate);
				OadrReportPayloadType oadrRptPayloadType=(OadrReportPayloadType)oadrReportType.getIntervals().getInterval().get(0).getStreamPayloadBase().get(0).getValue();

				String ridInCreateEvent=eachSpecifierPayloadType.getRID();
				//oadrRptPayloadType.setRID(ridInCreateEvent);
				oadrRptPayloadType.setRID(ridInCreateEvent);
				oadrUpdateReportType.getOadrReport().add(oadrReportType);

			}

		}

		return oadrUpdateReportType;
	}

	public static OadrUpdateReportType loadOadrUpdateReport_Update002()
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		PropertiesFileReader properties = new PropertiesFileReader();

		OadrUpdateReportType oadrUpdateReportType = null;
		oadrUpdateReportType = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile("Update_002.xml").getOadrUpdateReport());
		oadrUpdateReportType.setRequestID(OadrUtil.createUniqueUpdReportRequestID());
		oadrUpdateReportType.setVenID(properties.getVenID());	

		return oadrUpdateReportType;
	}
	public static OadrUpdateReportType loadOadrUpdateReport_Update001(ServiceType serivceType,boolean isCreaterOfRegisterReport,OadrCreateReportType oadrCreateReportType)
			throws JAXBException, FileNotFoundException,
			UnsupportedEncodingException {

		PropertiesFileReader properties = new PropertiesFileReader();

		OadrUpdateReportType oadrUpdateReportType = null;
		oadrUpdateReportType = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile("Update_001.xml").getOadrUpdateReport());

		// Last Created event received if oadrCreateReportType is empty.
		if(oadrCreateReportType==null){

			ArrayList<OadrCreateReportType>  createReportTypeReceivedList= TestSession.getOadrCreateReportTypeReceivedList();
			oadrCreateReportType=createReportTypeReceivedList.get(createReportTypeReceivedList.size()-1);
		}
		oadrUpdateReportType.setRequestID(OadrUtil.createUniqueUpdReportRequestID());

		oadrUpdateReportType.setVenID(properties.getVenID());

		OadrReportType reportTemplate=Clone.clone(oadrUpdateReportType.getOadrReport().get(0));		
		oadrUpdateReportType.getOadrReport().clear();


		List<OadrReportRequestType> oadrReportRequestTypeList =oadrCreateReportType.getOadrReportRequest();
		XMLDBUtil xmlDBUtil=new XMLDBUtil();
		for(OadrReportRequestType eachReportRequest:oadrReportRequestTypeList){
			String reportReqID = eachReportRequest.getReportRequestID();
			String reportSpecifierID = eachReportRequest.getReportSpecifier().getReportSpecifierID();
			String reportName = "";

			if(serivceType.equals(ServiceType.VTN) && !isCreaterOfRegisterReport ){
				reportName = xmlDBUtil.getReportNameFromReportsReceivedFromVEN(reportSpecifierID);
			}else if(serivceType.equals(ServiceType.VEN) && !isCreaterOfRegisterReport ){
				reportName =  xmlDBUtil.getReportNameFromReportsReceivedFromVTN(reportSpecifierID);	
			}else if(serivceType.equals(ServiceType.VTN) && isCreaterOfRegisterReport ){
				reportName = xmlDBUtil.getReportNameFromReportsReceivedFromVTN(reportSpecifierID);
			}else if(serivceType.equals(ServiceType.VEN) && isCreaterOfRegisterReport ){
				reportName =  xmlDBUtil.getReportNameFromReportsReceivedFromVEN(reportSpecifierID);	
			}
			if (StringUtils.isBlank(reportName)) {
				throw new FailedException("Unrecognized ReportSpecifierID=" + reportSpecifierID);
			}

			reportTemplate.setReportRequestID(reportReqID);
			reportTemplate.setReportSpecifierID(reportSpecifierID);
			XMLGregorianCalendar  currentTime = OadrUtil.getCurrentTime();
			//reportTemplate.getIntervals().getInterval().get(0).getDtstart().setDateTime(currentTime);

			reportTemplate.getDtstart().setDateTime(currentTime);

			reportTemplate.setReportName(reportName);
			reportTemplate.setCreatedDateTime(OadrUtil.getCurrentTime());

			IntervalType interval = Clone.clone(reportTemplate.getIntervals().getInterval().get(0));
			reportTemplate.getIntervals().getInterval().clear();

			if(interval.getStreamPayloadBase().size()==2){
				interval.getStreamPayloadBase().remove(1);	
			}

			reportTemplate.getIntervals().getInterval().add(interval);

			List<SpecifierPayloadType>  reportSpecifierList = eachReportRequest.getReportSpecifier().getSpecifierPayload();
			for(SpecifierPayloadType eachSpecifierPayloadType:reportSpecifierList){
				OadrReportType  oadrReportType = Clone.clone(reportTemplate);
				OadrReportPayloadType oadrRptPayloadType=(OadrReportPayloadType)oadrReportType.getIntervals().getInterval().get(0).getStreamPayloadBase().get(0).getValue();

				String ridInCreateEvent=eachSpecifierPayloadType.getRID();
				oadrRptPayloadType.setRID(ridInCreateEvent);
				oadrUpdateReportType.getOadrReport().add(oadrReportType);

			}


		}


		return oadrUpdateReportType;
	}

	public static OadrUpdateReportType loadOadrUpdateReport_Update_CBP(OadrCreateReportType createReport)
			throws JAXBException, FileNotFoundException, UnsupportedEncodingException {

		String filename = "Update_CBP.xml";
		OadrReportRequestType reportRequest = createReport.getOadrReportRequest().get(0);
		XMLGregorianCalendar currentTime = OadrUtil.getCurrentTime();

		OadrUpdateReportType updateReport = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile(filename).getOadrUpdateReport());
		updateReport.setRequestID(OadrUtil.createUniqueUpdReportRequestID());
		updateReport.setVenID(new PropertiesFileReader().getVenID());

		OadrReportType report = updateReport.getOadrReport().get(0);
		String reportName = getReportName(reportRequest);

		report.setReportRequestID(reportRequest.getReportRequestID());
		report.setReportSpecifierID(reportRequest.getReportSpecifier().getReportSpecifierID());
		report.setReportName(reportName);
		report.setCreatedDateTime(currentTime);
		report.getDtstart().setDateTime(currentTime);
		String minMax[] = new PropertiesFileReader().get("TelemetryUsage_PowerReal_Range").split(",");
		Float min = Float.parseFloat(minMax[0]);
		Float max = Float.parseFloat(minMax[1]);
		Float rng = max - min;
		Float fbase = 0.0f;
		if(TestSession.dataValue == null){
			fbase = (float) (Math.random()*((rng)*0.9) + min);
		} else {
			fbase = TestSession.dataValue;
		}
		Float value = (float) (Math.random()*((rng)/10.0) + fbase);
		TestSession.dataValue = value;		
		setInterval1RID_fwd_Value(0, 1, report, reportRequest, currentTime, true, value);

		return updateReport;
	}

	public static OadrUpdateReportType loadOadrUpdateReport_Update_FDR_CMP(OadrCreateReportType createReport)
			throws JAXBException, FileNotFoundException, UnsupportedEncodingException {

		String filename = "Update_FDR_CMP.xml";
		OadrReportRequestType reportRequest = createReport.getOadrReportRequest().get(0);
		XMLGregorianCalendar currentTime = OadrUtil.getCurrentTime();

		OadrUpdateReportType updateReport = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile(filename).getOadrUpdateReport());
		updateReport.setRequestID(OadrUtil.createUniqueUpdReportRequestID());
		updateReport.setVenID(new PropertiesFileReader().getVenID());
		OadrReportType report = updateReport.getOadrReport().get(0);
		String reportName = getReportName(reportRequest);

		report.setReportRequestID(reportRequest.getReportRequestID());
		report.setReportSpecifierID(reportRequest.getReportSpecifier().getReportSpecifierID());
		report.setReportName(reportName);
		report.setCreatedDateTime(currentTime);
		report.getDtstart().setDateTime(currentTime);
		setInterval_RID_fwd_SEC_Value(0, 5, report, reportRequest, currentTime, true);
		return updateReport;
	}

	public static OadrUpdateReportType loadOadrUpdateReport_Update_THR(OadrCreateReportType createReport)
			throws JAXBException, FileNotFoundException, UnsupportedEncodingException {

		String filename = "Update_THR.xml";
		OadrReportRequestType reportRequest = createReport.getOadrReportRequest().get(0);
		XMLGregorianCalendar currentTime = OadrUtil.getCurrentTime();

		OadrUpdateReportType updateReport = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile(filename).getOadrUpdateReport());
		updateReport.setRequestID(OadrUtil.createUniqueUpdReportRequestID());
		updateReport.setVenID(new PropertiesFileReader().getVenID());
		OadrReportType report = updateReport.getOadrReport().get(0);
		String reportName = getReportName(reportRequest);

		report.setReportRequestID(reportRequest.getReportRequestID());
		report.setReportSpecifierID(reportRequest.getReportSpecifier().getReportSpecifierID());
		report.setReportName(reportName);
		report.setCreatedDateTime(currentTime);
		report.getDtstart().setDateTime(currentTime);
		setInterval_RID_fwd_MIN_Value(0, 1, report, reportRequest, currentTime, true);
		return updateReport;
	}

	public static OadrUpdateReportType loadOadrUpdateReport_Update003(OadrCreateReportType createReport) 
			throws JAXBException, FileNotFoundException, UnsupportedEncodingException {

		String filename = "Update_003.xml";
		OadrReportRequestType reportRequest = createReport.getOadrReportRequest().get(0);
		XMLGregorianCalendar currentTime = OadrUtil.getCurrentTime(); 
		XMLGregorianCalendar requestTime = reportRequest.getReportSpecifier().getReportInterval().getProperties().getDtstart().getDateTime(); 
		
		//Knock the current time back 25 minutes so the first interval matches the report time and real current time minus 5 minutes matches the last interval
		//Duration offset = OadrUtil.createDuration(false, 0, 0, 0, 0, 25, 0);
		//requestTime.add(offset);

		OadrUpdateReportType updateReport = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile(filename).getOadrUpdateReport());
		updateReport.setRequestID(OadrUtil.createUniqueUpdReportRequestID());
		updateReport.setVenID(new PropertiesFileReader().getVenID());

		OadrReportType report = updateReport.getOadrReport().get(0);
		String reportName = getReportName(reportRequest);

		report.setReportRequestID(reportRequest.getReportRequestID());
		report.setReportSpecifierID(reportRequest.getReportSpecifier().getReportSpecifierID());
		report.setReportName(reportName);
		report.setCreatedDateTime(currentTime);
		report.getDtstart().setDateTime(requestTime);

		for(int i=0;i<6;i++){
			setIntervalRIDs_fwd(i, report, reportRequest, requestTime);
		}

		return updateReport;
	}

	public static OadrUpdateReportType loadOadrUpdateReport_Update004(OadrCreateReportType createReport)
			throws JAXBException, FileNotFoundException, UnsupportedEncodingException {

		String filename = "Update_004.xml";
		OadrReportRequestType reportRequest = createReport.getOadrReportRequest().get(0);
		XMLGregorianCalendar currentTime = OadrUtil.getCurrentTime();

		OadrUpdateReportType updateReport = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile(filename).getOadrUpdateReport());
		updateReport.setRequestID(OadrUtil.createUniqueUpdReportRequestID());
		updateReport.setVenID(new PropertiesFileReader().getVenID());

		OadrReportType report = updateReport.getOadrReport().get(0);
		report.setReportRequestID(reportRequest.getReportRequestID());
		report.setReportSpecifierID(reportRequest.getReportSpecifier().getReportSpecifierID());

		String reportName = getReportName(reportRequest);
		report.setReportName(reportName);
		report.setCreatedDateTime(currentTime);

		XMLGregorianCalendar timeMinusHalfDuration = (XMLGregorianCalendar) currentTime.clone();
		String durationText = "-PT30M"; // "-" + OadrUtil.divideBy2(xmlDBUtil.getAttributeValue(reportNode, "duration"));
		Duration offset = OadrUtil.createDuration(durationText);
		timeMinusHalfDuration.add(offset);

		report.getDtstart().setDateTime(timeMinusHalfDuration);

		setInterval1RID(0, 10, report, reportRequest, timeMinusHalfDuration, false);
		setInterval1RID(1, 10, report, reportRequest, timeMinusHalfDuration, false);
		setInterval1RID(2, 10, report, reportRequest, timeMinusHalfDuration, false);

		return updateReport;
	}

	public static OadrUpdateReportType loadOadrUpdateReport_Update005(OadrCreateReportType createReport)
			throws JAXBException, FileNotFoundException, UnsupportedEncodingException {

		String filename = "Update_005.xml";
		OadrReportRequestType reportRequest = createReport.getOadrReportRequest().get(0);
		XMLGregorianCalendar currentTime = OadrUtil.getCurrentTime();

		OadrUpdateReportType updateReport = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile(filename).getOadrUpdateReport());
		updateReport.setRequestID(OadrUtil.createUniqueUpdReportRequestID());
		updateReport.setVenID(new PropertiesFileReader().getVenID());

		OadrReportType report = updateReport.getOadrReport().get(0);
		String reportName = getReportName(reportRequest);

		report.setReportRequestID(reportRequest.getReportRequestID());
		report.setReportSpecifierID(reportRequest.getReportSpecifier().getReportSpecifierID());
		report.setReportName(reportName);
		report.setCreatedDateTime(currentTime);
		report.getDtstart().setDateTime(currentTime);

		setInterval2RIDs(0, report, reportRequest, currentTime);

		return updateReport;
	}

	public static OadrUpdateReportType loadOadrUpdateReport_Update006(OadrCreateReportType createReport)
			throws JAXBException, FileNotFoundException, UnsupportedEncodingException {

		String filename = "Update_006.xml";
		OadrReportRequestType reportRequest = createReport.getOadrReportRequest().get(0);
		XMLGregorianCalendar currentTime = OadrUtil.getCurrentTime();

		OadrUpdateReportType updateReport = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile(filename).getOadrUpdateReport());
		updateReport.setRequestID(OadrUtil.createUniqueUpdReportRequestID());
		updateReport.setVenID(new PropertiesFileReader().getVenID());

		OadrReportType report = updateReport.getOadrReport().get(0);
		String reportName = getReportName(reportRequest);

		report.setReportRequestID(reportRequest.getReportRequestID());
		report.setReportSpecifierID(reportRequest.getReportSpecifier().getReportSpecifierID());
		report.setReportName(reportName);
		report.setCreatedDateTime(currentTime);
		report.getDtstart().setDateTime(currentTime);

		setInterval1RID_fwd(0, 1, report, reportRequest, currentTime, true);
		setInterval1RID_fwd(1, 1, report, reportRequest, currentTime, true);

		return updateReport;
	}

	public static OadrUpdateReportType loadOadrUpdateReport_Update007(OadrCreateReportType createReport)
			throws JAXBException, FileNotFoundException, UnsupportedEncodingException {

		String filename = "Update_007.xml";
		OadrReportRequestType reportRequest = createReport.getOadrReportRequest().get(0);
		XMLGregorianCalendar currentTime = OadrUtil.getCurrentTime();
		PropertiesFileReader properties = new PropertiesFileReader();

		OadrUpdateReportType updateReport = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile(filename).getOadrUpdateReport());
		updateReport.setRequestID(OadrUtil.createUniqueUpdReportRequestID());
		updateReport.setVenID(properties.getVenID());

		String resourceID1 = properties.getOneResourceID();
		String marketContext = properties.get("DR_MarketContext_1_Name");

		int i = 0;
		for (OadrReportType report : updateReport.getOadrReport()) {
			report.setReportRequestID(reportRequest.getReportRequestID());
			report.setReportSpecifierID(OadrUtil.createUniqueReportSpecifierID() + "_" + i++);
			report.setCreatedDateTime(currentTime);

			int j = 0;
			List<OadrReportDescriptionType> reportDescriptions = report.getOadrReportDescription();
			for (OadrReportDescriptionType reportDescription : reportDescriptions) {
				EiTargetType targetType =  reportDescription.getReportDataSource();
				targetType.getResourceID().clear();
				targetType.getResourceID().add(resourceID1);

				String rID = OadrUtil.createUniqueRID( )+ "_" + j++;			 		
				reportDescription.setRID(rID);					
				reportDescription.setMarketContext(marketContext);			 		
			}
		}

		return updateReport;
	}

	public static OadrUpdateReportType loadOadrUpdateReport_Update008(OadrCreateReportType createReport)
			throws JAXBException, FileNotFoundException, UnsupportedEncodingException {

		String filename = "Update_008.xml";
		OadrReportRequestType reportRequest = createReport.getOadrReportRequest().get(0);
		XMLGregorianCalendar currentTime = OadrUtil.getCurrentTime();
		PropertiesFileReader properties = new PropertiesFileReader();

		OadrUpdateReportType updateReport = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile(filename).getOadrUpdateReport());
		updateReport.setRequestID(OadrUtil.createUniqueUpdReportRequestID());
		updateReport.setVenID(properties.getVenID());

		OadrReportType report = updateReport.getOadrReport().get(0);
		String reportName = getReportName(reportRequest);

		report.setReportRequestID(reportRequest.getReportRequestID());
		report.setReportSpecifierID(reportRequest.getReportSpecifier().getReportSpecifierID());
		report.setReportName(reportName);
		report.setCreatedDateTime(currentTime);
		report.getDtstart().setDateTime(currentTime);

		setInterval1RID(0, 0, report, reportRequest, currentTime, true);

		return updateReport;
	}

	public static OadrUpdateReportType loadOadrUpdateReport_Update009(OadrReportRequestType reportRequest1, OadrReportRequestType reportRequest2)
			throws JAXBException, FileNotFoundException, UnsupportedEncodingException {

		String filename = "Update_009.xml";
		XMLGregorianCalendar currentTime = OadrUtil.getCurrentTime();

		OadrUpdateReportType updateReport = ((OadrUpdateReportType) new SchemaHelper().loadTestDataXMLFile(filename).getOadrUpdateReport());
		updateReport.setRequestID(OadrUtil.createUniqueUpdReportRequestID());
		updateReport.setVenID(new PropertiesFileReader().getVenID());

		OadrReportType report1 = updateReport.getOadrReport().get(0);
		String reportName1 = getReportName(reportRequest1);

		report1.setReportRequestID(reportRequest1.getReportRequestID());
		report1.setReportSpecifierID(reportRequest1.getReportSpecifier().getReportSpecifierID());
		report1.setReportName(reportName1);
		report1.setCreatedDateTime(currentTime);
		report1.getDtstart().setDateTime(currentTime);

		setInterval1RID(0, 0, report1, reportRequest1, currentTime, false);

		OadrReportType report2 = updateReport.getOadrReport().get(1);
		String reportName2 = getReportName(reportRequest2);

		report2.setReportRequestID(reportRequest2.getReportRequestID());
		report2.setReportSpecifierID(reportRequest2.getReportSpecifier().getReportSpecifierID());
		report2.setReportName(reportName2);
		report2.setCreatedDateTime(currentTime);
		report2.getDtstart().setDateTime(currentTime);

		setInterval2RIDs(0, report2, reportRequest2, currentTime);

		return updateReport;
	}

	private static void setInterval2RIDs(int index, OadrReportType report, OadrReportRequestType reportRequest, XMLGregorianCalendar currentTime) {
		Duration offset = OadrUtil.createDuration(false, 0, 0, 0, 0, 10 * index, 0);
		XMLGregorianCalendar dtstart = (XMLGregorianCalendar) currentTime.clone();
		dtstart.add(offset);

		IntervalType interval = report.getIntervals().getInterval().get(index);
		interval.getDtstart().setDateTime(dtstart);

		List<SpecifierPayloadType> specifierPayload = reportRequest.getReportSpecifier().getSpecifierPayload();
		String rID1 = specifierPayload.get(0).getRID();
		String rID2 = specifierPayload.get(1).getRID();

		List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
		OadrReportPayloadType reportPayload1 = (OadrReportPayloadType) streamPayloadBase.get(0).getValue();
		OadrReportPayloadType reportPayload2 = (OadrReportPayloadType) streamPayloadBase.get(1).getValue();

		reportPayload1.setRID(rID1);
		reportPayload2.setRID(rID2);
	}

	private static void setIntervalRIDs_fwd(int index, OadrReportType report, OadrReportRequestType reportRequest, XMLGregorianCalendar currentTime) {
		Duration offset = OadrUtil.createDuration(true, 0, 0, 0, 0, 5 * index, 0);
		XMLGregorianCalendar dtstart = (XMLGregorianCalendar) currentTime.clone();
		dtstart.add(offset);

		IntervalType interval = report.getIntervals().getInterval().get(index);
		interval.getDtstart().setDateTime(dtstart);

		List<SpecifierPayloadType> specifierPayload = reportRequest.getReportSpecifier().getSpecifierPayload();

		ListIterator<SpecifierPayloadType> reqSpecifierPayloadIterator = specifierPayload.listIterator();
		ArrayList<String> ridList = new ArrayList<String>();

		//Get RIDs from Request
		while(reqSpecifierPayloadIterator.hasNext()){
			SpecifierPayloadType eachSpecifierPayload = reqSpecifierPayloadIterator.next();
			ridList.add(eachSpecifierPayload.getRID());
		}

		List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();

		JAXBElement<? extends StreamPayloadBaseType> report1 = Clone.clone(streamPayloadBase.get(0));
		JAXBElement<? extends StreamPayloadBaseType> report2 = Clone.clone(streamPayloadBase.get(0));

		streamPayloadBase.remove(0);
		streamPayloadBase.remove(0);

		boolean isUseFirtReportFrmTemplate=true;

		for(String eachRID:ridList){

			OadrReportPayloadType reportPayload = null;
			JAXBElement<? extends StreamPayloadBaseType> reportToAdd = null;
			if(isUseFirtReportFrmTemplate){
				reportToAdd = Clone.clone(report1);
				isUseFirtReportFrmTemplate=false;
			}else{
				reportToAdd = Clone.clone(report2);
				isUseFirtReportFrmTemplate=true;
			}

			reportPayload = (OadrReportPayloadType) reportToAdd.getValue();
			reportPayload.setRID(eachRID);
			streamPayloadBase.add(reportToAdd);

		}

	}

	private static void setInterval1RID(int index, int minutesOffset, OadrReportType report, OadrReportRequestType reportRequest, XMLGregorianCalendar currentTime, boolean hasDtstart) {
		Duration offset = OadrUtil.createDuration(false, 0, 0, 0, 0, (minutesOffset * index), 0);
		XMLGregorianCalendar dtstart = (XMLGregorianCalendar) currentTime.clone();
		dtstart.add(offset);

		List<SpecifierPayloadType> specifierPayload = reportRequest.getReportSpecifier().getSpecifierPayload();
		String rID = specifierPayload.get(0).getRID();

		IntervalType interval = report.getIntervals().getInterval().get(index);
		if (hasDtstart) {
			interval.getDtstart().setDateTime(dtstart);
		}

		List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
		OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(0).getValue();

		reportPayload.setRID(rID);
	}

	private static void setInterval1RID_fwd(int index, int minutesOffset, OadrReportType report, OadrReportRequestType reportRequest, XMLGregorianCalendar currentTime, boolean hasDtstart) {
		//This provides a method for putting the dstart times in the proper order where the idea is to create a time stream forward from the current time.
		Duration offset = OadrUtil.createDuration(true, 0, 0, 0, 0, (minutesOffset * index), 0);
		XMLGregorianCalendar dtstart = (XMLGregorianCalendar) currentTime.clone();
		dtstart.add(offset);

		List<SpecifierPayloadType> specifierPayload = reportRequest.getReportSpecifier().getSpecifierPayload();
		String rID = specifierPayload.get(0).getRID();

		IntervalType interval = report.getIntervals().getInterval().get(index);
		if (hasDtstart) {
			interval.getDtstart().setDateTime(dtstart);
		}

		List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
		OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(0).getValue();

		reportPayload.setRID(rID);
	}

	private static void setInterval1RID_fwd_Value(int index, int minutesOffset, OadrReportType report, OadrReportRequestType reportRequest, XMLGregorianCalendar currentTime, boolean hasDtstart, float value) {
		//This provides a method for putting the dstart times in the proper order where the idea is to create a time stream forward from the current time.
		Duration offset = OadrUtil.createDuration(true, 0, 0, 0, 0, (minutesOffset * index), 0);
		XMLGregorianCalendar dtstart = (XMLGregorianCalendar) currentTime.clone();
		dtstart.add(offset);

		List<SpecifierPayloadType> specifierPayload = reportRequest.getReportSpecifier().getSpecifierPayload();
		String rID = specifierPayload.get(0).getRID();

		IntervalType interval = report.getIntervals().getInterval().get(index);
		if (hasDtstart) {
			interval.getDtstart().setDateTime(dtstart);
		}

		List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
		OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(0).getValue();
		((PayloadFloatType)reportPayload.getPayloadBase().getValue()).setValue(value);
		reportPayload.setRID(rID);
	}

	private static void setInterval_RID_fwd_SEC_Value(int index, int secondsOffset, OadrReportType report, OadrReportRequestType reportRequest, XMLGregorianCalendar currentTime, boolean hasDtstart) {
		//This provides a method for putting the dstart times in the proper order where the idea is to create a time stream forward from the current time.
		Duration offset = OadrUtil.createDuration(true, 0, 0, 0, 0, 0, secondsOffset*index);
		XMLGregorianCalendar dtstart = (XMLGregorianCalendar) currentTime.clone();
		dtstart.add(offset);

		List<SpecifierPayloadType> specifierPayload = reportRequest.getReportSpecifier().getSpecifierPayload();

		IntervalType interval = report.getIntervals().getInterval().get(0);
		if (hasDtstart) {
			interval.getDtstart().setDateTime(dtstart);
		}
		String rID1 = specifierPayload.get(0).getRID();
		String rID2 = specifierPayload.get(1).getRID();

		String minMax[] = new PropertiesFileReader().get("TelemetryUsage_PowerReal_Range").split(",");
		Float min = Float.parseFloat(minMax[0]);
		Float max = Float.parseFloat(minMax[1]);
		Float rng = max - min;
		Float fbase = 0.0f;
		if(TestSession.dataValue == null){
			fbase = (float) (Math.random()*((rng)*0.9) + min);
		} else {
			fbase = TestSession.dataValue;
		}
		Float value1 = (float) (Math.random()*((rng)/10.0) + fbase);
		TestSession.dataValue = value1;		
		minMax = new PropertiesFileReader().get("TelemetryUsage_Voltage_Range").split(",");
		min = Float.parseFloat(minMax[0]);
		max = Float.parseFloat(minMax[1]);
		rng = max - min;
		fbase = 0.0f;
		if(TestSession.dataValue2 == null){
			fbase = (float) (Math.random()*((rng)*0.9) + min);
		} else {
			fbase = TestSession.dataValue2;
		}
		value1 = (float) (Math.random()*((rng)/10.0) + fbase);
		TestSession.dataValue2 = value1;	
		Float valuex = null;
		Float valuey = null;
		if(rID1.contains( new PropertiesFileReader().get("TelemetryUsage_Voltage_WellKnown_rID"))){
			valuex = TestSession.dataValue2;
			valuey = TestSession.dataValue;
		} else {
			valuex = TestSession.dataValue;
			valuey = TestSession.dataValue2;
		}

		List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
		OadrReportPayloadType reportPayload1 = (OadrReportPayloadType) streamPayloadBase.get(0).getValue();
		OadrReportPayloadType reportPayload2 = (OadrReportPayloadType) streamPayloadBase.get(1).getValue();

		reportPayload1.setRID(rID1);
		((PayloadFloatType)reportPayload1.getPayloadBase().getValue()).setValue(valuex);
		reportPayload2.setRID(rID2);
		((PayloadFloatType)reportPayload2.getPayloadBase().getValue()).setValue(valuey);
	}

	private static void setInterval_RID_fwd_MIN_Value(int index, int minOffset, OadrReportType report, OadrReportRequestType reportRequest, XMLGregorianCalendar currentTime, boolean hasDtstart) {
		//This provides a method for putting the dstart times in the proper order where the idea is to create a time stream forward from the current time.
		Duration offset = OadrUtil.createDuration(true, 0, 0, 0, 0, minOffset*index, 0);
		XMLGregorianCalendar dtstart = (XMLGregorianCalendar) currentTime.clone();
		dtstart.add(offset);
		PropertiesFileReader properties = new PropertiesFileReader();

		List<SpecifierPayloadType> specifierPayload = reportRequest.getReportSpecifier().getSpecifierPayload();

		IntervalType interval = report.getIntervals().getInterval().get(0);
		if (hasDtstart) {
			interval.getDtstart().setDateTime(dtstart);
		}
		for (int j=0; j<specifierPayload.size();j++){
			String rID = specifierPayload.get(j).getRID();
			if(rID.contains( properties.get("ThermostatState_Status_WellKnown_rID"))){
				String minMax[] = properties.get("ThermostatState_Status_LevelOffset_Current_Range").split(",");
				Float min = Float.parseFloat(minMax[0]);
				Float max = Float.parseFloat(minMax[1]);
				Float rng = max - min;
				Float fbase = 0.0f;
				if(TestSession.dataValue == null){
					fbase = (float) (int)(Math.random()*(rng + 1) + min);
				} else {
					fbase = TestSession.dataValue;
				}
				Float valuex = fbase;
				TestSession.dataValue = valuex;		
				List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(j).getValue();
				reportPayload.setRID(rID);
				((OadrPayloadResourceStatusType)reportPayload.getPayloadBase().getValue()).setOadrManualOverride(false);
				((OadrPayloadResourceStatusType)reportPayload.getPayloadBase().getValue()).setOadrOnline(true);
				((OadrPayloadResourceStatusType)reportPayload.getPayloadBase().getValue()).getOadrLoadControlState().getOadrLevelOffset().setOadrCurrent(valuex);

			}
			if(rID.contains( properties.get("ThermostatState_CurrentTemp_WellKnown_rID"))){
				String minMax[] = properties.get("ThermostatState_CurrentTemp_Range").split(",");
				Float min = Float.parseFloat(minMax[0]);
				Float max = Float.parseFloat(minMax[1]);
				Float rng = max - min;
				Float fbase = 0.0f;
				if(TestSession.dataValue2 == null){
					fbase = (float) ((int) ((int)(Math.random()*(rng + 1) + min)*10.0f))/10.0f;
				} else {
					fbase = TestSession.dataValue2;
					fbase = (float)((int) ((Math.random()*((rng)/100.0) + fbase)*10.0f))/10.0f;
				}
				Float valuex = fbase;
				TestSession.dataValue2 = valuex;	
				List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(j).getValue();
				reportPayload.setRID(rID);
				((PayloadFloatType)reportPayload.getPayloadBase().getValue()).setValue(valuex);
			}
			if(rID.contains( properties.get("ThermostatState_HeatTempSetting_WellKnown_rID"))){
				String minMax[] = properties.get("ThermostatState_HeatTempSetting_Range").split(",");
				Float min = Float.parseFloat(minMax[0]);
				Float max = Float.parseFloat(minMax[1]);
				Float rng = max - min;
				Float fbase = 0.0f;
				if(TestSession.dataValue3 == null){
					fbase = (float) (int)(Math.random()*(rng + 1) + min);
				} else {
					fbase = TestSession.dataValue3;
				}
				Float valuex = fbase;
				TestSession.dataValue3 = valuex;		
				List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(j).getValue();
				reportPayload.setRID(rID);
				((PayloadFloatType)reportPayload.getPayloadBase().getValue()).setValue(valuex);
			}
			if(rID.contains( properties.get("ThermostatState_CoolTempSetting_WellKnown_rID"))){
				String minMax[] = properties.get("ThermostatState_CoolTempSetting_Range").split(",");
				Float min = Float.parseFloat(minMax[0]);
				Float max = Float.parseFloat(minMax[1]);
				Float rng = max - min;
				Float fbase = 0.0f;
				if(TestSession.dataValue4 == null){
					fbase = (float) (int)(Math.random()*(rng + 1) + min);
				} else {
					fbase = TestSession.dataValue4;
				}
				Float valuex = fbase;
				TestSession.dataValue4 = valuex;		
				List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(j).getValue();
				reportPayload.setRID(rID);
				((PayloadFloatType)reportPayload.getPayloadBase().getValue()).setValue(valuex);
			}
			if(rID.contains( properties.get("ThermostatState_HVACModeSetting_WellKnown_rID"))){
				String minMax[] = properties.get("ThermostatState_HVACModeSetting_Range").split(",");
				Float min = Float.parseFloat(minMax[0]);
				Float max = Float.parseFloat(minMax[1]);
				Float rng = max - min;
				Float fbase = 0.0f;
				if(TestSession.dataValue5 == null){
					fbase = (float) (int)(Math.random()*(rng + 1) + min);
				} else {
					fbase = TestSession.dataValue5;
				}
				Float valuex = fbase;
				TestSession.dataValue5 = valuex;		
				List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(j).getValue();
				reportPayload.setRID(rID);
				((PayloadFloatType)reportPayload.getPayloadBase().getValue()).setValue(valuex);
			}
			if(rID.contains( properties.get("ThermostatState_CurrentHVACMode_WellKnown_rID"))){
				String minMax[] = properties.get("ThermostatState_HVACMode_Range").split(",");
				Float min = Float.parseFloat(minMax[0]);
				Float max = Float.parseFloat(minMax[1]);
				Float rng = max - min;
				Float fbase = 0.0f;
				if(TestSession.dataValue6 == null){
					fbase = (float) (int)(Math.random()*(rng + 1) + min);
				} else {
					fbase = TestSession.dataValue6;
				}
				Float valuex = fbase;
				TestSession.dataValue6 = valuex;		
				List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(j).getValue();
				reportPayload.setRID(rID);
				((PayloadFloatType)reportPayload.getPayloadBase().getValue()).setValue(valuex);
			}
			if(rID.contains( properties.get("ThermostatState_FanModeSetting_WellKnown_rID"))){
				String minMax[] = properties.get("ThermostatState_FanModeSetting_Range").split(",");
				Float min = Float.parseFloat(minMax[0]);
				Float max = Float.parseFloat(minMax[1]);
				Float rng = max - min;
				Float fbase = 0.0f;
				if(TestSession.dataValue7 == null){
					fbase = (float) (int)(Math.random()*(rng + 1) + min);
				} else {
					fbase = TestSession.dataValue7;
				}
				Float valuex = fbase;
				TestSession.dataValue7 = valuex;		
				List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(j).getValue();
				reportPayload.setRID(rID);
				((PayloadFloatType)reportPayload.getPayloadBase().getValue()).setValue(valuex);
			}
			if(rID.contains( properties.get("ThermostatState_CurrentHoldMode_WellKnown_rID"))){
				String minMax[] = properties.get("ThermostatState_CurrentHoldMode_Range").split(",");
				Float min = Float.parseFloat(minMax[0]);
				Float max = Float.parseFloat(minMax[1]);
				Float rng = max - min;
				Float fbase = 0.0f;
				if(TestSession.dataValue8 == null){
					fbase = (float) (int)(Math.random()*(rng + 1) + min);
				} else {
					fbase = TestSession.dataValue8;
				}
				Float valuex = fbase;
				TestSession.dataValue8 = valuex;		
				List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(j).getValue();
				reportPayload.setRID(rID);
				((PayloadFloatType)reportPayload.getPayloadBase().getValue()).setValue(valuex);
			}
			if(rID.contains( properties.get("ThermostatState_CurrentAwayMode_WellKnown_rID"))){
				String minMax[] = properties.get("ThermostatState_CurrentAwayMode_Range").split(",");
				Float min = Float.parseFloat(minMax[0]);
				Float max = Float.parseFloat(minMax[1]);
				Float rng = max - min;
				Float fbase = 0.0f;
				if(TestSession.dataValue9 == null){
					fbase = (float) (int)(Math.random()*(rng + 1) + min);
				} else {
					fbase = TestSession.dataValue9;
				}
				Float valuex = fbase;
				TestSession.dataValue9 = valuex;		
				List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(j).getValue();
				reportPayload.setRID(rID);
				((PayloadFloatType)reportPayload.getPayloadBase().getValue()).setValue(valuex);
			}
			if(rID.contains( properties.get("ThermostatState_CurrentHumidity_WellKnown_rID"))){
				String minMax[] = properties.get("ThermostatState_CurrentHumidity_Range").split(",");
				Float min = Float.parseFloat(minMax[0]);
				Float max = Float.parseFloat(minMax[1]);
				Float rng = max - min;
				Float fbase = 0.0f;
				if(TestSession.dataValue10 == null){
					fbase = (float) ((int) ((int)(Math.random()*(rng + 1) + min)*10.0f))/10.0f;
				} else {
					fbase = TestSession.dataValue10;
					fbase = (float)((int) ((Math.random()*((rng)/100.0) + fbase)*10.0f))/10.0f;
				}
				Float valuex = fbase;
				TestSession.dataValue10 = valuex;		
				List<JAXBElement<? extends StreamPayloadBaseType>> streamPayloadBase = interval.getStreamPayloadBase();
				OadrReportPayloadType reportPayload = (OadrReportPayloadType) streamPayloadBase.get(j).getValue();
				reportPayload.setRID(rID);
				((PayloadFloatType)reportPayload.getPayloadBase().getValue()).setValue(valuex);
			}
		}
	}

	private static String getReportName(OadrReportRequestType reportRequest) {
		String reportName = new XMLDBUtil().getReportNameFromReportsReceivedFromVEN(reportRequest.getReportSpecifier().getReportSpecifierID());
		if (StringUtils.isBlank(reportName)) {
			throw new FailedException("Unrecognized ReportSpecifierID=" + reportRequest.getReportSpecifier().getReportSpecifierID());
		}
		return reportName;
	}
}
