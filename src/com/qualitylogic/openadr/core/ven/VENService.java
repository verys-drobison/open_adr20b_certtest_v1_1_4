package com.qualitylogic.openadr.core.ven;

import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.channel.Listener;
import com.qualitylogic.openadr.core.channel.factory.ChannelFactory;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;
import com.qualitylogic.openadr.core.ven.channel.EiRegisterPartyVENHandler;
import com.qualitylogic.openadr.core.ven.channel.EiReportVENHandler;
import com.qualitylogic.openadr.core.ven.channel.ResourceHandler;
import com.qualitylogic.openadr.core.ven.channel.VENHandler;

public class VENService {

	private static Listener listener;
	
	public static void startVENService() throws Exception {
		PropertiesFileReader propertiesFileReader = new PropertiesFileReader();

    	listener = ChannelFactory.getListener();
    	
    	if (propertiesFileReader.isXmppTransportType()) {
	    	listener.addHandler(propertiesFileReader.getXmppVENResourceName(), new ResourceHandler());
    	} else {
    		listener.addHandler("EiReport", new EiReportVENHandler());
    		listener.addHandler("EiEvent", new VENHandler());
    		listener.addHandler("EiRegisterParty", new EiRegisterPartyVENHandler());
    	}
    	
    	listener.start(ServiceType.VEN);

	}

	public static void stopVENService() throws Exception {
		if (listener != null) {
			listener.stop();
		}

	}


}
