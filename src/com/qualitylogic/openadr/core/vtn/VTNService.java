package com.qualitylogic.openadr.core.vtn;

import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.channel.BusinessLogic;
import com.qualitylogic.openadr.core.channel.Listener;
import com.qualitylogic.openadr.core.channel.factory.ChannelFactory;
import com.qualitylogic.openadr.core.oadrpoll.channel.OadrPollHandler;
import com.qualitylogic.openadr.core.vtn.channel.EiOptVTNHandler;
import com.qualitylogic.openadr.core.vtn.channel.EiRegisterPartyVTNHandler;
import com.qualitylogic.openadr.core.vtn.channel.EiReportVTNHandler;
import com.qualitylogic.openadr.core.vtn.channel.VTNHandler;

public class VTNService {

	private static Listener listener;
	
	public static void startVTNService() throws Exception {
    	listener = ChannelFactory.getListener();
    	listener.addHandler("EiReport", new EiReportVTNHandler());
		listener.addHandler("EiEvent", new VTNHandler());
		listener.addHandler("EiOpt", new EiOptVTNHandler());
		listener.addHandler("EiRegisterParty", new EiRegisterPartyVTNHandler());
		if (BusinessLogic.isOadrPollAllowed()) {
			listener.addHandler("OadrPoll", new OadrPollHandler());
		}
    	
    	listener.start(ServiceType.VTN);

	}

	public static void stopVTNService() throws Exception {
    	if (listener != null) {
    		listener.stop();
    	}

	}

}
