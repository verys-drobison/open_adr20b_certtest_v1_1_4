package testcases.pull.DR_Program_Guide.cbp.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.IResponseCreateReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_CBP_CMP_Action;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCreateReportRequest_CBP_CMP_Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;
import com.qualitylogic.openadr.core.util.LogResult;

public class CBP_CMP_B1_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new CBP_CMP_B1_TH_VTN_1());
	}
	
	@Override
	public void test() throws Exception {

		ConformanceRuleValidator.setDisableRegisterReport_VenReportNameValid_Check(true);
		
		listenForRequests();
		String ptext ="If this is a self-test, please click 'Yes' to respond to a RegisterReport. If testing a real DUT click 'No' to skip the RegisterReport.\n\n";
		ptext +="This test case assumes the following report was offered as part of the registration process with the DUT:\n\n";
		ptext +="\tReport Name: TELEMETRY_USAGE\n";
		ptext +="\tReport Frequency: every 1 minute\n";
		ptext +="\tReport Granularity: 1 minute\n";
		ptext +="\t\tReport Type: usage\n";
		ptext +="\t\tUnits: powerReal\n";
		ptext +="\t\tReading Type: Direct Read\n";
		ptext +="\t\tWell Known rID Property: ![TelemetryUsage_PowerReal_WellKnown_rID!]\n";
		ptext +="\t\tWell Known rID Values: ![TelemetryUsage_PowerReal_Range!]\n\n";
		boolean yesClicked = promptYes(ptext);
		if (!yesClicked) {
			LogHelper.addTrace("TestCase RegisterReport was skipped by the user");
			telemetry_Usage_CBP_DBCheck();
		} else {
			addRegisteredReportResponse().setlastEvent(true);

			waitForCompletion();
			checkRegisterReportRequest_CBP(1);
			telemetry_Usage_CBP_DBCheck();
		}
		
		String propFileResourceID[]= properties.getTwoResourceID();
		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_CBP_CMP_Action();
		distributeEvent.setEventCompleted(false);
		ICreatedEventResult createdEvent = queueDistributeEvent(distributeEvent);
		createdEvent.setLastCreatedEventResult(true);
		createdEvent.setExpectedOptInCount(1);
		
		addResponse();

		ptext = "Please confirm that the VEN DUT is configured to have its associated load shedding resource(s) ";
		ptext +="respond to the BID_LOAD signal values shown below. ";
		ptext +="The event your VEN will receive will have the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 4 minutes\n";
		ptext +="\tRandomization: None\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 2\n";
		ptext +="\tSignal Name: BID_LOAD\n";
		ptext +="\t\tSignal Type: setpoint\n";
		ptext +="\t\tNumber of intervals: 2\n";
		ptext +="\t\tInterval Duration(s):2 minutes, 2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Bid_Load_Setpoint_High!]\n\t\t\t\t![Bid_Load_Setpoint_Medium!] \n";
		ptext +="\t\tUnits: powerReal\n";
		ptext +="\t\titemUnits: ![powerReal_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![powerReal_siScaleCode!] \n";
		ptext +="\t\t\tHertz: ![powerReal_powerAttributes_hertz!] \n";
		ptext +="\t\t\tVoltage: ![powerReal_powerAttributes_voltage!] \n";
		ptext +="\t\t\tAc: ![powerReal_powerAttributes_ac!] \n";
		ptext +="\tSignal Name: BID_PRICE\n";
		ptext +="\t\tSignal Type: price\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 4 minutes\n";
		ptext +="\t\tInterval Value(s): ![Bid_Price_Value!] \n";
		ptext +="\t\tUnits: currencyPerKWh\n";
		ptext +="\t\t\titemUnits: ![currencyPerKWh_itemUnits!] \n";
		ptext +="\t\t\tsiScaleCode: ![currencyPerKWh_siScaleCode!] \n";
		ptext +="\tEvent Target(s): " + propFileResourceID[0] + ", " + propFileResourceID[1] + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n";
		ptext +="\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		TestSession.setTestCaseDone(false);
		waitForCompletion();

		checkCreatedEventReceived(createdEvent);

		//buffer the responses
		IResponseCreateReportTypeAckAction createReport = new DefaultResponseCreateReportRequest_CBP_CMP_Action(ServiceType.VTN);
		queueResponse(createReport);
	
		addResponse();
		
		addUpdatedReportResponse().setlastEvent(false);

		addUpdatedReportResponse().setlastEvent(true);

		String eitargs = "";
		int ix = distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getResourceID().size();
		String comma = " ";
		for (int i=0; i<ix; i++){
			eitargs +=comma + distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getResourceID().get(i);
			comma = ", ";
		}
		ptext = "Observe transition from Bid_Load_Setpoint_High to Bid_Load_Setpoint_Medium and that ONLY the resources targeted ";
		ptext += "by this event (as shown below) responded as expected to the signal value(s) sent:\n\n";
		ptext += "\tTargeted Resoures:" + eitargs + "\n";
		ptext +="\tSignal Name: BID_LOAD\n";
		ptext +="\t\tSignal Type: setpoint\n";
		ptext +="\t\tNumber of intervals: 2\n";
		ptext +="\t\tInterval Duration(s):2 minutes, 2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Bid_Load_Setpoint_High!]\n\t\t\t\t![Bid_Load_Setpoint_Medium!] \n";
		ptext +="\t\tUnits: powerReal\n";
		ptext +="\t\titemUnits: ![powerReal_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![powerReal_siScaleCode!] \n";
		ptext +="\t\t\tHertz: ![powerReal_powerAttributes_hertz!] \n";
		ptext +="\t\t\tVoltage: ![powerReal_powerAttributes_voltage!] \n";
		ptext +="\t\t\tAc: ![powerReal_powerAttributes_ac!] \n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		

		pause(100);
		//TestSession.setTestCaseDone(false);
		//waitForCompletion();
		checkCreatedReport(1);
		checkUpdateReport_CBP(2);
		
		
		ptext = "Confirm that ONLY the resources targeted by this event (as shown below) responded as expected to the signal value(s) sent:\n\n";
		ptext += "\tTargeted Resoures:" + eitargs + "\n";
		ptext +="\tSignal Name: BID_LOAD\n";
		ptext +="\t\tSignal Type: setpoint\n";
		ptext +="\t\tNumber of intervals: 2\n";
		ptext +="\t\tInterval Duration(s):2 minutes, 2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Bid_Load_Setpoint_High!]\n\t\t\t\t![Bid_Load_Setpoint_Medium!] \n";
		ptext +="\t\tUnits: powerReal\n";
		ptext +="\t\titemUnits: ![powerReal_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![powerReal_siScaleCode!] \n";
		ptext +="\t\t\tHertz: ![powerReal_powerAttributes_hertz!] \n";
		ptext +="\t\t\tVoltage: ![powerReal_powerAttributes_voltage!] \n";
		ptext +="\t\t\tAc: ![powerReal_powerAttributes_ac!] \n\n";
		yesClicked = promptYes(ptext);
		if (!yesClicked) {
			throw new FailedException("The VEN did not shed load as expected.");
		}
	}
}
