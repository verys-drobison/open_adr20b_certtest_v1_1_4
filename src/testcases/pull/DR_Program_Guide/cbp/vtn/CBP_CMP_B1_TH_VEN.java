package testcases.pull.DR_Program_Guide.cbp.vtn;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OptTypeType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.signal.helper.RegisterReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.UpdateReportEventHelper;
import com.qualitylogic.openadr.core.util.LogResult;


public class CBP_CMP_B1_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new CBP_CMP_B1_TH_VEN());
	}

	@Override
	public void test() throws Exception {
		pause(5);
		sendRegisterReport(RegisterReportEventHelper.loadMetadata_001_CBP());
			
		String propFileResourceID[]= properties.getTwoResourceID();

		String ptext = "The VTN DUT should be configured so that there is a single pending Capacity Bidding Program event with the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 4 minutes\n";
		ptext +="\tRandomization: None\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 2\n";
		ptext +="\tSignal Name: BID_LOAD\n";
		ptext +="\t\tSignal Type: setpoint\n";
		ptext +="\t\tNumber of intervals: 2\n";
		ptext +="\t\tInterval Duration(s):2 minutes, 2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Bid_Load_Setpoint_High!]\n\t\t\t\t![Bid_Load_Setpoint_Medium!] \n";
		ptext +="\t\tUnits: powerReal\n";
		ptext +="\t\titemUnits: ![powerReal_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![powerReal_siScaleCode!] \n";
		ptext +="\t\t\tHertz: ![powerReal_powerAttributes_hertz!] \n";
		ptext +="\t\t\tVoltage: ![powerReal_powerAttributes_voltage!] \n";
		ptext +="\t\t\tAc: ![powerReal_powerAttributes_ac!] \n";
		ptext +="\tSignal Name: BID_PRICE\n";
		ptext +="\t\tSignal Type: price\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 4 minutes\n";
		ptext +="\t\tInterval Value(s): ![Bid_Price_Value!] \n";
		ptext +="\t\tUnits: currencyPerKWh\n";
		ptext +="\t\t\titemUnits: ![currencyPerKWh_itemUnits!] \n";
		ptext +="\t\t\tsiScaleCode: ![currencyPerKWh_siScaleCode!] \n";
		ptext +="\tEvent Target(s): " + propFileResourceID[0] + ", " + propFileResourceID[1] + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}


		
		String distributeEventText = pollDistributeEventString();
		sendCreatedEvent(distributeEventText, OptTypeType.OPT_IN);
		
		OadrDistributeEventType distributeEvent = DistributeEventSignalHelper.createOadrDistributeEventFromString(distributeEventText);
		checkDistributeEvent_CBP_CMP(distributeEvent);

		ptext ="Configure DUT_VTN to request periodic report defined in use case data set.\n";
		ptext +="The test expects the following report:\n\n";
		ptext +="\tReport Name: TELEMETRY_USAGE\n";
		ptext +="\tReport Frequency: every 1 minute\n";
		ptext +="\tReport Granularity: 1 minute\n";
		ptext +="\t\tReport Type: usage\n";
		ptext +="\t\tUnits: powerReal\n";
		ptext +="\t\tReading Type: Direct Read\n";
		ptext +="\t\tWell Known rID Property: ![TelemetryUsage_PowerReal_WellKnown_rID!]\n";
		ptext +="\t\tWell Known rID Values: ![TelemetryUsage_PowerReal_Range!]\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		OadrCreateReportType createReport = pollCreateReport();
		
		pollResponse();
		
		checkCreateReport_CBP(createReport);
		
		sendCreatedReport();
		
		sendUpdateReport(UpdateReportEventHelper.loadOadrUpdateReport_Update_CBP(createReport));
		pause(60);
		sendUpdateReport(UpdateReportEventHelper.loadOadrUpdateReport_Update_CBP(createReport));
		prompt("Please re-register the DUT VTN before running any non-Program Guide test cases as the list of available reports sent to the DUT VTN has changed.");
	}
}
