package testcases.pull.DR_Program_Guide.der.vtn;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.LogResult;
import com.qualitylogic.openadr.core.util.XMLDBUtil;


public class DER_TYP_B1_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new DER_TYP_B1_TH_VEN());
	}

	@Override
	public void test() throws Exception {

		XMLDBUtil xmlDB=new XMLDBUtil();

		String ptext = "The VTN DUT should be configured so that there is a single pending Distributed Energy Resources DR Program event with the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 48 minutes\n";
		ptext +="\tRandomization: None\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 1\n";
		ptext +="\tSignal Name: ELECTRICITY_PRICE\n";
		ptext +="\t\tSignal Type: price\n";
		ptext +="\t\tNumber of intervals: 24\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Electricity_Price_Medium!]\n\t\t\t\t![Electricity_Price_High!]\n\t\t\t\talternating\n";
		ptext +="\t\tUnits: currencyPerKWh\n";
		ptext +="\t\titemUnits: ![currencyPerKWh_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![currencyPerKWh_siScaleCode!] \n";
		ptext +="\tEvent Target(s): " + xmlDB.getVENID() + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: never\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		String distributeEventText = pollDistributeEventString();
		
		OadrDistributeEventType distributeEvent = DistributeEventSignalHelper.createOadrDistributeEventFromString(distributeEventText);
		checkDistributeEvent_DER_TYP(distributeEvent);
		pause(5);

		boolean yesClicked = promptYes("If this is a self-test, please click 'Yes' to validate the TH_VTN Cancel Event. If testing a real DUT click 'No' to skip the Cancel Event test.");
		if (!yesClicked) {
			LogHelper.addTrace("TestCase check for Cancel Event was skipped by the user");
		} else {

			distributeEventText = pollDistributeEventString();
			
			distributeEvent = DistributeEventSignalHelper.createOadrDistributeEventFromString(distributeEventText);
			checkDistributeEvent_DER_TYP_Cancel(distributeEvent);
			
		}

	}
}
