package testcases.pull.DR_Program_Guide.evr.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_EVR_TYP_Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.LogResult;
import com.qualitylogic.openadr.core.util.XMLDBUtil;

public class EVR_TYP_B1_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new EVR_TYP_B1_TH_VTN_1());
	}
	
	@Override
	public void test() throws Exception {

		listenForRequests();

		XMLDBUtil xmlDB=new XMLDBUtil();
		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_EVR_TYP_Action();
		ICreatedEventResult createdEvent = queueDistributeEvent(distributeEvent);
		createdEvent.setLastCreatedEventResult(true);
		createdEvent.setExpectedOptInCount(1);
		
		addResponse();

		String ptext = "Please confirm that the VEN DUT is configured to have its associated load shedding resource(s) ";
		ptext +="respond to the Electricity_Price signal values shown below. ";
		ptext +="Note that the VEN may respond to either the SIMPLE or ELECTRICITY_PRICE signal. ";
		ptext +="The event your VEN will receive will have the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 4 minutes\n";
		ptext +="\tRandomization: None\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 2\n";
		ptext +="\tSignal Name: Simple\n";
		ptext +="\t\tSignal Type: level\n";
		ptext +="\t\tNumber of intervals: 3\n";
		ptext +="\t\tInterval Duration(s): 1 minute, 2 minutes, 1 minute\n";
		ptext +="\t\tInterval Value(s):\t![Simple_Medium!],\n\t\t\t\t![Simple_High!],\n\t\t\t\t![Simple_Medium!] \n";
		ptext +="\tSignal Name: ELECTRICITY_PRICE\n";
		ptext +="\t\tSignal Type: price\n";
		ptext +="\t\tNumber of intervals: 3\n";
		ptext +="\t\tInterval Duration(s): 1 minute, 2 minutes, 1 minute\n";
		ptext +="\t\tInterval Value(s):\t![Electricity_Price_Medium!],\n\t\t\t\t![Electricity_Price_High!],\n\t\t\t\t![Electricity_Price_Medium!] \n";
		ptext +="\t\tUnits: currencyPerKWh\n";
		ptext +="\t\t\titemUnits: ![currencyPerKWh_itemUnits!] \n";
		ptext +="\t\t\tsiScaleCode: ![currencyPerKWh_siScaleCode!] \n";
		ptext +="\tEvent Target(s): " + xmlDB.getVENID() + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		waitForCompletion();

		checkCreatedEventReceived(createdEvent);

		String eitargs = "";
		int ix = distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getVenID().size();
		String comma = " ";
		for (int i=0; i<ix; i++){
			eitargs +=comma + distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getVenID().get(i);
			comma = ", ";
		}
		ptext = "Confirm that ONLY the resources targeted by this event (as shown below) responded as expected to ONLY the Simple_High signal value(s) sent:\n\n";
		ptext += "\tTargeted Resoures:" + eitargs + "\n";
		ptext +="\tSignal Name: ELECTRICITY_PRICE or SIMPLE\n";
		ptext +="\t\tSignal Type: price\n";
		ptext +="\t\tNumber of intervals: 3\n";
		ptext +="\t\tInterval Duration(s): 1 minute, 2 minutes, 1 minute\n";
		ptext +="\t\tInterval Value(s):\t![Electricity_Price_Medium!],\n\t\t\t\t![Electricity_Price_High!],\n\t\t\t\t![Electricity_Price_Medium!] \n";
		ptext +="\t\t\tor\n";
		ptext +="\t\t\t\t![Simple_Medium!],\n\t\t\t\t![Simple_High!],\n\t\t\t\t![Simple_Medium!] \n";
		ptext +="\t\tUnits: currencyPerKWh\n";
		ptext +="\t\titemUnits: ![currencyPerKWh_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![currencyPerKWh_siScaleCode!] \n";
		boolean yesClicked = promptYes(ptext);
		if (!yesClicked) {
			throw new FailedException("The VEN did not shed load as expected.");
		}
	}
}
