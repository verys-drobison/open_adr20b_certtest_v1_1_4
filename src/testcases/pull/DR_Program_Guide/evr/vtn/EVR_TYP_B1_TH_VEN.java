package testcases.pull.DR_Program_Guide.evr.vtn;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OptTypeType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.LogResult;
import com.qualitylogic.openadr.core.util.XMLDBUtil;


public class EVR_TYP_B1_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new EVR_TYP_B1_TH_VEN());
	}

	@Override
	public void test() throws Exception {

		XMLDBUtil xmlDB=new XMLDBUtil();

		String ptext = "The VTN DUT should be configured so that there is a single pending Residential Electric Vehicle Time of Use Program event with the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 4 minutes\n";
		ptext +="\tRandomization: None\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 2\n";
		ptext +="\tSignal Name: Simple\n";
		ptext +="\t\tSignal Type: level\n";
		ptext +="\t\tNumber of intervals: 3\n";
		ptext +="\t\tInterval Duration(s): 1 minute, 2 minutes, 1 minute\n";
		ptext +="\t\tInterval Value(s):\t![Simple_Medium!],\n\t\t\t\t![Simple_High!],\n\t\t\t\t![Simple_Medium!] \n";
		ptext +="\tSignal Name: ELECTRICITY_PRICE\n";
		ptext +="\t\tSignal Type: price\n";
		ptext +="\t\tNumber of intervals: 3\n";
		ptext +="\t\tInterval Duration(s): 1 minute, 2 minutes, 1 minute\n";
		ptext +="\t\tInterval Value(s):\t![Electricity_Price_Medium!],\n\t\t\t\t![Electricity_Price_High!],\n\t\t\t\t![Electricity_Price_Medium!] \n";
		ptext +="\t\tUnits: currencyPerKWh\n";
		ptext +="\t\t\titemUnits: ![currencyPerKWh_itemUnits!] \n";
		ptext +="\t\t\tsiScaleCode: ![currencyPerKWh_siScaleCode!] \n";
		ptext +="\tEvent Target(s): " + xmlDB.getVENID() + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		String distributeEventText = pollDistributeEventString();
		sendCreatedEvent(distributeEventText, OptTypeType.OPT_IN);
		
		OadrDistributeEventType distributeEvent = DistributeEventSignalHelper.createOadrDistributeEventFromString(distributeEventText);
		checkDistributeEvent_EVR_TYP(distributeEvent);

	}
}
