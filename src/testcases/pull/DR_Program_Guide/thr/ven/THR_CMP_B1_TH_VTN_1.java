package testcases.pull.DR_Program_Guide.thr.ven;

import java.util.List;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_THR_CMP_Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.OadrCreateOptType;
import com.qualitylogic.openadr.core.signal.OptTypeType;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.LogResult;

public class THR_CMP_B1_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new THR_CMP_B1_TH_VTN_1());
	}
	
	@Override
	public void test() throws Exception {

		listenForRequests();

		String propFileResourceID[]= properties.getTwoResourceID();
		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_THR_CMP_Action();
		ICreatedEventResult createdEvent = queueDistributeEvent(distributeEvent);
		createdEvent.setLastCreatedEventResult(true);
		createdEvent.setExpectedOptInCount(1);
		
		addResponse();

		String ptext = "Please confirm that the VEN DUT is configured to have its associated load shedding resource(s) ";
		ptext +="respond to the LOAD_CONTROL signal values shown below. ";
		ptext +="The event your VEN will receive will have the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 4 minutes\n";
		ptext +="\tRandomization: 1 minute\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 1\n";
		ptext +="\tSignal Name: LOAD_CONTROL\n";
		ptext +="\t\tSignal Type: x-loadControlCapacity\n";
		ptext +="\t\tNumber of intervals: 2\n";
		ptext +="\t\tInterval Duration(s): 1 minute, 3 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Load_Control_Capacity_High!]\n\t\t\t\t![Load_Control_Capacity_Medium!] \n";
		ptext +="\tEvent Target(s): " + propFileResourceID[0] + ", " + propFileResourceID[1] + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n";
		ptext +="\tVEN Expected Response: optin, optOut " + propFileResourceID[0] + "\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		waitForCompletion();

		checkCreatedEventReceived(createdEvent);
		addCreatedOptResponse().setlastEvent(true);
		TestSession.setTestCaseDone(false);

		ptext ="After event transitions from Load_Control_Capacity_High to Load_Control_Capacity_Medium please take the following action:\n\n" 
				+ "Cause the DUT_VEN to to opt out " + propFileResourceID[0] + " using an oadrCreateOpt payload.\n\n";
		prompt(ptext);
		
		TestSession.setTestCaseDone(false);
		waitForCompletion();
		checkCreateOptRequest();
		

		String eitargs = "";
		int ix = distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getResourceID().size();
		String comma = " ";
		for (int i=0; i<ix; i++){
			eitargs +=comma + distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getResourceID().get(i);
			comma = ", ";
		}
		ptext = "Confirm that ONLY the resources targeted by this event (as shown below) responded as expected to the signal value(s) sent:\n\n";
		ptext += "\tTargeted Resoures:" + eitargs + "\n";
		ptext +="\tSignal Name: LOAD_CRONTROL\n";
		ptext +="\t\tSignal Type: x-loadControlCapacity\n";
		ptext +="\t\tNumber of intervals: 2\n";
		ptext +="\t\tInterval Duration(s): 1 minute, 3 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Load_Control_Capacity_High!]\n\t\t\t\t![Load_Control_Capacity_Medium!] \n\n";
		boolean yesClicked = promptYes(ptext);
		if (!yesClicked) {
			throw new FailedException("The VEN did not shed load as expected.");
		}
	}
	private void checkCreateOptRequest() {
		String propFileResourceID[]= properties.getTwoResourceID();
		List<OadrCreateOptType> createOpts = TestSession.getCreateOptEventReceivedList();
		if (createOpts.size() != 1) {
			throw new FailedException("Expected 1 OadrCreateOpt(s), received " + createOpts.size());
		}
		
		OadrCreateOptType createOpt1 = createOpts.get(0);
		
		EiTargetType  eiTargetType  = createOpt1.getEiTarget();
		
		if(eiTargetType==null || eiTargetType.getResourceID().size()<1 || eiTargetType.getResourceID().get(0).length()<1 ){
			throw new FailedException("Expected eiTarget in the CreateOpt");
		}

		if(!eiTargetType.getResourceID().get(0).contentEquals(propFileResourceID[0])){
			throw new FailedException("Expected to find resource " + propFileResourceID[0] + " in CreateOpt, but received " 
					+ eiTargetType.getResourceID().get(0) + " instead.");
		}
		
		
		if (!createOpt1.getOptType().equals(OptTypeType.OPT_OUT)) {
			throw new FailedException("Expected the CreateOpt to be optOut");
		}
		
	}
}
