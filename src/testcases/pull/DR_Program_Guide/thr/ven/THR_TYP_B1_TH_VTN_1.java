package testcases.pull.DR_Program_Guide.thr.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.IResponseCreateReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_THR_TYP_Action;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCreateReportRequest_THR_TYP_Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;
import com.qualitylogic.openadr.core.util.LogResult;

public class THR_TYP_B1_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new THR_TYP_B1_TH_VTN_1());
	}
	
	@Override
	public void test() throws Exception {

		ConformanceRuleValidator.setDisableRegisterReport_VenReportNameValid_Check(true);
		
		listenForRequests();
		String ptext ="If this is a self-test, please click 'Yes' to respond to a RegisterReport. If testing a real DUT click 'No' to skip the RegisterReport.\n\n";
		ptext +="This test case assumes the following report was offered as part of the registration process with the DUT:\n\n";
		ptext +="Report Name: TELEMETRY_STATUS\n";
		ptext +="Report Frequency: every 1 minute\n";
		ptext +="Report Granularity: 1 minute\n";
		ptext +="Reading Type: x-notApplicable  (all data points)\n";
		ptext +="Report Subject: Thermostat (all data points)\n\n";
		
		ptext +="<Status>\n";
		ptext +="Report Type: x-resourceStatus (oadrOnline, oadrLevelOffset:current)\n";
		ptext +="Units: x-notApplicable\n";
		ptext +="Well Known rID Property: ![ThermostatState_Status_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_Status_LevelOffset_Current_Range!]\n\n";
		
		ptext +="<Current Temp>\n";
		ptext +="Report Type: setPoint\n";
		ptext +="Units: temperature\n";
		ptext +="Well Known rID Property: ![ThermostatState_CurrentTemp_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_CurrentTemp_Range!]\n\n";
		
		ptext +="<Heat Temp Setting>\n";
		ptext +="Report Type:  setPoint\n";
		ptext +="Units: temperature\n";
		ptext +="Well Known rID Property: ![ThermostatState_HeatTempSetting_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_HeatTempSetting_Range!]\n\n";
		
		ptext +="<Cool Temp Setting>\n";
		ptext +="Report Type: setPoint\n";
		ptext +="Units: temperature\n";
		ptext +="Well Known rID Property: ![ThermostatState_CoolTempSetting_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_CoolTempSetting_Range!]\n\n";
		
		ptext +="<HVAC Mode Setting>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit\n";
		ptext +="Well Known rID Property: ![ThermostatState_HVACModeSetting_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_HVACModeSetting_Range!]\n\n";
		
		ptext +="<Current HVAC Mode>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit\n";
		ptext +="Well Known rID Property: ![ThermostatState_CurrentHVACMode_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_HVACMode_Range!]\n\n";
		
		ptext +="<Fan Mode Setting>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit\n";
		ptext +="Well Known rID Property: ![ThermostatState_FanModeSetting_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_FanModeSetting_Range!]\n\n";
		
		ptext +="<Current Hold Mode>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit\n";
		ptext +="Well Known rID Property: ![ThermostatState_CurrentHoldMode_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_CurrentHoldMode_Range!]\n\n";
		
		ptext +="<Current Away Mode>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit\n";
		ptext +="Well Known rID Property: ![ThermostatState_CurrentAwayMode_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_CurrentAwayMode_Range!]\n\n";
		
		ptext +="<Current Humidity>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit (percent)\n";
		ptext +="Well Known rID Property: ![ThermostatState_CurrentHumidity_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_CurrentHumidity_Range!]\n\n";
		boolean yesClicked = promptYes(ptext);
		if (!yesClicked) {
			LogHelper.addTrace("TestCase RegisterReport was skipped by the user");
		} else {
			addRegisteredReportResponse().setlastEvent(true);
			waitForCompletion();
			checkRegisterReportRequest_THR(1);
		}

		telemetry_Status_THR_DBCheck();

		String propFileResourceID[]= properties.getTwoResourceID();
		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_THR_TYP_Action();
		ICreatedEventResult createdEvent = queueDistributeEvent(distributeEvent);
		createdEvent.setLastCreatedEventResult(true);
		createdEvent.setExpectedOptInCount(1);
		
		addResponse();

		ptext = "Please confirm that the VEN DUT is configured to have its associated load shedding resource(s) ";
		ptext +="respond to the LOAD_CONTROL signal values shown below. ";
		ptext +="Note that the VEN may respond to either the SIMPLE or LOAD_CONTROL signal. ";
		ptext +="The event your VEN will receive will have the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 2 minutes\n";
		ptext +="\tRandomization: 1 minute\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 2\n";
		ptext +="\tSignal Name: Simple\n";
		ptext +="\t\tSignal Type: level\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Simple_Medium!] \n";
		ptext +="\tSignal Name: LOAD_CONTROL\n";
		ptext +="\t\tSignal Type: x-loadControlLevelOffset\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s):![Load_Control_TempOffset_Medium!]\n";
		ptext +="\t\titemUnits: ![temperature_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: none\n";
		ptext +="\tEvent Target(s): " + propFileResourceID[0] + ", " + propFileResourceID[1] + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n";
		ptext +="\tVEN Expected Response: optin\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		TestSession.setTestCaseDone(false);
		waitForCompletion();

		checkCreatedEventReceived(createdEvent);
		//buffer the responses
		IResponseCreateReportTypeAckAction createReport = new DefaultResponseCreateReportRequest_THR_TYP_Action(ServiceType.VTN);
		queueResponse(createReport);
	
		addResponse();
		
		addUpdatedReportResponse().setlastEvent(false);

		addUpdatedReportResponse().setlastEvent(true);

		String eitargs = "";
		int ix = distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getResourceID().size();
		String comma = " ";
		for (int i=0; i<ix; i++){
			eitargs +=comma + distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getResourceID().get(i);
			comma = ", ";
		}
		ptext = "Observe that ONLY the resources targeted ";
		ptext += "by this event (as shown below) responded as expected to the signal value(s) sent:\n\n";
		ptext += "\tTargeted Resoures:" + eitargs + "\n";
		ptext +="\tSignal Name: LOAD_CONTROL or SIMPLE\n";
		ptext +="\t\tSignal Type: x-loadControlLevelOffset\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s):![Load_Control_TempOffset_Medium!]\n";
		ptext +="\t\t\tor\n";
		ptext +="\t\t\t![Simple_Medium!] for the SIMPLE signal\n";
		ptext +="\t\titemUnits: ![temperature_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: none\n";
		ptext +="\tEvent Target(s): " + propFileResourceID[0] + ", " + propFileResourceID[1] + "\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		pause(100);
		checkCreatedReport(1);
		checkUpdateReport_THR_TYP(2);
		

		ptext = "Confirm that ONLY the resources targeted by this event (as shown below) responded as expected to the signal value(s) sent:\n\n";
		ptext += "\tTargeted Resoures:" + eitargs + "\n";
		ptext +="\tSignal Name: LOAD_CONTROL or SIMPLE\n";
		ptext +="\t\tSignal Type: x-loadControlLevelOffset\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s):![Load_Control_TempOffset_Medium!]\n";
		ptext +="\t\t\tor\n";
		ptext +="\t\t\t![Simple_Medium!] for the SIMPLE signal\n\n";
		ptext +="\t\titemUnits: ![temperature_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: none\n";
		yesClicked = promptYes(ptext);
		if (!yesClicked) {
			throw new FailedException("The VEN did not shed load as expected.");
		}
	}
}
