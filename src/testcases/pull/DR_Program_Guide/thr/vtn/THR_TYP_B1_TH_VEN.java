package testcases.pull.DR_Program_Guide.thr.vtn;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OptTypeType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.signal.helper.RegisterReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.UpdateReportEventHelper;
import com.qualitylogic.openadr.core.util.LogResult;


public class THR_TYP_B1_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new THR_TYP_B1_TH_VEN());
	}

	@Override
	public void test() throws Exception {

		String propFileResourceID[]= properties.getTwoResourceID();
		pause(5);
		sendRegisterReport(RegisterReportEventHelper.loadMetadata_001_THR_TYP());

		String ptext = "The VTN DUT should be configured so that there is a single pending Thermostat Program event with the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 2 minutes\n";
		ptext +="\tRandomization: 1 minute\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 2\n";
		ptext +="\tSignal Name: Simple\n";
		ptext +="\t\tSignal Type: level\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s):![Simple_Medium!] \n";
		ptext +="\tSignal Name: LOAD_CONTROL\n";
		ptext +="\t\tSignal Type: x-loadControlLevelOffset\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s):![Load_Control_TempOffset_Medium!]\n";
		ptext +="\t\titemUnits: ![temperature_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: none\n";
		ptext +="\tEvent Target(s): " + propFileResourceID[0] + ", " + propFileResourceID[1] + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n";
		ptext +="\tVEN Expected Response: optin\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		String distributeEventText = pollDistributeEventString();
		sendCreatedEvent(distributeEventText, OptTypeType.OPT_IN);
		
		OadrDistributeEventType distributeEvent = DistributeEventSignalHelper.createOadrDistributeEventFromString(distributeEventText);
		checkDistributeEvent_THR_TYP(distributeEvent);

		ptext ="Configure DUT_VTN to request periodic report defined in use case data set.\n";
		ptext +="The test expects the following report:\n\n";
		ptext +="Report Name: TELEMETRY_STATUS\n";
		ptext +="Report Frequency: every 1 minute\n";
		ptext +="Report Granularity: 1 minute\n";
		ptext +="Reading Type: x-notApplicable (all data points)\n";
		ptext +="Report Subject: Thermostat (all data points)\n\n";
		
		ptext +="<Status>\n";
		ptext +="Report Type: x-resourceStatus (oadrOnline, oadrLevelOffset:current)\n";
		ptext +="Units: x-notApplicable\n";
		ptext +="Well Known rID Property: ![ThermostatState_Status_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_Status_LevelOffset_Current_Range!]\n\n";
		
		ptext +="<Current Temp>\n";
		ptext +="Report Type: setPoint\n";
		ptext +="Units: temperature\n";
		ptext +="Well Known rID Property: ![ThermostatState_CurrentTemp_WellKnown_rID!]\n\n";
		ptext +="Well Known rID Values: ![ThermostatState_CurrentTemp_Range!]\n";
		
		ptext +="<Heat Temp Setting>\n";
		ptext +="Report Type:  setPoint\n";
		ptext +="Units: temperature\n";
		ptext +="Well Known rID Property: ![ThermostatState_HeatTempSetting_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_HeatTempSetting_Range!]\n\n";
		
		ptext +="<Cool Temp Setting>\n";
		ptext +="Report Type: setPoint\n";
		ptext +="Units: temperature\n";
		ptext +="Well Known rID Property: ![ThermostatState_CoolTempSetting_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_CoolTempSetting_Range!]\n\n";
		
		ptext +="<HVAC Mode Setting>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit\n";
		ptext +="Well Known rID Property: ![ThermostatState_HVACModeSetting_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_HVACModeSetting_Range!]\n\n";
		
		ptext +="<Current HVAC Mode>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit\n";
		ptext +="Well Known rID Property: ![ThermostatState_CurrentHVACMode_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_HVACMode_Range!]\n\n";
		
		ptext +="<Fan Mode Setting>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit\n";
		ptext +="Well Known rID Property: ![ThermostatState_FanModeSetting_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_FanModeSetting_Range!]\n\n";
		
		ptext +="<Current Hold Mode>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit\n";
		ptext +="Well Known rID Property: ![ThermostatState_CurrentHoldMode_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_CurrentHoldMode_Range!]\n\n";
		
		ptext +="<Current Away Mode>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit\n";
		ptext +="Well Known rID Property: ![ThermostatState_CurrentAwayMode_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_CurrentAwayMode_Range!]\n\n";
		
		ptext +="<Current Humidity>\n";
		ptext +="Report Type: operatingState\n";
		ptext +="Units: customUnit (percent)\n";
		ptext +="Well Known rID Property: ![ThermostatState_CurrentHumidity_WellKnown_rID!]\n";
		ptext +="Well Known rID Values: ![ThermostatState_CurrentHumidity_Range!]\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		OadrCreateReportType createReport = pollCreateReport();
		
		pollResponse();
		
		checkCreateReport_CBP(createReport);
		
		sendCreatedReport();
		
		sendUpdateReport(UpdateReportEventHelper.loadOadrUpdateReport_Update_THR(createReport));
		waitForReportBackDuration(createReport);
		sendUpdateReport(UpdateReportEventHelper.loadOadrUpdateReport_Update_THR(createReport));
		prompt("Please re-register the DUT VTN before running any non-Program Guide test cases as the list of available reports sent to the DUT VTN has changed.");

		
	}
}
