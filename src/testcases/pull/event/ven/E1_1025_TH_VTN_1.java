package testcases.pull.event.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_004Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.EndDeviceAssetType;

public class E1_1025_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new E1_1025_TH_VTN_1());
	}
	
	@Override
	public void test() throws Exception {
		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_004Action();
		
		EiTargetType device = new EiTargetType();
		String[] deviceClasses = properties.getDeviceClass();
		for (String deviceClass : deviceClasses) {
			EndDeviceAssetType endDeviceAsset = new EndDeviceAssetType();
			endDeviceAsset.setMrid(deviceClass);
			device.getEndDeviceAsset().add(endDeviceAsset);
		}
		distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiEventSignals().getEiEventSignal().get(0).setEiTarget(device);

		ICreatedEventResult createdEvent = queueDistributeEvent(distributeEvent);
		createdEvent.setLastCreatedEventResult(true);
		
		addResponse();

		listenForRequests();

		waitForCompletion();

		checkCreatedEventReceived(createdEvent);
		
				
	}
}
