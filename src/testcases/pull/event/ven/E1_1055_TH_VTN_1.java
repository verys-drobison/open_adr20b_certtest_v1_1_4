package testcases.pull.event.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_003Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;

public class E1_1055_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new E1_1055_TH_VTN_1());
	}
	
	@Override
	public void test() throws Exception {
		ICreatedEventResult createdEvent = queueDistributeEvent(new DefaultDistributeEvent_003Action());
		createdEvent.setExpectedOptInCount(1);

		addResponse().setlastEvent(true);

		listenForRequests();

		waitForCompletion();

		checkCreatedEventReceived(createdEvent);
		pause(15);
	}
}
