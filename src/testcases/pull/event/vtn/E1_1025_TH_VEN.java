package testcases.pull.event.vtn;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OptTypeType;
import com.qualitylogic.openadr.core.signal.SignalTypeEnumeratedType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;

public class E1_1025_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new E1_1025_TH_VEN());
	}

	@Override
	public void test() throws Exception {
		prompt(resources.prompt_038());

		String distributeEventText = pollDistributeEventString();
		sendCreatedEvent(distributeEventText, OptTypeType.OPT_IN);
	
		OadrDistributeEventType distributeEvent = DistributeEventSignalHelper.createOadrDistributeEventFromString(distributeEventText);
		checkSignalNames(distributeEvent, "LOAD_DISPATCH");
				
		EiTargetType  deviceClass  = distributeEvent.getOadrEvent().get(0).getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getEiTarget();   
		
		if(deviceClass==null){
			throw new FailedException("Expected eiTarget device class target in the first event signal");
		}
		
		if(!distributeEvent.getOadrEvent().get(0).getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getSignalType().equals(SignalTypeEnumeratedType.SETPOINT))
			throw new FailedException("Expected signalType to be 'setpoint' in the first event signal");
	}
}
