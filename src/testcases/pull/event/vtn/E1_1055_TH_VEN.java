package testcases.pull.event.vtn;

import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.impl.CreatedEventAction_One;
import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.ven.VENServerResource;

public class E1_1055_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new E1_1055_TH_VEN());
	}

	@Override
	public void test() throws Exception {
		prompt(resources.TestCase_1055_UIUserPromptText());
		
		String distributeEventReceived = pollDistributeEventString();
		OadrDistributeEventType distributeEvent = DistributeEventSignalHelper.createOadrDistributeEventFromString(distributeEventReceived);
		
		VENServerResource.getOadrDistributeEventReceivedsList().add(distributeEvent);
		
		ICreatedEventAction createdEventAction=new CreatedEventAction_One(true, true);
		sendCreatedEvent(createdEventAction);
		
		checkDistributeEvent_Ex055(distributeEvent);
			
	}
}
