package testcases.pull.event.vtn;

import java.util.List;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.OadrCreateOptType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType.OadrEvent;
import com.qualitylogic.openadr.core.signal.OptTypeType;
import com.qualitylogic.openadr.core.signal.helper.DistributeEventSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.SchemaHelper;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.ven.VenToVtnClient;

public class E1_1065_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new E1_1065_TH_VEN());
	}

	@Override
	public void test() throws Exception {
				
		prompt(resources.prompt_010());
		//receive first signal opt in/out
		OadrDistributeEventType distributeEvent = pollDistributeEvent();
		checkSignalNames(distributeEvent, "ELECTRICITY_PRICE");
		
		OadrEvent oadrEvent = distributeEvent.getOadrEvent().get(0);
		
		//optin/optout to accept resource array value (0 based)
		
		//Opt out of second resource
		OadrCreateOptType createOpt = getCreateOptOptOut_ResId(oadrEvent, 1);
		sendCreateOpt(createOpt);
		

		prompt(resources.prompt_073());
		//receive second signal (containing two events) opt in/out
		distributeEvent = pollDistributeEvent2();
		checkSignalNames(distributeEvent, "ELECTRICITY_PRICE", "ELECTRICITY_PRICE");
		//first event
		oadrEvent = distributeEvent.getOadrEvent().get(0);
		
		//Opt out of First resource
		createOpt = getCreateOptOptOut_ResId(oadrEvent, 0);
		sendCreateOpt(createOpt);
		

		boolean yesClicked = promptYes(resources.prompt_040());
		if (!yesClicked) {
			throw new FailedException("The VTN should show the VEN as still opt'd in.");
		}
	}


	private OadrDistributeEventType pollDistributeEvent() throws Exception {
		String distributeEvent = VenToVtnClient.poll(OadrDistributeEventType.class);
		if (!OadrUtil.isExpected(distributeEvent, OadrDistributeEventType.class)) {
			throw new FailedException("Expected OadrDistributeEvent has not been received");
		}
		
		OadrDistributeEventType oadrDistributeEventType = DistributeEventSignalHelper.createOadrDistributeEventFromString(distributeEvent);
		List<String> resourceIDs = oadrDistributeEventType.getOadrEvent().get(0).getEiEvent().getEiTarget().getResourceID();
		if (resourceIDs == null || resourceIDs.size() != 2) {
			throw new FailedException("Expected 2 ResourceIDs in OadrDistributeEvent has not been received");
		}
		
		sendCreatedEvent(distributeEvent, OptTypeType.OPT_IN);
		
		return oadrDistributeEventType;
	}

	private OadrDistributeEventType pollDistributeEvent2() throws Exception {
		String distributeEvent = VenToVtnClient.poll(OadrDistributeEventType.class);
		if (!OadrUtil.isExpected(distributeEvent, OadrDistributeEventType.class)) {
			throw new FailedException("Expected OadrDistributeEvent has not been received");
		}
		
		OadrDistributeEventType oadrDistributeEventType = DistributeEventSignalHelper.createOadrDistributeEventFromString(distributeEvent);
		List<String> resourceIDs = oadrDistributeEventType.getOadrEvent().get(0).getEiEvent().getEiTarget().getResourceID();
		if (resourceIDs == null || resourceIDs.size() != 2) {
			throw new FailedException("Expected 2 ResourceIDs in OadrDistributeEvent (Event 1) has not been received");
		}
		resourceIDs = oadrDistributeEventType.getOadrEvent().get(1).getEiEvent().getEiTarget().getResourceID();
		if (resourceIDs == null || resourceIDs.size() != 2) {
			throw new FailedException("Expected 2 ResourceIDs in OadrDistributeEvent (Event 2) has not been received");
		}
		
		sendCreatedEvent(distributeEvent, OptTypeType.OPT_IN);
		
		return oadrDistributeEventType;
	}
}
