package testcases.pull.general.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_002Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.common.TestSession;

public class G1_4005_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new G1_4005_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		checkSecurity();
		
		TestSession.setSecurityDebug("true");
		
		String securityMode = properties.getSecurityMode();
		prompt(String.format(resources.prompt_042(), securityMode));
		
		// This following code is already set in properties, so it not required.
		/*
		boolean isSha256 = securityMode.endsWith("SHA256_Security");
		if (isSha256) {
			TestSession.setClientTLS12Ciphers(new String[] {"TLS_RSA_WITH_AES_128_CBC_SHA256", "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"});
		}
		*/
		
		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_002Action();
		ICreatedEventResult createdEvent = queueDistributeEvent(distributeEvent);
		
		addResponse().setlastEvent(true);
		
		listenForRequests();
		
		waitForCompletion();

		checkCreatedEventReceived(createdEvent);
	}
}
