package testcases.pull.general.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_002Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.exception.NotApplicableException;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.LogResult;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;

public class G1_4007_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new G1_4007_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {

		boolean yesClicked = false;
		
		TestSession.setSecurityDebug("true");
		
		LogHelper.addTraceAndConsole("\nCurrent Transport Type: " + properties.get("Transport_Type"));
		LogHelper.addTraceAndConsole("Current Security Mode: " + properties.get("HTTP_Security"));
		
		if(!properties.get("HTTP_Security").equals("HTTP_SHA256_Expired") || !properties.get("Transport_Type").equals("HTTP")){
			prompt(resources.prompt_074());
			LogHelper.addTraceAndConsole("\nHTTP_Security or Transport_Type not configured correctly");
			LogHelper.addTraceAndConsole("Stopping test cases execution so user can configure properties.");
			
			throw new NotApplicableException("");
		}
		
		prompt("Expired certificate test case properties properly configured.");
						
		//None of this is expected to work as this is a negative test to confirm the failure of expired certificates.
		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_002Action();
		queueDistributeEvent(distributeEvent);
		
		addResponse().setlastEvent(true);

		listenForRequests();
		
		waitForCompletion();
		
		pause(15,"\nAllowing time for expected communication failure prior to exiting.\n");


		yesClicked = promptYes(resources.prompt_075());	
		if (!yesClicked) {
			LogHelper.setResult(LogResult.FAIL);
			TestSession.getTraceObj().setResult("FAIL");
			throw new FailedException("The SHA256 connection should have failed to connect using expired certificates.");
		} 
		else {
			LogHelper.setResult(LogResult.PASS);
			TestSession.getTraceObj().setResult("PASS");
		}

	}

}

