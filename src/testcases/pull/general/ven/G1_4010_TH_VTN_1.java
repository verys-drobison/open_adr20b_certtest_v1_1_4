package testcases.pull.general.ven;

import com.qualitylogic.openadr.core.base.VtnPullTestCase;

public class G1_4010_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new G1_4010_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		new G1_4005_TH_VTN_1().test();
	}
}
