package testcases.pull.general.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_002Action;
import com.qualitylogic.openadr.core.base.ErrorConst;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;

public class G1_4012_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new G1_4012_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		
		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_002Action();
		ICreatedEventResult createdEvent = queueDistributeEvent(distributeEvent);

		addResponse().setlastEvent(true);
		
		listenForRequests();
		
		waitForCompletion();

		checkCreatedEventReceived(createdEvent);
	}
}
