package testcases.pull.general.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_007Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.common.UIUserPrompt;

public class G1_4030_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new G1_4030_TH_VTN_1());
	}
	
	@Override
	public void test() throws Exception {
		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_007Action();
		distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEventDescriptor().setTestEvent("");
		ICreatedEventResult createdEvent = queueDistributeEvent(distributeEvent);
		createdEvent.setExpectedOptInCount(1);
		
		listenForRequests();
		
		waitForCreatedEvent(1);
		
		prompt(resources.prompt_014());
		
		UIUserPrompt promptx = new UIUserPrompt(resources.prompt_015(), UIUserPrompt.SMALL);
		promptx.setPauseDisabled(true);
		addCreatedOptResponse().setPrompt(promptx);
				
		addCanceledOptResponse().setlastEvent(true);

		waitForCompletion();

		checkCreatedEventReceived(createdEvent);
		checkCreateOptRequest(1);
		checkCancelOptRequest(1);
	}
}
