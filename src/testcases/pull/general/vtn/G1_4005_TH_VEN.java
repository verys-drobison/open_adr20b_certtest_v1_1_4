package testcases.pull.general.vtn;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.signal.OptTypeType;

public class G1_4005_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new G1_4005_TH_VEN());
	}

	@Override
	public void test() throws Exception {
		checkSecurity();
		
		TestSession.setSecurityDebug("true");

		String securityMode = properties.getSecurityMode();
		prompt(String.format(resources.prompt_042(), securityMode));
		
		boolean isSha256 = securityMode.endsWith("SHA256_Security");
		if (isSha256) {
			TestSession.setClientTLS12Ciphers(new String[] {"TLS_RSA_WITH_AES_128_CBC_SHA256"});
		}
		
		prompt(resources.prompt_010());

		String distributeEvent1 = pollDistributeEventString();
		sendCreatedEvent(distributeEvent1, OptTypeType.OPT_IN);
	}
}
