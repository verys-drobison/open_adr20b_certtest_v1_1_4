package testcases.pull.general.vtn;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.exception.NotApplicableException;
import com.qualitylogic.openadr.core.signal.OptTypeType;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;
import com.qualitylogic.openadr.core.util.LogResult;

public class G1_4007_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new G1_4007_TH_VEN());
	}

	@Override
	public void test() throws Exception {
		
		boolean yesClicked = false;
		
		TestSession.setSecurityDebug("true");
		
		LogHelper.addTraceAndConsole("\nCurrent Transport Type: " + properties.get("Transport_Type"));
		LogHelper.addTraceAndConsole("Current Security Mode: " + properties.get("HTTP_Security"));
		
		if(!properties.get("HTTP_Security").equals("HTTP_SHA256_Expired") || !properties.get("Transport_Type").equals("HTTP")){
			prompt(resources.prompt_074());
			LogHelper.addTraceAndConsole("\nHTTP_Security or Transport_Type not configured correctly");
			LogHelper.addTraceAndConsole("Stopping test cases execution so user can configure properties.");
			
			throw new NotApplicableException("");
		}

		prompt("Expired certificate test case properties properly configured.\n\n" + resources.prompt_010());
		
		try{
			String distributeEvent1 = pollDistributeEventString();
			sendCreatedEvent(distributeEvent1, OptTypeType.OPT_IN);
		} catch (Exception e) {
			//This should fail, but if not, we want to go through user prompt in any case.
		} finally {
			//This test is designed to fail, so we clear that and ask the user
			TestSession.clear_atleastOneValidationErrorPresent();
			LogHelper.setResult(LogResult.PASS);
			TestSession.getTraceObj().setResult("PASS");

			yesClicked = promptYes(resources.prompt_075());
			if (!yesClicked) {
				LogHelper.setResult(LogResult.FAIL);
				TestSession.getTraceObj().setResult("FAIL");
				throw new FailedException("The SHA256 connection should have failed to connect using expired certificates.");
			} else {
				LogHelper.setResult(LogResult.PASS);
				TestSession.getTraceObj().setResult("PASS");
			}
		}
	}
}
