package testcases.pull.general.vtn;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.signal.OptTypeType;

public class G1_4012_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new G1_4012_TH_VEN());
	}

	@Override
	public void test() throws Exception {
		
		prompt(resources.prompt_010());
		String distributeEvent1 = pollDistributeEventString();
		sendCreatedEvent(distributeEvent1, OptTypeType.OPT_IN);
		pollResponse();
	}
}
