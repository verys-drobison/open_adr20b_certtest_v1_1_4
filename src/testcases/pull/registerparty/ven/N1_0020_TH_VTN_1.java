package testcases.pull.registerparty.ven;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultRespRegReportMetadata_003AckAction;
import com.qualitylogic.openadr.core.action.impl.Default_NoEvent_DistributeEventAction;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.common.UIUserPrompt;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;

public class N1_0020_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {

		execute(new N1_0020_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
	
		testcases.util.TH_ClearRegistrationDatabases.main(null);
		
		addCreatedPartyRegistrationResponse();
		queueResponse(new DefaultRespRegReportMetadata_003AckAction());
		addResponse();
		addRegisteredReportResponse().setlastEvent(true);
			
		prompt(resources.prompt_002());
		
		listenForRequests();
		
		prompt(resources.prompt_003());

		IDistributeEventAction distributeEvent = new Default_NoEvent_DistributeEventAction();
		queueResponse(distributeEvent);
		waitForCompletion();
		waitForRegisteredReportRequest(1);
		
		checkCreatePartyRegistrationRequest(1);
		checkRegisteredReportRequest(1);
		checkRegisterReportRequest(1);
		pause (3);
		waitForoadrRequestedEvent();
		
		
		
		
	 if (!disable_DR_Guide_Registration_Report_Check && !disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check)
				ven_Bootstrap_Report_DBCheck();
		else{
			if(disable_DR_Guide_Registration_Report_Check)
				LogHelper.addTrace("\n*** Skipping Registration Report Check - disable_DR_Guide_Registration_Report_Check = true\n\n");
			else
				LogHelper.addTrace("\n*** Skipping Registration Report Check - disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check = true\n\n");
		}	
	 
	 waitForoadrRequestedEvent();
	}
	
	protected static PropertiesFileReader properties = new PropertiesFileReader();

	private static boolean disable_DR_Guide_Registration_Report_Check;

	static String  p_disable_DR_Guide_Registration_Report_Check =   properties.get("disable_DR_Guide_Registration_Report_Check");

	static{
		if(p_disable_DR_Guide_Registration_Report_Check!=null && p_disable_DR_Guide_Registration_Report_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_Registration_Report_Check=true;
		}
	}
	
	private static boolean disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check;
	static String  p_disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check = properties.get("disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check");
	static{
		if(p_disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check!=null && p_disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check.equalsIgnoreCase("true")){
			disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check=true;
		}
	}
	
}
