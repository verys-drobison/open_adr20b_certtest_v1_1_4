package testcases.pull.registerparty.ven;

import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.IResponseCreatedPartyRegistrationAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultRespRegReportMetadata_003AckAction;
import com.qualitylogic.openadr.core.action.impl.Default_NoEvent_DistributeEventAction;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;

public class N1_0060_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new N1_0060_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		checkActiveRegistration();
		
		IResponseCreatedPartyRegistrationAckAction createdPartyRegistrationAckAction= addCreatedPartyRegistrationResponse();
		createdPartyRegistrationAckAction.setNewRegistration(true);
		
		queueResponse(new DefaultRespRegReportMetadata_003AckAction(createdPartyRegistrationAckAction));
		addResponse();
		addRegisteredReportResponse().setlastEvent(true);

		listenForRequests();
		
		prompt(resources.prompt_006());
		prompt(resources.prompt_035());

		IDistributeEventAction distributeEvent = new Default_NoEvent_DistributeEventAction();
		queueResponse(distributeEvent);
		waitForCompletion();
		waitForRegisteredReportRequest(1);
		waitForoadrRequestedEvent();

		checkCreatePartyRegistrationRequest(1);
		checkRegisteredReportRequest(1);
		checkRegisterReportRequest(1);
	}
}
