package testcases.pull.registerparty.vtn;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.bean.VTNServiceType;
import com.qualitylogic.openadr.core.signal.OadrCreatePartyRegistrationType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OadrRegisterReportType;
import com.qualitylogic.openadr.core.signal.OadrRequestEventType;
import com.qualitylogic.openadr.core.signal.helper.CreatePartyRegistrationReqSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.signal.helper.RegisterReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.RequestEventSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.SchemaHelper;
import com.qualitylogic.openadr.core.util.LogResult;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.ven.VenToVtnClient;

public class N1_0020_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new N1_0020_TH_VEN());
	}

	@Override
	public void test() throws Exception {
		prompt(resources.prompt_002());
		OadrCreatePartyRegistrationType createPartyRegistration = CreatePartyRegistrationReqSignalHelper.loadCreatePartyRegistrationRequest();
		createPartyRegistration.setRegistrationID(null);
		createPartyRegistration.setVenID(null);
		sendCreatePartyRegistration(createPartyRegistration);
		
		sendPoll(OadrRegisterReportType.class);
		
		sendRegisteredReport();
		
		sendRegisterReport(RegisterReportEventHelper.loadMetadata_001());

		OadrRequestEventType oadrRequestEvent = RequestEventSignalHelper.getOadrRequestEvent();
		String strOadrRequestEvent = SchemaHelper.getRequestEventAsString(oadrRequestEvent);
		String distributeEventResponse = VenToVtnClient.post(strOadrRequestEvent,VTNServiceType.EiEvent);
		
		if(OadrUtil.isValidationErrorPresent()){
			return;
		}
		
		boolean isExpectedReceived = OadrUtil.isExpected(distributeEventResponse,OadrDistributeEventType.class);
		
		if(!isExpectedReceived){
			LogHelper.setResult(LogResult.FAIL);
			LogHelper
					.addTrace("Expected OadrDistributeEvent has not been received");
			LogHelper.addTrace("Test Case has Failed");
		    return;
		}
	}
}
