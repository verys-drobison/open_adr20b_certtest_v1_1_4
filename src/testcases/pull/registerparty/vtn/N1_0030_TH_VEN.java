package testcases.pull.registerparty.vtn;

import testcases.push.registerparty.vtn.N0_5030_TH_VEN;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.channel.util.StringUtil;
import com.qualitylogic.openadr.core.util.ResourceFileReader;
import com.qualitylogic.openadr.core.util.XMLDBUtil;

public class N1_0030_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new N1_0030_TH_VEN());
	}

	@Override
	public void test() throws Exception {
		new N0_5030_TH_VEN().test();
	}
	
	@Override
	public void cleanUp() throws Exception {
		super.cleanUp();
		
		String registrationID = new XMLDBUtil().getRegistrationID();
		if (!StringUtil.isBlank(registrationID)){
			
			alert(new ResourceFileReader().prompt_071());
			
		}
		
	}
}
