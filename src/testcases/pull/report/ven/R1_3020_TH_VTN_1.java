package testcases.pull.report.ven;

import com.qualitylogic.openadr.core.action.IResponseCreateReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCancelReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCreateReportRequest_002Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;

public class R1_3020_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new R1_3020_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		checkActiveRegistration();

		IResponseCreateReportTypeAckAction createReport = new DefaultResponseCreateReportRequest_002Action(ServiceType.VTN);
		queueResponse(createReport);
	
		addResponse();
		
		addUpdatedReportResponse();
		addUpdatedReportResponse().setlastEvent(true);
		
		listenForRequests();
		
		waitForCompletion();
		pause(10, "The test case is pausing for about 10 seconds");
		checkCreatedReport(1);
		checkUpdateReport(2);
		
		//To cancel report at end of test case
		ConformanceRuleValidator.setDisableCanceledReport_ResponseCodeSuccess_Check(true);
		queueResponse(new DefaultResponseCancelReportTypeAckAction(ServiceType.VTN, createReport));
		addResponse();	
		
		pause(35, "The test case is pausing for about 35 seconds");
		
	}
}
