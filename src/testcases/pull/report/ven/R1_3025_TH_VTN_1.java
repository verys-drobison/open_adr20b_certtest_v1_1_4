package testcases.pull.report.ven;

import com.qualitylogic.openadr.core.action.IResponseCreateReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCancelReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCreateReportRequest_002Action;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCreateReportRequest_011Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;

public class R1_3025_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new R1_3025_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		checkActiveRegistration();
		
		IResponseCreateReportTypeAckAction createReport = new DefaultResponseCreateReportRequest_011Action();
		queueResponse(createReport);
		
		addResponse();
		addUpdatedReportResponse().setlastEvent(true);
				
		listenForRequests();
		
		waitForCompletion();

		boolean yesClicked = promptYes(resources.prompt_054());
		if (!yesClicked) {
			throw new FailedException("Expected a delay in transmission of the oadrUpdateReport payload.");
		}
		pause(15);
		checkCreatedReport(1);
		checkUpdateReport(1);
		
		//To cancel report at end of test case
		ConformanceRuleValidator.setDisableCanceledReport_ResponseCodeSuccess_Check(true);
		queueResponse(new DefaultResponseCancelReportTypeAckAction(ServiceType.VTN, createReport));
		addResponse();	
		
		pause(35, "The test case is pausing for about 35 seconds");
	}
}
