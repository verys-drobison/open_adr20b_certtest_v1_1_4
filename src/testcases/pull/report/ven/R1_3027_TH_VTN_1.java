package testcases.pull.report.ven;

import com.qualitylogic.openadr.core.action.IResponseCreateReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCancelReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCreateReportRequest_011Action;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCreateReportRequest_014Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;

public class R1_3027_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new R1_3027_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		checkActiveRegistration();
		
		IResponseCreateReportTypeAckAction createReport = new DefaultResponseCreateReportRequest_014Action();
		queueResponse(createReport);
		
		//queueResponse(new DefaultResponseCreateReportRequest_014Action());

		addResponse();

		addUpdatedReportResponse();
		addUpdatedReportResponse();

		listenForRequests();
		
		waitForCreatedReport(1);
		
		prompt(resources.prompt_060());

		pause(210, "Pausing for 210 seconds...");

		checkCreatedReport(1);
		checkUpdateReport(2, 3);
		
		//To cancel report at end of test case
		ConformanceRuleValidator.setDisableCanceledReport_ResponseCodeSuccess_Check(true);
    	queueResponse(new DefaultResponseCancelReportTypeAckAction(ServiceType.VTN, createReport));
		addResponse();	
		
	}
}
