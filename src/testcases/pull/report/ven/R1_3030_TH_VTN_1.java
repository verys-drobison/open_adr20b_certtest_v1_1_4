package testcases.pull.report.ven;

import com.qualitylogic.openadr.core.action.IResponseCreateReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCancelReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCreateReportRequest_002Action;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCreateReportRequest_003Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;

public class R1_3030_TH_VTN_1 extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new R1_3030_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		//ConformanceRuleValidator.setDisableCreateReport_reportRequestIDUnique_Check(true);
		
		checkActiveRegistration();

		IResponseCreateReportTypeAckAction createReportAction1 = new DefaultResponseCreateReportRequest_002Action(ServiceType.VTN);
		queueResponse(createReportAction1);
	
		addResponse();
		
		addUpdatedReportResponse();

		listenForRequests();
		
		waitForUpdateReport(1);

		IResponseCreateReportTypeAckAction createReportAction2 = new DefaultResponseCreateReportRequest_003Action(ServiceType.VTN);
		queueResponse(createReportAction2);
		
		addResponse();
		
		addUpdatedReportResponse();
		addUpdatedReportResponse();
		addUpdatedReportResponse();
		
		waitForUpdateReports_Rx030(createReportAction1.getOadrCreateReportResponse(), createReportAction2.getOadrCreateReportResponse());
		pause(30);
		
		//To cancel report at end of test case
		ConformanceRuleValidator.setDisableCanceledReport_ResponseCodeSuccess_Check(true);
		queueResponse(new DefaultResponseCancelReportTypeAckAction(ServiceType.VTN, createReportAction1));
		addResponse();	
		queueResponse(new DefaultResponseCancelReportTypeAckAction(ServiceType.VTN, createReportAction2));
		addResponse();	
	
		pause(35);
		
		
	}
}
