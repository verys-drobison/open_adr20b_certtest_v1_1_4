package testcases.pull.report.vtn;

import com.qualitylogic.openadr.core.base.VenPullTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.signal.OadrCancelReportType;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.OadrCreatedReportType;
import com.qualitylogic.openadr.core.signal.OadrUpdateReportType;
import com.qualitylogic.openadr.core.signal.helper.CreatedReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.UpdateReportEventHelper;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;

public class R1_3030_TH_VEN extends VenPullTestCase {

	public static void main(String[] args) {
		execute(new R1_3030_TH_VEN());
	}
	
	@Override
	public void test() throws Exception {
		//ConformanceRuleValidator.setDisableCreateReport_reportRequestIDUnique_Check(true);
		
		checkActiveRegistration();

		prompt(resources.prompt_020());

		OadrCreateReportType createReport1 = pollCreateReport();
		String reportRequestID1 = createReport1.getOadrReportRequest().get(0).getReportRequestID();
		sendCreatedReport();

		OadrUpdateReportType updateReport1 = UpdateReportEventHelper.loadOadrUpdateReport_Update001(ServiceType.VEN, true, createReport1);
		sendUpdateReport(updateReport1);
		long updateReportDelay1 = System.currentTimeMillis() + 60_000;
		
		prompt(resources.prompt_021());
		
		OadrCreateReportType createReport2 = pollCreateReport();
		OadrCreatedReportType createdReport2 = CreatedReportEventHelper.loadOadrCreatedReport();
		createdReport2.getOadrPendingReports().getReportRequestID().add(reportRequestID1);
		sendCreatedReport(createdReport2);

		OadrUpdateReportType updateReport2 = UpdateReportEventHelper.loadOadrUpdateReport_Update001(ServiceType.VEN, true, createReport2);
		sendUpdateReport(updateReport2);
		long updateReportDelay2 = System.currentTimeMillis() + 60_000;

		while (System.currentTimeMillis() < updateReportDelay1) {
			pause(1);
		}
		sendUpdateReport(updateReport1);

		while (System.currentTimeMillis() < updateReportDelay2) {
			pause(1);
		}
		sendUpdateReport(updateReport2);
		

	}
}
