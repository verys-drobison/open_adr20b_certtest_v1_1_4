package testcases.push.DR_Program_Guide.cpp.vtn;

import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.impl.Default_CreatedEventActionOnLastDistributeEventReceived;
import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.LogResult;

public class CPP_CMP_B0_TH_VEN_1 extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new CPP_CMP_B0_TH_VEN_1());
	}

	@Override
	public void test() throws Exception {

		listenForRequests();

		String propFileResourceID[]= properties.getTwoResourceID();
		ICreatedEventAction createdEvent = new Default_CreatedEventActionOnLastDistributeEventReceived(false, true, null);
		addCreatedEventResponse(createdEvent).setLastCreateEvent(true);

		String ptext = "The VTN DUT should be configured so that there is a single pending Critical Peak Pricing Program event with the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 4 minutes\n";
		ptext +="\tRandomization: None\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 1\n";
		ptext +="\tSignal Name: ELECTRICITY_PRICE\n";
		ptext +="\t\tSignal Type: price\n";
		ptext +="\t\tNumber of intervals: 3\n";
		ptext +="\t\tInterval Duration(s): 1 minute, 2 minutes, 1 minute\n";
		ptext +="\t\tInterval Value(s):\t![Electricity_Price_Medium!],\n\t\t\t\t![Electricity_Price_High!],\n\t\t\t\t![Electricity_Price_Special!] \n";
		ptext +="\t\tUnits: currencyPerKWh\n";
		ptext +="\t\t\titemUnits: ![currencyPerKWh_itemUnits!] \n";
		ptext +="\t\t\tsiScaleCode: ![currencyPerKWh_siScaleCode!] \n";
		ptext +="\tEvent Target(s): " + propFileResourceID[0] + ", " + propFileResourceID[1] + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}
		
		waitForCompletion();
		
		checkCreatedEventCompleted(createdEvent);
		
		OadrDistributeEventType distributeEvent = checkDistributeEventRequest(1);
		checkDistributeEvent_CPP_CMP(distributeEvent);
	}
}
