package testcases.push.DR_Program_Guide.der.ven;

import java.util.ArrayList;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_DER_TYP_Action;
import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.EventStatusEnumeratedType;
import com.qualitylogic.openadr.core.signal.OadrCreatedEventType;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.LogResult;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.util.Trace;
import com.qualitylogic.openadr.core.util.XMLDBUtil;

public class DER_TYP_B0_TH_VTN extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new DER_TYP_B0_TH_VTN());
	}
	
	@Override
	public void test() throws Exception {

		listenForRequests();

		XMLDBUtil xmlDB=new XMLDBUtil();

		String ptext = "Please confirm that the VEN DUT is configured to have its associated load shedding resource(s) ";
		ptext +="respond to the Electricity_Price signal values shown below. ";
		ptext +="The event your VEN will receive will have the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 48 minutes\n";
		ptext +="\tRandomization: None\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 1\n";
		ptext +="\tSignal Name: ELECTRICITY_PRICE\n";
		ptext +="\t\tSignal Type: price\n";
		ptext +="\t\tNumber of intervals: 24\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Electricity_Price_Medium!]\n\t\t\t\t![Electricity_Price_High!]\n\t\t\t\talternating\n";
		ptext +="\t\tUnits: currencyPerKWh\n";
		ptext +="\t\titemUnits: ![currencyPerKWh_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![currencyPerKWh_siScaleCode!] \n";
		ptext +="\tEvent Target(s): " + xmlDB.getVENID() + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: never\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}
		

		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_DER_TYP_Action();
		ICreatedEventResult createdEvent = sendDistributeEvent(distributeEvent);
		createdEvent.setLastCreatedEventResult(false);
		createdEvent.setExpectedOptInCount(0);

//		waitForCompletion();
		
		pause(10);
		ptext = "Observe two intervals of load shed in the DUT VEN before clicking resume on this prompt. After clicking resume, the test harness will cancel the event.";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}
		

		IDistributeEventAction distributeEvent2 = distributeEvent;
		String previousEventID = distributeEvent2.getDistributeEvent().getOadrEvent().get(0)
				.getEiEvent().getEventDescriptor().getEventID();


		// Cancel Event
		distributeEvent2.getDistributeEvent().getOadrEvent().get(0).getEiEvent()
				.getEventDescriptor().setEventID(previousEventID);

		distributeEvent2.getDistributeEvent().getOadrEvent().get(0).getEiEvent()
				.getEventDescriptor().setEventStatus(EventStatusEnumeratedType.CANCELLED);
		
		distributeEvent2.getDistributeEvent().getOadrEvent().get(0).getEiEvent()
				.getEventDescriptor().setCreatedDateTime(OadrUtil.getCurrentTime());
		
		String distributeRequestID = OadrUtil.createoadrDistributeRequestID();
		distributeEvent2.getDistributeEvent().setRequestID(distributeRequestID);
		distributeEvent2.getDistributeEvent().getOadrEvent().get(0).getEiEvent()
				.getEventDescriptor().setModificationNumber(1);

		distributeEvent2.setEventCompleted(false);
		ICreatedEventResult createdEvent2 = sendDistributeEvent(distributeEvent2);
		createdEvent2.setLastCreatedEventResult(true);
		createdEvent2.setExpectedOptInCount(0);

		waitForCompletion();
		
		pause(10);

		Trace trace = TestSession.getTraceObj();

		if (TestSession.isAtleastOneValidationErrorPresent() && trace != null) {
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("Validation Error(s) are present");
			LogHelper.addTrace("Test Case has Failed");
			return;
		}
		ArrayList<OadrCreatedEventType> createdEventReceivedList = TestSession
				.getCreatedEventReceivedList();

		if (createdEventReceivedList.size() == 0) {
			LogHelper.setResult(LogResult.PASS);
			LogHelper.addTrace("Received no OadrCreatedEventType as expected");
		} else {
			LogHelper.addTrace("No OadrCreatedEventType was expected. Received "
					+ createdEventReceivedList.size() + " OadrCreatedEvent");
			LogHelper.addTrace("Test Case has Failed");
			LogHelper.setResult(LogResult.FAIL);

		}


		String eitargs = "";
		int ix = distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getVenID().size();
		String comma = " ";
		for (int i=0; i<ix; i++){
			eitargs +=comma + distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getVenID().get(i);
			comma = ", ";
		}
		ptext = "Confirm that ONLY the resources targeted by this event (as shown below) responded as expected to the signal value(s) sent:\n\n";
		ptext += "\tTargeted Resoures:" + eitargs + "\n";
		ptext +="\tSignal Name: ELECTRICITY_PRICE\n";
		ptext +="\t\tSignal Type: price\n";
		ptext +="\t\tNumber of intervals: 24\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Electricity_Price_Medium!]\n\t\t\t\t![Electricity_Price_High!]\n\t\t\t\talternating\n";
		ptext +="\t\tUnits: currencyPerKWh\n";
		ptext +="\t\titemUnits: ![currencyPerKWh_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![currencyPerKWh_siScaleCode!] \n";
		boolean yesClicked = promptYes(ptext);
		if (!yesClicked) {
			throw new FailedException("The VEN did not shed load as expected.");
		}
	}
}
