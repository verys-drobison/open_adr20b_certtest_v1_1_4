package testcases.push.DR_Program_Guide.der.vtn;

import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.impl.Default_CreatedEventActionOnLastDistributeEventReceived;
import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.LogResult;
import com.qualitylogic.openadr.core.util.XMLDBUtil;

public class DER_TYP_B0_TH_VEN_1 extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new DER_TYP_B0_TH_VEN_1());
	}

	@Override
	public void test() throws Exception {

		listenForRequests();

		XMLDBUtil xmlDB=new XMLDBUtil();
		ICreatedEventAction createdEvent = new Default_CreatedEventActionOnLastDistributeEventReceived(false, false, null);

		String ptext = "The VTN DUT should be configured so that there is a single pending Distributed Energy Resources DR Program event with the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 48 minutes\n";
		ptext +="\tRandomization: None\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 1\n";
		ptext +="\tSignal Name: ELECTRICITY_PRICE\n";
		ptext +="\t\tSignal Type: price\n";
		ptext +="\t\tNumber of intervals: 24\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Electricity_Price_Medium!]\n\t\t\t\t![Electricity_Price_High!]\n\t\t\t\talternating\n";
		ptext +="\t\tUnits: currencyPerKWh\n";
		ptext +="\t\titemUnits: ![currencyPerKWh_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![currencyPerKWh_siScaleCode!] \n";
		ptext +="\tEvent Target(s): " + xmlDB.getVENID() + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: never\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}
		

		
		pause(20);
		
		OadrDistributeEventType distributeEvent = checkDistributeEventRequest(1);
		checkDistributeEvent_DER_TYP(distributeEvent);

		boolean yesClicked = promptYes("If this is a self-test, please click 'Yes' to validate the TH_VTN Cancel Event. If testing a real DUT click 'No' to skip the Cancel Event test.");
		if (!yesClicked) {
			LogHelper.addTrace("TestCase check for Cancel Event was skipped by the user");
		} else {
			createdEvent = new Default_CreatedEventActionOnLastDistributeEventReceived(false, false, null);
			
			// Giving some time to see if any created event is received.
			pause(20);
			
			distributeEvent = checkDistributeEventRequest(2);
			checkDistributeEvent_DER_TYP_Cancel(distributeEvent);
		}
	}
}
