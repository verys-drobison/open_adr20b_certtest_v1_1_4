package testcases.push.DR_Program_Guide.fdr.ven;

import java.util.ArrayList;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_FDR_SMP_Action;
import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.EventStatusEnumeratedType;
import com.qualitylogic.openadr.core.signal.OadrCreatedEventType;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.Clone;
import com.qualitylogic.openadr.core.util.LogResult;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.util.Trace;
import com.qualitylogic.openadr.core.util.XMLDBUtil;

public class FDR_SMP_B0_TH_VTN extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new FDR_SMP_B0_TH_VTN());
	}
	
	@Override
	public void test() throws Exception {

		listenForRequests();

		XMLDBUtil xmlDB=new XMLDBUtil();

		String ptext = "Please confirm that the VEN DUT is configured to have its associated load shedding resource(s) ";
		ptext +="respond to the Simple signal values shown below. ";
		ptext +="The event your VEN will receive will have the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 0 minutes\n";
		ptext +="\tRandomization: None\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 1\n";
		ptext +="\tSignal Name: Simple\n";
		ptext +="\t\tSignal Type: level\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 0 minutes (open ended event)\n";
		ptext +="\t\tInterval Value(s): ![Simple_Special!] \n";
		ptext +="\tEvent Target(s): " + xmlDB.getVENID() + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}
		addResponse();
		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_FDR_SMP_Action();
		ICreatedEventResult createdEvent = sendDistributeEvent(distributeEvent);
		createdEvent.setLastCreatedEventResult(true);
		createdEvent.setExpectedOptInCount(1);
		

		waitForCompletion();
		checkCreatedEventReceived(createdEvent);
		pause(2);
		ptext = "Once you observe a change in load shed behavior in the DUT VEN, click resume, and the test harness will cancel the event.";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}
		
		IDistributeEventAction distributeEvent2 = Clone.clone(distributeEvent);
		String previousEventID = distributeEvent.getDistributeEvent().getOadrEvent().get(0)
				.getEiEvent().getEventDescriptor().getEventID();


		// Cancel Event
		distributeEvent2.getDistributeEvent().getOadrEvent().get(0).getEiEvent()
				.getEventDescriptor().setEventID(previousEventID);

		distributeEvent2.getDistributeEvent().getOadrEvent().get(0).getEiEvent()
				.getEventDescriptor().setEventStatus(EventStatusEnumeratedType.CANCELLED);
		
		distributeEvent2.getDistributeEvent().getOadrEvent().get(0).getEiEvent()
				.getEventDescriptor().setCreatedDateTime(OadrUtil.getCurrentTime());
		
		String distributeRequestID = OadrUtil.createoadrDistributeRequestID();
		distributeEvent2.getDistributeEvent().setRequestID(distributeRequestID);
		distributeEvent2.getDistributeEvent().getOadrEvent().get(0).getEiEvent()
				.getEventDescriptor().setModificationNumber(1);

		distributeEvent2.setEventCompleted(false);
		TestSession.setTestCaseDone(false);
		ICreatedEventResult createdEvent2 = sendDistributeEvent(distributeEvent2);
		createdEvent2.setLastCreatedEventResult(true);
		createdEvent2.setExpectedOptInCount(1);

		TestSession.setTestCaseDone(false);
		waitForCompletion();
		checkCreatedEventReceived(createdEvent2);
		

		Trace trace = TestSession.getTraceObj();

		if (TestSession.isAtleastOneValidationErrorPresent() && trace != null) {
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("Validation Error(s) are present");
			LogHelper.addTrace("Test Case has Failed");
			return;
		}
		ArrayList<OadrCreatedEventType> createdEventReceivedList = TestSession
				.getCreatedEventReceivedList();

		if (createdEventReceivedList.size() == 2) {
			LogHelper.setResult(LogResult.PASS);
			LogHelper.addTrace("Received OadrCreatedEventType as expected");
		} else {
			LogHelper.addTrace("Two OadrCreatedEventType were not found. Received "
					+ createdEventReceivedList.size() + " OadrCreatedEvent");
			LogHelper.addTrace("Test Case has Failed");
			LogHelper.setResult(LogResult.FAIL);

		}


		String eitargs = "";
		int ix = distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getVenID().size();
		String comma = " ";
		for (int i=0; i<ix; i++){
			eitargs +=comma + distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getVenID().get(i);
			comma = ", ";
		}
		ptext = "Confirm that ONLY the resources targeted by this event (as shown below) responded as expected to the signal value(s) sent:\n\n";
		ptext += "\tTargeted Resoures:" + eitargs + "\n";
		ptext +="\tSignal Name: Simple\n";
		ptext +="\t\tSignal Type: level\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 0 minutes (open ended event)\n";
		ptext +="\t\tInterval Value(s): ![Simple_Special!] \n";
		boolean yesClicked = promptYes(ptext);
		if (!yesClicked) {
			throw new FailedException("The VEN did not shed load as expected.");
		}
	}
}
