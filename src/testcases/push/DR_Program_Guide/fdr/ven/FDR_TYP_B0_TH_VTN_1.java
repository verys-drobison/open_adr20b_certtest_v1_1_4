package testcases.push.DR_Program_Guide.fdr.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_FDR_TYP_Action;
import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.helper.CreateReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;
import com.qualitylogic.openadr.core.util.LogResult;
import com.qualitylogic.openadr.core.util.XMLDBUtil;

public class FDR_TYP_B0_TH_VTN_1 extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new FDR_TYP_B0_TH_VTN_1());
	}
	
	@Override
	public void test() throws Exception {

		ConformanceRuleValidator.setDisableRegisterReport_VenReportNameValid_Check(true);
		
		listenForRequests();
		String ptext ="If this is a self-test, please click 'Yes' to respond to a RegisterReport. If testing a real DUT click 'No' to skip the RegisterReport.\n\n";
		ptext +="This test case assumes the following report was offered as part of the registration process with the DUT:\n\n";
		ptext +="\tReport Name: TELEMETRY_USAGE\n";
		ptext +="\tReport Frequency: every 1 minute\n";
		ptext +="\tReport Granularity: 1 minute\n";
		ptext +="\t\tReport Type: usage\n";
		ptext +="\t\tUnits: powerReal\n";
		ptext +="\t\tReading Type: Direct Read\n";
		ptext +="\t\tWell Known rID Property: ![TelemetryUsage_PowerReal_WellKnown_rID!]\n";
		ptext +="\t\tWell Known rID Values: ![TelemetryUsage_PowerReal_Range!]\n\n";
		boolean yesClicked = promptYes(ptext);
		if (!yesClicked) {
			LogHelper.addTrace("TestCase RegisterReport was skipped by the user");
			telemetry_Usage_CBP_DBCheck();
		} else {
			
			addRegisteredReportResponse().setlastEvent(true);

			waitForCompletion();
			checkRegisterReportRequest_FDR_TYP(1);
			telemetry_Usage_CBP_DBCheck();
		}

		XMLDBUtil xmlDB=new XMLDBUtil();

		ptext = "Please confirm that the VEN DUT is configured to have its associated load shedding resource(s) ";
		ptext +="respond to the LOAD_DISPATCH signal values shown below. ";
		ptext +="The event your VEN will receive will have the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 2 minute\n";
		ptext +="\tDuration: 2 minutes\n";
		ptext +="\tRandomization: None\n";
		ptext +="\tRamp Up: 1 minute\n";
		ptext +="\tRecovery: 1 minute\n";
		ptext +="\tNumber of Signals: 1\n";
		ptext +="\tSignal Name: Simple\n";
		ptext +="\t\tSignal Type: level\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Simple_High!]\n";
		ptext +="\tSignal Name: LOAD_DISPATCH\n";
		ptext +="\t\tSignal Type: delta\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s):2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Load_Dispatch_Delta_High!]\n";
		ptext +="\t\tUnits: powerReal\n";
		ptext +="\t\titemUnits: ![powerReal_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![powerReal_siScaleCode!] \n";
		ptext +="\t\t\tHertz: ![powerReal_powerAttributes_hertz!] \n";
		ptext +="\t\t\tVoltage: ![powerReal_powerAttributes_voltage!] \n";
		ptext +="\t\t\tAc: ![powerReal_powerAttributes_ac!] \n";
		ptext +="\tEvent Target(s): " + xmlDB.getVENID() + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n";
		ptext +="\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}
		addResponse();
		
		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_FDR_TYP_Action();
		ICreatedEventResult createdEvent = sendDistributeEvent(distributeEvent);
		createdEvent.setLastCreatedEventResult(true);
		createdEvent.setExpectedOptInCount(1);

		TestSession.setTestCaseDone(false);
		waitForCompletion();
		
		checkCreatedEventReceived(createdEvent);

		String eitargs = "";
		int ix = distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getVenID().size();
		String comma = " ";
		for (int i=0; i<ix; i++){
			eitargs +=comma + distributeEvent.getDistributeEvent().getOadrEvent().get(0).getEiEvent().getEiTarget().getVenID().get(i);
			comma = ", ";
		}
		ptext = "Observe that ONLY the resources targeted by this event (as shown below) responded as expected to the signal value(s) sent:\n\n";
		ptext += "\tTargeted Resoures:" + eitargs + "\n";
		ptext +="\tSignal Name: LOAD_DISPATCH\n";
		ptext +="\t\tSignal Type: delta\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s):2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Load_Dispatch_Delta_High!]\n";
		ptext +="\t\tUnits: powerReal\n";
		ptext +="\t\titemUnits: ![powerReal_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![powerReal_siScaleCode!] \n";
		ptext +="\t\t\tHertz: ![powerReal_powerAttributes_hertz!] \n";
		ptext +="\t\t\tVoltage: ![powerReal_powerAttributes_voltage!] \n";
		ptext +="\t\t\tAc: ![powerReal_powerAttributes_ac!] \n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		OadrCreateReportType createReport = CreateReportEventHelper.loadOadrCreateReport_Request_CBP_CMP(ServiceType.VTN);
		sendCreateReport(createReport);
		waitForReportBackDuration(createReport);
		pause(15);
		checkUpdateReport_FDR_TYP(2);

		ptext = "Confirm that ONLY the resources targeted by this event (as shown below) responded as expected to the signal value(s) sent:\n\n";
		ptext += "\tTargeted Resoures:" + eitargs + "\n";
		ptext +="\tSignal Name: LOAD_DISPATCH\n";
		ptext +="\t\tSignal Type: delta\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s):2 minutes\n";
		ptext +="\t\tInterval Value(s):\t![Load_Dispatch_Delta_High!]\n";
		ptext +="\t\tUnits: powerReal\n";
		ptext +="\t\titemUnits: ![powerReal_itemUnits!] \n";
		ptext +="\t\tsiScaleCode: ![powerReal_siScaleCode!] \n";
		ptext +="\t\t\tHertz: ![powerReal_powerAttributes_hertz!] \n";
		ptext +="\t\t\tVoltage: ![powerReal_powerAttributes_voltage!] \n";
		ptext +="\t\t\tAc: ![powerReal_powerAttributes_ac!] \n\n";
		yesClicked = promptYes(ptext);
		if (!yesClicked) {
			throw new FailedException("The VEN did not shed load as expected.");
		}
	}
}
