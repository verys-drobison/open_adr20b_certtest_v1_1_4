package testcases.push.DR_Program_Guide.fdr.vtn;

import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.impl.Default_CreatedEventActionOnLastDistributeEventReceived;
import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.LogResult;
import com.qualitylogic.openadr.core.util.XMLDBUtil;

public class FDR_SMP_B0_TH_VEN_1 extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new FDR_SMP_B0_TH_VEN_1());
	}

	@Override
	public void test() throws Exception {

		listenForRequests();

		XMLDBUtil xmlDB=new XMLDBUtil();
		ICreatedEventAction createdEvent = new Default_CreatedEventActionOnLastDistributeEventReceived(false, true, null);
		addCreatedEventResponse(createdEvent).setLastCreateEvent(true);

		String ptext = "The VTN DUT should be configured so that there is a simple pending Fast DR Dispatch Program event with the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 0 minutes\n";
		ptext +="\tRandomization: None\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 1\n";
		ptext +="\tSignal Name: Simple\n";
		ptext +="\t\tSignal Type: level\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 0 minutes (open ended event)\n";
		ptext +="\t\tInterval Value(s): ![Simple_Special!] \n";
		ptext +="\tEvent Target(s): " + xmlDB.getVENID() + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		waitForCompletion();
		checkCreatedEventCompleted(createdEvent);


		OadrDistributeEventType distributeEvent = checkDistributeEventRequest(1);
		checkDistributeEvent_FDR(distributeEvent);

		createdEvent = new Default_CreatedEventActionOnLastDistributeEventReceived(false, true, null);
		addCreatedEventResponse(createdEvent).setLastCreateEvent(true);

		boolean yesClicked = promptYes("If this is a self-test, please click 'Yes' to validate the TH_VTN Cancel Event. If testing a real DUT click 'No' to skip the Cancel Event test.");
		if (!yesClicked) {
			LogHelper.addTrace("TestCase check for Cancel Event was skipped by the user");
		} else {

			// Giving some time to see if any created event is received.
			TestSession.setTestCaseDone(false);
			waitForCompletion();
			checkCreatedEventCompleted(createdEvent);


			distributeEvent = checkDistributeEventRequest(2);
			checkDistributeEvent_FDR_Cancel(distributeEvent);
		}
	}
}
