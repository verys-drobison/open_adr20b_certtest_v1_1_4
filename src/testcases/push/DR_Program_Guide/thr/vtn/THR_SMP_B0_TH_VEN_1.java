package testcases.push.DR_Program_Guide.thr.vtn;

import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.impl.Default_CreatedEventActionOnLastDistributeEventReceived;
import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.LogResult;

public class THR_SMP_B0_TH_VEN_1 extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new THR_SMP_B0_TH_VEN_1());
	}

	@Override
	public void test() throws Exception {

		listenForRequests();

		String propFileResourceID[]= properties.getTwoResourceID();
		ICreatedEventAction createdEvent = new Default_CreatedEventActionOnLastDistributeEventReceived(false, true, null);
		addCreatedEventResponse(createdEvent).setLastCreateEvent(true);

		String ptext = "The VTN DUT should be configured so that there is a single pending Thermostat Program event with the following characteristics:\n\n";
		ptext +="\tStart Time: Current time plus 1 minute\n";
		ptext +="\tDuration: 2 minutes\n";
		ptext +="\tRandomization: 1 minute\n";
		ptext +="\tRamp Up: None\n";
		ptext +="\tRecovery: None\n";
		ptext +="\tNumber of Signals: 1\n";
		ptext +="\tSignal Name: Simple\n";
		ptext +="\t\tSignal Type: level\n";
		ptext +="\t\tNumber of intervals: 1\n";
		ptext +="\t\tInterval Duration(s): 2 minutes\n";
		ptext +="\t\tInterval Value(s): ![Simple_Medium!] \n";
		ptext +="\tEvent Target(s): " + propFileResourceID[0] + "\n";
		ptext +="\tPriority: 1\n";
		ptext +="\tVEN Response required: always\n\n";
		prompt(ptext);
		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}
		
		waitForCompletion();
		
		checkCreatedEventCompleted(createdEvent);
		
		OadrDistributeEventType distributeEvent = checkDistributeEventRequest(1);
		checkDistributeEvent_THR(distributeEvent, "Simple_Medium");
	}
}
