package testcases.push.event.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_003Action;
import com.qualitylogic.openadr.core.base.VtnPushTestCase;

public class E0_6055_TH_VTN extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new E0_6055_TH_VTN());
	}
	
	@Override
	public void test() throws Exception {
		addResponse();
		
		listenForRequests();

		IDistributeEventAction distributeEventAction = new DefaultDistributeEvent_003Action();
		distributeEventAction.getDistributeEvent().setEiResponse(null);
		ICreatedEventResult createdEvent = sendDistributeEvent(distributeEventAction);
		createdEvent.setExpectedOptInCount(1);
		createdEvent.setLastCreatedEventResult(true);

		waitForCompletion();
		
		checkCreatedEventReceived(createdEvent);
	}
}
