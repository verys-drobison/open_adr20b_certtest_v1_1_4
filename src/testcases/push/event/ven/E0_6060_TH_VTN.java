package testcases.push.event.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_002Action;
import com.qualitylogic.openadr.core.base.VtnPushTestCase;

public class E0_6060_TH_VTN extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new E0_6060_TH_VTN());
	}
	
	@Override
	public void test() throws Exception {
		addResponse();
		
		addCreatedOptResponse().setlastEvent(true);
		
		listenForRequests();

		DefaultDistributeEvent_002Action distributeEventAction = new DefaultDistributeEvent_002Action();
		ICreatedEventResult createdEvent = sendDistributeEvent(distributeEventAction);
		createdEvent.setExpectedCreateOpt_OptOutCount(1);

		waitForCreatedEvent(1);
		
		prompt(resources.prompt_013());

		waitForCompletion();
	
		checkCreatedEventReceived(distributeEventAction.getDistributeEvent(), createdEvent);
	}
}
