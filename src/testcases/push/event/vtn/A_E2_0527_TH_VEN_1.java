package testcases.push.event.vtn;

import java.util.ArrayList;
import java.util.List;

import com.qualitylogic.openadr.core.action.CreatedEventActionList;
import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.impl.Default_CreatedEventActionOnLastDistributeEventReceived;
import com.qualitylogic.openadr.core.base.PUSHBaseTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.common.UIUserPrompt;
import com.qualitylogic.openadr.core.signal.EventStatusEnumeratedType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType.OadrEvent;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.util.LogResult;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;
import com.qualitylogic.openadr.core.util.ResourceFileReader;
import com.qualitylogic.openadr.core.ven.VENServerResource;
import com.qualitylogic.openadr.core.ven.VENService;
import com.qualitylogic.openadr.core.ven.VenToVtnClient;

public class A_E2_0527_TH_VEN_1 extends PUSHBaseTestCase {

	public static void main(String[] args) {
		try {
			A_E2_0527_TH_VEN_1 testClass = new A_E2_0527_TH_VEN_1();
			testClass.setEnableLogging(true);
			testClass.executeTestCase();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void test() throws Exception {
		// Create a list of CreatedEventAction
		ICreatedEventAction createdAction1 = new Default_CreatedEventActionOnLastDistributeEventReceived(
				true, true, "Response Description");
		createdAction1.setLastCreateEvent(true);
		
		CreatedEventActionList.addCreatedEventAction(createdAction1);
				
		ResourceFileReader resourceFileReader = new ResourceFileReader();
		UIUserPrompt uiUserPrompt = new UIUserPrompt();
		uiUserPrompt.Prompt(
				resourceFileReader.TestCase_0527_Push_UIUserPromptText(), 1);

		VENService.startVENService();

		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		
		pauseForTestCaseTimeout();


		if (TestSession.isValidationErrorFound()) {
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("Validation Error(s) are present");
			LogHelper.addTrace("Test Case has Failed");
			return;
		}
		
		pause(60);

		UIUserPrompt uiUserPrompt2 = new UIUserPrompt();
		uiUserPrompt2.Prompt(resourceFileReader.TestCase_0527_Second_UIUserPromptText(), 1);

		if (!TestSession.isUserClickedContinuePlay()) {
			LogHelper.setResult(LogResult.NA);
			LogHelper.addTrace("TestCase execution was cancelled by the user");
			return;
		}

		PropertiesFileReader propertiesFileReader = new PropertiesFileReader();

		long totalDurationInput = Long.valueOf(propertiesFileReader
				.get("asyncResponseTimeout"));
		long testCaseTimeout = System.currentTimeMillis() + totalDurationInput;

		ArrayList<OadrDistributeEventType> oadrDistributeEventList = null;

		while (System.currentTimeMillis() < testCaseTimeout) {
			
			oadrDistributeEventList = (ArrayList<OadrDistributeEventType>) VENServerResource
					.getOadrDistributeEventReceivedsList();
			
			if(oadrDistributeEventList.size()>1){
		
				ICreatedEventAction createdAction2 = new Default_CreatedEventActionOnLastDistributeEventReceived(
						true, true, null);
				String oadrCreatedEvent =  createdAction2.getCreateEvent();
				VenToVtnClient.post(oadrCreatedEvent);
				break;
			}
			
			pause(5);
			
		}
		
	
		if (TestSession.isValidationErrorFound()) {
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("Validation Error(s) are present");
			LogHelper.addTrace("Test Case has Failed");
			return;
		}
		
		ArrayList<OadrDistributeEventType> distributeEventList = VENServerResource
				.getOadrDistributeEventReceivedsList();


		if (distributeEventList.size() < 2) {
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("Expected two DistributeEvent");
			LogHelper.addTrace("Test Case has Failed");
			return;
		}

		
		if (!distributeEventList.get(0).getOadrEvent().get(0).getEiEvent()
				.getEventDescriptor().getEventStatus()
				.equals(EventStatusEnumeratedType.FAR)) {
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("Expected Event with EventStatus as FAR");
			LogHelper.addTrace("Test Case has Failed");
			return;
		} 
		
		List<OadrEvent> eiEventList = distributeEventList.get(0).getOadrEvent();
		if (eiEventList.size() != 1) {
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("Expected only one Event in the first DistributeEvent");
			LogHelper.addTrace("Test Case has Failed");
			return;
		}

		String firstEventID=eiEventList.get(0).getEiEvent().getEventDescriptor().getEventID();

		List<OadrEvent> eiEventList2 = distributeEventList.get(1).getOadrEvent();
		if (eiEventList2.size() != 2) {
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("Expected two Event in the second DistributeEvent");
			LogHelper.addTrace("Test Case has Failed");
			return;
		}

		OadrEvent oadrEventInitiallySent = null; 
		for(OadrEvent oadrEvent:eiEventList2){
			if(firstEventID.equals(oadrEvent.getEiEvent().getEventDescriptor().getEventID())){
				oadrEventInitiallySent = oadrEvent;
			}				
		}
		
		if(oadrEventInitiallySent==null){
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("Expected the initial Event sent in the first DistributeEvent in the second DistributeEvent");
			LogHelper.addTrace("Test Case has Failed");			
			return;
		}
		
		if(!oadrEventInitiallySent.getEiEvent().getEventDescriptor().getEventStatus().equals(EventStatusEnumeratedType.NEAR)){
			LogHelper.setResult(LogResult.FAIL);
			LogHelper.addTrace("Expected Event with EventStatus NEAR");
			LogHelper.addTrace("Test Case has Failed");
			return;
		}else{
			LogHelper.setResult(LogResult.PASS);
			LogHelper.addTrace("Received the expected Event with EventStatus NEAR");
			LogHelper.addTrace("Test Case has Passed");
		}
	}

	@Override
	public void cleanUp() throws Exception {
		OadrUtil.Push_VTN_Shutdown_Prompt();

		VENService.stopVENService();
	}

}
