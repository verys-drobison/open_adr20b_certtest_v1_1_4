package testcases.push.event.vtn;

import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.impl.Default_CreatedEventActionOnLastDistributeEventReceived;
import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.SignalTypeEnumeratedType;

public class E0_6025_TH_VEN_1 extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new E0_6025_TH_VEN_1());
	}

	@Override
	public void test() throws Exception {
		ICreatedEventAction createdEvent = new Default_CreatedEventActionOnLastDistributeEventReceived(false, true, null);
		addCreatedEventResponse(createdEvent).setLastCreateEvent(true);
		
		listenForRequests();

		prompt(resources.prompt_038());
		
		waitForCompletion();
		
		checkCreatedEventCompleted(createdEvent);
		OadrDistributeEventType distributeEvent = checkDistributeEventRequest(1);
		checkSignalNames(distributeEvent, "LOAD_DISPATCH");
		
		EiTargetType  deviceClass  = distributeEvent.getOadrEvent().get(0).getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getEiTarget();   
		
		if(deviceClass==null){
			throw new FailedException("Expected eiTarget device class target in the first event signal");
		}
		
		if(!distributeEvent.getOadrEvent().get(0).getEiEvent().getEiEventSignals().getEiEventSignal().get(0).getSignalType().equals(SignalTypeEnumeratedType.SETPOINT))
			throw new FailedException("Expected signalType to be 'setpoint' in the first event signal");
	}
}
