package testcases.push.event.vtn;

import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.impl.Default_CreatedEventActionOnLastDistributeEventReceived;
import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;

public class E0_6027_TH_VEN_1 extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new E0_6027_TH_VEN_1());
	}

	@Override
	public void test() throws Exception {
		ICreatedEventAction createdEvent = new Default_CreatedEventActionOnLastDistributeEventReceived(false, true, null);
		addCreatedEventResponse(createdEvent).setLastCreateEvent(true);
		
		listenForRequests();

		prompt(resources.prompt_041());
		
		waitForCompletion();
		
		checkCreatedEventCompleted(createdEvent);
		OadrDistributeEventType distributeEvent = checkDistributeEventRequest(1);
		checkSignalNames(distributeEvent, "ELECTRICITY_PRICE");
		
		//Check to see if baseline is in payload
		if (distributeEvent.getOadrEvent().get(0).getEiEvent().getEiEventSignals().getEiEventBaseline() == null){				
			throw new FailedException("Baseline not present in oadrDistributeEvent payload");
		}
	}
}
