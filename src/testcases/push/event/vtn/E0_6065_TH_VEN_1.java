package testcases.push.event.vtn;

import java.util.ArrayList;
import java.util.List;

import com.qualitylogic.openadr.core.action.CreatedEventActionList;
import com.qualitylogic.openadr.core.action.ICreateOptEventAction;
import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.impl.Default_CreateOptEventActionOnLastDistFourResEvntRecd;
import com.qualitylogic.openadr.core.action.impl.Default_CreateOptEventActionOnLastDistTwoResEvntRecd;
import com.qualitylogic.openadr.core.action.impl.Default_CreatedEventActionOnLastDistributeEventReceived;
import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.OadrCreateOptType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OptTypeType;

public class E0_6065_TH_VEN_1 extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new E0_6065_TH_VEN_1());
	}

	@Override
	public void test() throws Exception {
		listenForRequests();
				
		ICreatedEventAction createdEvent = new Default_CreatedEventActionOnLastDistributeEventReceived(true, true, null);
		addCreatedEventResponseImpl(createdEvent);
		
		prompt(resources.prompt_010());
		
		waitForResponse(1);
		
		OadrDistributeEventType distributeEvent = waitForDistributeEvent(1);
		checkSignalNames(distributeEvent, "ELECTRICITY_PRICE");
		checkDistributeEventRequest(1, 1, 2);
		
		//Opt out of second resource
		OadrCreateOptType createOpt = getCreateOptOptOut_ResId(distributeEvent.getOadrEvent().get(0), 1);
		sendCreateOpt(createOpt);

		waitForCreatedOpt(1);
		
		checkResponse(1);

				
		ICreatedEventAction createdEvent2 = new Default_CreatedEventActionOnLastDistributeEventReceived(true, true, null);
		createdEvent2.setLastCreateEvent(true);
		addCreatedEventResponseImpl2(createdEvent2);

		prompt(resources.prompt_073());
		
		waitForResponse(2);
		
		OadrDistributeEventType distributeEvent2 = waitForDistributeEvent(2);
		checkSignalNames(distributeEvent2, "ELECTRICITY_PRICE", "ELECTRICITY_PRICE");

		OadrCreateOptType createOpt2 = getCreateOptOptOut_ResId(distributeEvent2.getOadrEvent().get(0), 0);
		sendCreateOpt(createOpt2);
		
		waitForCreatedOpt(2);

		waitForCompletion();
		
		checkDistributeEventRequest(2, 1, 2);

		boolean yesClicked = promptYes(resources.prompt_040());
		if (!yesClicked) {
			throw new FailedException("The VTN should show the VEN as still opt'd in.");
		}
	}

	private void addCreatedEventResponseImpl(ICreatedEventAction createdEvent) {
		OptTypeType optType = OptTypeType.OPT_IN;
		int eventToSelect = 1;
		List<Integer> selectResourcePositionInPayload = new ArrayList<Integer>();
		selectResourcePositionInPayload.add(2);
		
		ICreateOptEventAction createOptEvent = new Default_CreateOptEventActionOnLastDistTwoResEvntRecd(optType, eventToSelect, selectResourcePositionInPayload);
		createOptEvent.setlastEvent(true);
		//createdEvent.addCreateOptEventAction(createOptEvent);

		CreatedEventActionList.addCreatedEventAction(createdEvent);
	}
	private void addCreatedEventResponseImpl2(ICreatedEventAction createdEvent) {
		OptTypeType optType = OptTypeType.OPT_IN;
		int eventToSelect = 1;
		List<Integer> selectResourcePositionInPayload = new ArrayList<Integer>();
		selectResourcePositionInPayload.add(2);
		
		ICreateOptEventAction createOptEvent = new Default_CreateOptEventActionOnLastDistFourResEvntRecd(optType, eventToSelect, selectResourcePositionInPayload);
		createOptEvent.setlastEvent(true);
		//createdEvent.addCreateOptEventAction(createOptEvent);
		CreatedEventActionList.clearCreatedEventAction();
		CreatedEventActionList.addCreatedEventAction(createdEvent);
	}
}
