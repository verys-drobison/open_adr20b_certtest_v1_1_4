package testcases.push.event.vtn.selftest_a_ported;


import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import com.qualitylogic.openadr.core.action.DistributeEventActionList;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.IResponseEventAction;
import com.qualitylogic.openadr.core.action.ResponseEventActionList;
import com.qualitylogic.openadr.core.action.impl.Default_ResponseEventAction;
import com.qualitylogic.openadr.core.action.impl.E2_0527_CPP_DistributeEventAction;
import com.qualitylogic.openadr.core.base.PUSHBaseTestCase;
import com.qualitylogic.openadr.core.signal.EventStatusEnumeratedType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType.OadrEvent;
import com.qualitylogic.openadr.core.signal.helper.SchemaHelper;
import com.qualitylogic.openadr.core.util.Clone;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.vtn.VTNService;
import com.qualitylogic.openadr.core.vtn.VtnToVenClient;

public class A_E2_0527_DUT_VTN extends PUSHBaseTestCase {

	public static void main(String[] args) {
		try {
			A_E2_0527_DUT_VTN testClass = new A_E2_0527_DUT_VTN();
			testClass.executeTestCase();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void test() throws Exception {

		IDistributeEventAction distributeEventAction1 = new E2_0527_CPP_DistributeEventAction();
		DistributeEventActionList
				.addDistributeEventAction(distributeEventAction1);
		distributeEventAction1.getDistributeEvent();

		IResponseEventAction responseEventAction = new Default_ResponseEventAction();
		ResponseEventActionList.addResponseEventAction(responseEventAction);
		OadrDistributeEventType distributeEvent = distributeEventAction1
				.getDistributeEvent();

		OadrEvent prevEvent = Clone.clone(distributeEvent.getOadrEvent().get(0));

		VTNService.startVTNService();

		String strOadrDistributeEvent = SchemaHelper.getDistributeEventAsString( 
				distributeEvent);

		VtnToVenClient.post(strOadrDistributeEvent);

		pause(60);

		
		XMLGregorianCalendar createdDateTime = OadrUtil.getCurrentTime();

		prevEvent.getEiEvent().getEventDescriptor()
					.setCreatedDateTime(createdDateTime);
	
		prevEvent.getEiEvent().getEventDescriptor().setEventID(OadrUtil.createDescriptorEventID());
		
		XMLGregorianCalendar newIntervalTime = OadrUtil
				.getCurrentTime();
		Duration durationOffset = OadrUtil.createDuration(
				10, 0);
		newIntervalTime.add(durationOffset);
		
		prevEvent.getEiEvent().getEiActivePeriod().getProperties()
		.getDtstart().setDateTime(newIntervalTime);

		prevEvent.getEiEvent().getEiActivePeriod().getProperties().setXEiRampUp(null);
		
		distributeEvent.getOadrEvent().add(prevEvent);
		
		distributeEvent.getOadrEvent().get(0).getEiEvent()
		.getEventDescriptor()
		.setEventStatus(EventStatusEnumeratedType.NEAR);

		distributeEvent.getOadrEvent().get(0).getEiEvent()
		.getEventDescriptor().setCreatedDateTime(createdDateTime);
		
		strOadrDistributeEvent = SchemaHelper.getDistributeEventAsString( 
				distributeEvent);
		
		VtnToVenClient.post(strOadrDistributeEvent);

		pause(50);
		
		VTNService.stopVTNService();

	}

	@Override
	public void cleanUp() throws Exception {

	}

}
