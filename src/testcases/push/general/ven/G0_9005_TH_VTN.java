package testcases.push.general.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_002Action;
import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.common.TestSession;

public class G0_9005_TH_VTN extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new G0_9005_TH_VTN());
	}

	@Override
	public void test() throws Exception {
		checkSecurity();
		
		TestSession.setSecurityDebug("true");
		
		String securityMode = properties.getSecurityMode();
		prompt(String.format(resources.prompt_042(), securityMode));
		
		// This following code is already set in properties, so it not required.
		/*
		boolean isSha256 = securityMode.endsWith("SHA256_Security");
		if (isSha256) {
			TestSession.setClientTLS12Ciphers(new String[] {"TLS_RSA_WITH_AES_128_CBC_SHA256", "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"});
		}
		*/
		
		addResponse().setlastEvent(true);
		
		listenForRequests();

		IDistributeEventAction distributeEvent = new DefaultDistributeEvent_002Action();
		ICreatedEventResult createdEvent = sendDistributeEvent(distributeEvent);
		
		waitForCreatedEvent(createdEvent);
		waitForCompletion();

		checkCreatedEventReceived(createdEvent);
	}
}
