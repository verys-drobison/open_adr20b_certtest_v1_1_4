package testcases.push.general.ven;

import com.qualitylogic.openadr.core.base.VtnPushTestCase;

public class G0_9010_TH_VTN extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new G0_9010_TH_VTN());
	}

	@Override
	public void test() throws Exception {
		new G0_9005_TH_VTN().test();
	}
}
