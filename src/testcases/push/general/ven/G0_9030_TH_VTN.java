package testcases.push.general.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_007Action;
import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.common.UIUserPrompt;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;

public class G0_9030_TH_VTN extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new G0_9030_TH_VTN());
	}
	
	@Override
	public void test() throws Exception {
			
		listenForRequests();
     
		ICreatedEventResult createdEvent = sendDistributeEvent_EmptyTestEvent(new DefaultDistributeEvent_007Action());
		createdEvent.setExpectedOptInCount(1);
			
		waitForCreatedEvent(1);
		
		prompt(resources.prompt_014());
		
		UIUserPrompt prompt = new UIUserPrompt(resources.prompt_015(), UIUserPrompt.SMALL);
		prompt.setPauseDisabled(true);
		addCreatedOptResponse().setPrompt(prompt);
		
		addCanceledOptResponse().setlastEvent(true);

		waitForCompletion();
				
		checkCreatedEventReceived(createdEvent);
		checkCreateOptRequest(1);
		checkCancelOptRequest(1);
	}
}
