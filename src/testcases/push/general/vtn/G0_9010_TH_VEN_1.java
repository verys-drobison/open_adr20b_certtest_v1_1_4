package testcases.push.general.vtn;

import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.impl.CreatedEventAction_One;
import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.common.TestSession;

public class G0_9010_TH_VEN_1 extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new G0_9010_TH_VEN_1());
	}

	@Override
	public void test() throws Exception {
		checkSecurity();
		
		TestSession.setSecurityDebug("true");
		
		String securityMode = properties.getSecurityMode();
		prompt(String.format(resources.prompt_042(), securityMode));
		
		boolean isSha256 = securityMode.endsWith("SHA256_Security");
		if (isSha256) {
			TestSession.setClientTLS12Ciphers(new String[] {"TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"});
			TestSession.setServerTLS12Ciphers(new String[] {"TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"});
		}
	
		ICreatedEventAction createEvent = new CreatedEventAction_One(true,true);
		addCreatedEventResponse(createEvent);
	
		listenForRequests();
		
		prompt(resources.prompt_010());

		waitForResponse(1);
		
		//waitForCompletion();
		
		//checkCreatedEventCompleted(createEvent);
	}
}
