package testcases.push.general.vtn;

import com.qualitylogic.openadr.core.action.ICreatedEventAction;
import com.qualitylogic.openadr.core.action.impl.Default_CreatedEventActionOnLastDistributeEventReceived;
import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.signal.OadrCreateOptType;
import com.qualitylogic.openadr.core.signal.OadrDistributeEventType;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;

public class G0_9030_TH_VEN_1 extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new G0_9030_TH_VEN_1());
	}

	@Override
	public void test() throws Exception {
	
		ICreatedEventAction createdEvent = new Default_CreatedEventActionOnLastDistributeEventReceived(false, true, null);
		addCreatedEventResponse(createdEvent).setLastCreateEvent(true);
		
		listenForRequests();

		prompt(resources.prompt_010());

		waitForResponse(1);

		OadrDistributeEventType distributeEvent = waitForDistributeEvent(1);
		checkSignalNames(distributeEvent, "ELECTRICITY_PRICE");
		
		pause(10);
		
		OadrCreateOptType createOpt = getCreateOptSchedule_001();
		createOpt.setMarketContext("");
		sendCreateOpt(createOpt);
				
		waitForCreatedOpt(1);
		
		sendCancelOpt(createOpt.getOptID());
		
		waitForCompletion(10);
		
	}
}
