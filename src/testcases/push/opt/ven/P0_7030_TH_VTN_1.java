package testcases.push.opt.ven;

import java.util.List;

import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.common.UIUserPrompt;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.EiTargetType;
import com.qualitylogic.openadr.core.signal.OadrCreateOptType;
import com.qualitylogic.openadr.core.signal.OptTypeType;

public class P0_7030_TH_VTN_1 extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new P0_7030_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		
		prompt(resources.prompt_039());
		
		UIUserPrompt prompt = new UIUserPrompt(resources.prompt_016());
		prompt.setPauseDisabled(true);
		addCreatedOptResponse().setPrompt(prompt);

		addCreatedOptResponse().setlastEvent(true);
		
		listenForRequests();

		waitForCompletion();
		
		checkCreateOptRequest();
	}

	private void checkCreateOptRequest() {
		List<OadrCreateOptType> createOpts = TestSession.getCreateOptEventReceivedList();
		if (createOpts.size() != 2) {
			throw new FailedException("Expected 2 OadrCreateOpt(s), received " + createOpts.size());
		}
		
		OadrCreateOptType createOpt1 = createOpts.get(0);
		OadrCreateOptType createOpt2 = createOpts.get(1);
		
		EiTargetType  eiTargetType  = createOpt1.getEiTarget();
		EiTargetType  eiTargetType2  = createOpt2.getEiTarget();
		
		String marketContext = createOpt1.getMarketContext();
		String marketContext2 = createOpt2.getMarketContext();

		EiTargetType  deviceClass  = createOpt1.getOadrDeviceClass();
		EiTargetType  deviceClass2  = createOpt2.getOadrDeviceClass();

		if(eiTargetType==null || eiTargetType.getVenID().size()<1 || eiTargetType.getVenID().get(0).length()<1 ){
			if(eiTargetType==null || eiTargetType.getResourceID().size()<1 || eiTargetType.getResourceID().get(0).trim().length()<1 ){
				throw new FailedException("Expected venID or resourceID eiTarget in the first CreateOpt");
			}
		}
		
		
		if(marketContext==null || marketContext.length()<1){
			throw new FailedException("Expected MarketContext in the first CreateOpt");
		}
		
		if(deviceClass==null){
			throw new FailedException("Expected DeviceClass in the first CreateOpt");
		}

		
		if(eiTargetType2==null || eiTargetType2.getVenID().size()<1 || eiTargetType2.getVenID().get(0).trim().length()<1 ){
			if(eiTargetType2==null || eiTargetType2.getResourceID().size()<1 || eiTargetType2.getResourceID().get(0).trim().length()<1 ){
				throw new FailedException("Expected venID or resourceID eiTarget in the second CreateOpt");
			}
		}

		
		if(marketContext2==null || marketContext2.length()<1){
			throw new FailedException("Expected MarketContext in the second CreateOpt");
		}
		
		if(deviceClass2==null){
			throw new FailedException("Expected DeviceClass in the second CreateOpt");
		}

		
		String target1;
		String target2;
		
		if (eiTargetType.getVenID().size()>0){
			target1 = eiTargetType.getVenID().get(0);
		}
		else {
			target1 = eiTargetType.getResourceID().get(0);	
		}
		
		if (eiTargetType2.getVenID().size()>0){
			target2 = eiTargetType2.getVenID().get(0);
		}
		else {
			target2 = eiTargetType2.getResourceID().get(0);	
		}		
		
		if (!target1.equals(target2)) {
			throw new FailedException("Expected the target information to be the same in the first and second CreateOpt");
		}
		
		if (!createOpt1.getOptType().equals(OptTypeType.OPT_IN)) {
			throw new FailedException("Expected the first CreateOpt to be optIn");
		}
		
		if (!createOpt2.getOptType().equals(OptTypeType.OPT_OUT)) {
			throw new FailedException("Expected the second CreateOpt to be optOut");
		}
	}
}
