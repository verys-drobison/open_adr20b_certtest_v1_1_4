package testcases.push.registerparty.ven;

import com.qualitylogic.openadr.core.action.ICreatedEventResult;
import com.qualitylogic.openadr.core.action.IDistributeEventAction;
import com.qualitylogic.openadr.core.action.IResponseEventAction;
import com.qualitylogic.openadr.core.action.ResponseEventActionList;
import com.qualitylogic.openadr.core.action.impl.DefaultDistributeEvent_002Action;
import com.qualitylogic.openadr.core.action.impl.Default_CreatedEventResultOptINOptOut;
import com.qualitylogic.openadr.core.action.impl.Default_NoEvent_DistributeEventAction;
import com.qualitylogic.openadr.core.action.impl.Default_ResponseEventAction;
import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.common.UIUserPrompt;
import com.qualitylogic.openadr.core.oadrpoll.OadrPollQueue;
import com.qualitylogic.openadr.core.signal.helper.LogHelper;
import com.qualitylogic.openadr.core.signal.helper.RegisterReportEventHelper;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;
import com.qualitylogic.openadr.core.util.ResourceFileReader;

public class N0_5020_TH_VTN_1 extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new N0_5020_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		testcases.util.TH_ClearRegistrationDatabases.main(null);
		
		prompt(resources.prompt_002());

		listenForRequests();
		
		addCreatedPartyRegistrationResponse();
		addRegisteredReportResponse().setlastEvent(true);;
		
		prompt(resources.prompt_003());
		
		waitForCreatePartyRegistration(1);
		
		waitForRegisterReport(1);
		
		sendRegisterReport(RegisterReportEventHelper.loadMetadata_003());
		
		waitForoadrRequestedEvent();

		waitForCompletion();
		checkRegisterReportRequest(1);
		pause(3);

		if (!disable_DR_Guide_Registration_Report_Check && !disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check)
			ven_Bootstrap_Report_DBCheck();
		else{
			if(disable_DR_Guide_Registration_Report_Check)
				LogHelper.addTrace("\n*** Skipping Registration Report Check - disable_DR_Guide_Registration_Report_Check = true\n\n");
			else
				LogHelper.addTrace("\n*** Skipping Registration Report Check - disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check = true\n\n");
		}
	}
	
	protected static PropertiesFileReader properties = new PropertiesFileReader();

	private static boolean disable_DR_Guide_Registration_Report_Check;

	static String  p_disable_DR_Guide_Registration_Report_Check =   properties.get("disable_DR_Guide_Registration_Report_Check");

	static{
		if(p_disable_DR_Guide_Registration_Report_Check!=null && p_disable_DR_Guide_Registration_Report_Check.equalsIgnoreCase("true")){
			disable_DR_Guide_Registration_Report_Check=true;
		}
	}
	
	private static boolean disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check;
	static String  p_disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check = properties.get("disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check");
	static{
		if(p_disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check!=null && p_disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check.equalsIgnoreCase("true")){
			disableRegisterReport_ReportNameReportTypeItemBaseReadingTypeCombination_Check=true;
		}
	}
}
