package testcases.push.registerparty.ven;

import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.channel.util.StringUtil;
import com.qualitylogic.openadr.core.util.ResourceFileReader;
import com.qualitylogic.openadr.core.util.XMLDBUtil;

public class N0_5030_TH_VTN_1 extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new N0_5030_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		prompt(resources.prompt_006());
		
		addCanceledPartyRegistrationResponse().setlastEvent(true);

		listenForRequests();

		prompt(resources.prompt_004());

		waitForCompletion();

		checkCancelPartyRegistrationRequest(1);

		alert(resources.prompt_005());
	}
	
	@Override
	public void cleanUp() throws Exception {
		super.cleanUp();
		
		String registrationID = new XMLDBUtil().getRegistrationID();
		if (!StringUtil.isBlank(registrationID)){
			
			alert(new ResourceFileReader().prompt_071());
			
		}
		
	}

}
