package testcases.push.registerparty.ven;

import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.bean.VENServiceType;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.exception.NotApplicableException;
import com.qualitylogic.openadr.core.signal.OadrRegisterReportType;
import com.qualitylogic.openadr.core.signal.OadrRegisteredReportType;
import com.qualitylogic.openadr.core.signal.OadrResponseType;
import com.qualitylogic.openadr.core.signal.helper.RegisterReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.RegisteredReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.ResponseHelper;
import com.qualitylogic.openadr.core.signal.helper.SchemaHelper;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.vtn.VtnToVenClient;

public class N0_5080_TH_VTN_1 extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new N0_5080_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		ConformanceRuleValidator.setDisable_ActiveRegistration_Check(true);
		ConformanceRuleValidator.setDisableResponseEvent_ResponseCodeSuccess_Check(true);
		ConformanceRuleValidator.setDisableRegisteredReport_ResponseCodeSuccess_Check(true);
		ConformanceRuleValidator.setDisable_VenIDValid_Check(true);
		
		prompt(resources.prompt_002());
		
		checkNoActiveRegistration();
		
		boolean yesClicked = promptYes(resources.prompt_061());
		if (yesClicked) {
			TestSession.setResponseExpected(false);
		
			sendRegisterReportWithNoResponse(RegisterReportEventHelper.loadMetadata_003());
		} else {
			// listenForRequests();
			
			waitForCompletion(15 * 1000);
			
			throw new NotApplicableException("Selftest placeholder");
		}

	}

	private void sendRegisterReportWithNoResponse(OadrRegisterReportType registerReport) throws Exception {
		String text = SchemaHelper.getRegisterReportTypeAsString(registerReport);
		String response = VtnToVenClient.post(text,VENServiceType.EiReport);
		if (!OadrUtil.isEmpty(response)) {
			
			Class<?> objectType = SchemaHelper.getObjectType(response);
				
				if(objectType.equals(OadrResponseType.class)){
					OadrResponseType oadrResponse = ResponseHelper.createOadrResponseFromString(response);
					if(oadrResponse.getEiResponse()==null || OadrUtil.isEmpty(oadrResponse.getEiResponse().getResponseCode()) || !oadrResponse.getEiResponse().getResponseCode().trim().startsWith("4")){
						throw new FailedException("Expected Error ResponseCode 463 in OadrResponse.");						
					}
					
				}else if(objectType.equals(OadrRegisteredReportType.class)){
					OadrRegisteredReportType oadrRegisteredReport = RegisteredReportEventHelper.createOadrRegisteredReportFromString(response);
					if(oadrRegisteredReport.getEiResponse()==null || OadrUtil.isEmpty(oadrRegisteredReport.getEiResponse().getResponseCode()) || !oadrRegisteredReport.getEiResponse().getResponseCode().trim().startsWith("4")){
						throw new FailedException("Expected Error ResponseCode 463 in OadrRegisteredReport.");						
					}
					
				}else {
					throw new FailedException("Expected no response or OadrRegisteredReport or OadrResponse with ResponseCode 463.");
				}
				
		}
	}
}
