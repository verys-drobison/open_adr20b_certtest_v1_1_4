package testcases.push.registerparty.vtn;

import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.bean.VTNServiceType;
import com.qualitylogic.openadr.core.exception.ValidationException;
import com.qualitylogic.openadr.core.signal.OadrCreatePartyRegistrationType;
import com.qualitylogic.openadr.core.signal.OadrCreatedPartyRegistrationType;
import com.qualitylogic.openadr.core.signal.OadrRequestEventType;
import com.qualitylogic.openadr.core.signal.helper.CreatePartyRegistrationReqSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.RegisterReportEventHelper;
import com.qualitylogic.openadr.core.signal.helper.RequestEventSignalHelper;
import com.qualitylogic.openadr.core.signal.helper.SchemaHelper;
import com.qualitylogic.openadr.core.ven.VenToVtnClient;

public class N0_5025_TH_VEN extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new N0_5025_TH_VEN());
	}

	@Override
	public void test() throws Exception {
		addRegisteredReportResponse().setlastEvent(true);

		listenForRequests();

		prompt(resources.prompt_002());		
		
		String preallocatedVenId = properties.get("VEN_ID");
		
		OadrCreatePartyRegistrationType createPartyRegistration = CreatePartyRegistrationReqSignalHelper.loadCreatePartyRegistrationRequest();
		createPartyRegistration.setRegistrationID(null);
		createPartyRegistration.setVenID(preallocatedVenId);
		OadrCreatedPartyRegistrationType createdPartyRegistration = sendCreatePartyRegistration(createPartyRegistration);
		
		if (!preallocatedVenId.equals(createdPartyRegistration.getVenID())) {
			throw new ValidationException("Expecting preallocated venID " + preallocatedVenId); 
		}
		
		sendRegisterReport(RegisterReportEventHelper.loadMetadata_001());

		
		OadrRequestEventType oadrRequestEvent = RequestEventSignalHelper.getOadrRequestEvent();
		String strOadrRequestEvent = SchemaHelper.getRequestEventAsString(oadrRequestEvent);
		String distributeEventResponse = VenToVtnClient.post(strOadrRequestEvent,VTNServiceType.EiEvent);
		
		waitForCompletion();
		checkRegisterReportRequest(1);
	}
}
