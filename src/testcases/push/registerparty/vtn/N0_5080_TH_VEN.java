package testcases.push.registerparty.vtn;

import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.bean.VTNServiceType;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.exception.NotApplicableException;
import com.qualitylogic.openadr.core.signal.OadrCreateOptType;
import com.qualitylogic.openadr.core.signal.OadrCreatedOptType;
import com.qualitylogic.openadr.core.signal.OadrResponseType;
import com.qualitylogic.openadr.core.signal.helper.CreatedOptEventHelper;
import com.qualitylogic.openadr.core.signal.helper.ResponseHelper;
import com.qualitylogic.openadr.core.signal.helper.SchemaHelper;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.ven.VenToVtnClient;

public class N0_5080_TH_VEN extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new N0_5080_TH_VEN());
	}

	@Override
	public void test() throws Exception {
		ConformanceRuleValidator.setDisable_ActiveRegistration_Check(true);
		ConformanceRuleValidator.setDisableResponseEvent_ResponseCodeSuccess_Check(true);
		ConformanceRuleValidator.setDisableCreatedOpt_ResponseCodeSuccess_Check(true);
		
		checkNoActiveRegistration();
		
		boolean yesClicked = promptYes(resources.prompt_061());
		if (yesClicked) {
			TestSession.setResponseExpected(false);
			
			sendCreateOptWithNoResponse(getCreateOptSchedule_001());
		} else {
			 //listenForRequests();
			
			waitForCompletion(20 * 1000);
			
			throw new NotApplicableException("Selftest placeholder");
		}
	}

	private void sendCreateOptWithNoResponse(OadrCreateOptType createOpt) throws Exception {
		String text = SchemaHelper.getOadrCreateOptAsString(createOpt);
		String response = VenToVtnClient.post(text, VTNServiceType.EiOpt);
		
		if (!OadrUtil.isEmpty(response)) {
			Class<?> objectType = null;

			objectType = SchemaHelper.getObjectType(response);

			if(objectType.equals(OadrResponseType.class)){
				OadrResponseType oadrResponse = ResponseHelper.createOadrResponseFromString(response);
				if(oadrResponse.getEiResponse()==null || OadrUtil.isEmpty(oadrResponse.getEiResponse().getResponseCode()) || !oadrResponse.getEiResponse().getResponseCode().trim().startsWith("4")){
					throw new FailedException("Expected Error ResponseCode 463 in OadrResponse.");						
				}
				
			}else if(objectType.equals(OadrCreatedOptType.class)){
				OadrCreatedOptType oadrCreatedOptType = CreatedOptEventHelper.createCreatedOptTypeEventFromString(response);
				if(oadrCreatedOptType.getEiResponse()==null || OadrUtil.isEmpty(oadrCreatedOptType.getEiResponse().getResponseCode()) || !oadrCreatedOptType.getEiResponse().getResponseCode().trim().startsWith("4")){
					throw new FailedException("Expected Error ResponseCode 463 in OadrCreatedOpt.");						
				}
				
			}else{
				throw new FailedException("Expected no response or OadrCreatedOpt or OadrResponse with ResponseCode 463.");
			}
			
			
		}
	}
}
