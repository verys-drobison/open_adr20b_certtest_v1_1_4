package testcases.push.report.ven;

import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.exception.FailedException;
import com.qualitylogic.openadr.core.signal.OadrCanceledReportType;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.helper.CreateReportEventHelper;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;

public class R0_8025_TH_VTN extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new R0_8025_TH_VTN());
	}

	@Override
	public void test() throws Exception {
		checkActiveRegistration();
		
		addUpdatedReportResponse().setlastEvent(true);

		listenForRequests();

		OadrCreateReportType createReport = CreateReportEventHelper.loadOadrCreateReport_Request_011();
		sendCreateReport(createReport);
		
		boolean yesClicked = promptYes(resources.prompt_054());
		if (!yesClicked) {
			throw new FailedException("Expected a delay in transmission of the oadrUpdateReport payload.");
		}
		
		waitForCompletion();
		
		checkUpdateReport(1);
		
		ConformanceRuleValidator.setDisableCanceledReport_ResponseCodeSuccess_Check(true);
		sendCancelReport(createReport);
	}
}