package testcases.push.report.ven;

import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.helper.CreateReportEventHelper;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;

public class R0_8027_TH_VTN extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new R0_8027_TH_VTN());
	}

	@Override
	public void test() throws Exception {
		checkActiveRegistration();

		addUpdatedReportResponse();
		addUpdatedReportResponse().setlastEvent(true);

		listenForRequests();

		OadrCreateReportType createReport = CreateReportEventHelper.loadOadrCreateReport_Request_014();
		sendCreateReport(createReport);

		prompt(resources.prompt_060());
		
		pause(3 * 60, "Pausing for 3 minutes...");
		
		checkUpdateReport(2, 3);
		
		ConformanceRuleValidator.setDisableCanceledReport_ResponseCodeSuccess_Check(true);
		sendCancelReport(createReport);
	}
}