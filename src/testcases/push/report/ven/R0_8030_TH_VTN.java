package testcases.push.report.ven;

import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.helper.CreateReportEventHelper;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;

public class R0_8030_TH_VTN extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new R0_8030_TH_VTN());
	}

	@Override
	public void test() throws Exception {
		checkActiveRegistration();

		addUpdatedReportResponse();
		addUpdatedReportResponse();
		addUpdatedReportResponse();
		addUpdatedReportResponse();
		
		listenForRequests();

		OadrCreateReportType createReport1 = CreateReportEventHelper.loadOadrCreateReport_Request_002(ServiceType.VTN);
		sendCreateReport(createReport1);

		waitForUpdateReport(1);
		
		OadrCreateReportType createReport2 = CreateReportEventHelper.loadOadrCreateReport_Request_003(ServiceType.VTN);
		sendCreateReport(createReport2);

		// waitForUpdateReport(2);

		waitForUpdateReports_Rx030(createReport1, createReport2);
		ConformanceRuleValidator.setDisableCanceledReport_ResponseCodeSuccess_Check(true);
		sendCancelReport(createReport1);
		sendCancelReport(createReport2);
	}
}