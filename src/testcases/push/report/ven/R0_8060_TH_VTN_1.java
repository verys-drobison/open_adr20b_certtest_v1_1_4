package testcases.push.report.ven;

import com.qualitylogic.openadr.core.action.impl.DefaultResponseRegisteredReportWithRequestTypeAckAction;
import com.qualitylogic.openadr.core.base.VtnPushTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;

public class R0_8060_TH_VTN_1 extends VtnPushTestCase {

	public static void main(String[] args) {
		execute(new R0_8060_TH_VTN_1());
	}

	@Override
	public void test() throws Exception {
		checkActiveRegistration();

		prompt(resources.prompt_051());

		addRegisteredReportResponse(new DefaultResponseRegisteredReportWithRequestTypeAckAction(ServiceType.VTN));
		addResponse();
		addUpdatedReportResponse().setlastEvent(true);
		
		listenForRequests();
		
		waitForCompletion();

		checkCreatedReport(1);
		checkUpdateReport(1);
	}
}
