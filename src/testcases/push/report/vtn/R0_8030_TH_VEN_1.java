package testcases.push.report.vtn;

import com.qualitylogic.openadr.core.base.VenPushTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.signal.OadrCreateReportType;
import com.qualitylogic.openadr.core.signal.OadrUpdateReportType;
import com.qualitylogic.openadr.core.signal.helper.UpdateReportEventHelper;

public class R0_8030_TH_VEN_1 extends VenPushTestCase {

	public static void main(String[] args) {
		execute(new R0_8030_TH_VEN_1());
	}
	
	@Override
	public void test() throws Exception {
		checkActiveRegistration();

		addCreatedReportResponse();
		
		listenForRequests();
					
		prompt(resources.prompt_020());
		
		OadrCreateReportType createReport1 = waitForCreateReport(1);
		String reportRequestID1 = createReport1.getOadrReportRequest().get(0).getReportRequestID();
		addCreatedReportResponse().addReportRequestID(reportRequestID1);
		
		OadrUpdateReportType updateReport1 = UpdateReportEventHelper.loadOadrUpdateReport_Update001(ServiceType.VEN,true,null);
		sendUpdateReport(updateReport1);
		long updateReportDelay1 = System.currentTimeMillis() + 60_000;
		
		prompt(resources.prompt_021());
		
		waitForCreateReport(2);
		
		OadrUpdateReportType updateReport2 = UpdateReportEventHelper.loadOadrUpdateReport_Update001(ServiceType.VEN,true,null);
		
		sendUpdateReport(updateReport2);
		long updateReportDelay2 = System.currentTimeMillis() + 60_000;
	
		while (System.currentTimeMillis() < updateReportDelay1) {
			pause(1);
		}
		sendUpdateReport(updateReport1);
		
		while (System.currentTimeMillis() < updateReportDelay2) {
			pause(1);
		}
		sendUpdateReport(updateReport2);
		pause(30);
	}
}
