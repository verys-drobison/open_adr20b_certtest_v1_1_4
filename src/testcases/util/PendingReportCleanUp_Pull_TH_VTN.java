package testcases.util;

import com.qualitylogic.openadr.core.action.IResponseCreateReportTypeAckAction;
import com.qualitylogic.openadr.core.action.impl.DefaultResponseCreateReportRequest_001Action;
import com.qualitylogic.openadr.core.base.VtnPullTestCase;
import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;

public class PendingReportCleanUp_Pull_TH_VTN extends VtnPullTestCase {

	public static void main(String[] args) {
		execute(new PendingReportCleanUp_Pull_TH_VTN());
	}

	@Override
	public void test() throws Exception {
		checkActiveRegistration();
		
		ConformanceRuleValidator.setDisableRegisterReport_RequestIDMatchPreviousCreateReport_Check(true);
		ConformanceRuleValidator.setDisableUpdateReport_isRIDValid_Check(true);
		ConformanceRuleValidator.setDisableUpdateReport_RequiredElementsAndReportName_Check(true);
		ConformanceRuleValidator.setDisable_UpdateReportDtstartValid_Check(true);
		ConformanceRuleValidator.setDisable_UpdateReportDurationValid_Check(true);
		ConformanceRuleValidator.setDisableUpdatedReport_ResponseCodeSuccess_Check(true);
		ConformanceRuleValidator.setDisable_UpdateReportLoadControlStateConsistentForAllIntervals_Check(true);
		
		//Send metadata request to clear any pending reports
		IResponseCreateReportTypeAckAction createReport = new DefaultResponseCreateReportRequest_001Action(ServiceType.VTN);
		createReport.getOadrCreateReportResponse().getOadrReportRequest().get(0).getReportSpecifier().setReportSpecifierID("METADATA");
		createReport.getOadrCreateReportResponse().getOadrReportRequest().get(0).getReportSpecifier().getSpecifierPayload().get(0).setRID("0");
		
		queueResponse(createReport);

		addResponse();
		
		addUpdatedReportResponse().setlastEvent(true);
		
		listenForRequests();
		
		waitForCompletion();

	}
}
