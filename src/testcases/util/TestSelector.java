package testcases.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.text.DefaultCaret;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

import testcases.pull.DR_Program_Guide.cbp.ven.*;
import testcases.pull.DR_Program_Guide.cpp.ven.*;
import testcases.pull.DR_Program_Guide.der.ven.*;
import testcases.pull.DR_Program_Guide.evp.ven.*;
import testcases.pull.DR_Program_Guide.evr.ven.*;
import testcases.pull.DR_Program_Guide.thr.ven.*;
import testcases.pull.event.ven.*;
import testcases.pull.general.ven.*;
import testcases.pull.opt.ven.*;
import testcases.pull.registerparty.ven.*;
import testcases.pull.report.ven.*;
import testcases.push.DR_Program_Guide.cbp.ven.*;
import testcases.push.DR_Program_Guide.cpp.ven.*;
import testcases.push.DR_Program_Guide.der.ven.*;
import testcases.push.DR_Program_Guide.evp.ven.*;
import testcases.push.DR_Program_Guide.fdr.ven.*;
import testcases.push.DR_Program_Guide.thr.ven.*;
import testcases.push.event.ven.*;
import testcases.push.general.ven.*;
import testcases.push.opt.ven.*;
import testcases.push.registerparty.ven.*;
import testcases.push.report.ven.*;


import testcases.pull.DR_Program_Guide.cbp.vtn.*;
import testcases.pull.DR_Program_Guide.cpp.vtn.*;
import testcases.pull.DR_Program_Guide.der.vtn.*;
import testcases.pull.DR_Program_Guide.evp.vtn.*;
import testcases.pull.DR_Program_Guide.evr.vtn.*;
import testcases.pull.DR_Program_Guide.thr.vtn.*;
import testcases.pull.event.vtn.*;
import testcases.pull.general.vtn.*;
import testcases.pull.opt.vtn.*;
import testcases.pull.registerparty.vtn.*;
import testcases.pull.report.vtn.*;
import testcases.push.DR_Program_Guide.cbp.vtn.*;
import testcases.push.DR_Program_Guide.cpp.vtn.*;
import testcases.push.DR_Program_Guide.der.vtn.*;
import testcases.push.DR_Program_Guide.evp.vtn.*;
import testcases.push.DR_Program_Guide.fdr.vtn.*;
import testcases.push.DR_Program_Guide.thr.vtn.*;
import testcases.push.event.vtn.*;
import testcases.push.general.vtn.*;
import testcases.push.opt.vtn.*;
import testcases.push.registerparty.vtn.*;
import testcases.push.report.vtn.*;

import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.internal.IniData;
import com.qualitylogic.openadr.core.util.ConformanceRuleValidator;
import com.qualitylogic.openadr.core.util.OadrUtil;
import com.qualitylogic.openadr.core.util.PropertiesFileReader;
import com.qualitylogic.openadr.core.util.ResourceFileReader;
import com.qualitylogic.openadr.core.util.Trace;




public class TestSelector {

	//Left is tree split up/down with right which also split across with Log on top and console on the bottom
	private static JTree tree;
	public static JScrollPane testsPane;
	public static JEditorPane txtConsole;
	public static JEditorPane txtLog;
	public static JScrollPane consolePane;

	public static JScrollPane logPane;

	public static JSplitPane txtLogPane;

	public static JSplitPane spPane;

	public static JFrame frame = new JFrame("OpenADR 2.0b Certification Test Runner");
	public static JButton btnRun = new JButton("Run");
	private static Boolean bdoRun = false;
	public static JButton btnResume = new JButton("Resume");
	private static Boolean bdoResume = false;
	private static ArrayList<String> runList = new ArrayList<String>();
	private static ArrayList<String> runListRes = new ArrayList<String>();
	private static ArrayList<String> runListLog = new ArrayList<String>();
	private static ArrayList<Integer> runListNum = new ArrayList<Integer>();
	private static ArrayList<File> fileList = new ArrayList<File>();

	private static JMenuItem exitI, copyOutI, reloadI;
	private static Timer timer = null;
	private static Timer timerL = null;
	private static Timer timerT = null;
	private static TitledBorder title = null;

	public static JCheckBox doScroll = new JCheckBox("Auto Scroll ");
	private static boolean bdoScroll = true;
	public static JCheckBox doPause = new JCheckBox("Pause Between Tests ");
	private static boolean bdoPause = true;
	public static JCheckBox noPrompt = new JCheckBox("No Prompts ");
	private static boolean bnoPrompt = false;
	public static JComboBox<String> setSelect = new JComboBox<String>();
	private static Integer saveSelected = 0;
	private static String tstStor = "not set";

	private static String currentTest = "";
	private static Boolean btestRunning = false;
	private static String sChk = "\u2713";
	private static ArrayList<TreePath> nList = new ArrayList<TreePath>();
	private static ArrayList<TreePath> pnList = new ArrayList<TreePath>();


	public static Double logSplit = 0.66d;
	public static Double psclWid = 0.33d;
	public static int pscrLocX = 50;
	public static int pscrLocY = 50;
	public static int pscrSzHgt = 600;
	public static int pscrSzWid = 800;

	public static ByteArrayOutputStream baosStd = new ByteArrayOutputStream();
	public static PrintStream pStd = new PrintStream(baosStd);

	static ImageIcon barRed = new ImageIcon(getSq(30,10,Color.red));
	static ImageIcon barGreen = new ImageIcon(getSq(30,10,Color.green));
	static ImageIcon barGray = new ImageIcon(getSq(30,10,Color.gray));
	static ImageIcon barCyan = new ImageIcon(getSq(30,10,Color.cyan));
	static ImageIcon barWhite = new ImageIcon(getSq(30,10,Color.white));
	static ImageIcon barBlack = new ImageIcon(getSq(30,10,Color.black));
	static ImageIcon barYellow = new ImageIcon(getSq(30,10,Color.yellow));
	private static Boolean lclick = false; 
	private static Boolean bUnk = false;

	private static Map<String, CellStyle> styles;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {

				//		TestSession.setBdoPrompt(true);
				//		TestSession.setBalwaysPrompt(true);
				//setup output teeing
				PrintStream teeStdOut = new TeeStream(System.out, pStd);
				PrintStream teeStdErr = new TeeStream(System.err, pStd);

				System.setOut(teeStdOut);
				System.setErr(teeStdErr);
				getIni();
				int inset = 50;
				mDir("TestResults");
				//check for test sets to load.
				if(!(new File("TestSets").exists() && new File("TestSets").isDirectory())){
					System.out.println("Cannot start the test selection without test sets in a TestSets directory.");
					JOptionPane.showMessageDialog(null, "Cannot start the test selection without test sets in a TestSets directory.\nExiting",
							"Missing TestSets Directory.", JOptionPane.ERROR_MESSAGE);
					System.exit(0);
				}
				if(!checkForTxt()){
					System.out.println("Cannot start the test selection without test sets in a TestSets directory.");
					JOptionPane.showMessageDialog(null, "Cannot start the test selection without test sets in the TestSets directory.\nExiting",
							"Missing TestSets.", JOptionPane.ERROR_MESSAGE);
					System.exit(0);
				}
				loadSetSelect();

				setSelect.setToolTipText("Choose a test set to run.");
				//JFrame frame = new JFrame("OpenADR 2.0b Certification Test Runner");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				//save the window sizing on close
				frame.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent windowEvent) {
						setIni();
					}
				});

				txtConsole = new JEditorPane();
				txtLog = new JEditorPane();
				txtConsole.setEditable(false);
				txtLog.setEditable(false);
				txtLog.setText("");
				txtConsole.setText("");
				testsPane = new JScrollPane(getTree(),
						JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
				monitorOutput();
				consolePane = new JScrollPane(txtConsole,
						JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

				logPane = new JScrollPane(txtLog,
						JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

				txtLogPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,logPane,consolePane);

				spPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,testsPane,txtLogPane);

				doScroll.setToolTipText("The display will move to the end of the scroll when checked.");
				doPause.setToolTipText("Pause between tests until resume is clicked.");
				noPrompt.setToolTipText("Disable user prompts unless specifically overriden in test.");

				copyOutI = new JMenuItem("Copy console to Clipboard");
				copyOutI.setMnemonic('C');
				reloadI = new JMenuItem("Reload Current Test Set");
				reloadI.setMnemonic('R');
				exitI = new JMenuItem("Exit");
				exitI.setMnemonic('x');
				JMenuBar menuBar = new JMenuBar();
				menuBar.setBorder(new BevelBorder(BevelBorder.RAISED));
				frame.setJMenuBar(menuBar);

				JMenu fileM = new JMenu("File");
				fileM.setHorizontalAlignment(SwingConstants.LEFT);
				fileM.setMnemonic(KeyEvent.VK_F);
				menuBar.add(fileM);
				fileM.add(copyOutI);
				fileM.add(reloadI);
				fileM.add(exitI);
				menuBar.add( Box.createHorizontalStrut( 10 ) ); 
				menuBar.add(setSelect);
				menuBar.add( Box.createHorizontalStrut( 10 ) ); 
				menuBar.add(btnRun);
				menuBar.add( Box.createHorizontalStrut( 10 ) ); 
				menuBar.add(doScroll);
				menuBar.add( Box.createHorizontalStrut( 10 ) ); 
				menuBar.add(doPause);
				menuBar.add( Box.createHorizontalStrut( 10 ) ); 
				menuBar.add(noPrompt);
				menuBar.add( Box.createHorizontalStrut( 10 ) ); 
				menuBar.add(btnResume);
				menuBar.add( Box.createHorizontalStrut( 10 ) ); 

				btnResume.setVisible(false);
				btnResume.setBackground(Color.YELLOW);

				copyOutI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));
				copyOutI.addActionListener(new MenuActionListener());
				reloadI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));
				reloadI.addActionListener(new MenuActionListener());
				exitI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
				exitI.addActionListener(new MenuActionListener());

				//run or stop the tests
				btnRun.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) { 
						if(bdoRun){
							bdoRun = false;
							btnRun.setText("Stopping");
							btnRun.setBackground(Color.YELLOW);
							frame.getContentPane().revalidate();
							frame.getContentPane().repaint();
						}else{
							if(btnRun.getText().contains("Stopping"))
								return;

							bdoRun = true;
							btnRun.setText("Stop");
							btnRun.setBackground(Color.GREEN);
							frame.getContentPane().revalidate();
							frame.getContentPane().repaint();
							pause(100);
							setIni();
							StartTestThread tst = new StartTestThread();
							Thread newTest = new Thread(tst);
							newTest.start();
						}
					} 
				} );

				//resume after a pause
				btnResume.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) { 
						bdoResume=true;
						btnResume.setVisible(false);
					} 
				} );

				//update the tree with the tests selected from the dropdown
				setSelect.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) { 
						if (lclick){
							lclick = false;
							return;
						}
						runListNum.clear();
						runList.clear();
						runListRes.clear();
						runListLog.clear();
						saveSelected = setSelect.getSelectedIndex();
						testsPane.setViewportView(getTree());
						frame.getContentPane().validate();
						frame.getContentPane().repaint();
					} 
				} );

				doScroll.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						if(doScroll.isSelected()){
							bdoScroll=true;
							DefaultCaret caret = (DefaultCaret)txtConsole.getCaret();
							caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
							caret = (DefaultCaret)txtLog.getCaret();
							caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
						}else{
							bdoScroll=false;
							DefaultCaret caret = (DefaultCaret)txtConsole.getCaret();
							caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
							caret = (DefaultCaret)txtLog.getCaret();
							caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
						}
					}
				});

				//set the pause on or off
				doPause.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						if(doPause.isSelected()){
							bdoPause=true;
						}else{
							bdoPause=false;
						}
					}
				});

				//set the prompt on or off
				noPrompt.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						if(noPrompt.isSelected()){
							bnoPrompt=true;
						}else{
							bnoPrompt=false;
						}
					}
				});

				windowInit();
				frame.setIconImage(Toolkit.getDefaultToolkit().getImage(TestSelector.class.getResource("/resources/q.png")));
				frame.setBounds(inset, inset, pscrSzWid, pscrSzHgt);
				frame.setLocation(pscrLocX, pscrLocY);
				Border empty = BorderFactory.createEmptyBorder(0, 5, 25, 5);
				title = BorderFactory.createTitledBorder(empty, "");
				title.setTitlePosition(TitledBorder.BOTTOM);
				updateTitle();
				spPane.setBorder(title);
				frame.getContentPane().add(spPane);
				frame.setSize(pscrSzWid, pscrSzHgt);
				frame.setVisible(true);
				spPane.setDividerLocation(psclWid);
				txtLogPane.setDividerLocation(logSplit);

				logPane.setSize(txtLogPane.getWidth()-10, logPane.getHeight());
			}
		});

	}
	public static JTree getTree(){
		DefaultMutableTreeNode top = new DefaultMutableTreeNode(" " + setSelect.getItemAt(saveSelected));
		//Create a tree that allows one selection at a time.
		tree = new JTree(top);
		ToolTipManager.sharedInstance().registerComponent(tree);
		tree.setCellRenderer(new MyTreeCellRenderer());
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
		createNodes(top);
		tree.setShowsRootHandles(true);
		for (int i = 0; i < tree.getRowCount(); i++) {
			tree.expandRow(i);
		}


		//Listen for when the selection changes.
		tree.addTreeSelectionListener(new TreeSelectionListener(){public void valueChanged(TreeSelectionEvent e) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)
					tree.getLastSelectedPathComponent();
			if (node == null) return;

			if (node.isRoot()) {
			} else {
				if(nList != null){
					pnList.clear();
					for(int j=0;j<nList.size();j++){
						pnList.add(nList.get(j));
					}
				}
				nList.clear();
				for (TreePath npath : tree.getSelectionPaths()) {
					nList.add(npath);
				}
			}
		}
		});
		tree.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.NOBUTTON) {
					//logger.info("No button clicked...");
				} else if (e.getButton() == MouseEvent.BUTTON1) {
					//logger.info("Button 1 clicked...");
					DefaultMutableTreeNode node = (DefaultMutableTreeNode)
							tree.getLastSelectedPathComponent();
					if (node == null)return;
					if (node.isRoot()) {
						return;
					}else {
					}
				} else if (e.getButton() == MouseEvent.BUTTON2) {
					//logger.info("Button 2 clicked...");
				} else if (e.getButton() == MouseEvent.BUTTON3) {
					//logger.info("Button 3 clicked...");
					TreePath path = tree.getPathForLocation(e.getX(), e.getY());
					if (path != null && !bdoRun && !btestRunning) {
						runListRes.clear();
						int selRow = tree.getRowForLocation(e.getX(), e.getY());
						tree.setSelectionPath(path); 
						if (selRow>-1){
							tree.setSelectionRow(selRow); 
						}
						DefaultMutableTreeNode node = (DefaultMutableTreeNode)
								tree.getLastSelectedPathComponent();
						tree.scrollPathToVisible(path);
						tree.setSelectionPath(path);
						if (node.isRoot()) {
							String stst = node.getUserObject().toString();
							if(stst.startsWith(sChk)){
								//unckeck all
								runListNum.clear();
								node.setUserObject(" " + stst.substring(1));
								for(int i = 0 ; i < node.getChildCount() ; i++)
								{
									DefaultMutableTreeNode aNode = (DefaultMutableTreeNode) node.getChildAt(i);
									String stst2 = aNode.getUserObject().toString();
									if(stst2.startsWith(sChk)){
										aNode.setUserObject(" " + stst2.substring(1));
									}
								}
							}else {
								//ckeck all
								node.setUserObject(sChk + stst.substring(1));
								for(int i = 0 ; i < node.getChildCount() ; i++)
								{
									DefaultMutableTreeNode aNode = (DefaultMutableTreeNode) node.getChildAt(i);
									String stst2 = aNode.getUserObject().toString();
									if(stst2.startsWith(" ")){
										aNode.setUserObject(sChk + stst2.substring(1));
									}
									if(runListNum.indexOf(i) == -1){
										runListNum.add(i);
									}
								}
								Collections.sort(runListNum);

							}
							DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
							DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
							model.reload(root);
						}else {
							//A means to determine whether a single click or multiple click was done as the 
							//right-click tends to wipe out the current information and you do not get 
							//the selection paths here that you get in treeselection above.
							//pnlist = 0 is an initial right-click
							//pnlist = 1, just go with nlist(which is always one due to the right-click
							//pnlist > 1, check if nlist is in pnlist. If so, do pnlist, if not, do nlist
							ArrayList<TreePath> lnList = new ArrayList<TreePath>();
							if(pnList.size() < 2){
								lnList.add(nList.get(0));
							}else{
								if(pnList.indexOf(nList.get(0)) == -1){
									lnList.add(nList.get(0));
								} else {
									for(int j=0;j<pnList.size();j++){
										lnList.add(pnList.get(j));
									}
								}
							}
							for(int j=0;j<lnList.size();j++){
								tree.setSelectionPath(lnList.get(j)); 
								node = (DefaultMutableTreeNode)	tree.getLastSelectedPathComponent();
								int ic = node.getParent().getIndex(node);
								String stst = node.getUserObject().toString();
								if(stst.startsWith(sChk)){
									node.setUserObject(" " + stst.substring(1));

									if(runListNum.indexOf(ic)>-1){
										runListNum.remove(runListNum.indexOf(ic));
									}
								}else {
									node.setUserObject(sChk + stst.substring(1));
									DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
									DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
									String stst2 = root.getUserObject().toString();
									if(!stst.startsWith(sChk)){
										root.setUserObject(sChk + stst2.substring(1));
										if(runListNum.indexOf(ic) == -1){
											runListNum.add(ic);
											Collections.sort(runListNum);
										}
									}
								}
							}
							DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
							DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
							model.reload(root);
							frame.getContentPane().validate();
							frame.getContentPane().repaint();
						}

						int isel=0;
						int itotal = 0;
						DefaultMutableTreeNode tNode = (DefaultMutableTreeNode) tree.getModel().getRoot();
						for(int i = 0 ; i < tNode.getChildCount() ; i++)
						{
							DefaultMutableTreeNode aNode = (DefaultMutableTreeNode) tNode.getChildAt(i);
							String stst2 = aNode.getUserObject().toString();
							if(stst2.startsWith(sChk)){
								isel++;
								itotal++;
							} else {
								itotal++;
							}
						}
						if(isel == 0){
							DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
							DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
							String stst2 = root.getUserObject().toString();
							root.setUserObject(" " + stst2.substring(1));
							model.reload(root);
							frame.getContentPane().validate();
							frame.getContentPane().repaint();

						}
						title.setTitle( isel + " test selected. "  + itotal + " tests available.    Results Dir: " + tstStor);
						spPane.setBorder(title);
						frame.getContentPane().validate();
						frame.getContentPane().repaint();

						//						txtStat.setText(" " + isel + " test files selected." + el + " " + itotal + " test files available." + el);

					}
				}

				//logger.info("Number of click: " + e.getClickCount());
				//logger.info("Click position (X, Y):  " + e.getX() + ", " + e.getY());
			}
			public void mousePressed(MouseEvent e) {
				int selRow = tree.getRowForLocation(e.getX(), e.getY());
				TreePath path = tree.getPathForLocation(e.getX(), e.getY());
				if(selRow != -1) {
					if(e.getClickCount() == 1 && e.getButton() == MouseEvent.BUTTON1) {
						if (bdoRun)
							return;
						tree.setSelectionPath(path); 
						if (selRow>-1){
							tree.setSelectionRow(selRow); 
						}
						DefaultMutableTreeNode node = (DefaultMutableTreeNode)
								tree.getLastSelectedPathComponent();
						tree.scrollPathToVisible(path);
						tree.setSelectionPath(path);
						if (node.isRoot()) {
						}else {
							//Get the Status
							//String snode = (String) node.getUserObject();
							//snode=snode.substring(1);
							//setOpaque(true);
							//setForeground(Color.black);
							int ic = node.getParent().getIndex(node);
							if(runListNum.indexOf(ic) > -1){
								int ix = runListNum.indexOf(ic);
								if(ix < runListLog.size()){
									File mfFile = new File("log/"+runListLog.get(ix));
									if(mfFile.exists()){
										try {
											txtLog.setText(FileUtils.readFileToString(mfFile, "UTF-8"));
										} catch (IOException e1) {
											e1.printStackTrace();
										}
									}
								}
							}
						}

					}
					else if(e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
						//logger.info("Double click");
						tree.setSelectionPath(path); 
						if (selRow>-1){
							tree.setSelectionRow(selRow); 
						}
						DefaultMutableTreeNode node = (DefaultMutableTreeNode)
								tree.getLastSelectedPathComponent();
						tree.scrollPathToVisible(path);
						tree.setSelectionPath(path);
						if (node.isRoot()) {
						}else {
							//Get the Status
							//String snode = (String) node.getUserObject();
							//snode=snode.substring(1);
							//setOpaque(true);
							//setForeground(Color.black);
							int ic = node.getParent().getIndex(node);
							if(runListNum.indexOf(ic) > -1){
								int ix = runListNum.indexOf(ic);
								if(ix < runListLog.size()){
									File mfFile = new File("log/"+runListLog.get(ix));
									if(mfFile.exists()){
										if (Desktop.isDesktopSupported()) {
											Desktop desktop = Desktop.getDesktop();
											//logger.info("Opening " + mfFile + ".....");
											try {
												desktop.open(mfFile);
											} catch (IOException e1) {
												e1.printStackTrace();
											}
										}
									}
								}
							}
						}
						//myDoubleClick(selRow, selPath);
					}
				}
			}
		});
		tree.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				int x = (int) e.getPoint().getX();
				int y = (int) e.getPoint().getY();
				TreePath path = tree.getPathForLocation(x, y);
				if (path == null) {
					tree.setCursor(Cursor.getDefaultCursor());
				} else {
					tree.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				}
			}

		});


		return tree;
	}

	//set the title at the base of the frame with tests and the save directory if available
	private static void updateTitle(){
		int isel=0;
		int itotal = 0;
		DefaultMutableTreeNode tNode = (DefaultMutableTreeNode) tree.getModel().getRoot();
		for(int i = 0 ; i < tNode.getChildCount() ; i++)
		{
			DefaultMutableTreeNode aNode = (DefaultMutableTreeNode) tNode.getChildAt(i);
			String stst2 = aNode.getUserObject().toString();
			if(stst2.startsWith(sChk)){
				isel++;
				itotal++;
			} else {
				itotal++;
			}
		}
		if(isel == 0){
			DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
			DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
			String stst2 = root.getUserObject().toString();
			root.setUserObject(" " + stst2.substring(1));
			model.reload(root);
			frame.getContentPane().validate();
			frame.getContentPane().repaint();

		}
		title.setTitle( isel + " test selected. "  + itotal + " tests available.    Results Dir: " + tstStor);
		spPane.setBorder(title);
		frame.getContentPane().validate();
		frame.getContentPane().repaint();

	}
	//determine if any txt files are available for loading from TestSets
	private static Boolean checkForTxt(){
		File folder = new File("TestSets");
		FileFilter fileFilter = new WildcardFileFilter("*.txt");
		File[] listOfFiles = folder.listFiles(fileFilter);				
		if(listOfFiles.length > 0){
			return true;
		}else{
			return false;
		}

	}

	//determine if any txt files are available for loading from TestSets
	private static void loadSetSelect(){
		File folder = new File("TestSets");
		FileFilter fileFilter = new WildcardFileFilter("*.txt");
		File[] listOfFiles = folder.listFiles(fileFilter);				
		Arrays.sort(listOfFiles);//this would be based on the full file name
		int sz = 20;
		//setSelect.removeAllItems();
		setSelect.setMaximumSize(new Dimension(sz, 200));
		for (File f:listOfFiles){
			try {
				List<String> txtlist =FileUtils.readLines(f, "UTF-8");
				for(String txt:txtlist){
					if(txt.toLowerCase().startsWith("setname")){
						txt = txt.substring(txt.indexOf("\"")+1);
						txt = txt.substring(0,txt.indexOf("\""));
						setSelect.addItem(txt);
						fileList.add(f);
						if(txt.length()*8 > sz){
							sz = 8 * txt.length();
							setSelect.setMaximumSize(new Dimension(sz, 200));
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	private static void createNodes(DefaultMutableTreeNode top){
		//DefaultMutableTreeNode node = null;
		try {
			if(bdoScroll) doScroll.doClick();
			if(saveSelected >= fileList.size() || saveSelected < 0) saveSelected = 0;
			txtLog.setText(FileUtils.readFileToString(fileList.get(saveSelected), "UTF-8"));
			List<String> txtlist =FileUtils.readLines(fileList.get(saveSelected), "UTF-8");
			int icnt = 0;
			for(String txt:txtlist){
				if(txt.startsWith("#") || txt.equals("") || txt.toLowerCase().startsWith("setname"))
					continue;
				String[] stxt = txt.split("#");
				String pfx = " ";
				if(runListNum.indexOf(icnt)>-1){
					pfx = sChk;
				}
				top.add(new DefaultMutableTreeNode(pfx + stxt[0].trim()));
				icnt++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	public static class StartTestThread implements Runnable {
		StartTestThread(){
		}
		public void run() {
			bdoResume=false;
			if(!bdoScroll) doScroll.doClick();
			String tstDate = OadrUtil.createUniqueID();
			tstStor = "TestResults/TestResult_" + tstDate;
			mDir(tstStor);
			updateTitle();
			PropertiesFileReader propertiesFileReader = new PropertiesFileReader();
			//Changed the com.qualitylogic.openadr.core.base.NewTestCase.execute to public and this ran, which with test runner setup, ran the other half, but not the prompts at this point.
			//		com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_SMP_B1_TH_VTN_1());

			//get the checked tests to run
			runList.clear();
			runListRes.clear();
			runListLog.clear();
			monitorLog();
			DefaultMutableTreeNode tNode = (DefaultMutableTreeNode) TestSelector.getCurrentTree().getModel().getRoot();
			for(int i = 0 ; i < tNode.getChildCount() ; i++)
			{
				DefaultMutableTreeNode aNode = (DefaultMutableTreeNode) tNode.getChildAt(i);
				String stst2 = aNode.getUserObject().toString();
				if(stst2.startsWith(sChk)){
					runList.add(stst2.substring(1));
					runListRes.add(" ");
				}
			}
			rLoad();
			int icnt =0;
			for (String stest:runList){
				//check for other options
				String[] stestIn = stest.split(",");
				bUnk = false;
				bdoResume = false;
				TestSession.init();
				//is the override prompt set
				if(bnoPrompt){
					TestSession.setBdoPrompt(false);
				} else {
					TestSession.setBdoPrompt(true);
				}
				//is the always prompt override set
				TestSession.setBalwaysPrompt(false);
				TestSession.clearoadrRequestEventFound();
				if(stestIn.length > 1){
					if(stestIn[1].toLowerCase().contains("prompt")){
						TestSession.setBalwaysPrompt(true);
					}
				}
				runListRes.set(icnt, "Started");
				rLoad();
				TestSelector.setcurrentTest(stestIn[0]);
				TestSelector.setTestStart();
				runAtest();
				if(bUnk){
					clearSession();
					continue;
				}
				pause(200);
				//if this is setup to run batch, then we need to monitor as the thread will release after the first click.
				if(propertiesFileReader.isTestRunner()){
					monitorTest();
					while(btestRunning){
						pause(100);
					}
					//Get the log file
					findLog(tstDate, stestIn[0]);
				}
				//we will have to pull the log for test runner 
				if(txtLog.getText().toLowerCase().contains("test case has passed")){
					runListRes.set(icnt, "Passed");
				}else if(txtLog.getText().toLowerCase().contains("test case has failed")){
					runListRes.set(icnt, "Failed");
				}else if(txtLog.getText().toLowerCase().contains("testcase result status: na")){
					runListRes.set(icnt, "Canceled");
				}else{
					runListRes.set(icnt, "Unknown");
				}
				rLoad();
				clearSession();
				icnt++;
				if(bdoPause && icnt < runList.size()){
					btnResume.setVisible(true);
					while(!bdoResume){
						pause(100);
					}
					btnResume.setVisible(false);
				}
				if(!TestSelector.getDoRun()){
					break;
				}
			}
			TestSelector.resetRun();
			for (String logf:runListLog){
				try {
					FileUtils.copyFileToDirectory(new File("log/"+ logf), new File(tstStor));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			//create the Excel spreadsheet for the run.
			//"logs/" + sCurrentRun + ".log"
			try {
				String filename = tstStor + "/Results_" + tstDate +  ".xls" ;
				Workbook workbook = new HSSFWorkbook();
				styles = createStyles(workbook);
				Sheet sheet = workbook.createSheet("Report for " + tstDate);  
				//header row
				int ncol = 3;
				Row rowhead = sheet.createRow((short)0);
				rowhead.createCell(0).setCellValue("OpenAdr2.0b Test Run");
				rowhead.createCell(1);
				rowhead.createCell(2);
				//sheet.addMergedRegion(rowFrom,rowTo,colFrom,colTo);
				sheet.addMergedRegion(new CellRangeAddress(0,0,0,2));

				rowhead = sheet.createRow((short)1);
				rowhead.createCell(0).setCellValue("Results Dir: " + tstStor);
				rowhead.createCell(1);
				rowhead.createCell(2);
				sheet.addMergedRegion(new CellRangeAddress(1,1,0,2));

				rowhead = sheet.createRow((short)2);
				rowhead.createCell(0).setCellValue("Test File");
				rowhead.createCell(1).setCellValue("Result");
				rowhead.createCell(2).setCellValue("Log");
				for (int i = 0; i< ncol;i++){
					rowhead.getCell(i).setCellStyle( (CellStyle) styles.get("style1") );
				}
				icnt = 0;
				for (String rf:runList){
					Row row = sheet.createRow(icnt+3);
					row.createCell(0).setCellValue(rf);
					row.getCell(0).setCellStyle( (CellStyle) styles.get("base_style") );
					row.createCell(1).setCellValue(runListRes.get(icnt));
					if(runListRes.get(icnt).toLowerCase().contains("passed")){
						row.getCell(1).setCellStyle( (CellStyle) styles.get("green_style") );
					}else if(runListRes.get(icnt).toLowerCase().contains("failed")){
						row.getCell(1).setCellStyle( (CellStyle) styles.get("red_style") );
					}else if(runListRes.get(icnt).toLowerCase().contains("unknown") ||
							runListRes.get(icnt).toLowerCase().contains("canceled")){
						row.getCell(1).setCellStyle( (CellStyle) styles.get("yellow_style") );
					}else{
						row.getCell(1).setCellStyle( (CellStyle) styles.get("base_style") );
					}
					if(icnt < runListLog.size()){
						row.createCell(2).setCellValue(runListLog.get(icnt));
						row.getCell(2).setCellStyle( (CellStyle) styles.get("base_style") );
					}else{
						row.createCell(2).setCellValue(" ");
						row.getCell(2).setCellStyle( (CellStyle) styles.get("base_style") );
					}
					icnt++;
				}
				for (int k=0;k<ncol;k++ ){
					//					icnt = 0;
					//					for(int j=0;j<runList.size()+1;j++){
					//						if(sheet.getRow(j).getCell(k).getStringCellValue().length() > icnt){
					//							icnt = sheet.getRow(j).getCell(k).getStringCellValue().length();
					//							//pad the header a bit since it is bolded.
					//							if(j==0)icnt +=5;
					//						}
					//					}
					//					int width = ((int)(icnt * 1.04388)) * 256;
					//					sheet.setColumnWidth(k, width);
					sheet.autoSizeColumn(k,true);
				}
				FileOutputStream fileOut = new FileOutputStream(filename);
				workbook.write(fileOut);
				fileOut.close();
				workbook.close();
			} catch ( Exception ex ) {
				System.out.println(ex);
				ex.printStackTrace();
			}
			System.out.println("\n\n********************************************************************************************\n\nTest run processing completed.\n");
			System.out.println("********************************************************************************************");
			if(bdoScroll) doScroll.doClick();
		}
	}
	private static void clearSession(){
		TestSession.getDistributeEventActionList().clear();
		TestSession.getCreatedEventActionList().clear();
		TestSession.getResponseEventActionList().clear();
		TestSession.getCreatedEventReceivedList().clear();
		TestSession.getPingDistributeEventMap().clear();
		TestSession.getRequestReregistrationActionList().clear();
		TestSession.getResponseCreatedOptAckActionList().clear();
		TestSession.getResponseCanceledOptAckActionList().clear();
		TestSession.getCreateOptEventReceivedList().clear();
		TestSession.getCreateOptEventSentList().clear();
		TestSession.getCreatedOptReceivedList().clear();
		TestSession.getCancelOptTypeReceivedList().clear();
		TestSession.getCreatePartyRegistrationTypeReceivedList().clear();
		TestSession.getOadrQueryRegistrationTypeReceivedList().clear();
		TestSession.getOadrRegisterReportTypeReceivedList().clear();
		TestSession.getOadrRegisterReportTypeSentList().clear();
		TestSession.getOadrCreateReportTypeReceivedList().clear();
		TestSession.getOadrUpdateReportTypeReceivedList().clear();
		TestSession.getOadrCancelReportTypeReceivedList().clear();
		TestSession.getOadrCancelReportTypeSentList().clear();
		TestSession.getCancelPartyRegistrationTypeListReceived().clear();
		TestSession.getCancelPartyRegistrationTypeSentList().clear();
		TestSession.getRegisterParty_OadrResponseListReceivedList().clear();
		TestSession.getCanceledPartyRegistrationReceivedToList().clear();
		TestSession.getOadrRequestReregistrationReceivedList().clear();
		TestSession.getCreatedPartyRegistrationAckActionList().clear();
		TestSession.getCreatedPartyRegistrationToQueryAckActionList().clear();
		TestSession.getResponseRegisteredReportTypeAckActionList().clear();
		TestSession.getResponseCreatedReportTypeAckActionList().clear();
		TestSession.getResponseUpdatedReportTypeAckActionList().clear();
		TestSession.getResponseCanceledReportTypeAckActionList().clear();
		TestSession.getResponseCanceledPartyRegistrationAckActionList().clear();
		TestSession.getResponseCancelPartyRegistrationAckActionList().clear();
		TestSession.getPingDistributeEventMap().clear();
		TestSession.getCreatedEventReceivedList().clear();
		TestSession.getOadrRegisteredReportTypeReceivedList().clear();
		TestSession.getOadrRegisteredReportTypeSentList().clear();
		TestSession.getOadrCreatedReportTypeReceivedList().clear();
		TestSession.getOadrUpdatedReportTypeReceivedList().clear();
		TestSession.getOadrCanceledReportTypeReceivedList().clear();
		TestSession.getOadrCreateReportTypeSentList().clear();
		TestSession.getOadrUpdateReportTypeSentList().clear();		

		TestSession.getResponseRegisterReportTypeAckActionList().clear();
		TestSession.getResponseCreateReportTypeAckActionList().clear();
		TestSession.getResponseUpdateReportTypeAckActionList().clear();
		TestSession.getResponseCancelReportTypeAckActionList().clear();
		ConformanceRuleValidator.reInit();


	}
	private static void getIni(){
		IniData inDat = new IniData();
		inDat.readIni();
		pscrSzHgt = inDat.pscrSzHgt;
		pscrSzWid = inDat.pscrSzWid;
		psclWid = inDat.psclWid;
		logSplit = inDat.logSplit;
		pscrLocX = inDat.pscrLocX;
		pscrLocY = inDat.pscrLocY;
		bdoScroll = inDat.bdoScroll;
		bdoPause = inDat.bdoPause;
		bnoPrompt = inDat.bnoPrompt;
		saveSelected = inDat.saveSelected;
		runListNum.clear();
		for(Integer ns:inDat.runListNum){
			runListNum.add(ns);
		}
		Collections.sort(runListNum);

	}
	private static void windowInit(){
		if(bdoScroll){
			if(!doScroll.isSelected()){
				doScroll.doClick();
			} else {
				DefaultCaret caret = (DefaultCaret)txtConsole.getCaret();
				caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
				caret = (DefaultCaret)txtLog.getCaret();
				caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
			}
		}else{
			if(doScroll.isSelected()){
				doScroll.doClick();
			} else {
				DefaultCaret caret = (DefaultCaret)txtConsole.getCaret();
				caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
				caret = (DefaultCaret)txtLog.getCaret();
				caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
			}
		}
		if(bdoPause){
			if(!doPause.isSelected()){
				doPause.doClick();
			}
		}else{
			if(doPause.isSelected()){
				doPause.doClick();
			}
		}
		if(bnoPrompt){
			if(!noPrompt.isSelected()){
				noPrompt.doClick();
			}
		}else{
			if(noPrompt.isSelected()){
				noPrompt.doClick();
			}
		}
		lclick = true;
		setSelect.setSelectedIndex(saveSelected);
	}
	private static void setIni(){
		IniData inDat = new IniData();
		int itmp = frame.getHeight();
		if(itmp > 0)
			inDat.pscrSzHgt = itmp;
		itmp = frame.getWidth();
		if(itmp > 0)
			inDat.pscrSzWid = itmp;
		itmp = frame.getLocation().x;
		if(itmp > 0)
			inDat.pscrLocX = itmp;
		itmp = frame.getLocation().y;
		if(itmp > 0)
			inDat.pscrLocY = itmp;
		inDat.psclWid = (double)spPane.getDividerLocation()/(double)(spPane.getWidth());
		inDat.logSplit = (double)txtLogPane.getDividerLocation()/(double)(txtLogPane.getHeight());
		inDat.bdoScroll = bdoScroll;
		inDat.bdoPause = bdoPause;
		inDat.bnoPrompt = bnoPrompt;
		inDat.runListNum.clear();
		Collections.sort(runListNum);
		for(Integer ns:runListNum){
			inDat.runListNum.add(ns);
		}
		inDat.saveSelected = saveSelected;
		inDat.writeIni();

	}
	static class MenuActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JMenuItem choice = (JMenuItem) e.getSource();
			if (choice == exitI){
				frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
			} else if(choice == copyOutI){
				StringSelection selection = new StringSelection(txtConsole.getText());
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents(selection, selection);
			} else if(choice == reloadI){
				runListNum.clear();
				runList.clear();
				runListRes.clear();
				runListLog.clear();
				saveSelected = setSelect.getSelectedIndex();
				testsPane.setViewportView(getTree());
				frame.getContentPane().validate();
				frame.getContentPane().repaint();
			}
		}
	}
	public static void monitorTest(){
		//a timer function to monitor the current test
		timerT = new Timer(200, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Process p = Runtime.getRuntime().exec("tasklist /V /FO CSV");
					String tasks = IOUtils.toString(p.getInputStream(), "UTF-8");
					IOUtils.closeQuietly(p.getInputStream());
					if(!tasks.contains(TestSelector.getcurrentTest())){
						//btestRunning = false;
						TestSelector.setTestDone();
						timerT.stop();
					}
				} catch (Exception err) {
					err.printStackTrace();
				}
			}
		});
		timerT.setRepeats(true);
		timerT.setCoalesce(true);
		timerT.start();

	}
	public static void monitorLog(){
		//a timer function to monitor the current test
		timerL = new Timer(250, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Trace trace = TestSession.getTraceObj();
				if(trace != null){
					txtLog.setText(trace.getLogFileContentTrace().toString());
				}
				if(!getDoRun()){
					//baosStd.reset();
					timerL.stop();
					return;
				}
				//txtConsole.setText(new String(baosStd.toByteArray(), StandardCharsets.UTF_8));
			}
		});
		timerL.setRepeats(true);
		timerL.setCoalesce(true);
		timerL.start();

	}
	public static void monitorOutput(){
		//a timer function to monitor the current test
		timer = new Timer(100, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(baosStd !=null && baosStd.toByteArray() != null)
					txtConsole.setText(new String(baosStd.toByteArray(), StandardCharsets.UTF_8));
			}
		});
		timer.setRepeats(true);
		timer.setCoalesce(true);
		timer.start();

	}
	private static void resetRun(){
		//reset the run button
		bdoRun = false;
		btestRunning = false;
		btnRun.setText("Run");
		btnRun.setBackground(new JButton().getBackground());
		frame.getContentPane().revalidate();
		frame.getContentPane().repaint();
	}
	private static void rLoad(){
		//reset the run button
		DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
		model.reload(root);
		frame.getContentPane().revalidate();
		frame.getContentPane().repaint();
	}
	private static void pause(int i){
		try {Thread.sleep(i);} catch (InterruptedException e) {
		}
		return;
	}
	private static Boolean getDoRun(){
		return bdoRun;
	}
	private static void setTestDone(){
		btestRunning = false;
	}
	private static void setTestStart(){
		btestRunning = true;
	}
	private static JTree getCurrentTree(){
		return tree;
	}
	private static String getcurrentTest(){
		return currentTest;
	}
	private static void setcurrentTest(String test){
		currentTest = test;
		return;
	}
	public static class TeeStream extends PrintStream {
		PrintStream out;
		public TeeStream(PrintStream out1, PrintStream out2) {
			super(out1);
			this.out = out2;
		}
		public void write(byte buf[], int off, int len) {
			try {
				super.write(buf, off, len);
				out.write(buf, off, len);
			} catch (Exception e) {
			}
		}
		public void flush() {
			super.flush();
			out.flush();
		}
	}
	private static Map<String, CellStyle> createStyles(Workbook wb){
		Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
		DataFormat df = wb.createDataFormat();

		CellStyle style;
		Font headerFont = wb.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 12);
		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.CENTER);;
		style.setFont(headerFont);
		styles.put("style1", style);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setFont(headerFont);
		style.setDataFormat(df.getFormat("d-mmm"));
		styles.put("date_style", style);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.LEFT);
		style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		//style.setFont(headerFont);
		styles.put("green_style", style);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.LEFT);
		//style.setFont(headerFont);
		styles.put("base_style", style);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.LEFT);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		//style.setFont(headerFont);
		styles.put("red_style", style);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.LEFT);
		style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		//style.setFont(headerFont);
		styles.put("yellow_style", style);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.LEFT);
		style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		//style.setFont(headerFont);
		styles.put("lightyellow_style", style);


		style = wb.createCellStyle();
		style.setAlignment(HorizontalAlignment.LEFT);
		//style.setFont(headerFont);
		styles.put("left_style", style);

		style = wb.createCellStyle();
		style.setAlignment(HorizontalAlignment.RIGHT);
		//style.setFont(headerFont);
		styles.put("right_style", style);

		return styles;
	}
	private static CellStyle createBorderedStyle(Workbook wb) {
		CellStyle style = wb.createCellStyle();
		style.setBorderRight(BorderStyle.THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderBottom(BorderStyle.THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(BorderStyle.THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(BorderStyle.THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		return style;
	} 
	private static void runAtest(){
		switch(TestSelector.getcurrentTest()){
		//*****************************************************************************************************************
		//VEN
		case "CBP_CMP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_CMP_B1_TH_VTN_1());
			break;
		case "CBP_SMP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_SMP_B1_TH_VTN_1());
			break;
		case "CBP_TYP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_TYP_B1_TH_VTN_1());
			break;
		case "CPP_CMP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_CMP_B1_TH_VTN_1());
			break;
		case "CPP_SMP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_SMP_B1_TH_VTN_1());
			break;
		case "CPP_TYP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_TYP_B1_TH_VTN_1());
			break;
		case "DER_TYP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new DER_TYP_B1_TH_VTN_1());
			break;
		case "EVP_TYP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new EVP_TYP_B1_TH_VTN_1());
			break;
		case "EVR_SMP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new EVR_SMP_B1_TH_VTN_1());
			break;
		case "EVR_TYP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new EVR_TYP_B1_TH_VTN_1());
			break;
		case "THR_CMP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_CMP_B1_TH_VTN_1());
			break;
		case "THR_SMP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_SMP_B1_TH_VTN_1());
			break;
		case "THR_TYP_B1_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_TYP_B1_TH_VTN_1());
			break;
		case "A_E1_0020_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0020_TH_VTN_1());
			break;
		case "A_E1_0040_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0040_TH_VTN_1());
			break;
		case "A_E1_0060_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0060_TH_VTN_1());
			break;
		case "A_E1_0070_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0070_TH_VTN_1());
			break;
		case "A_E1_0082_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0082_TH_VTN_1());
			break;
		case "A_E1_0086_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0086_TH_VTN_1());
			break;
		case "A_E1_0090_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0090_TH_VTN_1());
			break;
		case "A_E1_0092_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0092_TH_VTN_1());
			break;
		case "A_E1_0094_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0094_TH_VTN_1());
			break;
		case "A_E1_0096_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0096_TH_VTN_1());
			break;
		case "A_E1_0098_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0098_TH_VTN_1());
			break;
		case "A_E1_0100_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0100_TH_VTN_1());
			break;
		case "A_E1_0102_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0102_TH_VTN_1());
			break;
		case "A_E1_0104_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0104_TH_VTN_1());
			break;
		case "A_E1_0110_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0110_TH_VTN_1());
			break;
		case "A_E1_0130_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0130_TH_VTN_1());
			break;
		case "A_E1_0180_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0180_TH_VTN_1());
			break;
		case "A_E1_0190_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0190_TH_VTN_1());
			break;
		case "A_E1_0200_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0200_TH_VTN_1());
			break;
		case "A_E1_0210_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0210_TH_VTN_1());
			break;
		case "A_E1_0220_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0220_TH_VTN_1());
			break;
		case "A_E1_0230_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0230_TH_VTN_1());
			break;
		case "A_E1_0240_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0240_TH_VTN_1());
			break;
		case "A_E1_0250_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0250_TH_VTN_1());
			break;
		case "A_E1_0260_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0260_TH_VTN_1());
			break;
		case "A_E1_0262_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0262_TH_VTN_1());
			break;
		case "A_E1_0267_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0267_TH_VTN_1());
			break;
		case "A_E1_0268_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0268_TH_VTN_1());
			break;
		case "A_E1_0270_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0270_TH_VTN_1());
			break;
		case "A_E1_0280_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0280_TH_VTN_1());
			break;
		case "A_E1_0285_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0285_TH_VTN_1());
			break;
		case "A_E1_0300_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0300_TH_VTN_1());
			break;
		case "A_E1_0310_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0310_TH_VTN_1());
			break;
		case "A_E1_0340_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0340_TH_VTN_1());
			break;
		case "A_E1_0345_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0345_TH_VTN_1());
			break;
		case "A_E1_0360_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0360_TH_VTN_1());
			break;
		case "A_E1_0370_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0370_TH_VTN_1());
			break;
		case "A_E1_0390_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0390_TH_VTN_1());
			break;
		case "A_E1_0392_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E1_0392_TH_VTN_1());
			break;
		case "E1_1010_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1010_TH_VTN_1());
			break;
		case "E1_1020_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1020_TH_VTN_1());
			break;
		case "E1_1025_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1025_TH_VTN_1());
			break;
		case "E1_1027_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1027_TH_VTN_1());
			break;
		case "E1_1030_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1030_TH_VTN_1());
			break;
		case "E1_1040_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1040_TH_VTN_1());
			break;
		case "E1_1050_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1050_TH_VTN_1());
			break;
		case "E1_1055_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1055_TH_VTN_1());
			break;
		case "E1_1060_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1060_TH_VTN_1());
			break;
		case "E1_1065_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1065_TH_VTN_1());
			break;
		case "E1_1070_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1070_TH_VTN_1());
			break;
		case "E1_1080_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1080_TH_VTN_1());
			break;
		case "E1_1090_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1090_TH_VTN_1());
			break;
		case "G1_4005_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4005_TH_VTN_1());
			break;
		case "G1_4007_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4007_TH_VTN_1());
			break;
		case "G1_4010_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4010_TH_VTN_1());
			break;
		case "G1_4011_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4011_TH_VTN_1());
			break;
		case "G1_4012_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4012_TH_VTN_1());
			break;
		case "G1_4015_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4015_TH_VTN_1());
			break;
		case "G1_4030_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4030_TH_VTN_1());
			break;
		case "P1_2010_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2010_TH_VTN_1());
			break;
		case "P1_2015_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2015_TH_VTN_1());
			break;
		case "P1_2020_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2020_TH_VTN_1());
			break;
		case "P1_2030_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2030_TH_VTN_1());
			break;
		case "P1_2040_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2040_TH_VTN_1());
			break;
		case "P1_2050_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2050_TH_VTN_1());
			break;
		case "N1_0010_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0010_TH_VTN_1());
			break;
		case "N1_0015_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0015_TH_VTN_1());
			break;
		case "N1_0020_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0020_TH_VTN_1());
			break;
		case "N1_0025_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0025_TH_VTN_1());
			break;
		case "N1_0030_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0030_TH_VTN_1());
			break;
		case "N1_0040_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0040_TH_VTN_1());
			break;
		case "N1_0050_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0050_TH_VTN_1());
			break;
		case "N1_0060_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0060_TH_VTN_1());
			break;
		case "N1_0065_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0065_TH_VTN_1());
			break;
		case "N1_0070_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0070_TH_VTN_1());
			break;
		case "N1_0080_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0080_TH_VTN_1());
			break;
		case "R1_3010_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3010_TH_VTN_1());
			break;
		case "R1_3020_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3020_TH_VTN_1());
			break;
		case "R1_3025_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3025_TH_VTN_1());
			break;
		case "R1_3027_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3027_TH_VTN_1());
			break;
		case "R1_3030_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3030_TH_VTN_1());
			break;
		case "R1_3040_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3040_TH_VTN_1());
			break;
		case "R1_3045_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3045_TH_VTN_1());
			break;
		case "R1_3050_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3050_TH_VTN_1());
			break;
		case "R1_3060_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3060_TH_VTN_1());
			break;
		case "R1_3070_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3070_TH_VTN_1());
			break;
		case "R1_3080_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3080_TH_VTN_1());
			break;
		case "R1_3090_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3090_TH_VTN_1());
			break;
		case "R1_3100_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3100_TH_VTN_1());
			break;
		case "R1_3120_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3120_TH_VTN_1());
			break;
		case "R1_3130_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3130_TH_VTN_1());
			break;
		case "R1_3150_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3150_TH_VTN_1());
			break;
		case "R1_3160_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3160_TH_VTN_1());
			break;
		case "R1_3170_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3170_TH_VTN_1());
			break;
		case "R1_3180_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3180_TH_VTN_1());
			break;
		case "R1_3190_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3190_TH_VTN_1());
			break;
		case "CBP_CMP_B0_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_CMP_B0_TH_VTN_1());
			break;
		case "CBP_SMP_B0_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_SMP_B0_TH_VTN());
			break;
		case "CBP_TYP_B0_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_TYP_B0_TH_VTN());
			break;
		case "CPP_CMP_B0_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_CMP_B0_TH_VTN());
			break;
		case "CPP_SMP_B0_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_SMP_B0_TH_VTN());
			break;
		case "CPP_TYP_B0_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_SMP_B0_TH_VTN());
			break;
		case "DER_TYP_B0_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new DER_TYP_B0_TH_VTN());
			break;
		case "EVP_TYP_B0_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new EVP_TYP_B0_TH_VTN());
			break;
		case "FDR_CMP_B0_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new FDR_CMP_B0_TH_VTN_1());
			break;
		case "FDR_SMP_B0_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new FDR_SMP_B0_TH_VTN());
			break;
		case "FDR_TYP_B0_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new FDR_TYP_B0_TH_VTN_1());
			break;
		case "THR_CMP_B0_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_CMP_B0_TH_VTN());
			break;
		case "THR_SMP_B0_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_SMP_B0_TH_VTN());
			break;
		case "THR_TYP_B0_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_TYP_B0_TH_VTN_1());
			break;
		case "A_E0_0020_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0020_TH_VTN_1());
			break;
		case "A_E0_0040_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0040_TH_VTN());
			break;
		case "A_E0_0060_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0060_TH_VTN_1());
			break;
		case "A_E0_0070_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0070_TH_VTN_1());
			break;
		case "A_E0_0082_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0082_TH_VTN_1());
			break;
		case "A_E0_0086_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0086_TH_VTN_1());
			break;
		case "A_E0_0090_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0090_TH_VTN_1());
			break;
		case "A_E0_0092_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0092_TH_VTN_1());
			break;
		case "A_E0_0094_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0094_TH_VTN_1());
			break;
		case "A_E0_0096_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0096_TH_VTN());
			break;
		case "A_E0_0098_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0098_TH_VTN_1());
			break;
		case "A_E0_0100_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0100_TH_VTN_1());
			break;
		case "A_E0_0102_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0102_TH_VTN_1());
			break;
		case "A_E0_0104_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0104_TH_VTN_1());
			break;
		case "A_E0_0110_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0104_TH_VTN_1());
			break;
		case "A_E0_0130_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0130_TH_VTN_1());
			break;
		case "A_E0_0180_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0180_TH_VTN_1());
			break;
		case "A_E0_0185_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0185_TH_VTN_1());
			break;
		case "A_E0_0190_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0190_TH_VTN_1());
			break;
		case "A_E0_0200_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0200_TH_VTN_1());
			break;
		case "A_E0_0210_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0210_TH_VTN_1());
			break;
		case "A_E0_0220_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0220_TH_VTN_1());
			break;
		case "A_E0_0230_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0230_TH_VTN());
			break;
		case "A_E0_0240_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0240_TH_VTN());
			break;
		case "A_E0_0250_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0250_TH_VTN_1());
			break;
		case "A_E0_0260_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0260_TH_VTN_1());
			break;
		case "A_E0_0262_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0262_TH_VTN_1());
			break;
		case "A_E0_0267_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0262_TH_VTN_1());
			break;
		case "A_E0_0268_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0268_TH_VTN_1());
			break;
		case "A_E0_0270_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0270_TH_VTN_1());
			break;
		case "A_E0_0280_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0280_TH_VTN_1());
			break;
		case "A_E0_0285_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0285_TH_VTN_1());
			break;
		case "A_E0_0300_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0300_TH_VTN_1());
			break;
		case "A_E0_0310_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0310_TH_VTN_1());
			break;
		case "A_E0_0340_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0340_TH_VTN_1());
			break;
		case "A_E0_0345_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0345_TH_VTN_1());
			break;
		case "A_E0_0360_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0360_TH_VTN_1());
			break;
		case "A_E0_0370_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0370_TH_VTN_1());
			break;
		case "A_E0_0390_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0390_TH_VTN_1());
			break;
		case "A_E0_0392_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E0_0392_TH_VTN_1());
			break;
		case "E0_6010_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6010_TH_VTN());
			break;
		case "E0_6020_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6020_TH_VTN());
			break;
		case "E0_6025_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6025_TH_VTN());
			break;
		case "E0_6027_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6027_TH_VTN());
			break;
		case "E0_6030_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6030_TH_VTN());
			break;
		case "E0_6040_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6040_TH_VTN());
			break;
		case "E0_6050_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6050_TH_VTN_1());
			break;
		case "E0_6055_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6055_TH_VTN());
			break;
		case "E0_6060_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6060_TH_VTN());
			break;
		case "E0_6065_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6065_TH_VTN());
			break;
		case "E0_6070_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6070_TH_VTN());
			break;
		case "E0_6080_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6080_TH_VTN());
			break;
		case "E0_6090_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6090_TH_VTN());
			break;
		case "G0_9005_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G0_9005_TH_VTN());
			break;
		case "G0_9010_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G0_9010_TH_VTN());
			break;
		case "G0_9030_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G0_9030_TH_VTN());
			break;
		case "P0_7010_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7010_TH_VTN_1());
			break;
		case "P0_7015_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7015_TH_VTN_1());
			break;
		case "P0_7020_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7020_TH_VTN_1());
			break;
		case "P0_7030_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7030_TH_VTN_1());
			break;
		case "P0_7040_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7040_TH_VTN_1());
			break;
		case "P0_7050_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7050_TH_VTN_1());
			break;
		case "N0_5010_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5010_TH_VTN_1());
			break;
		case "N0_5015_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5015_TH_VTN_1());
			break;
		case "N0_5020_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5020_TH_VTN_1());
			break;
		case "N0_5025_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5025_TH_VTN_1());
			break;
		case "N0_5030_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5030_TH_VTN_1());
			break;
		case "N0_5040_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5040_TH_VTN());
			break;
		case "N0_5050_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5050_TH_VTN());
			break;
		case "N0_5060_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5060_TH_VTN_1());
			break;
		case "N0_5065_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5065_TH_VTN_1());
			break;
		case "N0_5070_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5070_TH_VTN_1());
			break;
		case "N0_5080_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5080_TH_VTN_1());
			break;
		case "R0_8010_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8010_TH_VTN());
			break;
		case "R0_8020_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8010_TH_VTN());
			break;
		case "R0_8025_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8025_TH_VTN());
			break;
		case "R0_8027_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8027_TH_VTN());
			break;
		case "R0_8030_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8030_TH_VTN());
			break;
		case "R0_8040_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8040_TH_VTN());
			break;
		case "R0_8045_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8045_TH_VTN());
			break;
		case "R0_8050_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8050_TH_VTN());
			break;
		case "R0_8060_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8060_TH_VTN_1());
			break;
		case "R0_8070_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8070_TH_VTN_1());
			break;
		case "R0_8080_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8080_TH_VTN_1());
			break;
		case "R0_8090_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8090_TH_VTN_1());
			break;
		case "R0_8100_TH_VTN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8100_TH_VTN_1());
			break;
		case "R0_8120_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8120_TH_VTN());
			break;
		case "R0_8130_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8130_TH_VTN());
			break;
		case "R0_8150_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8150_TH_VTN());
			break;
		case "R0_8160_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8160_TH_VTN());
			break;
		case "R0_8170_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8170_TH_VTN());
			break;
		case "R0_8180_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8180_TH_VTN());
			break;
		case "R0_8190_TH_VTN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8190_TH_VTN());
			break;

			//*****************************************************************************************************************
			//VTN
		case "CBP_CMP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_CMP_B1_TH_VEN());
			break;
		case "CBP_SMP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_SMP_B1_TH_VEN());
			break;
		case "CBP_TYP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_TYP_B1_TH_VEN());
			break;
		case "CPP_CMP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_CMP_B1_TH_VEN());
			break;
		case "CPP_SMP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_SMP_B1_TH_VEN());
			break;
		case "CPP_TYP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_TYP_B1_TH_VEN());
			break;
		case "DER_TYP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new DER_TYP_B1_TH_VEN());
			break;
		case "EVP_TYP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new EVP_TYP_B1_TH_VEN());
			break;
		case "EVR_SMP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new EVR_SMP_B1_TH_VEN());
			break;
		case "EVR_TYP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new EVR_TYP_B1_TH_VEN());
			break;
		case "THR_CMP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_CMP_B1_TH_VEN());
			break;
		case "THR_SMP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_SMP_B1_TH_VEN());
			break;
		case "THR_TYP_B1_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_TYP_B1_TH_VEN());
			break;
		case "A_E3_0420_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0420_TH_VEN());
			break;
		case "A_E3_0430_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0430_TH_VEN());
			break;
		case "A_E3_0432_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0432_TH_VEN());
			break;
		case "A_E3_0435_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0435_TH_VEN());
			break;
		case "A_E3_0440_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0440_TH_VEN());
			break;
		case "A_E3_0450_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0450_TH_VEN());
			break;
		case "A_E3_0452_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0452_TH_VEN());
			break;
		case "A_E3_0454_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0454_TH_VEN());
			break;
		case "A_E3_0468_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0468_TH_VEN());
			break;
		case "A_E3_0470_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0470_TH_VEN());
			break;
		case "A_E3_0474_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0474_TH_VEN());
			break;
		case "A_E3_0480_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0480_TH_VEN());
			break;
		case "A_E3_0484_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0484_TH_VEN());
			break;
		case "A_E3_0490_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0490_TH_VEN());
			break;
		case "A_E3_0492_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0492_TH_VEN());
			break;
		case "A_E3_0494_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0494_TH_VEN());
			break;
		case "A_E3_0496_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0496_TH_VEN());
			break;
		case "A_E3_0498_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0498_TH_VEN());
			break;
		case "A_E3_0500_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0500_TH_VEN());
			break;
		case "A_E3_0510_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0510_TH_VEN());
			break;
		case "A_E3_0525_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0525_TH_VEN());
			break;
		case "A_E3_0527_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0527_TH_VEN());
			break;
		case "A_E3_0655_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0655_TH_VEN());
			break;
		case "A_E3_0657_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0657_TH_VEN());
			break;
		case "A_E3_0680_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0680_TH_VEN());
			break;
		case "A_E3_0685_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0685_TH_VEN());
			break;
		case "A_E3_0710_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0710_TH_VEN());
			break;
		case "A_E3_0720_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0720_TH_VEN());
			break;
		case "A_E3_0730_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0730_TH_VEN());
			break;
		case "A_E3_0750_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0750_TH_VEN());
			break;
		case "A_E3_0780_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E3_0780_TH_VEN());
			break;
		case "E1_1010_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1010_TH_VEN());
			break;
		case "E1_1020_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1020_TH_VEN());
			break;
		case "E1_1025_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1025_TH_VEN());
			break;
		case "E1_1027_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1027_TH_VEN());
			break;
		case "E1_1030_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1030_TH_VEN());
			break;
		case "E1_1040_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1040_TH_VEN());
			break;
		case "E1_1050_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1050_TH_VEN());
			break;
		case "E1_1055_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1055_TH_VEN());
			break;
		case "E1_1060_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1060_TH_VEN());
			break;
		case "E1_1065_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1065_TH_VEN());
			break;
		case "E1_1070_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1070_TH_VEN());
			break;
		case "E1_1080_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1080_TH_VEN());
			break;
		case "E1_1090_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E1_1090_TH_VEN());
			break;
		case "G1_4005_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4005_TH_VEN());
			break;
		case "G1_4007_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4007_TH_VEN());
			break;
		case "G1_4010_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4010_TH_VEN());
			break;
		case "G1_4011_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4011_TH_VEN());
			break;
		case "G1_4012_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4012_TH_VEN());
			break;
		case "G1_4015_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4015_TH_VEN());
			break;
		case "G1_4020_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4020_TH_VEN_1());
			break;
		case "G1_4030_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G1_4030_TH_VEN());
			break;
		case "P1_2010_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2010_TH_VEN());
			break;
		case "P1_2015_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2015_TH_VEN());
			break;
		case "P1_2020_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2020_TH_VEN());
			break;
		case "P1_2030_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2030_TH_VEN());
			break;
		case "P1_2040_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2040_TH_VEN());
			break;
		case "P1_2050_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P1_2050_TH_VEN());
			break;
		case "N1_0010_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0010_TH_VEN());
			break;
		case "N1_0015_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0015_TH_VEN());
			break;
		case "N1_0020_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0020_TH_VEN());
			break;
		case "N1_0025_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0025_TH_VEN());
			break;
		case "N1_0030_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0030_TH_VEN());
			break;
		case "N1_0040_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0040_TH_VEN());
			break;
		case "N1_0050_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0050_TH_VEN());
			break;
		case "N1_0060_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0060_TH_VEN());
			break;
		case "N1_0065_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0065_TH_VEN());
			break;
		case "N1_0070_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0070_TH_VEN());
			break;
		case "N1_0080_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N1_0080_TH_VEN());
			break;
		case "R1_3010_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3010_TH_VEN());
			break;
		case "R1_3020_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3020_TH_VEN());
			break;
		case "R1_3025_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3025_TH_VEN());
			break;
		case "R1_3027_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3027_TH_VEN());
			break;
		case "R1_3030_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3030_TH_VEN());
			break;
		case "R1_3040_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3040_TH_VEN());
			break;
		case "R1_3045_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3045_TH_VEN());
			break;
		case "R1_3050_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3050_TH_VEN());
			break;
		case "R1_3060_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3060_TH_VEN());
			break;
		case "R1_3070_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3070_TH_VEN());
			break;
		case "R1_3080_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3080_TH_VEN());
			break;
		case "R1_3090_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3090_TH_VEN());
			break;
		case "R1_3100_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3100_TH_VEN());
			break;
		case "R1_3120_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3120_TH_VEN());
			break;
		case "R1_3130_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3130_TH_VEN());
			break;
		case "R1_3150_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3150_TH_VEN());
			break;
		case "R1_3160_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3160_TH_VEN());
			break;
		case "R1_3170_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3170_TH_VEN());
			break;
		case "R1_3180_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3180_TH_VEN());
			break;
		case "R1_3190_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R1_3190_TH_VEN());
			break;
		case "CBP_CMP_B0_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_CMP_B0_TH_VEN());
			break;
		case "CBP_SMP_B0_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_SMP_B0_TH_VEN_1());
			break;
		case "CBP_TYP_B0_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CBP_TYP_B0_TH_VEN_1());
			break;
		case "CPP_CMP_B0_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_CMP_B0_TH_VEN_1());
			break;
		case "CPP_SMP_B0_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_SMP_B0_TH_VEN_1());
			break;
		case "CPP_TYP_B0_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new CPP_TYP_B0_TH_VEN_1());
			break;
		case "DER_TYP_B0_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new DER_TYP_B0_TH_VEN_1());
			break;
		case "EVP_TYP_B0_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new EVP_TYP_B0_TH_VEN_1());
			break;
		case "FDR_CMP_B0_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new FDR_CMP_B0_TH_VEN());
			break;
		case "FDR_SMP_B0_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new FDR_SMP_B0_TH_VEN_1());
			break;
		case "FDR_TYP_B0_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new FDR_TYP_B0_TH_VEN());
			break;
		case "THR_CMP_B0_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_CMP_B0_TH_VEN_1());
			break;
		case "THR_SMP_B0_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_SMP_B0_TH_VEN_1());
			break;
		case "THR_TYP_B0_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new THR_TYP_B0_TH_VEN());
			break;
		case "A_E2_0430_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0430_TH_VEN_1());
			break;
		case "A_E2_0432_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0432_TH_VEN_1());
			break;
		case "A_E2_0435_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0435_TH_VEN_1());
			break;
		case "A_E2_0440_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0440_TH_VEN_1());
			break;
		case "A_E2_0450_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0450_TH_VEN_1());
			break;
		case "A_E2_0452_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0452_TH_VEN_1());
			break;
		case "A_E2_0454_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0454_TH_VEN_1());
			break;
		case "A_E2_0468_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0468_TH_VEN_1());
			break;
		case "A_E2_0470_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0470_TH_VEN_1());
			break;
		case "A_E2_0474_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0474_TH_VEN_1());
			break;
		case "A_E2_0480_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0480_TH_VEN_1());
			break;
		case "A_E2_0484_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0484_TH_VEN_1());
			break;
		case "A_E2_0490_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0490_TH_VEN_1());
			break;
		case "A_E2_0492_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0492_TH_VEN_1());
			break;
		case "A_E2_0494_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0494_TH_VEN_1());
			break;
		case "A_E2_0496_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0496_TH_VEN_1());
			break;
		case "A_E2_0498_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0498_TH_VEN_1());
			break;
		case "A_E2_0500_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0500_TH_VEN_1());
			break;
		case "A_E2_0510_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0510_TH_VEN_1());
			break;
		case "A_E2_0525_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0525_TH_VEN_1());
			break;
		case "A_E2_0527_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0527_TH_VEN_1());
			break;
		case "A_E2_0657_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0657_TH_VEN_1());
			break;
		case "A_E2_0680_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0680_TH_VEN_1());
			break;
		case "A_E2_0685_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0685_TH_VEN_1());
			break;
		case "A_E2_0710_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0710_TH_VEN_1());
			break;
		case "A_E2_0720_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0720_TH_VEN_1());
			break;
		case "A_E2_0730_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0730_TH_VEN_1());
			break;
		case "A_E2_0750_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0750_TH_VEN_1());
			break;
		case "A_E2_0780_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new A_E2_0780_TH_VEN_1());
			break;
		case "E0_6010_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6010_TH_VEN_1());
			break;
		case "E0_6020_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6020_TH_VEN_1());
			break;
		case "E0_6025_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6025_TH_VEN_1());
			break;
		case "E0_6027_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6027_TH_VEN_1());
			break;
		case "E0_6030_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6030_TH_VEN_1());
			break;
		case "E0_6040_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6040_TH_VEN_1());
			break;
		case "E0_6050_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6050_TH_VEN());
			break;
		case "E0_6055_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6055_TH_VEN_1());
			break;
		case "E0_6060_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6060_TH_VEN_1());
			break;
		case "E0_6065_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6065_TH_VEN_1());
			break;
		case "E0_6070_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6070_TH_VEN_1());
			break;
		case "E0_6080_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6080_TH_VEN_1());
			break;
		case "E0_6090_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new E0_6090_TH_VEN_1());
			break;
		case "G0_9005_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G0_9005_TH_VEN_1());
			break;
		case "G0_9010_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G0_9010_TH_VEN_1());
			break;
		case "G0_9020_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G0_9020_TH_VEN_1());
			break;
		case "G0_9030_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new G0_9030_TH_VEN_1());
			break;
		case "P0_7010_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7010_TH_VEN());
			break;
		case "P0_7015_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7015_TH_VEN());
			break;
		case "P0_7020_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7020_TH_VEN());
			break;
		case "P0_7030_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7030_TH_VEN());
			break;
		case "P0_7040_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7040_TH_VEN());
			break;
		case "P0_7050_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new P0_7050_TH_VEN());
			break;
		case "N0_5010_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5010_TH_VEN());
			break;
		case "N0_5015_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5015_TH_VEN());
			break;
		case "N0_5020_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5020_TH_VEN());
			break;
		case "N0_5025_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5025_TH_VEN());
			break;
		case "N0_5030_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5030_TH_VEN());
			break;
		case "N0_5040_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5040_TH_VEN_1());
			break;
		case "N0_5050_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5050_TH_VEN_1());
			break;
		case "N0_5060_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5060_TH_VEN());
			break;
		case "N0_5065_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5065_TH_VEN());
			break;
		case "N0_5070_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5070_TH_VEN());
			break;
		case "N0_5080_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new N0_5080_TH_VEN());
			break;
		case "R0_8010_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8010_TH_VEN_1());
			break;
		case "R0_8020_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8020_TH_VEN_1());
			break;
		case "R0_8025_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8025_TH_VEN_1());
			break;
		case "R0_8027_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8027_TH_VEN_1());
			break;
		case "R0_8030_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8030_TH_VEN_1());
			break;
		case "R0_8040_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8040_TH_VEN_1());
			break;
		case "R0_8045_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8045_TH_VEN_1());
			break;
		case "R0_8050_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8050_TH_VEN_1());
			break;
		case "R0_8060_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8060_TH_VEN());
			break;
		case "R0_8070_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8070_TH_VEN());
			break;
		case "R0_8080_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8080_TH_VEN());
			break;
		case "R0_8090_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8090_TH_VEN());
			break;
		case "R0_8100_TH_VEN":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8100_TH_VEN());
			break;
		case "R0_8120_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8120_TH_VEN_1());
			break;
		case "R0_8130_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8130_TH_VEN_1());
			break;
		case "R0_8150_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8150_TH_VEN_1());
			break;
		case "R0_8160_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8160_TH_VEN_1());
			break;
		case "R0_8170_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8170_TH_VEN_1());
			break;
		case "R0_8180_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8180_TH_VEN_1());
			break;
		case "R0_8190_TH_VEN_1":
			com.qualitylogic.openadr.core.base.NewTestCase.execute(new R0_8190_TH_VEN_1());
			break;
		case "TH_ClearRegistrationDatabases":
			testcases.util.TH_ClearRegistrationDatabases.main(null);
			break;
		case "PendingReportCleanUp_Pull_TH_VTN":
			testcases.util.PendingReportCleanUp_Pull_TH_VTN.main(null);
			break;

		default:
			JOptionPane.showMessageDialog(null, "Unknown test case: " + TestSelector.getcurrentTest(),
					"Unknown Test Case.", JOptionPane.ERROR_MESSAGE);
			bUnk=true;
			return;

		}
		Trace trace = TestSession.getTraceObj();
		if(trace != null){
			txtLog.setText(trace.getLogFileContentTrace().toString());
			runListLog.add(trace.getLogFileName());
		}
	}
	private static BufferedImage getSq(Integer iw, Integer ih, Color clr){
		BufferedImage img = new BufferedImage(
				iw, ih, BufferedImage.TYPE_INT_ARGB);
		Graphics2D    graphics = img.createGraphics();
		graphics.setPaint ( clr );
		graphics.fillRect ( 0, 0, img.getWidth(), img.getHeight() );
		return img;
	}
	public static class MyTreeCellRenderer extends DefaultTreeCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public Component getTreeCellRendererComponent(JTree tree,
				Object value, boolean selected, boolean expanded,
				boolean isLeaf, int row, boolean focused) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			Component c = super.getTreeCellRendererComponent(tree, value,
					selected, expanded, isLeaf, row, focused);

			//setOpaque(true);
			//setForeground(Color.black);
			if(node.isRoot())
				return c;

			// Assuming you have a tree of Strings
			String snode = (String) node.getUserObject();
			snode=snode.substring(1);
			if(snode.contains(",")){
				snode = snode.substring(0,snode.indexOf(",")-1).trim();
			}
			ResourceFileReader resourceFileReader = new ResourceFileReader();

			setToolTipText(resourceFileReader.TestCase_Intent_Description_Named(snode));
			if(runListRes.size() == 0)
				return c;

			int ic = node.getParent().getIndex(node);
			if(node.isLeaf() && runListNum.indexOf(ic) > -1){
				//int i = runList.indexOf(snode);
				int i = runListNum.indexOf(ic);
				if(i<runListRes.size()){
					String tst = runListRes.get(i);
					if(tst.equalsIgnoreCase(" "))
						setIcon(barBlack);
					else if(tst.equalsIgnoreCase("started"))
						setIcon(barCyan);
					else if(tst.equalsIgnoreCase("passed"))
						setIcon(barGreen);
					else if(tst.equalsIgnoreCase("failed"))
						setIcon(barRed);
					else if(tst.equalsIgnoreCase("unknown") || tst.equalsIgnoreCase("canceled"))
						setIcon(barYellow);
				}
			}

			return c;
		}
	}
	private static Boolean mDir(String cDir){
		Path path = Paths.get(cDir);
		if (!Files.exists(path)) {
			try {
				Files.createDirectories(path);
			} catch (IOException e) {
				//fail to create directory
				JOptionPane.showMessageDialog(null, "Error creating " + cDir + " directory, exiting: " +  "\n" + e.getMessage()  
						+ "\n", "File I/O error: creating " + cDir + " directory.", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	private static void	findLog(String tstDate, String stest){
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyy_HHmmss_SSS");
			Long ldate = dateFormat.parse(tstDate).getTime();
			File file = new File("log");
			Collection<File> files = FileUtils.listFiles(file,TrueFileFilter.TRUE, TrueFileFilter.TRUE);
			File[] listOfFiles = files.toArray(new File[files.size()]);
			Arrays.sort(listOfFiles, Collections.reverseOrder());//this would be based on the full file name
			for (File f:listOfFiles){
				String lfile = f.getName();
				lfile = lfile.replace("TraceLog_", "");
				lfile = lfile.replace(".txt", "");
				Long fd = dateFormat.parse(lfile).getTime();
				if (fd>ldate){
					try {
						String testline = FileUtils.readFileToString(f);
						if(testline.startsWith("Test Case Name : " + stest)){
							txtLog.setText(testline);
							runListLog.add(f.getName());
							System.out.println(f.getName());
							break;
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

}
