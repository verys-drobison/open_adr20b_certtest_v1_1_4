package testcases.util;

import com.qualitylogic.openadr.core.bean.ServiceType;
import com.qualitylogic.openadr.core.common.TestSession;
import com.qualitylogic.openadr.core.ven.VenToVtnClient;

public class VTNPollQueue_Clear {

	public static void main(String []args){
		TestSession.setServiceType(ServiceType.VTN);
		VenToVtnClient.emptyQueue();
	}
}
